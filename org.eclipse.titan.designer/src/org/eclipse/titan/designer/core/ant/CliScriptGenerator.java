/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.core.ant;

import java.io.File;
import java.io.IOException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.common.utils.CommentUtils;
import org.eclipse.titan.common.utils.IOUtils;
import org.eclipse.titan.common.utils.StringUtils;
import org.eclipse.titan.designer.GeneralConstants;
import org.eclipse.titan.designer.properties.data.MakefileCreationData;
import org.eclipse.titan.designer.properties.data.ProjectBuildPropertyData;

/**
 * Utility class for generating Linux shell and Windows command scripts.
 * @author Adam Knapp
 */
public final class CliScriptGenerator {

	public static final String SCRIPT_NAME = "titan_java_start";
	public static final String CMD_SCRIPT_NAME = SCRIPT_NAME + ".cmd";
	public static final String SHELL_SCRIPT_NAME = SCRIPT_NAME + ".sh";

	private static final String SHELL_SCRIPT_HEADER = "#!/usr/bin/env sh\n";
	private static final String SHELL_SCRIPT_TEXT =
			  "function help() {\n"
			+ "\techo \"usage: " + SHELL_SCRIPT_NAME + " executable.jar config.cfg [-m|--mode execution_mode] "
			+ "[-p|--project project_name]  [-vmargs \"arguments_for_jvm\"]\"\n"
			+ "\techo \"  -m|--mode [hc|host_controller|mc|main_controller|p|parallel|s|single]\"\n"
			+ "}\n\n"
			+ "function proc_mode() {\n"
			+ "\tcase $mode in\n"
			+ "\t\thc) start_hc;;\n"
			+ "\t\thost_controller) start_hc;;\n"
			+ "\t\tmc) start_mc;;\n"
			+ "\t\tmain_controller) start_mc;;\n"
			+ "\t\tp) start_parallel;;\n"
			+ "\t\tparallel) start_parallel;;\n"
			+ "\t\ts) start_single;;\n"
			+ "\t\tsingle) start_single;;\n"
			+ "\t\t?) echo \"Unknown execution mode: $mode\"; exit 1;;\n"
			+ "\tesac\n"
			+ "}\n\n"
			+ "function start_hc() {\n"
			+ "\tlocaladdress=$(awk '/LocalAddress/{print $3}' ${cfg_file} | sed 's/\\r$//')\n"
			+ "\tif [ -z \"${localaddress}\" ]; then\n"
			+ "\t\techo \"LocalAddress parameter is missing from ${cfg_file}. Using default: 127.0.0.1\";\n"
			+ "\t\tlocaladdress=\"127.0.0.1\"\n"
			+ "\tfi\n"
			+ "\ttcpport=$(awk '/TCPPort/{print $3}' ${cfg_file} | sed 's/\\r$//')\n"
			+ "\tif [ -z \"${tcpport}\" ]; then\n"
			+ "\t\techo \"TCPPort parameter is missing from ${cfg_file}\"; echo \"Unable to continue.\"\n"
			+ "\t\thelp\n"
			+ "\t\texit 1\n"
			+ "\tfi\n"
			+ "\techo \"Starting the Host Controller...\"\n"
			+ "\techo \"Connecting to ${localaddress}:${tcpport}...\"\n"
			+ "\tjava ${vmargs} -cp ${exe_file} org.eclipse.titan.${project}.generated.Parallel_main ${localaddress} ${tcpport}\n"
			+ "}\n\n"
			+ "function start_mc() {\n"
			+ "\techo \"Starting the Main Controller...\"\n"
			+ "\tjava ${vmargs} -cp ${exe_file} org.eclipse.titan.runtime.core.mctr.CliMain ${cfg_file}\n"
			+ "}\n\n"
			+ "function start_parallel() {\n"
			+ "\techo \"Starting automatic execution in Parallel mode...\"\n"
			+ "\tif [ ! -e \"$(which ttcn3_start)\" ]; then\n"
			+ "\t\techo \"ttcn3_start script is not found. Automatic execution in Parallel mode is not available.\"\n"
			+ "\t\texit 1\n"
			+ "\tfi\n"
			+ "\tttcn3_start ${exe_file} ${cfg_file}\n"
			+ "}\n\n"
			+ "function start_single() {\n"
			+ "\techo \"Starting execution in Single mode...\"\n"
			+ "\tjava ${vmargs} -cp ${exe_file} org.eclipse.titan.${project}.generated.Single_main ${cfg_file}\n"
			+ "}\n\n"
			+ "if [ ! -e \"$(which java)\" ]; then\n"
			+ "\techo \"java is not found.\"\n"
			+ "\texit 1\n"
			+ "fi\n\n"
			+ "exe_file=\"\"\n"
			+ "project=\"\"\n"
			+ "mode=\"parallel\"\n"
			+ "vmargs=\"-Dfile.encoding=UTF-8\"\n"
			+ "while [ -n \"$1\" ]; do\n"
			+ "\tcase \"$1\" in\n"
			+ "\t\t-h) help; exit 0;;\n"
			+ "\t\t--help) help; exit 0;;\n"
			+ "\t\t-m) \n"
			+ "\t\t\tif [ -n \"$2\" ]; then\n"
			+ "\t\t\t\tmode=$2; shift\n"
			+ "\t\t\telse\n"
			+ "\t\t\t\techo \"Execution mode option is empty. Using default: ${mode}\"\n"
			+ "\t\t\tfi;;\n"
			+ "\t\t--mode) \n"
			+ "\t\t\tif [ -n \"$2\" ]; then\n"
			+ "\t\t\t\tmode=$2; shift\n"
			+ "\t\t\telse\n"
			+ "\t\t\t\techo \"Execution mode option is empty. Using default: ${mode}\"\n"
			+ "\t\t\tfi;;\n"
			+ "\t\t-p) \n"
			+ "\t\t\tif [ -n \"$2\" ]; then\n"
			+ "\t\t\t\tproject=$2; shift\n"
			+ "\t\t\telse\n"
			+ "\t\t\t\techo \"Project name option is empty. Using default: ${project}\"\n"
			+ "\t\t\tfi;;\n"
			+ "\t\t--project)\n"
			+ "\t\t\tif [ -n \"$2\" ]; then\n"
			+ "\t\t\t\tproject=$2; shift\n"
			+ "\t\t\telse\n"
			+ "\t\t\t\techo \"Project name option is empty. Using default: ${project}\"\n"
			+ "\t\t\tfi;;\n"
			+ "\t\t-vmargs)\n"
			+ "\t\t\tif [ -n \"$2\" ]; then\n"
			+ "\t\t\t\tvmargs=$2; shift\n"
			+ "\t\t\telse\n"
			+ "\t\t\t\techo \"VM arguments option is empty. Using default: \\\"${vmargs}\\\"\"\n"
			+ "\t\t\tfi;;\n"
			+ "\t\t*) \n"
			+ "\t\t\tif [[ $1 =~ .*.(jar|zip) ]]; then\n"
			+ "\t\t\t\texe_file=$1 \n"
			+ "\t\t\telif [[ $1 =~ .*.(cfg) ]]; then \n"
			+ "\t\t\t\tcfg_file=$1\n"
			+ "\t\t\tfi\n"
			+ "\tesac\n"
			+ "\tshift\n"
			+ "done\n\n"
			+ "if [ ! -e \"${exe_file}\" ]; then\n"
			+ "\techo \"The provided executable JAR file is not exist: ${exe_file}\"\n"
			+ "\texit 1\n"
			+ "fi\n"
			+ "if [ ! -e \"${cfg_file}\" ]; then\n"
			+ "\techo \"The provided configuration file is not exist: ${cfg_file}\"\n"
			+ "\texit 1\n"
			+ "fi\n\n"
			+ "proc_mode\n";

	/**
	 * Generates the JAR start script content as cmd script
	 * @param project Project to which the script is related to
	 * @return The content of the script
	 */
	public static String createCmdScriptContent(final IProject project) {
		if (project == null) {
			return "";
		}
		final StringBuilder builder = new StringBuilder();
		builder.append(CommentUtils.getHeaderCommentsWithCopyright(":: ", GeneralConstants.VERSION_STRING));
		return builder.toString();
	}

	/**
	 * Generates the JAR start script content as shell script
	 * @param project Project to which the script is related to
	 * @return The content of the script
	 * @throws CoreException
	 */
	public static String createShellScriptContent(final IProject project) throws CoreException {
		if (project == null) {
			return "";
		}
		final StringBuilder builder = new StringBuilder();
		builder.append(SHELL_SCRIPT_HEADER)
		.append(CommentUtils.getHeaderCommentsWithCopyrightAndLicenseAndDoNotEdit("# ", GeneralConstants.VERSION_STRING))
		.append(SHELL_SCRIPT_TEXT.replace("exe_file=\"\"\n", "exe_file=\"" + getJarFileName(project) + "\"\n")
				.replace("project=\"\"\n", "project=\"" + project.getName() + "\"\n"));
		return builder.toString();
	}

	/**
	 * Checks whether the cmd script is already exist
	 * @param project Project to which the script is related to
	 * @return Returns whether the cmd script is already exist
	 * @throws CoreException
	 */
	public static boolean existCmdScript(final IProject project) throws CoreException {
		if (project == null) {
			return false;
		}
		final String jarFolder = getJarFolder(project);
		if (StringUtils.isNullOrEmpty(jarFolder)) {
			return false;
		}
		final File scriptFile = new File(jarFolder + File.separator + CMD_SCRIPT_NAME);
		return scriptFile.exists();
	}

	/**
	 * Checks whether the shell script is already exist
	 * @param project Project to which the script is related to
	 * @return Returns whether the shell script is already exist
	 * @throws CoreException
	 */
	public static boolean existShellScript(final IProject project) throws CoreException {
		if (project == null) {
			return false;
		}
		final String jarFolder = getJarFolder(project);
		if (StringUtils.isNullOrEmpty(jarFolder)) {
			return false;
		}
		final File scriptFile = new File(jarFolder + File.separator + SHELL_SCRIPT_NAME);
		return scriptFile.exists();
	}

	/**
	 * Creates and then stores the requested script(s) in the specified folder.
	 * This folder is specified by the path of the JAR file.
	 * @param project Project to which the script is related to
	 */
	public static void generateAndStoreScripts(final IProject project) {
		if (project == null) {
			return;
		}
		try {
			String temp = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.GENERATE_START_SH_SCRIPT_PROPERTY));
			final boolean shellScriptNeeded = ProjectBuildPropertyData.TRUE_STRING.equals(temp) ? true : false;
			temp = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.GENERATE_START_BAT_SCRIPT_PROPERTY));
			final boolean cmdScriptNeeded = ProjectBuildPropertyData.TRUE_STRING.equals(temp) ? true : false;

			String shellScriptContent = null;
			String cmdScriptContent = null;
			if (shellScriptNeeded) {
				if (!existShellScript(project)) {
					shellScriptContent = createShellScriptContent(project);
				}
			}
			if (cmdScriptNeeded) {
				if (!existCmdScript(project)) {
					cmdScriptContent = createCmdScriptContent(project);
				}
			}
			storeScripts(project, shellScriptContent, cmdScriptContent);
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace(e);
		} catch (IOException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
	}

	/**
	 * Returns the {@code File} representation of the JAR file
	 * @param project Project to which the JAR export is requested
	 * @return {@code File} containing the <i>absolute</i> path of the JAR file
	 * @throws CoreException
	 */
	private static File getJarFile(final IProject project) throws CoreException {
		final String jarPathString = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
				MakefileCreationData.TARGET_EXECUTABLE_PROPERTY));
		if (StringUtils.isNullOrEmpty(jarPathString)) {
			ErrorReporter.INTERNAL_ERROR("Jar file is null or empty");
			return null;
		}
		File jarFile = new File(jarPathString);
		if (!jarFile.isAbsolute()) {
			jarFile = new File(project.getLocation().toOSString() + File.separator + jarPathString);
		}

		return jarFile;
	}

	/**
	 * Returns the name of the JAR file as {@code String}
	 * @param project Project to which the JAR export is requested
	 * @return the name of the JAR file
	 * @throws CoreException
	 */
	private static String getJarFileName(final IProject project) throws CoreException {
		return getJarFile(project).getName();
	}

	/**
	 * Returns the path string of the containing folder of the JAR file
	 * @param project Project to which the JAR export is requested
	 * @return the path string of the containing folder of the JAR file
	 * @throws CoreException
	 */
	private static String getJarFolder(final IProject project) throws CoreException {
		return getJarFile(project).getParent();
	}

	/**
	 * Stores the generated content of the scripts in files in the folder of the JAR file
	 * @param project Project to which the scripts are related to
	 * @param shellScriptContent Content of the shell script
	 * @param cmdScriptContent Content of the cmd script
	 * @throws CoreException
	 * @throws IOException
	 */
	public static void storeScripts(final IProject project, final String shellScriptContent,
			final String cmdScriptContent) throws CoreException, IOException {
		if (project == null || (shellScriptContent == null && cmdScriptContent == null)) {
			return;
		}
		final String jarFolderString = getJarFolder(project);
		final File jarFolder = new File(jarFolderString);
		if (!jarFolder.exists()) {
			if (!jarFolder.mkdirs()) {
				ErrorReporter.parallelErrorDisplayInMessageDialog("Folder creation error",
						"Target folder for JAR export was not created: " + jarFolderString);
			}
		}
		if (shellScriptContent != null) {
			File shellScriptFile = new File(jarFolderString + File.separator + SHELL_SCRIPT_NAME);
			IOUtils.writeStringToFile(shellScriptFile, shellScriptContent);
			try {
				if (!shellScriptFile.setExecutable(true, false))
					if (!shellScriptFile.setExecutable(true, true))
						throw new SecurityException();
			} catch (SecurityException e) {
				ErrorReporter.logWarning("Setting execution permission for '" + SHELL_SCRIPT_NAME + "' failed.");
			}
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}
		if (cmdScriptContent != null) {
			File cmdScriptFile = new File(jarFolderString + File.separator + CMD_SCRIPT_NAME);
			IOUtils.writeStringToFile(cmdScriptFile, cmdScriptContent);
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		}
	}
}
