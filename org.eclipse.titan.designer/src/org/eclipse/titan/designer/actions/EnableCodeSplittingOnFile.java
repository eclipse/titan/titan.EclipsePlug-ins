package org.eclipse.titan.designer.actions;

import java.util.HashSet;
import java.util.Set;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.GlobalProjectStructureTracker;
import org.eclipse.titan.designer.properties.PropertyNotificationManager;
import org.eclipse.titan.designer.properties.data.FileBuildPropertyData;
import org.eclipse.titan.designer.properties.data.ProjectFileHandler;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressConstants;

/**
 * This action allows the user to toggle the code splitting enabled for file flag on
 * several files at the same time.
 * 
 * @author Adam Knapp
 */
public class EnableCodeSplittingOnFile extends AbstractHandler implements IObjectActionDelegate {
	private ISelection selection;

	/**
	 * This method traverses the list of selected files and
	 * toggles the code splitting enabled for file flag on each of them.
	 * <ul>
	 * <li>After setting the properties the decorator is refreshed to
	 * represent the actual state
	 * <li>The project settings are saved.
	 * </ul>
	 * <p>
	 * The execution of these items stops if any of them fails to execute
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 * 
	 * @param action  the action proxy that handles the presentation portion
	 *                of the action (not used here)
	 */
	@Override
	/** {@inheritDoc} */
	public void run(final IAction action) {
		doEnableCodeSplittingOnFile(selection);
	}

	@Override
	/** {@inheritDoc} */
	public void selectionChanged(final IAction action, final ISelection selection) {
		this.selection = selection;
	}

	@Override
	/** {@inheritDoc} */
	public void setActivePart(final IAction action, final IWorkbenchPart targetPart) {
		// Do nothing
	}

	@Override
	/** {@inheritDoc} */
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		selection = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getSelection();

		doEnableCodeSplittingOnFile(selection);

		return null;
	}

	/**
	 * Do the actual flag change. 
	 * @param selection the files selected to be splitted
	 */
	private void doEnableCodeSplittingOnFile(final ISelection selection) {
		if (!(selection instanceof IStructuredSelection)) {
			return;
		}

		final IStructuredSelection structSelection = (IStructuredSelection) selection;
		if (structSelection.isEmpty()) {
			return;
		}

		final WorkspaceJob op = new PropertyChangerWorkspaceJob(structSelection);
		op.setPriority(Job.SHORT);
		op.setSystem(false);
		op.setUser(true);
		op.setRule(ResourcesPlugin.getWorkspace().getRoot());
		op.setProperty(IProgressConstants.ICON_PROPERTY, ImageCache.getImageDescriptor("titan.gif"));
		op.schedule();
	}

	private class PropertyChangerWorkspaceJob extends WorkspaceJob {
		private final IStructuredSelection structSelection;

		public PropertyChangerWorkspaceJob(final IStructuredSelection structSelection) {
			super("Changing the code splitting enabled flag");
			this.structSelection = structSelection;
		}

		@Override
		public IStatus runInWorkspace(final IProgressMonitor monitor) {
			final Set<IProject> projectsToRefresh = new HashSet<IProject>();

			for (final Object selected : structSelection.toList()) {
				if (!(selected instanceof IFile)) {
					continue;
				}
				final IFile file = (IFile) selected;
				final boolean mode = FileBuildPropertyData.getPropertyValue(
						file, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY);
				FileBuildPropertyData.setPropertyValue(
						file, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY, !mode);

				projectsToRefresh.add(file.getProject());
			}


			final WorkspaceJob op = new WorkspaceJob(
					"Updating project descriptor files") {
				@Override
				public IStatus runInWorkspace(final IProgressMonitor monitor) {
					for (final IProject project : projectsToRefresh) {
						ProjectFileHandler projectFileHandler = new ProjectFileHandler(project);
						projectFileHandler.saveProjectSettings();
						GlobalProjectStructureTracker.projectChanged(project);
						PropertyNotificationManager.firePropertyChange(project);
					}

					return Status.OK_STATUS;
				}
			};
			op.setPriority(Job.SHORT);
			op.setSystem(true);
			op.setUser(false);
			op.setRule(ResourcesPlugin.getWorkspace().getRoot());
			op.setProperty(IProgressConstants.ICON_PROPERTY, ImageCache.getImageDescriptor("titan.gif"));
			op.schedule();
			return Status.OK_STATUS;
		}
	}
}
