/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.statements;

import java.util.List;

import org.eclipse.titan.designer.AST.ASTNode;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.ILocateableNode;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NULL_Location;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.TTCN3.IIncrementallyUpdateable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The SelectClassCase class is helper class for the SelectClassCase_Statement class.
 * Represents a select class case branch parsed from the source code.
 * 
 * @author Miklos Magyari
 *
 */
public final class SelectClassCase extends ASTNode implements ILocateableNode, IIncrementallyUpdateable {
	private static final String UNREACHABLE = "Control never reaches this code because of previous effective case(s)";
	private static final String CLASSEXPECTED = "Class type was expected";
	private static final String NEVERCHOSEN = "Control never reaches this code because the case will never be chosen";
	
	private final Type type;
	private final StatementBlock statementBlock;
	private boolean alwaysFalse;
	private Location location = NULL_Location.INSTANCE;
	
	public SelectClassCase(final Type type, final StatementBlock statementBlock) {
		this.type = type;
		this.statementBlock = statementBlock;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (statementBlock != null) {
			statementBlock.setMyScope(scope);
		}
	}
	
	public void setMyStatementBlock(final StatementBlock parStatementBlock, final int index) {
		if (statementBlock != null) {
			statementBlock.setMyStatementBlock(parStatementBlock, index);
		}
	}
	
	public void setMyDefinition(final Definition definition) {
		if (statementBlock != null) {
			statementBlock.setMyDefinition(definition);
		}
	}
	
	public void setMyAltguards(final AltGuards altGuards) {
		if (statementBlock != null) {
			statementBlock.setMyAltguards(altGuards);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (type != null) {
			type.findReferences(referenceFinder, foundIdentifiers);
		}
		if (statementBlock != null) {
			statementBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}
	
	public boolean check(final CompilationTimeStamp timestamp, final Class_Type governor, boolean unreachable) {
		if (unreachable) {
			getLocation().reportSemanticWarning(UNREACHABLE);
		}
		if (type != null) {
			type.check(timestamp);
			final IType typeLast = type.getTypeRefdLast(timestamp);
			if (! (typeLast instanceof Class_Type)) {
				type.getLocation().reportSemanticError(CLASSEXPECTED);
			} else if(! unreachable) {
				Class_Type classLast = typeLast.getClassType();
				if (governor != null && !classLast.isParentClass(timestamp, governor) &&
					!governor.isParentClass(timestamp, classLast)) {
					statementBlock.getLocation().reportSemanticWarning(NEVERCHOSEN);
					alwaysFalse = true;
				}
			}
		} else {
			unreachable = true;
		}
		statementBlock.check(timestamp);
		
		return unreachable;
	}
	
	/**
	 * Checks the properties of the statement, that can only be checked
	 * after the semantic check was completely run.
	 */
	public void postCheck() {
		if (statementBlock != null) {
			statementBlock.postCheck();
		}
	}
	
	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}

		if (statementBlock != null) {
			statementBlock.updateSyntax(reparser, false);
			reparser.updateLocation(statementBlock.getLocation());
		}
	}

	@Override
	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public Location getLocation() {
		return location; 
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (type != null && !type.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}
	
	public StatementBlock getStatementBlock() {
		return statementBlock;
	}
	
	public Type getType() {
		return type;
	}
}
