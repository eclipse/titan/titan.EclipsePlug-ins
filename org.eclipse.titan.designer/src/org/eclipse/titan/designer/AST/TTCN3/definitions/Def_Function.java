/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.SWT;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.AST.GovernedSimple.CodeSectionType;
import org.eclipse.titan.designer.AST.INamedNode;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.ISubReference;
import org.eclipse.titan.designer.AST.ISubReference.Subreference_type;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.TypeOwner_type;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.NamingConventionHelper;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceChain;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.TemplateRestriction;
import org.eclipse.titan.designer.AST.TTCN3.attributes.AttributeSpecification;
import org.eclipse.titan.designer.AST.TTCN3.attributes.ExtensionAttribute;
import org.eclipse.titan.designer.AST.TTCN3.attributes.ExtensionAttribute.ExtensionAttribute_type;
import org.eclipse.titan.designer.AST.TTCN3.attributes.PrototypeAttribute;
import org.eclipse.titan.designer.AST.TTCN3.attributes.Qualifiers;
import org.eclipse.titan.designer.AST.TTCN3.attributes.SingleWithAttribute;
import org.eclipse.titan.designer.AST.TTCN3.attributes.SingleWithAttribute.Attribute_Type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.SignatureExceptions;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.editors.actions.DeclarationCollector;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.editors.controls.HoverProposal;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3CodeSkeletons;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.extensionattributeparser.ExtensionAttributeAnalyzer;
import org.eclipse.titan.designer.parsers.ttcn3parser.IIdentifierReparser;
import org.eclipse.titan.designer.parsers.ttcn3parser.IdentifierReparser;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;

/**
 * The Def_Function class represents TTCN3 function definitions.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class Def_Function extends Def_FunctionBase {
	private static final String FULLNAMEPART1 = ".<runs_on_type>";
	private static final String FULLNAMEPART2 = ".<formal_parameter_list>";
	private static final String FULLNAMEPART3 = ".<type>";
	private static final String FULLNAMEPART4 = ".<statement_block>";
	private static final String FULLNAMEPART5 = ".<mtc_type>";
	private static final String FULLNAMEPART6 = ".<system_type>";
	private static final String FULLNAMEPART7 = ".<port_type>";
	private static final String FULLNAMEPART8 = ".<finally_block>";
	public static final String PORTRETURNNOTALLOWED = "Functions can not return ports";

	private static final String DASHALLOWEDONLYFORTEMPLATES = "Using not used symbol (`-') as the default parameter"
			+ " is allowed only for modified templates";

	private static final String CLASSNORUNSON = "Class functions cannot have a `runs on` clause";
	private static final String CLASSNOMTC = "Class functions cannot have an `mtc` clause";
	private static final String CLASSNOSYSTEM = "Class functions cannot have a `system` clause";
	private static final String EXCEPTIONSOOP = "Function exceptions are only supported in the OOP extension";
	
	private static final String KIND = "function";

	
	private final Reference runsOnRef;
	private Component_Type runsOnType = null;
	private final Reference mtcReference;
	private Component_Type mtcType = null;
	private final Reference systemReference;
	private Component_Type systemType = null;
	private final Reference portReference;
	private IType portType = null;

	private final boolean returnsTemplate;
	private final TemplateRestriction.Restriction_type templateRestriction;
	private final StatementBlock finallyBlock;
	private EncodingPrototype_type prototype;
	private Type inputType;
	private Type outputType;
	
	private final SignatureExceptions exceptions;
	private final boolean isClassFunction;
	
	private final Location classModifierLocation;
	private final Location funcModifierLocation;
	
	private boolean isConstructor = false;
	
	/** Indicates if this function is a class function overriding a function of the parent class */
	private boolean isOverride = false;
	
	// stores whether this function can be started or not
	private boolean isStartable;

	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isClassFunction, final boolean isAbstract, final boolean isFinal, final Location classModifierLocation,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation) {
		super(identifier, block, isAbstract, isFinal, isDeterministic, isControl, formalParameters, returnType);
		assignmentType = (returnType == null) ? Assignment_type.A_FUNCTION : (returnsTemplate ? Assignment_type.A_FUNCTION_RTEMP
				: Assignment_type.A_FUNCTION_RVAL);
		if (formalParameterList != null) {
			formalParameterList.setMyDefinition(this);
			formalParameterList.setFullNameParent(this);
		}
		this.runsOnRef = runsOnRef;
		this.mtcReference = mtcReference;
		this.systemReference = systemReference;
		this.portReference = portReference;
		this.returnsTemplate = returnsTemplate;
		this.templateRestriction = templateRestriction;
		this.finallyBlock = finallyBlock;
		this.isClassFunction = isClassFunction;
		this.classModifierLocation = classModifierLocation;
		this.funcModifierLocation = funcModifierLocation;
		this.exceptions = exceptions;

		this.prototype = EncodingPrototype_type.NONE;
		inputType = null;
		outputType = null;

		if (block != null) {
			block.setMyDefinition(this);
			block.setFullNameParent(this);
		}
		if (finallyBlock != null) {
			finallyBlock.setMyDefinition(this);
			finallyBlock.setFullNameParent(this);
			finallyBlock.setOwnerIsFinally();
		}
		if (runsOnRef != null) {
			runsOnRef.setFullNameParent(this);
		}
		if (mtcReference != null) {
			mtcReference.setFullNameParent(this);
		}
		if (systemReference != null) {
			systemReference.setFullNameParent(this);
		}
		if (portReference != null) {
			portReference.setFullNameParent(this);
		}
		if (returnType != null) {
			returnType.setOwnertype(TypeOwner_type.OT_FUNCTION_DEF, this);
			returnType.setFullNameParent(this);
		}
		if (isClassFunction && identifier.getName().equals("create")) {
			isConstructor = true;
		}
	}
	
	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation) {
		this(identifier, formalParameters, runsOnRef, mtcReference, systemReference, portReference, returnType,
			 returnsTemplate, templateRestriction, exceptions, block, finallyBlock, false, false, false, null,
			 isDeterministic, isControl, funcModifierLocation);
	}
	
	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Reference runsOnRef,
			final Reference mtcReference, final Reference systemReference, final Reference portReference,
			final Type returnType, final boolean returnsTemplate, final TemplateRestriction.Restriction_type templateRestriction,
			final SignatureExceptions exceptions, final StatementBlock block, final StatementBlock finallyBlock,
			final boolean isDeterministic, final boolean isControl, final Location funcModifierLocation, final boolean isClassFunction) {
		this(identifier, formalParameters, runsOnRef, mtcReference, systemReference, portReference, returnType,
			 returnsTemplate, templateRestriction, exceptions, block, finallyBlock, true, false, false, null,
			 isDeterministic, isControl, funcModifierLocation);
	}
	
	/**
	 * Constructor for functions defined by 'object'
	 * @param identifier
	 * @param returnType
	 */
	public Def_Function(final Identifier identifier, final FormalParameterList formalParameters, final Type returnType) {
		this(identifier, formalParameters, null, null, null, null, returnType, false, null, null, null, null, false, false, null);
	}

	public boolean isStartable() {
		return isStartable;
	}

	public static String getKind() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public StringBuilder getFullName(final INamedNode child) {
		final StringBuilder builder = super.getFullName(child);

		if (runsOnRef == child) {
			return builder.append(FULLNAMEPART1);
		} else if (formalParameterList == child) {
			return builder.append(FULLNAMEPART2);
		} else if (returnType == child) {
			return builder.append(FULLNAMEPART3);
		} else if (statementBlock == child) {
			return builder.append(FULLNAMEPART4);
		} else if (mtcReference == child) {
			return builder.append(FULLNAMEPART5);
		} else if (systemReference == child) {
			return builder.append(FULLNAMEPART6);
		} else if (portReference == child) {
			return builder.append(FULLNAMEPART7);
		} else if (finallyBlock == child) {
			return builder.append(FULLNAMEPART8);
		}

		return builder;
	}

	public EncodingPrototype_type getPrototype() {
		return prototype;
	}

	public Type getInputType() {
		return inputType;
	}

	public Type getOutputType() {
		return outputType;
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalKind() {
		return KIND;
	}

	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return "function";
	}

	@Override
	/** {@inheritDoc} */
	public String getProposalDescription() {
		final StringBuilder nameBuilder = new StringBuilder(identifier.getDisplayName());
		nameBuilder.append('(');
		formalParameterList.getAsProposalDesriptionPart(nameBuilder);
		nameBuilder.append(')');
		return nameBuilder.toString();
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}

		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());

		super.setMyScope(bridgeScope);
		if (runsOnRef != null) {
			runsOnRef.setMyScope(bridgeScope);
		}
		if (mtcReference != null) {
			mtcReference.setMyScope(bridgeScope);
		}
		if (systemReference != null) {
			systemReference.setMyScope(bridgeScope);
		}
		if (portReference != null) {
			portReference.setMyScope(bridgeScope);
		}
		if (formalParameterList != null) {
			formalParameterList.setMyScope(bridgeScope);
		}
		if (returnType != null) {
			returnType.setMyScope(bridgeScope);
		}
		if (statementBlock != null) {
			statementBlock.setMyScope(formalParameterList);
		}
		if (finallyBlock != null) {
			finallyBlock.setMyScope(formalParameterList);
		}
		if (statementBlock != null) {
			bridgeScope.addSubScope(statementBlock.getLocation(), statementBlock);
		}
		if (formalParameterList != null) {
			bridgeScope.addSubScope(formalParameterList.getLocation(), formalParameterList);
		}
	}

	public Component_Type getRunsOnType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnType;
	}

	public Reference getRunsOnReference(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return runsOnRef;
	}

	public Component_Type getMtcType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return mtcType;
	}

	public Component_Type getSystemType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return systemType;
	}

	public IType getPortType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return portType;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
		removeSyntaxDecoration();
		/** 
		 * Code minings are disabled for now
		 */
		//removeMining();

		isUsed = false;
		runsOnType = null;
		mtcType = null;
		systemType = null;
		portType = null;
		isStartable = false;

		if (exceptions != null) {
			final boolean isOopEnabled = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.ENABLEOOPEXTENSION);
			if (! isOopEnabled) {
				exceptions.getLocation().reportSemanticError(EXCEPTIONSOOP);
			} 
		}
		
		if (isClassFunction) {
			if (runsOnRef != null) {
				runsOnRef.getLocation().reportSemanticError(CLASSNORUNSON);
			}
			if (mtcReference != null) {
				mtcReference.getLocation().reportSemanticError(CLASSNOMTC);
			}
			if (systemReference!= null) {
				systemReference.getLocation().reportSemanticError(CLASSNOSYSTEM);
			}
		} else {
			if (runsOnRef != null && portReference != null) {
				runsOnRef.getLocation().reportSemanticError("A `runs on' and a `port' clause cannot be present at the same time.");
			}
	
			if (runsOnRef != null) {
				runsOnType = runsOnRef.chkComponentypeReference(timestamp);
				if (runsOnType != null) {
					final Scope formalParlistPreviosScope = formalParameterList.getParentScope();
					if (formalParlistPreviosScope instanceof RunsOnScope
							&& ((RunsOnScope) formalParlistPreviosScope).getParentScope() == myScope) {
						((RunsOnScope) formalParlistPreviosScope).setComponentType(runsOnType);
					} else {
						final Scope tempScope = new RunsOnScope(runsOnType, myScope);
						formalParameterList.setMyScope(tempScope);
					}
				}
			}
	
			if (mtcReference != null) {
				mtcType = mtcReference.chkComponentypeReference(timestamp);
			}
	
			if (systemReference != null) {
				systemType = systemReference.chkComponentypeReference(timestamp);
			}
		}

		boolean canSkip = false;
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					canSkip = true;
				}
			}
		}

		if(!canSkip) {
			formalParameterList.reset();
		}

		formalParameterList.check(timestamp, getAssignmentType());

		if (returnType != null) {
			if (! isConstructor) {
				returnType.check(timestamp);
			}
			final IType returnedType = returnType.getTypeRefdLast(timestamp);
			if (Type_type.TYPE_PORT.equals(returnedType.getTypetype()) && returnType.getLocation() != null) {
				returnType.getLocation().reportSemanticError(PORTRETURNNOTALLOWED);
			}
		}

		if (formalParameterList.hasNotusedDefaultValue()) {
			formalParameterList.getLocation().reportSemanticError(DASHALLOWEDONLYFORTEMPLATES);
			return;
		}

		prototype = EncodingPrototype_type.NONE;
		inputType = null;
		outputType = null;

		if (withAttributesPath != null) {
			withAttributesPath.checkGlobalAttributes(timestamp, false);
			withAttributesPath.checkAttributes(timestamp);
			analyzeExtensionAttributes(timestamp);
			checkPrototype(timestamp);
		}

		if (portReference != null) {
			final Assignment assignment = portReference.getRefdAssignment(timestamp, false);
			if (assignment != null) {
				portType = assignment.getType(timestamp);
				if (portType != null) {
					if (portType.getTypetype() == Type_type.TYPE_PORT) {
						final Scope tempScope = new PortScope((Port_Type)portType, myScope);
						formalParameterList.setMyScope(tempScope);
					} else {
						portReference.getLocation().reportSemanticError(MessageFormat.format("Reference `{0}'' does not refer to a port type.", portReference.getDisplayName()));
					}
				}
			}
		}

		// decision of startability
		isStartable = runsOnRef != null;
		isStartable &= formalParameterList.getStartability();
		if (isStartable && returnType != null && returnType.isComponentInternal(timestamp)) {
			isStartable = false;
		}

		if(canSkip) {
			return;
		}

		Scope sc = getMyScope();
		while (sc != null && sc instanceof StatementBlock || sc instanceof FormalParameterList || sc instanceof NamedBridgeScope) {
			sc = sc.getParentScope();
		}
		
		NamingConventionHelper.checkConvention(PreferenceConstants.REPORTNAMINGCONVENTION_FUNCTION, identifier, this);
		NamingConventionHelper.checkNameContents(identifier, getMyScope().getModuleScope().getIdentifier(), getDescription());

		if (statementBlock != null) {
			statementBlock.check(timestamp);

			if (returnType != null && isConstructor == false) {
				// check the presence of return statements
				switch (statementBlock.hasReturn(timestamp)) {
				case RS_NO:
					String retval = Type.typeToSrcTypeDefault(getType(timestamp).getTypetypeTtcn3());
					identifier.getLocation().reportSemanticError(
							"The function has a return type, but it does not have any return statement",
							new HoverProposal[] {
								new HoverProposal("Add missing return statement...",
										ISharedImages.IMG_OBJ_ADD,
										getLocation(), getLocation().getEndOffset() - 1, "return " + retval + ";\n")
							});
					break;
				case RS_MAYBE:
					identifier.getLocation()
					.reportSemanticError(
							"The function has return type, but control might leave it without reaching a return statement");
					break;
				default:
					break;
				}
			}

			statementBlock.postCheck();
			statementBlock.setCodeSection(CodeSectionType.CS_INLINE);
		}
		
		if (finallyBlock != null) {
			finallyBlock.check(timestamp);
		}
		
		checkDocumentComment();
	}

	/**
	 * Checks and returns whether the function is startable. Reports the
	 * appropriate error messages.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * @param errorLocation
	 *                the location to report the error to, if needed.
	 *
	 * @return true if startable, false otherwise
	 * */
	public boolean checkStartable(final CompilationTimeStamp timestamp, final Location errorLocation) {
		check(timestamp);

		if (runsOnRef == null) {
			errorLocation.reportSemanticError(MessageFormat.format(
					"Function `{0}'' cannot be started on parallel test component because it does not have a `runs on'' clause",
					getFullName()));
		}

		formalParameterList.checkStartability(timestamp, "Function", this, errorLocation);

		if (returnType != null && returnType.isComponentInternal(timestamp)) {
			final Set<IType> typeSet = new HashSet<IType>();
			final String errorMessage = "the return type or embedded in the return type of function `" + getFullName()
					+ "' if it is started on a parallel test component";
			returnType.checkComponentInternal(timestamp, typeSet, errorMessage);
		}

		if (isStartable) {
			return true;
		}

		return false;
	}

	/**
	 * Convert and check the encoding attributes applied to this function.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * */
	public void analyzeExtensionAttributes(final CompilationTimeStamp timestamp) {
		final List<SingleWithAttribute> realAttributes = withAttributesPath.getRealAttributes(timestamp);

		List<AttributeSpecification> specifications = null;
		for (final SingleWithAttribute attribute : realAttributes) {
			if (Attribute_Type.Extension_Attribute.equals(attribute.getAttributeType())) {
				final Qualifiers qualifiers = attribute.getQualifiers();
				if (qualifiers == null || qualifiers.getNofQualifiers() == 0) {
					if (specifications == null) {
						specifications = new ArrayList<AttributeSpecification>();
					}
					specifications.add(attribute.getAttributeSpecification());
				}
			}
		}

		if (specifications == null) {
			return;
		}

		final List<ExtensionAttribute> attributes = new ArrayList<ExtensionAttribute>();
		for (final AttributeSpecification specification: specifications) {
			final ExtensionAttributeAnalyzer analyzer = new ExtensionAttributeAnalyzer();
			analyzer.parse(specification);
			final List<ExtensionAttribute> temp = analyzer.getAttributes();
			if (temp != null) {
				attributes.addAll(temp);
			}
		}

		for (final ExtensionAttribute extensionAttribute: attributes) {
			if (ExtensionAttribute_type.PROTOTYPE.equals(extensionAttribute.getAttributeType())) {
				final PrototypeAttribute realAttribute = (PrototypeAttribute) extensionAttribute;
				if (EncodingPrototype_type.NONE.equals(prototype)) {
					prototype = realAttribute.getPrototypeType();
				} else {
					realAttribute.getLocation().reportSingularSemanticError("Duplicate attribute `prototype'");
				}
			}
		}
	}

	/**
	 * Checks the prototype attribute set for this function definition.
	 *
	 * @param timestamp
	 *                the timestamp of the actual build cycle.
	 * */
	public void checkPrototype(final CompilationTimeStamp timestamp) {
		if (EncodingPrototype_type.NONE.equals(prototype)) {
			return;
		}

		// checking formal parameter list
		if (EncodingPrototype_type.CONVERT.equals(prototype)) {
			if (formalParameterList.getNofParameters() == 1) {
				final FormalParameter parameter = formalParameterList.getParameterByIndex(0);
				final Assignment_type assignmentType = parameter.getRealAssignmentType();
				if (Assignment_type.A_PAR_VAL_IN.semanticallyEquals(assignmentType)) {
					inputType = parameter.getType(timestamp);
				} else {
					parameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), parameter.getAssignmentName()));
				}
			} else {
				formalParameterList.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"The function must have one parameter instead of {0} for attribute `prototype({1})''",
								formalParameterList.getNofParameters(), prototype.getName()));
			}
		} else if (formalParameterList.getNofParameters() == 2) {
			final FormalParameter firstParameter = formalParameterList.getParameterByIndex(0);
			if (EncodingPrototype_type.SLIDING.equals(prototype)) {
				if (Assignment_type.A_PAR_VAL_INOUT.semanticallyEquals(firstParameter.getRealAssignmentType())) {
					final Type firstParameterType = firstParameter.getType(timestamp);
					final IType last = firstParameterType.getTypeRefdLast(timestamp);
					if (last.getIsErroneous(timestamp)) {
						inputType = firstParameterType;
					} else {
						switch (last.getTypetypeTtcn3()) {
						case TYPE_OCTETSTRING:
						case TYPE_CHARSTRING:
							inputType = firstParameterType;
							break;
						default:
							firstParameter.getLocation()
							.reportSemanticError(
									MessageFormat.format(
											"The type of the first parameter must be `octetstring'' or `charstring'' for attribute `prototype({0})''",
											prototype.getName()));
							break;
						}
					}
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `inout'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			} else {
				final Assignment_type assignmentType = firstParameter.getRealAssignmentType();
				if (Assignment_type.A_PAR_VAL_IN.semanticallyEquals(assignmentType) || Assignment_type.A_PAR_VAL.semanticallyEquals(assignmentType)) {
					inputType = firstParameter.getType(timestamp);
				} else {
					firstParameter.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The first parameter must be an `in'' value parameter for attribute `prototype({0})'' instead of {1}",
									prototype.getName(), firstParameter.getAssignmentName()));
				}
			}

			final FormalParameter secondParameter = formalParameterList.getParameterByIndex(1);
			if (Assignment_type.A_PAR_VAL_OUT.semanticallyEquals(secondParameter.getRealAssignmentType())) {
				outputType = secondParameter.getType(timestamp);
			} else {
				secondParameter.getLocation()
				.reportSemanticError(
						MessageFormat.format(
								"The second parameter must be an `out'' value parameter for attribute `prototype({0})'' instead of {1}",
								prototype.getName(), secondParameter.getAssignmentName()));
			}
		} else {
			formalParameterList.getLocation().reportSemanticError(
					MessageFormat.format("The function must have two parameters for attribute `prototype({0})'' instead of {1}",
							prototype.getName(), formalParameterList.getNofParameters()));
		}

		// checking the return type
		if (EncodingPrototype_type.FAST.equals(prototype)) {
			if (returnType != null) {
				returnType.getLocation().reportSemanticError(
						MessageFormat.format("The function cannot have return type for attribute `prototype({0})''",
								prototype.getName()));
			}
		} else {
			if (returnType == null) {
				location.reportSemanticError(MessageFormat.format(
						"The function must have a return type for attribute `prototype({0})''", prototype.getName()));
			} else {
				if (Assignment_type.A_FUNCTION_RTEMP.semanticallyEquals(assignmentType)) {
					returnType.getLocation()
					.reportSemanticError(
							MessageFormat.format(
									"The function must return a value instead of a template for attribute `prototype({0})''",
									prototype.getName()));
				}

				if (EncodingPrototype_type.CONVERT.equals(prototype)) {
					outputType = returnType;
				} else {
					final IType last = returnType.getTypeRefdLast(timestamp);

					if (!last.getIsErroneous(timestamp) && !Type_type.TYPE_INTEGER.equals(last.getTypetypeTtcn3())) {
						returnType.getLocation()
						.reportSemanticError(
								MessageFormat.format(
										"The return type of the function must be `integer'' instead of `{0}'' for attribute `prototype({1})''",
										returnType.getTypename(), prototype.getName()));
					}
				}
			}
		}

		// checking the runs on clause
		if (runsOnType != null && runsOnRef != null) {
			runsOnRef.getLocation().reportSemanticError(
					MessageFormat.format("The function cannot have `runs on'' clause for attribute `prototype({0})''",
							prototype.getName()));
		}
	}

	@Override
	/** {@inheritDoc} */
	public void postCheck() {
		if (myScope != null) {
			final Module module = myScope.getModuleScope();
			if (module != null) {
				if (module.getSkippedFromSemanticChecking()) {
					return;
				}
			}
		}

		super.postCheck();

		formalParameterList.postCheck();
	}

	@Override
	/** {@inheritDoc} */
	public void addProposal(final ProposalCollector propCollector, final int index) {
		final List<ISubReference> subrefs = propCollector.getReference().getSubreferences();
		if (subrefs.size() <= index || Subreference_type.arraySubReference.equals(subrefs.get(index).getReferenceType())) {
			return;
		}

		if (subrefs.size() == index + 1 && identifier.getName().toLowerCase(Locale.ENGLISH).startsWith(subrefs.get(index).getId().getName().toLowerCase(Locale.ENGLISH))) {
			final StringBuilder patternBuilder = new StringBuilder(identifier.getDisplayName());
			patternBuilder.append('(');
			formalParameterList.getAsProposalPart(patternBuilder);
			patternBuilder.append(')');
			propCollector.addTemplateProposal(identifier.getDisplayName(),
					new Template(getProposalDescription(), "", propCollector.getContextIdentifier(), patternBuilder.toString(),
							false), TTCN3CodeSkeletons.SKELETON_IMAGE);
			super.addProposal(propCollector, index);
		} else if (subrefs.size() > index + 1 && returnType != null
				&& Subreference_type.parameterisedSubReference.equals(subrefs.get(index).getReferenceType())
				&& identifier.getName().equals(subrefs.get(index).getId().getName())) {
			// perfect match
			returnType.addProposal(propCollector, index + 1);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void addDeclaration(final DeclarationCollector declarationCollector, final int index) {
		final List<ISubReference> subrefs = declarationCollector.getReference().getSubreferences();
		if (subrefs.size() <= index || Subreference_type.arraySubReference.equals(subrefs.get(index).getReferenceType())) {
			return;
		}

		if (identifier.getName().equals(subrefs.get(index).getId().getName())) {
			if (subrefs.size() > index + 1 && returnType != null) {
				returnType.addDeclaration(declarationCollector, index + 1);
			} else {
				declarationCollector.addDeclaration(this);
			}
		}
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		final StringBuilder text = new StringBuilder(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return text.toString();
		}

		text.append('(');
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			if (i != 0) {
				text.append(", ");
			}
			final FormalParameter parameter = formalParameterList.getParameterByIndex(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				text.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					text.append("Unknown type");
				} else {
					text.append(type.getTypename());
				}
			}
		}
		text.append(')');
		return text.toString();
	}

	/**
	 * Returns the styled text to be displayed when using code completion feature 
	 * for method call parameters
	 * 
	 * @return function name and formal parameter list 
	 */
	public StyledString getStyledProposalText() {
		StyledString styled = new StyledString(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return styled;
		}
		
		styled.append(" ( ");

		Styler bluebold = new Stylers.BlueBoldStyler();
		Styler blackitalic = new Stylers.BlackItalicStyler();
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			if (i != 0) {
				styled.append(", ");
			}
			final FormalParameter parameter = formalParameterList.getParameterByIndex(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				styled.append("timer");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					styled.append("Unknown type");
				} else {
					styled.append(type.getTypename(), bluebold);
					styled.append(' ');
					styled.append(parameter.getGenName(), blackitalic);
				}
			}
		}
		styled.append(" )");
		return styled;
	}
	
	public String getProposalText() {
		StringBuilder styled = new StringBuilder(identifier.getDisplayName());
		if (formalParameterList == null || lastTimeChecked == null) {
			return styled.toString();
		}
		
		styled.append(" (");
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			if (i != 0) {
				styled.append(",");
			}
			styled.append("<br>");
			final FormalParameter parameter = formalParameterList.getParameterByIndex(i);
			if (Assignment_type.A_PAR_TIMER.semanticallyEquals(parameter.getRealAssignmentType())) {
				styled.append("<pre>    timer</pre>");
			} else {
				final IType type = parameter.getType(lastTimeChecked);
				if (type == null) {
					styled.append("<pre>    Unknown type</pre>");
				} else {
					styled.append("<pre>    " + type.getTypename() + "</pre>");
					styled.append(' ');
					styled.append(parameter.getGenName());
				}
			}
		}
		styled.append("<br>)");
		return styled.toString();
	}
	
	@Override
	/** {@inheritDoc} */
	public TemplateRestriction.Restriction_type getTemplateRestriction() {
		return templateRestriction;
	}

	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			lastTimeChecked = null;

			boolean enveloped = false;

			final Location temporalIdentifier = identifier.getLocation();
			if (reparser.envelopsDamage(temporalIdentifier) || reparser.isExtending(temporalIdentifier)) {
				reparser.extendDamagedRegion(temporalIdentifier);
				final IIdentifierReparser r = new IdentifierReparser(reparser);
				final int result = r.parseAndSetNameChanged();
				identifier = r.getIdentifier();
				// damage handled
				if (result == 0 && identifier != null) {
					enveloped = true;
				} else {
					removeBridge();
					throw new ReParseException(result);
				}
			}

			if (formalParameterList != null) {
				if (enveloped) {
					formalParameterList.updateSyntax(reparser, false);
					reparser.updateLocation(formalParameterList.getLocation());
				} else if (reparser.envelopsDamage(formalParameterList.getLocation())) {
					try {
						formalParameterList.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(formalParameterList.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (runsOnRef != null) {
				if (enveloped) {
					runsOnRef.updateSyntax(reparser, false);
					reparser.updateLocation(runsOnRef.getLocation());
				} else if (reparser.envelopsDamage(runsOnRef.getLocation())) {
					try {
						runsOnRef.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(runsOnRef.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (mtcReference != null) {
				if (enveloped) {
					mtcReference.updateSyntax(reparser, false);
					reparser.updateLocation(mtcReference.getLocation());
				} else if (reparser.envelopsDamage(mtcReference.getLocation())) {
					try {
						mtcReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(mtcReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (systemReference != null) {
				if (enveloped) {
					systemReference.updateSyntax(reparser, false);
					reparser.updateLocation(systemReference.getLocation());
				} else if (reparser.envelopsDamage(systemReference.getLocation())) {
					try {
						systemReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(systemReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (portReference != null) {
				if (enveloped) {
					portReference.updateSyntax(reparser, false);
					reparser.updateLocation(portReference.getLocation());
				} else if (reparser.envelopsDamage(portReference.getLocation())) {
					try {
						portReference.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(portReference.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (returnType != null) {
				if (enveloped) {
					returnType.updateSyntax(reparser, false);
					reparser.updateLocation(returnType.getLocation());
				} else if (reparser.envelopsDamage(returnType.getLocation())) {
					try {
						returnType.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(returnType.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (statementBlock != null) {
				if (enveloped) {
					statementBlock.updateSyntax(reparser, false);
					reparser.updateLocation(statementBlock.getLocation());
				} else if (reparser.envelopsDamage(statementBlock.getLocation())) {
					try {
						statementBlock.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(statementBlock.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (withAttributesPath != null) {
				if (enveloped) {
					withAttributesPath.updateSyntax(reparser, false);
					reparser.updateLocation(withAttributesPath.getLocation());
				} else if (reparser.envelopsDamage(withAttributesPath.getLocation())) {
					try {
						withAttributesPath.updateSyntax(reparser, true);
						enveloped = true;
						reparser.updateLocation(withAttributesPath.getLocation());
					} catch (ReParseException e) {
						removeBridge();
						throw e;
					}
				}
			}

			if (!enveloped) {
				removeBridge();
				throw new ReParseException();
			}

			return;
		}

		reparser.updateLocation(identifier.getLocation());

		if (formalParameterList != null) {
			formalParameterList.updateSyntax(reparser, false);
			reparser.updateLocation(formalParameterList.getLocation());
		}

		if (runsOnRef != null) {
			runsOnRef.updateSyntax(reparser, false);
			reparser.updateLocation(runsOnRef.getLocation());
		}

		if (mtcReference != null) {
			mtcReference.updateSyntax(reparser, false);
			reparser.updateLocation(mtcReference.getLocation());
		}

		if (systemReference != null) {
			systemReference.updateSyntax(reparser, false);
			reparser.updateLocation(systemReference.getLocation());
		}

		if (portReference != null) {
			portReference.updateSyntax(reparser, false);
			reparser.updateLocation(portReference.getLocation());
		}

		if (returnType != null) {
			if (! (identifier.getName().equals("create") && returnType instanceof Class_Type)) {
				returnType.updateSyntax(reparser, false);
				reparser.updateLocation(returnType.getLocation());
			}
		}

		if (statementBlock != null) {
			statementBlock.updateSyntax(reparser, false);
			reparser.updateLocation(statementBlock.getLocation());
		}

		if (withAttributesPath != null) {
			withAttributesPath.updateSyntax(reparser, false);
			reparser.updateLocation(withAttributesPath.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParameterList != null) {
			formalParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (returnType != null) {
			returnType.findReferences(referenceFinder, foundIdentifiers);
		}
		if (runsOnRef != null) {
			runsOnRef.findReferences(referenceFinder, foundIdentifiers);
		}
		if (mtcReference != null) {
			mtcReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (systemReference != null) {
			systemReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (portReference != null) {
			portReference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (statementBlock != null) {
			statementBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		if (returnType != null) {
			if (returnType instanceof Class_Type && !isConstructor) {
				if (! returnType.accept(v)) {
					return false;
				}
			}
		}
		if (runsOnRef != null && !runsOnRef.accept(v)) {
			return false;
		}
		if (mtcReference != null && !mtcReference.accept(v)) {
			return false;
		}
		if (systemReference != null && !systemReference.accept(v)) {
			return false;
		}
		if (portReference != null && !portReference.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void generateCode( final JavaGenData aData, final boolean cleanUp ) {
		if (portType != null) {
			return;
		}

		final StringBuilder sb = aData.getSrc();
		generateCodeInternal(aData, sb);
	}

	/**
	 * Generate Java code for a translation function, into the body of its port type.
	 *
	 * @param aData  the structure to put imports into and get temporal variable names from.
	 * @param source the source to extend with the generated code (port generator member).
	 * */
	public void generateCodePortBody( final JavaGenData aData, final StringBuilder source ) {
		generateCodeInternal(aData, source);
	}

	/**
	 * Generate Java code for a function definition, to a given source code target.
	 *
	 * TODO the compiler could also benefit from such a separation
	 *
	 * @param aData  the structure to put imports into and get temporal variable names from.
	 * @param source the source to extend with the generated code.
	 * */
	private void generateCodeInternal( final JavaGenData aData, final StringBuilder source) {
		final String genName = getGenName();
		if (formalParameterList != null) {
			formalParameterList.setGenName(genName);
		}

		final StringBuilder tempSource = new StringBuilder("public");
		if (portType == null) {
			tempSource.append(" static");
		}
		tempSource.append( " final " );

		// return value
		String returnTypeName = null;
		switch (assignmentType) {
		case A_FUNCTION:
			returnTypeName = "void";
			break;
		case A_FUNCTION_RVAL:
			returnTypeName = returnType.getGenNameValue( aData, tempSource );
			break;
		case A_FUNCTION_RTEMP:
			returnTypeName = returnType.getGenNameTemplate( aData, tempSource );
			break;
		default:
			ErrorReporter.INTERNAL_ERROR("Code generator reached erroneous definition `" + getFullName() + "''");
			break;
		}

		tempSource.append(returnTypeName);
		tempSource.append( ' ' );

		// function name
		tempSource.append( genName );

		// arguments
		tempSource.append( '(' );
		if ( formalParameterList != null ) {
			formalParameterList.generateCode( aData, tempSource );
		}
		tempSource.append( ") {\n" );
		getLocation().create_location_object(aData, tempSource, "FUNCTION", getIdentifier().getDisplayName());
		if ( formalParameterList != null ) {
			formalParameterList.generateCodeSetUnbound(aData, tempSource);
			formalParameterList.generateCodeShadowObjects(aData, tempSource);
		}
		if (aData.getAddSourceInfo()) {
			tempSource.append( "try {\n" );
		}
		statementBlock.generateCode(aData, tempSource);
		if (aData.getAddSourceInfo()) {
			tempSource.append( "} finally {\n" );
			getLocation().release_location_object(aData, tempSource);
			tempSource.append( "}\n" );
		}
		tempSource.append( "}\n" );

		if (isStartable) {
			aData.addBuiltinTypeImport("TitanComponent");
			aData.addBuiltinTypeImport("Text_Buf");
			aData.addCommonLibraryImport("TTCN_Logger");
			aData.addCommonLibraryImport("TTCN_Runtime");

			tempSource.append(MessageFormat.format("public static final void start_{0}(final TitanComponent component_reference", genName));
			if (formalParameterList != null && formalParameterList.getNofParameters() > 0) {
				tempSource.append(", ");
				formalParameterList.generateCode(aData, tempSource);
			}
			tempSource.append(") {\n");
			tempSource.append("TTCN_Logger.begin_event(TTCN_Logger.Severity.PARALLEL_PTC);\n");
			tempSource.append(MessageFormat.format("TTCN_Logger.log_event_str(\"Starting function {0}(\");\n", identifier.getDisplayName()));
			if (formalParameterList != null) {
				for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
					if (i > 0) {
						tempSource.append("TTCN_Logger.log_event_str(\", \");\n");
					}
					tempSource.append(MessageFormat.format("{0}.log();\n", formalParameterList.getParameterByIndex(i).getGeneratedReferenceName()));
				}
			}
			tempSource.append("TTCN_Logger.log_event_str(\") on component \");\n");
			tempSource.append("component_reference.log();\n");
			tempSource.append("TTCN_Logger.log_char('.');\n");
			tempSource.append("TTCN_Logger.end_event();\n");

			tempSource.append("final Text_Buf text_buf = new Text_Buf();\n");
			tempSource.append(MessageFormat.format("TTCN_Runtime.prepare_start_component(component_reference, \"{0}\", \"{1}\", text_buf);\n", myScope.getModuleScopeGen().getIdentifier().getDisplayName(), identifier.getDisplayName()));
			if (formalParameterList != null) {
				for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
					tempSource.append(MessageFormat.format("{0}.encode_text(text_buf);\n", formalParameterList.getParameterByIndex(i).getGeneratedReferenceName()));
				}
			}
			tempSource.append("TTCN_Runtime.send_start_component(text_buf);\n");
			tempSource.append("}\n");

			//entry into start function
			final StringBuilder startFunction = aData.getStartPTCFunction();
			startFunction.append(MessageFormat.format("if(\"{0}\".equals(function_name)) '{'\n", identifier.getDisplayName()));
			if (formalParameterList != null) {
				for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
					final FormalParameter formalParameter = formalParameterList.getParameterByIndex(i);

					formalParameter.generateCodeObject(aData, startFunction, "", false, true);
					startFunction.append(MessageFormat.format("\t\t\t{0}.decode_text(function_arguments);\n", formalParameter.getGeneratedReferenceName()));
				}

				startFunction.append("\t\t\tTTCN_Logger.begin_event(TTCN_Logger.Severity.PARALLEL_PTC);\n");
				startFunction.append(MessageFormat.format("\t\t\tTTCN_Logger.log_event_str(\"Starting function {0}(\");\n", identifier.getDisplayName()));

				for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
					if (i > 0) {
						startFunction.append("\t\t\tTTCN_Logger.log_event_str(\", \");\n");
					}
					startFunction.append(MessageFormat.format("\t\t\t{0}.log();\n", formalParameterList.getParameterByIndex(i).getGeneratedReferenceName()));
				}

				startFunction.append("\t\t\tTTCN_Logger.log_event_str(\").\");\n");
				startFunction.append("\t\t\tTTCN_Logger.end_event();\n");
			} else {
				startFunction.append(MessageFormat.format("\t\t\tTTCN_Logger.log_str(TTCN_Logger.Severity.PARALLEL_PTC, \"Starting function {0}().\");\n", identifier.getDisplayName()));
			}

			startFunction.append("\t\t\tTTCN_Runtime.function_started(function_arguments);\n");
			StringBuilder actualParList;
			if (formalParameterList == null) {
				actualParList = new StringBuilder();
			} else {
				actualParList = formalParameterList.generateCodeActualParlist("");
			}
			boolean returnValueKept = false;
			if (assignmentType == Assignment_type.A_FUNCTION_RVAL) {
				IType t = returnType;
				while (true) {
					if (t.hasDoneAttribute()) {
						returnValueKept = true;

						break;
					} else if (t instanceof Referenced_Type) {
						final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
						final IType t2 = ((Referenced_Type) t).getTypeRefd(CompilationTimeStamp.getBaseTimestamp(), referenceChain);
						referenceChain.release();
						if (t2.getIsErroneous(CompilationTimeStamp.getBaseTimestamp()) || t2 == t) {
							break;
						}
						t = t2;
					} else {
						break;
					}
				}
			}
			if (returnValueKept) {
				final String returnTypeDisplayName = returnType.getTypename();

				startFunction.append(MessageFormat.format("\t\t\tfinal {0} ret_val = new {0}({1}({2}));\n", returnTypeName, genName, actualParList));
				startFunction.append("\t\t\tTTCN_Logger.begin_event(TTCN_Logger.Severity.PARALLEL_UNQUALIFIED);\n");
				startFunction.append(MessageFormat.format("\t\t\tTTCN_Logger.log_event_str(\"Function {0} returned {1} : \");\n", identifier.getDisplayName(), returnTypeDisplayName));
				startFunction.append("\t\t\tret_val.log();\n");
				startFunction.append("\t\t\tTTCN_Logger.end_event();\n");
				startFunction.append("\t\t\tfinal Text_Buf text_buf = new Text_Buf();\n");
				startFunction.append(MessageFormat.format("\t\t\tTTCN_Runtime.prepare_function_finished(\"{0}\", text_buf);\n", returnTypeDisplayName));
				startFunction.append("\t\t\tret_val.encode_text(text_buf);\n");
				startFunction.append("\t\t\tTTCN_Runtime.send_function_finished(text_buf);\n");
			} else {
				startFunction.append(MessageFormat.format("\t\t\t{0}({1});\n", genName, actualParList));
				startFunction.append(MessageFormat.format("\t\t\tTTCN_Runtime.function_finished(\"{0}\");\n", identifier.getDisplayName()));
			}

			startFunction.append("\t\t\treturn true;\n");
			startFunction.append("\t\t} else ");
		}

		source.append(tempSource);
	}

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent(IEditorPart editor) {
		super.getHoverContent(editor);
		
		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = parseDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}

		hoverContent.addIcon(getOutlineIcon())
			.addText(KIND)
			.addStyledText(isDeterministic ? " @deterministic" : "", SWT.ITALIC)
			.addStyledText(isControl ? " @control " : " ", SWT.ITALIC)
			.addStyledText(getFullName(), SWT.BOLD)
			.addText("(");
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			final FormalParameter fp = formalParameterList.getParameterByIndex(i);
			if (i > 0) {
				hoverContent.addText(", ");
			}
			hoverContent.addText(fp.getFormalParamType()).addText(" ")
				.addText(fp.getType(lastTimeChecked).getTypename()).addText(" ")
				.addText(fp.getIdentifier().getDisplayName());
		}
		hoverContent.addText(")");
		if (runsOnRef != null) {
			hoverContent.addText(" runs on ").addStyledText(runsOnRef.getDisplayName(), SWT.BOLD);
		}
		if (portReference != null) {
			hoverContent.addText(" port ").addStyledText(portReference.getDisplayName(), SWT.BOLD);
		}
		if (mtcReference != null) {
			hoverContent.addText(" mtc ").addStyledText(mtcReference.getDisplayName(), SWT.BOLD);
		}
		if (systemReference != null) {
			hoverContent.addText(" system ").addStyledText(systemReference.getDisplayName(), SWT.BOLD);
		}
		if (returnType != null) {
			hoverContent.addText(" return ").addStyledText(returnType.getTypename(), SWT.BOLD);
		}
		hoverContent.closeHeader();

		getHoverContentFromComment(hoverContent, dc, formalParameterList);
		
		hoverContent.addContent(HoverContentType.INFO);		
		return hoverContent;
	}

	@Override
	public String generateDocComment(final String indentation) {
		return generateCommonDocComment(indentation, formalParameterList, returnType);
	}

	public boolean isOverride() {
		return isOverride;
	}

	public void setOverride(boolean isOverride) {
		this.isOverride = isOverride;
	}

	@Override
	public IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other) {
		if (! (other instanceof Def_Function)) {
			return IsIdenticalResult.RES_DIFFERS;
		}
		final Def_Function otherf = (Def_Function)other;
		final Assignment_type asstype = getAssignmentType();
		final Assignment_type asstype2 = otherf.getAssignmentType();
		
		if (getAssignmentType() != asstype2) {
			if ((asstype == Assignment_type.A_FUNCTION && asstype2 != Assignment_type.A_EXT_FUNCTION) ||
				(asstype == Assignment_type.A_EXT_FUNCTION && asstype2 != Assignment_type.A_FUNCTION) ||
				(asstype == Assignment_type.A_FUNCTION_RVAL && asstype2 != Assignment_type.A_EXT_FUNCTION_RVAL) ||
				(asstype == Assignment_type.A_EXT_FUNCTION_RVAL && asstype2 != Assignment_type.A_FUNCTION_RVAL) ||
				(asstype == Assignment_type.A_FUNCTION_RTEMP && asstype2 != Assignment_type.A_EXT_FUNCTION_RTEMP) ||
				(asstype == Assignment_type.A_EXT_FUNCTION_RTEMP && asstype2 != Assignment_type.A_FUNCTION_RTEMP)) {
				return IsIdenticalResult.RES_DIFFERS;
			}
		}
		if (returnType != null && ! otherf.getType(timestamp).isIdentical(timestamp, returnType)) {
			return IsIdenticalResult.RES_DIFFERS;
		}
		final FormalParameterList otherFpl = otherf.getFormalParameterList();
		IsIdenticalResult identical = getFormalParameterList().isIdentical(timestamp, otherFpl);
		if (identical == IsIdenticalResult.RES_DIFFERS) {
			return IsIdenticalResult.RES_DIFFERS;
		}
		if (exceptions != null || otherf.exceptions != null) {
			if (exceptions == null || otherf.exceptions == null) {
				return IsIdenticalResult.RES_DIFFERS;
			}
			if (exceptions.getNofExceptions() != otherf.exceptions.getNofExceptions()) {
				return IsIdenticalResult.RES_DIFFERS;
			}
			for (int i = 0; i < exceptions.getNofExceptions(); i++) {
				if (otherf.exceptions.hasException(timestamp, exceptions.getExceptionByIndex(i))) {
					return IsIdenticalResult.RES_DIFFERS;
				}
			}
		}
		return identical;
	}
}
