/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Boolean_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * Represents a class of-operator 
 * ETSI ES 203 790 V1.3.1 (2021-05) 
 * 5.1.2.5 
 * 
 * @author Miklos Magyari
 *
 */
public class OfClassExpression extends Expression_Value {
	private static final String CLASSREFERENCEEXPECTED = "Reference to a class object was expected";
	private static final String CLASSTYPEEXPECTED = "Class type was expected";
	
	private final Type type;
	private final Reference reference;
	private boolean value;
	
	public OfClassExpression(final Type type, final Reference reference) {
		this.type = type;
		this.reference = reference;
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.CLASS_OF_OPERATION;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (type != null) {
			type.setMyScope(scope);
		}
		if (reference != null) {
			reference.setMyScope(scope);
		}
	}
	
	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}
		
		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;
		
		if (reference == null || type == null) {
			setIsErroneous(true);
			return lastValue;
		}
		
		checkExpressionOperands(timestamp, expectedValue, referenceChain);
		
		if (getIsErroneous(timestamp)) {
			return lastValue;
		}

		if (isUnfoldable(timestamp, referenceChain)) {
			return lastValue;
		}
		
		if (!isUnfoldable(timestamp)) {
			value = false;
		}
		return lastValue;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		if (reference != null) {
			reference.updateSyntax(reparser, false);
			reparser.updateLocation(reference.getLocation());
		}
		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		// class of object reference
		final Class_Type refClass = reference.checkVariableReference(timestamp)
			.getTypeRefdLast(timestamp).getClassType();
		// expected class
		final Class_Type expectedClass = type.getTypeRefdLast(timestamp).getClassType();

		// the result is always false if the classes are not related to each other,
        // otherwise a runtime check is required
		return refClass.isParentClass(timestamp, expectedClass) ||
			expectedClass.isParentClass(timestamp, refClass);
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkExpressionSelfReferenceValue(final CompilationTimeStamp timestamp, final Assignment lhs) {
		if (lhs != null) {
			final Assignment assignment = reference.getRefdAssignment(timestamp, false);
			return assignment == lhs;
		}
		return false;
	}
	
	@Override
	public String createStringRepresentation() {
		return reference.getDisplayName() + " of " + type.getTypename();
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return Type_type.TYPE_BOOL;
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (myGovernor != null) {
			return myGovernor;
		}
		
		return new Boolean_Type();
	}
	
	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (reference != null && !reference.accept(v)) {
			return false;
		}
		return true;
	}
	
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		IType typeRefdLast = null;
		final IType refType = reference.checkVariableReference(timestamp);
		if (refType != null) {
			typeRefdLast = refType.getTypeRefdLast(timestamp);
			if (typeRefdLast.getTypetype() != Type_type.TYPE_CLASS) {
				reference.getLocation().reportSemanticError(CLASSREFERENCEEXPECTED);
				setIsErroneous(true);
			}
		} else {
			setIsErroneous(true);
		}
		type.check(timestamp);
		final IType typeLast = type.getTypeRefdLast(timestamp);
		if (typeLast.getTypetype() != Type_type.TYPE_CLASS) {
			type.getLocation().reportSemanticError(CLASSTYPEEXPECTED);
			setIsErroneous(true);
		}
	}
}
