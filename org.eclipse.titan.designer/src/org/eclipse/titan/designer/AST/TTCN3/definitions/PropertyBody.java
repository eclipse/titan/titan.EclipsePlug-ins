/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.IGovernedSimple;
import org.eclipse.titan.designer.AST.IVisitableNode;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

public class PropertyBody implements IVisitableNode {
	private static final String DUPLICATEGETTER = "Duplicate getter declaration";
	private static final String PREVGETTERHERE = "Previous getter declared here";
	private static final String DUPLICATESETTER = "Duplicate setter declaration";
	private static final String PREVSETTERHERE = "Previous setter declared here";
	private static final String PROPERTYOUTSIDECLASS = "Properties can only be declared as class members";
	private static final String INITIALWITHOUTSETTER = "Property without a setter cannot have an initial value";
	
	private Getter getter;
	private Setter setter;
	private Definition myDef;
	
	public PropertyBody() {
		getter = null;
		setter = null;
		myDef = null;
	}
	
	public void setFunction(Property_Function function) {
		if (function instanceof Getter) {
			if (getter != null) {
				function.getLocation().reportSemanticError(DUPLICATEGETTER);
				getter.getLocation().reportMixedWarning(PREVGETTERHERE);
			} else {
				getter = (Getter)function;
			}
		}
		if (function instanceof Setter) {
			if (setter != null) {
				function.getLocation().reportSemanticError(DUPLICATESETTER);
				setter.getLocation().reportMixedWarning(PREVSETTERHERE);
			} else {
				setter = (Setter)function;
			}
		}
	}
	
	public Getter getGetter() {
		return getter;
	}
	
	public Setter getSetter() {
		return setter;
	}
	
	public void check(CompilationTimeStamp timestamp, Definition definition) {
		myDef = definition;
		if (!(myDef.getMyScope().isClassScope())) {
			myDef.getLocation().reportSemanticError(PROPERTYOUTSIDECLASS);
			return;
		}
		final boolean isTemplate = myDef.getAssignmentType() == Assignment_type.A_VAR_TEMPLATE;
		IGovernedSimple initialValue = null;
		if (isTemplate) {
			initialValue = ((Def_Template)myDef).getTemplate(timestamp);
		} else {
			
		}
		
		if (getter != null) {
			getter.check(timestamp, myDef);
		}
		if (setter != null) {
			setter.check(timestamp, myDef);
		} else if (initialValue != null) {
			initialValue.getLocation().reportSemanticError(INITIALWITHOUTSETTER);
		}
	}

	@Override
	public boolean accept(ASTVisitor v) {
		if (getter != null && !getter.accept(v)) {
			return false;
		}
		if (setter != null && !setter.accept(v)) {
			return false;
		}
		return true;
	}
	
	public static PropertyFunctionContainer getInheritedPropertyFunction(CompilationTimeStamp timestamp, final Assignment def, boolean isGetter) {
		PropertyFunctionContainer container = new PropertyFunctionContainer();
		container.isAutomatic = false;
		container.function = null;
		
		Class_Type baseClass = def.getMyScope().getScopeClass().getBaseClass();
		while (container.function == null && baseClass != null) {
			final Assignment propertyDef = baseClass.getLocalAssignmentByID(timestamp, def.getIdentifier());
			if (propertyDef != null && propertyDef.isProperty()) {
				final PropertyBody inheritedBody = propertyDef.getPropertyBody();
				if (inheritedBody != null) {
					if (isGetter) {
						container.function = isGetter ? inheritedBody.getGetter() : inheritedBody.getSetter();
					}
				} else if (!propertyDef.isAbstract()) {
					container.isAutomatic = true;
					container.definition = propertyDef;
					return container;
				}
			}
			baseClass = baseClass.getBaseClass();
		}
		
		return container;
	}
}
