/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.statements;

import java.util.List;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.GovernedSimple.CodeSectionType;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * This class represents OOP extension 'select class' statement
 * 
 * @author Miklos Magyari
 * 
 * @see SelectClassCases
 * @see SelectClassCase
 *
 */
public class SelectClass_Statement extends Statement {
	private static final String CLASSEXPECTED = "Reference to a class object was expected";
	private static final String STATEMENT_NAME = "select-class";

	private final SelectClassCases selectClassCases;
	private final Reference reference;
	
	
	public SelectClass_Statement(final Reference reference, final SelectClassCases selectClassCases) {
		this.selectClassCases = selectClassCases;
		this.reference = reference;
		
		if (reference != null) {
			reference.setFullNameParent(this);
		}
		selectClassCases.setFullNameParent(this);
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		if (reference != null) {
			reference.setMyScope(scope);
		}
		selectClassCases.setMyScope(scope);
	}
	
	@Override
	public Statement_type getType() {
		return Statement_type.S_SELECT_CLASS;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyStatementBlock(final StatementBlock statementBlock, final int index) {
		super.setMyStatementBlock(statementBlock, index);
		selectClassCases.setMyStatementBlock(statementBlock, index);
	}
	
	@Override
	public String getStatementName() {
		return STATEMENT_NAME;
	}

	@Override
	public void check(CompilationTimeStamp timestamp) {
		final IType refType = reference.checkVariableReference(timestamp);
		Class_Type refClass = null;
		if (refType != null) {
			final IType refTypeLast = refType.getTypeRefdLast(timestamp);
			if (! (refTypeLast instanceof Class_Type)) {
				reference.getLocation().reportSemanticError(CLASSEXPECTED);
			} else {
				refClass = refTypeLast.getClassType();
			}
		}
		selectClassCases.check(timestamp, refClass);
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		if (reference != null) {
			reference.updateSyntax(reparser, false);
			reparser.updateLocation(reference.getLocation());
		}

		if (selectClassCases != null) {
			selectClassCases.updateSyntax(reparser, false);
		}
	}
	
	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (reference != null && !reference.accept(v)) {
			return false;
		}
		if (selectClassCases != null && !selectClassCases.accept(v)) {
			return false;
		}
		return true;
	}
	
	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (reference != null) {
			reference.findReferences(referenceFinder, foundIdentifiers);
		}
		if (selectClassCases != null) {
			selectClassCases.findReferences(referenceFinder, foundIdentifiers);
		}
	}
	
	@Override
	public void setCodeSection(CodeSectionType codeSection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void generateCode(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		
	}
}
