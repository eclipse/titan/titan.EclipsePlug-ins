/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * This class implements class property getters
 * 
 * @author Miklos Magyari
 *
 */
public class Getter extends Property_Function {
	private static final String GETTERNORETURN = "Property getter's statement block does not have a return statement";
	private static final String GETTERNORETURNMAYBE = "Control might leave the Property getter's statement block without reaching a return statement";
	
	private final boolean shortForm;
	private boolean isTemplate;
	private ITTCN3Template template;
	private IValue value;
	private StatementBlock statementBlock;
	
	public Getter(boolean isFinal, boolean isDeterministic, ITTCN3Template template) {
		super(false, isFinal, isDeterministic);
		shortForm = true;
		isTemplate = true;
		this.template = template;
		
		if (template == null) {
			// TODO: fatal
		}
	}
	
	public Getter(boolean isFinal, boolean isDeterministic, StatementBlock statementBlock) {
		super(false, isFinal, isDeterministic);
		shortForm = false;
		isTemplate = false;
		this.statementBlock = statementBlock;
	}
	
	public Getter(boolean isDeterministic) {
		super(true, false, isDeterministic);
		shortForm = false;
		isTemplate = false;
		statementBlock = null;
	}
	
	@Override
	public boolean accept(ASTVisitor v) {
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}
	
	public void check(final CompilationTimeStamp timestamp, Definition definition) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
		super.check(timestamp, definition);
		if (!isAbstract) {
			if (shortForm) {
				IType type = definition.getType(timestamp);
				template.setMyGovernor(type);
				template.setMyScope(this);
				isTemplate = definition.getAssignmentType() == Assignment_type.A_VAR_TEMPLATE;
				if (isTemplate) {
					type.checkThisTemplateRef(timestamp, template);
					// check generic ?
				} else {
					if (template.isValue(timestamp)) {
						IValue value = template.getValue();
						value.setMyGovernor(type);
						type.checkThisValueRef(timestamp, value);
						// check value
						this.value = value;
					}
				}
			} else {
				statementBlock.setParentScope(this);
				statementBlock.check(timestamp);
				switch (statementBlock.hasReturn(timestamp)) {
				case RS_NO:
					statementBlock.getLocation().reportSemanticError(GETTERNORETURN);
					break;
				case RS_MAYBE:
					statementBlock.getLocation().reportSemanticError(GETTERNORETURNMAYBE);
					break;
				default:
					break;
				}
			}
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean isInGetterScope() {
		return true;
	}
	
	@Override
	/** {@inheritDoc} */
	public Getter getScopeGetter() {
		return this;
	}
}
