/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.text.MessageFormat;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.Value;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter.parameterEvaluationType;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * This class represents the class property definitions
 *  
 * @author Miklos Magyari
 *
 */
public class Def_Property extends Def_Var {
	private static final String CANNOTMODIFYPROPERTY = "Cannot modify property `{0}'' because it doesn't have a setter";
	
	private PropertyBody body;
	
	public Def_Property(Identifier identifier, Type type, Value initialValue, parameterEvaluationType evaluationType,
			PropertyBody body) {
		super(identifier, type, initialValue, evaluationType);
		this.body = body;
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean isProperty() {
		return true;
	}
	
	@Override
	/** {@inheritDoc} */
	public String getAssignmentName() {
		return "property";
	}
	
	@Override
	/** {@inheritDoc} */
	public String getOutlineIcon() {
		return "variable.gif";
	}
	
	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);

		return type;
	}

	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}
	
	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp, final IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
		super.check(timestamp, refChain);
		if (body != null) {
			body.check(timestamp, this);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (type != null && !type.accept(v)) {
			return false;
		}
		if (body != null && !body.accept(v)) {
			return false;
		}
		return true;
	}
	
	public void useAsLvalue(final Location location) {
		if (body != null && body.getSetter() == null) {
			location.reportSemanticError(MessageFormat.format(CANNOTMODIFYPROPERTY, identifier.getDisplayName()));
		}
	}
	
	@Override
	public PropertyBody getPropertyBody() {
		return body;
	}
	
	@Override
	public boolean isAbstract() {
		return isAbstract();
	}
	
	@Override
	public boolean isFinal() {
		return isFinal();
	}
}
