/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.templates;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.NULL_Location;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.statements.Return_Statement;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Represents a template for implication matching.
 *
 * @author Adam Knapp
 * @author Miklos Magyari
 * */
public class DynamicMatch_template extends TTCN3Template {
	private static final String MISSINGRETURN = "The dynamic template's statement block does not have a return statement";
	private static final String LEAVEWITHOUTRETURN = 
			"Control might leave the dynamic template's statement block without reaching a return statement";

	/** function reference used in defining the dynamic template, not owned */
	private Reference reference;
	/** statement block of the dynamic template
     * (if the template is defined using a function reference instead of a statement block,
     * then a new statement block is generated with a return statement containing the function reference) */
	private StatementBlock statementBlock;
	/** Pointer to the formal parameter list if this template is part of
     * a parameterized template. It is {@code null} otherwise. */
	private FormalParameterList formalParameterList;

	public DynamicMatch_template(final Reference reference) {
		// '@dynamic F' is equivalent to '@dynamic { return F(value); }'
		// the parser has already added the function parameter 'value',
	    // the rest of the statement block is constructed here
		this.reference = reference;
		statementBlock = new StatementBlock();
		statementBlock.setDynamicTemplate(this);
		statementBlock.addStatement(new Return_Statement(
				new SpecificValue_Template(new Referenced_Value(reference))));

		if (reference != null) {
			reference.setFullNameParent(this);
		}
	}

	public DynamicMatch_template(final StatementBlock block) {
		statementBlock = block;
		statementBlock.setDynamicTemplate(this);
		this.setMyScope(block);
	}

	@Override
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append("@dynamic ");
		if (reference != null) {
			builder.append(reference.getDisplayName());
		} else {
			builder.append("template through statement block");
		}
		return builder.toString();
	}

	@Override
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		if (reference == null) {
			return false;
		}

		return reference.getRefdAssignment(timestamp, false) == lhs;
	}

	@Override
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		// TODO Auto-generated method stub

	}

	@Override
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		final TTCN3Template temp = getTemplateReferencedLast(timestamp);
		if (temp != this && !temp.getIsErroneous(timestamp)) {
			temp.checkSpecificValue(timestamp, allowOmit);
		}
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {

		if (getIsErroneous(timestamp)) {
			return false;
		}

		final Type myType = (Type)statementBlock.getMyDefinition().getType(timestamp);
		final FormalParameter valueParam = 
				new FormalParameter(null, Assignment_type.A_PAR_VAL_IN, myType, new Identifier(Identifier_type.ID_TTCN, "value"), null, null);
		final List<FormalParameter> paramList = new ArrayList<FormalParameter>();
		paramList.add(valueParam);
		final FormalParameterList fpList = new FormalParameterList(paramList);
		fpList.setFullNameParent(this);
		fpList.setMyScope(this.getMyScope());
		setDynamicFormalParameterList(fpList);
		statementBlock.setValueParamList(fpList);
		statementBlock.getValueParamList().check(timestamp, null);
		statementBlock.check(timestamp);

		switch (statementBlock.hasReturn(timestamp)) {
	    case RS_NO:
	    	location.reportSemanticError(MISSINGRETURN);
	    	break;
	    case RS_MAYBE:
	    	location.reportSemanticError(LEAVEWITHOUTRETURN);
	    	break;
	    default:
	    	break;
	    }

		checkLengthRestriction(timestamp, type);

		if (!allowOmit && isIfpresent) {
			if (location != null && !(location instanceof NULL_Location)) {
				location.reportSemanticError("`ifpresent' is not allowed here");
			}
		}

		if (reference == null) {
			return false;
		}

		final Assignment assignment = reference.getRefdAssignment(timestamp, true);
		if (assignment == null) {
			return false;
		}

		boolean selfReference = lhs == assignment;
		assignment.check(timestamp);

		IType governor = assignment.getType(timestamp);
		if (governor != null) {
			governor = governor.getFieldType(timestamp, reference, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false);
		}
		if (governor == null) {
			setIsErroneous(true);
		}

		return selfReference;
	}

	@Override
	public void generateCodeInit(final JavaGenData aData, final StringBuilder source, final String name) {
		if (lastTimeBuilt != null && !lastTimeBuilt.isLess(aData.getBuildTimstamp())) {
			return;
		}
		lastTimeBuilt = aData.getBuildTimstamp();

		final String classId = aData.getTemporaryVariableName();
		final String typeValue = myGovernor.getGenNameValue(aData, source);

		// TODO
	}

	/**
	 * Returns the formal parameter of the dynamic match template
	 * @return formal parameter of the dynamic match template
	 */
	public FormalParameter getDynamicFormalParameter() {
		return formalParameterList.getParameterByIndex(0);
	}

	/**
	 * Returns the formal parameter list of the dynamic match template
	 * @return formal parameter list of the dynamic match template
	 */
	public FormalParameterList getDynamicFormalParameterList() {
		return formalParameterList;
	}

	/**
	 * Returns the statement block of the dynamic match template
	 * @return statement block of the dynamic match template
	 */
	public StatementBlock getDynamicStatementBlock() {
		return statementBlock;
	}

	@Override
	/** {@inheritDoc} */
	public Template_type getTemplatetype() {
		return Template_type.DYNAMIC_MATCH;
	}

	@Override
	/** {@inheritDoc} */
	public String getTemplateTypeName() {
		if (isErroneous) {
			return "erroneous dynamic match";
		}

		return "dynamic match";
	}

	/**
	 * Returns the reference attribute of the dynamic match template
	 * @return reference of the dynamic match template
	 */
	public Reference getReference() {
		return reference;
	}

	/**
	 * Returns whether the reference attribute of the dynamic match template is set or not
	 * @return {@code true} if the reference attribute of
	 * 						the dynamic match template is not {@code null}, {@code false} otherwise
	 */
	public boolean hasReference() {
		return reference != null;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasSingleExpression() {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public void setCodeSection(final CodeSectionType codeSection) {
		super.setCodeSection(codeSection);
		statementBlock.setCodeSection(codeSection);
	}

	/**
	 * Sets the formalParameterList attribute of the dynamic match template
	 * @param dynamicFormalParameterList formal parameter list for the dynamic match template 
	 */
	public void setDynamicFormalParameterList(final FormalParameterList dynamicFormalParameterList) {
		if (dynamicFormalParameterList == null) {
			ErrorReporter.INTERNAL_ERROR("DynamicMatch_template.setDynamicFormalParameterList(FormalParameterList dynamicFormalParameterList)");
		}
		this.formalParameterList = dynamicFormalParameterList;
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(final Definition definition) {
		statementBlock.setMyDefinition(definition);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		statementBlock.setMyScope(scope);
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean accept(ASTVisitor v) {
		if (lengthRestriction != null && !lengthRestriction.accept(v)) {
			return false;
		}
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}
}
