package org.eclipse.titan.designer.AST.TTCN3.values.expressions;

import java.text.MessageFormat;

import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.values.Expression_Value;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/** 
 * Represents TTCN3 class casting expressions (OOP extension)
 * 
 * @author Miklos Magyari
 *
 */
public class ClassCastingExpression extends Expression_Value {
	private static final String UNNECESSARYCAST = "Unnecessary cast: class is cast to itself";
	private static final String CLASSREFERENCEEXPECTED = "Reference to a class object was expected";
	private static final String CLASSTYPEEXPECTED = "Class type was expected";
	private static final String CANNOTCAST = "Cannot cast an object of class type `{0}'' to class type `{1}''";
	
	private final Reference rightReference;
	private Type type;
	private Reference leftReference;
	
	public ClassCastingExpression(final Type type, final Reference reference) {
		this.type = type;
		this.rightReference = reference;
		
		leftReference = null;
		
		if (this.type != null) {
			this.type .setFullNameParent(this);
		}
		if (this.rightReference != null) {
			this.rightReference.setFullNameParent(this);
		}
	}
	
	public ClassCastingExpression(final Reference leftReference, final Reference reference) {
		this.leftReference = leftReference;
		this.rightReference = reference;
		
		type = null;
		
		if (this.leftReference != null) {
			this.leftReference.setFullNameParent(this);
		}
		if (this.rightReference != null) {
			this.rightReference.setFullNameParent(this);
		}
	}
	
	@Override
	public Operation_type getOperationType() {
		return Operation_type.CLASS_CASTING_OPERATION;
	}

	@Override
	public Type_type getExpressionReturntype(CompilationTimeStamp timestamp, Expected_Value_type expectedValue) {
		return Type_type.TYPE_CLASS;
	}
	
	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		final IType governor = super.getMyGovernor();

		if (governor != null) {
			return governor;
		}
		
		return new Class_Type();
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);

		if (leftReference != null) {
			leftReference.setMyScope(scope);
		}
		if (rightReference != null) {
			rightReference.setMyScope(scope);
		}
		if (type != null) {
			type.setMyScope(scope);
		}
	}
	
	@Override
	public IValue evaluateValue(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return lastValue;
		}
		
		isErroneous = false;
		lastTimeChecked = timestamp;
		lastValue = this;
		
		if ((leftReference == null && rightReference == null) || (type == null && rightReference == null)) {
			setIsErroneous(true);
			return lastValue;
		}
		
		checkExpressionOperands(timestamp, expectedValue, referenceChain);
		
		if (getIsErroneous(timestamp)) {
			return lastValue;
		}
		
		if (!isUnfoldable(timestamp)) {
			if (type != null) {
				setMyGovernor(type.getTypeRefdLast(timestamp));
			}
			
			// this differs from titan core as we still have the reference
			// and it is not converted to a type
			if (leftReference != null) {
				final IType leftRefType = leftReference.checkVariableReference(timestamp);
				if (leftRefType != null) {
					setMyGovernor(leftRefType.getTypeRefdLast(timestamp));
				}
			}
	    }
				
		return lastValue;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		if (leftReference != null) {
			leftReference.updateSyntax(reparser, false);
			reparser.updateLocation(leftReference.getLocation());
		}
		if (rightReference != null) {
			rightReference.updateSyntax(reparser, false);
			reparser.updateLocation(rightReference.getLocation());
		}
		if (type != null) {
			type.updateSyntax(reparser, false);
			reparser.updateLocation(type.getLocation());
		}
	}

	@Override
	public boolean isUnfoldable(CompilationTimeStamp timestamp, Expected_Value_type expectedValue,
			IReferenceChain referenceChain) {
		if (type != null) {
			return type.isIdentical(timestamp, rightReference.checkVariableReference(timestamp));
		}
		
		// this differs from titan core as we still have the reference
		// and it is not converted to a type
		if (leftReference != null) {
			final IType leftRefType = leftReference.checkVariableReference(timestamp);
			if (leftRefType != null) {
				return leftRefType.isIdentical(timestamp, rightReference.checkVariableReference(timestamp));
			}
		}
		return false;
	}

	@Override
	public String createStringRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (leftReference != null && !leftReference.accept(v)) {
			return false;
		}
		if (rightReference != null && !rightReference.accept(v)) {
			return false;
		}
		return true;
	}
	
	private void checkExpressionOperands(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue,
			final IReferenceChain referenceChain) {
		IType typeRefdLast = null;
		final IType refType = rightReference.checkVariableReference(timestamp);
		if (refType != null) {
			typeRefdLast = refType.getTypeRefdLast(timestamp);
			if (typeRefdLast.getTypetype() != Type_type.TYPE_CLASS) {
				rightReference.getLocation().reportSemanticError(CLASSREFERENCEEXPECTED);
				setIsErroneous(true);
			}
		} else {
			setIsErroneous(true);
		}

		if (type != null) {
			type.check(timestamp);
			final IType typeLast = type.getTypeRefdLast(timestamp);
			if (typeLast.getTypetype() != Type_type.TYPE_CLASS) {
				type.getLocation().reportSemanticError(CLASSTYPEEXPECTED);
				setIsErroneous(true);
			} else if (! getIsErroneous(timestamp)) {
				final Class_Type newClass = typeLast.getClassType();
				final Class_Type oldClass = typeRefdLast.getClassType();
				if (!newClass.isParentClass(timestamp, oldClass) && !oldClass.isParentClass(timestamp, newClass)) {
					getLocation().reportSemanticError(MessageFormat.format(CANNOTCAST, typeRefdLast.getTypename(), type.getTypename()));
				}
			}
		}
		
		 // We differ from titan core here as we do not convert left reference to type
		if (leftReference != null) {
			if (leftReference.checkVariableReference(timestamp) == null) {
				setIsErroneous(true);
			}
		}
	}
}
