/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
 */
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.designer.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;


/**
 * This class represents a template variable (dynamic template) definition.
 * 
 * @author Miklos Magyari
 * 
 */
public class Def_Property_Template extends Def_Var_Template {
	private PropertyBody body;
	
	public Def_Property_Template(final Identifier identifier, final Type type, TTCN3Template initialValue,
			Restriction_type restriction, PropertyBody body) {
		super(restriction, identifier, type, null, initialValue); 
		this.body = body;
	}
	
	public boolean isAutomatic() {
		return body == null;
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean isProperty() {
		return true;
	}
	
	@Override
	public String getOutlineIcon() {
		return "template_dynamic.gif";
	}

	@Override
	public String getProposalKind() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub

	}

	@Override
	public Assignment_type getAssignmentType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAssignmentName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void check(CompilationTimeStamp timestamp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void check(CompilationTimeStamp timestamp, IReferenceChain refChain) {
		// TODO Auto-generated method stub

	}

	@Override
	public void generateCode(JavaGenData aData, boolean cleanUp) {
		// TODO Auto-generated method stub
	}

	@Override
	public PropertyBody getPropertyBody() {
		return body;
	}

	@Override
	public boolean isAbstract() {
		return isAbstract();
	}
	
	@Override
	public boolean isFinal() {
		return isFinal();
	}
}
