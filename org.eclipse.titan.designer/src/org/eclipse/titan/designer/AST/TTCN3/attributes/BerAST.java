/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.attributes;

/**
 * Represents the BER encoding related setting extracted from variant attributes.
 *
 * @author Farkas Izabella Ingrid
 * */
public class BerAST {
	public enum ber_encode_t {
		CER,
		DER
	}

	public enum ber_decode_t {
		LENGTH_ACCEPT_SHORT,
		LENGTH_ACCEPT_LONG,
		LENGTH_ACCEPT_INDEFINITE,
		LENGTH_ACCEPT_DEFINITE,
		ACCEPT_ALL
	}

	public ber_encode_t encode_param;
	public ber_decode_t decode_param;

	public BerAST(BerAST berAttribute) {
		encode_param = berAttribute.encode_param;
		decode_param = berAttribute.decode_param;
	}

	public BerAST() {
		encode_param = ber_encode_t.DER;
		decode_param = ber_decode_t.ACCEPT_ALL;
	}


	public String get_encode_str() {
		switch (encode_param) {
		case CER:
			return "BER_ENCODE_CER";
		case DER:
			return "BER_ENCODE_DER";
		default:
			throw new IllegalArgumentException("Unexpected value: " + encode_param);
		}
	}

	public String get_decode_str() {
		switch (decode_param) {
		case LENGTH_ACCEPT_SHORT:
			return "BER_ACCEPT_SHORT";
		case LENGTH_ACCEPT_LONG:
			return "BER_ACCEPT_LONG";
		case LENGTH_ACCEPT_INDEFINITE:
			return "BER_ACCEPT_INDEFINITE";
		case LENGTH_ACCEPT_DEFINITE:
			return "BER_ACCEPT_DEFINITE";
		case ACCEPT_ALL:
			return "BER_ACCEPT_ALL";
		default:
			throw new IllegalArgumentException("Unexpected value: " + decode_param);
		}
	}
}
