/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.NULL_Location;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.designer.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock.ReturnStatus_type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

public class Setter extends Property_Function {
	private static final String SETTERCANNOTHAVERETURN = "Property setter's statement block cannot have a return statement";
	private final boolean shortForm;
	private Assignment_Statement assignment;
	private StatementBlock statementBlock;
	private FormalParameterList formalParameterList;
	
	public Setter(final boolean isFinal, final boolean isDeterministic, final Assignment_Statement assignment) {
		super(false, isFinal, isDeterministic);
		shortForm = true;
		this.assignment = assignment;
		formalParameterList = null;
	}

	public Setter(final boolean isFinal, final boolean isDeterministic, final StatementBlock statementBlock) {
		super(false, isFinal, isDeterministic);
		shortForm = false;
		this.statementBlock = statementBlock;
		formalParameterList = null;
	}
	
	public Setter(final boolean isDeterministic) {
		super(true, false, isDeterministic);
		shortForm = false;
		statementBlock = null;
		formalParameterList = null;
	}
	
	@Override
	public boolean accept(ASTVisitor v) {
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		if (statementBlock != null && !statementBlock.accept(v)) {
			return false;
		}
		return true;
	}
	
	public FormalParameter getFormalParameter() {
		if (formalParameterList == null) {
			// TODO: fatal
		}
		return formalParameterList.getParameterByIndex(0);
	}
	
	public void check(final CompilationTimeStamp timestamp, Definition definition) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
		super.check(timestamp, definition);
		final Identifier identifier = new Identifier(Identifier_type.ID_TTCN, "value");
		identifier.setLocation(NULL_Location.INSTANCE);
		FormalParameter fp = new FormalParameter(Restriction_type.TR_NONE, Assignment_type.A_PAR_VAL_IN, (Type)definition.getType(timestamp),
				identifier, null, null);
		fp.setLocation(NULL_Location.INSTANCE);
		fp.setMyScope(this);
		List<FormalParameter> fplist = new ArrayList<>();
		fplist.add(fp);
		formalParameterList = new FormalParameterList(fplist);
		//formalParameterList.setFullNameParent(getFullName() + ".<formalparlist>");
		formalParameterList.setMyScope(this);
		fp.check(timestamp);
		if (!isAbstract) {
			if (shortForm) {
				assignment.setMyScope(this);
				assignment.check(timestamp);
			} else {
				statementBlock.setParentScope(this);
				statementBlock.check(timestamp);
				if (statementBlock.hasReturn(timestamp) != ReturnStatus_type.RS_NO) {
					statementBlock.getLocation().reportSemanticError(SETTERCANNOTHAVERETURN);
				}
			}
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public boolean isInSetterScope() {
		return true;
	}
		
	@Override
	/** {@inheritDoc} */
	public Setter getScopeSetter() {
		return this;
	}
}
