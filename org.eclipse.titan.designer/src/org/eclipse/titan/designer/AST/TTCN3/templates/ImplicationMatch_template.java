/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.templates;

import java.text.MessageFormat;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.TemplateRestriction.Restriction_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.values.expressions.ExpressionStruct;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Represents a template for implication matching.
 *
 * @author Adam Knapp
 * */
public class ImplicationMatch_template extends TTCN3Template {

	private TemplateInstance precondition;
	private TemplateInstance implied_template;

	public ImplicationMatch_template(final TTCN3Template precondition, final TemplateInstance impliedTemplate) {
		// Creating a TemplateInstance with just a template body for now.
	    // The parser adds the precondition's type indicator and derived reference (if it has either)
	    // to the TemplateInstance containing this template. They will be moved during semantic analysis.
		this.precondition = new TemplateInstance(null, null, precondition);
		this.precondition.setLocation(precondition.getLocation());
		this.implied_template = impliedTemplate;
		this.precondition.setFullNameParent(this);
		this.implied_template.setFullNameParent(this);
	}

	@Override
	public boolean checkExpressionSelfReferenceTemplate(final CompilationTimeStamp timestamp, final Assignment lhs) {
		if (precondition != null && precondition.getTemplateBody().checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
			return true;
		}
		if (implied_template != null && implied_template.getTemplateBody().checkExpressionSelfReferenceTemplate(timestamp, lhs)) {
			return true;
		}

		return false;
	}

	@Override
	public void checkRecursions(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		if (precondition != null) {
			precondition.checkRecursions(timestamp, referenceChain);
		}
		if (implied_template != null) {
			implied_template.checkRecursions(timestamp, referenceChain);
		}
	}

	@Override
	public void checkSpecificValue(final CompilationTimeStamp timestamp, final boolean allowOmit) {
		getLocation().reportSemanticError("A specific value expected instead of an implication match");
	}

	@Override
	/** {@inheritDoc} */
	public boolean checkThisTemplateGeneric(final CompilationTimeStamp timestamp, final IType type, final boolean isModified,
			final boolean allowOmit, final boolean allowAnyOrOmit, final boolean subCheck, final boolean implicitOmit, final Assignment lhs) {

		precondition.check(timestamp, type);
		implied_template.check(timestamp, type);
		checkLengthRestriction(timestamp, type);

		return false;
	}

	@Override
	public String createStringRepresentation() {
		final StringBuilder builder = new StringBuilder();
		builder.append(precondition.createStringRepresentation())
			.append(" implies ")
			.append(implied_template.createStringRepresentation());
		return builder.toString();
	}

	@Override
	public void generateCodeInit(final JavaGenData aData, final StringBuilder source, final String name) {
		if (lastTimeBuilt != null && !lastTimeBuilt.isLess(aData.getBuildTimstamp())) {
			return;
		}
		lastTimeBuilt = aData.getBuildTimstamp();

		final String preconditionId = aData.getTemporaryVariableName();
		final String impliedTemplateId = aData.getTemporaryVariableName();
		final String typeName = myGovernor.getGenNameTemplate(aData, source);

	    final ExpressionStruct preconditionExpression = new ExpressionStruct();
	    precondition.generateCode(aData, preconditionExpression, Restriction_type.TR_NONE);
	    if (preconditionExpression.preamble.length() > 0) {
	    	source.append(preconditionExpression.preamble);
	    }
	    source.append(MessageFormat.format("final {0} {1} = new {2}({3});\n", typeName, preconditionId, typeName, preconditionExpression.expression));
	    if (preconditionExpression.postamble.length() > 0) {
	    	source.append(preconditionExpression.postamble);
	    }

	    final ExpressionStruct impliedExpression = new ExpressionStruct();
	    implied_template.generateCode(aData, impliedExpression, Restriction_type.TR_NONE);
	    if (impliedExpression.preamble.length() > 0) {
	    	source.append(impliedExpression.preamble);
	    }
	    source.append(MessageFormat.format("final {0} {1} = new {2}({3});\n", typeName, impliedTemplateId, typeName, impliedExpression.expression));
	    if (impliedExpression.postamble.length() > 0) {
	    	source.append(impliedExpression.postamble);
	    }

	    source.append(MessageFormat.format("{0} = new {1}({2}, {3});\n", name, typeName, preconditionId, impliedTemplateId));
	    
	    if (lengthRestriction != null) {
			if (getCodeSection() == CodeSectionType.CS_POST_INIT) {
				lengthRestriction.reArrangeInitCode(aData, source, myScope.getModuleScopeGen());
			}
			lengthRestriction.generateCodeInit(aData, source, name);
		}
	    
	    if (isIfpresent) {
			source.append(name);
			source.append(".set_ifPresent();\n");
		}
	}

	@Override
	/** {@inheritDoc} */
	public IType getExpressionGovernor(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (myGovernor != null) {
			return myGovernor;
		}

		final IType type = precondition.getExpressionGovernor(timestamp, expectedValue);
		if (type != null) {
			return type;
		}

		return implied_template.getExpressionGovernor(timestamp, expectedValue);
	}

	@Override
	/** {@inheritDoc} */
	public Type_type getExpressionReturntype(final CompilationTimeStamp timestamp, final Expected_Value_type expectedValue) {
		if (getIsErroneous(timestamp)) {
			return Type_type.TYPE_UNDEFINED;
		}

		final Type_type type = precondition.getExpressionReturntype(timestamp, expectedValue);
		if (!Type_type.TYPE_UNDEFINED.equals(type)) {
			return type;
		}

		return implied_template.getExpressionReturntype(timestamp, expectedValue);
	}

	/**
	 * Returns the implied template instance
	 * @return implied template instance
	 */
	public TemplateInstance getImpliedTemplate() {
		return implied_template;
	}

	/**
	 * Returns the precondition template
	 * @return precondition template
	 */
	public TemplateInstance getPrecondition() {
		return precondition;
	}

	@Override
	public Template_type getTemplatetype() {
		return Template_type.IMPLICATION_MATCH;
	}

	@Override
	public String getTemplateTypeName() {
		if (isErroneous) {
			return "erroneous implication match";
		}

		return "implication match";
	}

	@Override
	/** {@inheritDoc} */
	public boolean needsTemporaryReference() {
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean hasSingleExpression() {
		return false;
	}

	@Override
	/** {@inheritDoc} */
	public void setCodeSection(final CodeSectionType codeSection) {
		super.setCodeSection(codeSection);
		precondition.setCodeSection(codeSection);
		implied_template.setCodeSection(codeSection);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyDefinition(Definition definition) {
		precondition.getTemplateBody().setMyDefinition(definition);
		implied_template.getTemplateBody().setMyDefinition(definition);
	}

	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		super.setMyScope(scope);
		precondition.setMyScope(scope);
		implied_template.setMyScope(scope);
	}
}
