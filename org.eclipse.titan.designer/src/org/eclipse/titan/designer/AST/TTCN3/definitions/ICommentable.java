/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.ui.IEditorPart;

/**
 * This interface represents the language element that can have document comment
 * (information, source code, etc.)
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public interface ICommentable {

	public static final String COMMENT_START = "/**\n";
	public static final String COMMENT_END = " */\n";
	public static final String LINE_START = " * ";
	public static final String AT = "@";
	public static final String AUTHOR_TAG = "@author";
	public static final String CONFIG_TAG = "@config";
	public static final String DESC_TAG = "@desc";
	public static final String EXCEPTION_TAG = "@exception";
	public static final String MEMBER_TAG = "@member";
	public static final String PARAM_TAG = "@param";
	public static final String PRIORITY_TAG = "@priority";
	public static final String PURPOSE_TAG = "@purpose";
	public static final String REFERENCE_TAG = "@reference";
	public static final String REMARK_TAG = "@remark";
	public static final String REQUIREMENT_TAG = "@requirement";
	public static final String RETURN_TAG = "@return";
	public static final String SEE_TAG = "@see";
	public static final String SINCE_TAG = "@since";
	public static final String STATUS_TAG = "@status";
	public static final String URL_TAG = "@url";
	public static final String VERDICT_TAG = "@verdict";
	public static final String VERSION_TAG = "@version";
	
	// non-standard tags
	public static final String CATEGORY_TAG = "@category";

	public static final String[] DOC_COMMENT_TAGS = { ICommentable.AUTHOR_TAG, ICommentable.CONFIG_TAG, ICommentable.DESC_TAG,
			ICommentable.EXCEPTION_TAG, ICommentable.MEMBER_TAG, ICommentable.PARAM_TAG, ICommentable.PRIORITY_TAG,
			ICommentable.PURPOSE_TAG, ICommentable.REFERENCE_TAG, ICommentable.REMARK_TAG,
			ICommentable.REQUIREMENT_TAG, ICommentable.RETURN_TAG, ICommentable.SEE_TAG, ICommentable.SINCE_TAG,
			ICommentable.STATUS_TAG, ICommentable.URL_TAG, ICommentable.VERDICT_TAG, ICommentable.VERSION_TAG,
			// non-standard tags
			ICommentable.CATEGORY_TAG };

	/**
	 * Returns the object containing the documentation comment
	 * @return the object containing the documentation comment
	 */
	public DocumentComment getDocumentComment();

	/**
	 * Returns whether the documentation comment exists for this node
	 * @return {@code true} if the documentation comment is available, {@code false} otherwise
	 */
	public boolean hasDocumentComment();

	/**
	 * Parses the documentation comment and returns the documentation comment object containing the comments by tags
	 * @return the documentation comment object containing the comments by tags 
	 */
	public DocumentComment parseDocumentComment();

	/**
	 * Sets the documentation comment for this definition (ETSI ES 201 873-10 format)
	 * @param docComment the unparsed documentation comment object
	 */
	public void setDocumentComment(DocumentComment docComment);

	/** 
	 * Gets information for the given node that could be displayed in a source editor hover
	 * @param editor 
	 * @return
	 */
	Ttcn3HoverContent getHoverContent(IEditorPart editor);

	/**
	 * Generates the documentation comment skeleton for this node based on AST information
	 */
	public String generateDocComment(String indentation);
}
