/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.List;

import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.ILocateableNode;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Base class for class property getters and setters
 * 
 * @author Miklos Magyari
 *
 */
public abstract class Property_Function extends Scope implements ILocateableNode {
	private static final String CANNOTHAVEABSTRACT = "Concrete class type `{0}'' cannot have abstract {1}";
	private static final String CANNOTINHERITFINAL = "Cannot inherit {0} with the @final modifier";
	
	private Location location;
	private Location modifierLocation;
	private Location visibilityLocation;
	protected CompilationTimeStamp lastTimeChecked;

	protected boolean isAbstract;
	protected boolean isFinal;
	protected boolean isDeterministic;
	private VisibilityModifier visibility;
	protected Definition myDef;

	public Property_Function(boolean isAbstract, boolean isFinal, boolean isDeterministic) {
		this.isAbstract = isAbstract;
		this.isFinal = isFinal;
		this.isDeterministic = isDeterministic;
	}
	
	@Override
	public Assignment getAssBySRef(CompilationTimeStamp timestamp, Reference reference) {
		return parentScope.getAssBySRef(timestamp, reference);
	}
	
	@Override
	public Assignment getAssBySRef(CompilationTimeStamp timestamp, Reference reference,
			IReferenceChain referenceChain) {
		return parentScope.getAssBySRef(timestamp, reference, referenceChain);
	}
	
	public void check(CompilationTimeStamp timestamp, Definition definition) {
		myDef = definition;
		parentScope = myDef.getMyScope();
		if (visibility == VisibilityModifier.Friend) {
			visibility = myDef.getVisibility();
		}
		
		final String functionName = isInGetterScope() ? "getter" : "setter";
		Class_Type parentClass = parentScope.getScopeClass();
		if (parentClass == null) {
			ErrorReporter.INTERNAL_ERROR("Property_Function::check()");
		}
		if (isAbstract && !parentClass.isAbstract() && !parentClass.isTrait()) {
			location.reportSemanticError(
			MessageFormat.format(CANNOTHAVEABSTRACT, parentClass.getTypename(), functionName));
		}
		
		final PropertyFunctionContainer container = PropertyBody.getInheritedPropertyFunction(timestamp, myDef, isInGetterScope());
		if (container.isAutomatic && container.function != null && container.function.isFinal) {
			location.reportSemanticError(
				MessageFormat.format(CANNOTINHERITFINAL, functionName));
		} else if (container.isAutomatic && container.definition.isFinal()) {
			location.reportSemanticError(
					MessageFormat.format(CANNOTINHERITFINAL, functionName));
		}
	}

	public Definition getMyDefinition() {
		return myDef;
	}
	
	public void setModifierLocation(Location location) {
		modifierLocation = location;
	}
	
	public Location getModifierLocation() {
		return modifierLocation;
	}

	@Override
	public Assignment getEnclosingAssignment(int offset) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findReferences(ReferenceFinder referenceFinder, List<Hit> foundIdentifiers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public Location getLocation() {
		return location;
	}

	public VisibilityModifier getVisibility() {
		return visibility;
	}

	public void setVisibility(VisibilityModifier visibility) {
		this.visibility = visibility;
	}

	public Location getVisibilityLocation() {
		return visibilityLocation;
	}

	public void setVisibilityLocation(Location visibilityLocation) {
		this.visibilityLocation = visibilityLocation;
	}
	
	public Scope getFinalScope(CompilationTimeStamp timestamp) {
		final Class_Type classType = getScopeClass();
		Scope currentScope = this;
		final Module currentMod = getModuleScope();
		if (currentMod == null) {
			ErrorReporter.INTERNAL_ERROR("PropertyFunction::getFinalScope()");
		}
		if (classType.getSystemType(timestamp) != null) {
			final Component_Type component = currentMod.getMtcSystemComponentType(timestamp, true);
			if (component != null) {
				final Scope systemScope = component.getMyScope();
				systemScope.setParentScope(currentScope);
				currentScope = systemScope;
			}
		}
		if (classType.getMtcType(timestamp) != null) {
			final Component_Type component = currentMod.getMtcSystemComponentType(timestamp, false);
			if (component != null) {
				final Scope mtcScope = component.getMyScope();
				mtcScope.setParentScope(currentScope);
				currentScope = mtcScope;
			}
		}
		if (classType.getRunsOnType(timestamp) != null) {
			final Scope runsOnScope = currentMod.getScopeRunsOn();
			runsOnScope.setParentScope(currentScope);
			currentScope = runsOnScope;
		}
		
		return currentScope;
	}
}
