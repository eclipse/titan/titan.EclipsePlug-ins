/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.statements;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.designer.AST.ASTNode;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.TTCN3.IIncrementallyUpdateable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock.ReturnStatus_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * The SelectCases class is helper class for the SelectClassCase_Statement class.
 * Holds a list of the select cases that were parsed from the source code.
 * 
 * @author Miklos Magyari
 *
 */
public class SelectClassCases extends ASTNode implements IIncrementallyUpdateable {
	private final List<SelectClassCase> select_class_cases;
	
	public SelectClassCases() {
		select_class_cases = new ArrayList<SelectClassCase>();
	}
	
	public void addSelectClassCase(final SelectClassCase selectCase) {
		synchronized(select_class_cases) {
			select_class_cases.add(selectCase);
		}
		selectCase.setFullNameParent(this);
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		for (final SelectClassCase select_class_case : select_class_cases) {
			select_class_case.setMyScope(scope);
		}
	}
	
	public void setMyStatementBlock(final StatementBlock statementBlock, final int index) {
		for (final SelectClassCase scc : select_class_cases) {
			scc.setMyStatementBlock(statementBlock, index);
		}
	}
	
	public void setMyDefinition(final Definition definition) {
		for (final SelectClassCase scc : select_class_cases) {
			scc.setMyDefinition(definition);
		}
	}
	
	public void setMyAltguards(final AltGuards altGuards) {
		for (final SelectClassCase scc : select_class_cases) {
			scc.setMyAltguards(altGuards);
		}
	}
	
	/**
	 * Used to tell break and continue statements if they are located with an altstep, a loop or none.
	 *
	 * @param pAltGuards the altguards set only within altguards
	 * @param pLoopStmt the loop statement, set only within loops.
	 * */
	public void setMyLaicStmt(final AltGuards pAltGuards, final Statement pLoopStmt) {
		for (final SelectClassCase scc : select_class_cases) {
			scc.getStatementBlock().setMyLaicStmt(pAltGuards, pLoopStmt);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		if (select_class_cases == null) {
			return;
		}

		for (final SelectClassCase sc : select_class_cases) {
			sc.findReferences(referenceFinder, foundIdentifiers);
		}
	}
	
	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		for (final SelectClassCase sc : select_class_cases) {
			sc.updateSyntax(reparser, false);
			reparser.updateLocation(sc.getLocation());
		}
	}

	@Override
	protected boolean memberAccept(ASTVisitor v) {
		if (select_class_cases != null) {
			for (final SelectClassCase sc : select_class_cases) {
				if (!sc.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}

	public void check(final CompilationTimeStamp timestamp, final Class_Type refClass) {
		boolean unrechable = false;
		for (final SelectClassCase sc: select_class_cases) {
			unrechable = sc.check(timestamp, refClass, unrechable);
		}
	}
	
	public void postCheck() {
		for (final SelectClassCase sc: select_class_cases) {
			sc.postCheck();
		}
	}
	
	/**
	 * Checks whether the select cases have a return statement, either
	 * directly or embedded.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 *
	 * @return the return status of the select cases.
	 * */
	public StatementBlock.ReturnStatus_type hasReturn(final CompilationTimeStamp timestamp) {
		ReturnStatus_type retval = ReturnStatus_type.RS_MAYBE;
		boolean hasElse = false;
		for (SelectClassCase scc : select_class_cases) {
			switch(scc.getStatementBlock().hasReturn(timestamp)) {
			case RS_NO:
				if (retval == ReturnStatus_type.RS_YES) {
					return ReturnStatus_type.RS_MAYBE;
				} else {
					retval = ReturnStatus_type.RS_NO;
				}
				break;
			case RS_YES:
				if (retval == ReturnStatus_type.RS_NO) {
					return ReturnStatus_type.RS_MAYBE;
				} else {
					retval = ReturnStatus_type.RS_YES;
				}
				break;
			default:
				return ReturnStatus_type.RS_MAYBE;
			}
			if (scc.getType() == null) {
				hasElse = true;
				break;
			}
		}
		if (!hasElse && retval == ReturnStatus_type.RS_YES) {
			return ReturnStatus_type.RS_MAYBE;
		} else {
			return retval;
		}
	}	
}
