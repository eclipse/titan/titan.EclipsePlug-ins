/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Base class for function definitions, including:
 * <ul>
 * <li>Plain ttcn3 functions</li>
 * <li>Class constructors</li>
 * <li>Abstract class methods</li>
 * <li>External functions</li>
 * </ul>
 * @author Miklos Magyari
 * @author Adam Knapp
 *
 */
public abstract class Def_FunctionBase extends Definition implements IParameterisedAssignment {
	/**
	 * The encoding prototype. Used with the extension attributes. Also used
	 * by Def_ExtFunction
	 * */
	public enum EncodingPrototype_type {
		/** no prototype extension. */
		NONE("<no prototype>"),
		/** backtrack prototype. */
		BACKTRACK("backtrack"),
		/** conversion prototype. */
		CONVERT("convert"),
		/** fast prototype. */
		FAST("fast"),
		/** sliding window prototype */
		SLIDING("sliding");
	
		private String name;
	
		private EncodingPrototype_type(final String name) {
			this.name = name;
		}
	
		public String getName() {
			return name;
		}
	}

	/** Location of the function's signature (from 'function' keyword up to the return value)
	 *  Used when we want to report errors/warnings for a whole function but underlining the 
	 *  entire definition (including the body and finally block) looks ugly
	 *  
	 *  Right now, only used for class methods.
	 */
	protected Location signatureLocation = null;
	
	protected Assignment_type assignmentType;
	protected final FormalParameterList formalParameterList;
	protected final StatementBlock statementBlock;
	protected final Type returnType;
	protected final boolean isFinal;
	protected final boolean isAbstract;
	protected final boolean isDeterministic;
	protected final boolean isControl;
	protected NamedBridgeScope bridgeScope = null;
	
	protected Def_FunctionBase(final Identifier identifier, final StatementBlock statementBlock, final boolean isAbstract,
			final boolean isFinal, final boolean isDeterministic, final boolean isControl,
			FormalParameterList formalParameterList, Type returnType) {
		super(identifier);
		this.statementBlock = statementBlock;
		this.isAbstract = isAbstract;
		this.isControl = isControl;
		this.isDeterministic = isDeterministic;
		this.isFinal = isFinal;
		this.formalParameterList = formalParameterList;
		this.returnType = returnType;
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineIcon() {
		if (returnType == null) {
			return "function.gif";
		}

		return "function_return.gif";
	}

	@Override
	/** {@inheritDoc} */
	public FormalParameterList getFormalParameterList() {
		return formalParameterList;
	}

	@Override
	public Assignment_type getAssignmentType() {
		return assignmentType;
	}
	
	@Override
	/** {@inheritDoc} */
	public Type getType(final CompilationTimeStamp timestamp) {
		check(timestamp);
		return returnType;
	}
	
	/**
	 * Gets the location of function signature (name and parameters)
	 * 
	 * @return
	 */
	public Location getSignatureLocation() {
		return signatureLocation;
	}

	/**
	 * Gets the location of function signature (name and parameters)
	 * 
	 * @param signatureLocation
	 */
	public void setSignatureLocation(Location signatureLocation) {
		this.signatureLocation = signatureLocation;
	}
	
	/**
	 * Checks if a function is final (cannot be overridden)
	 * @return
	 */
	public boolean isFinal() {
		return isFinal;
	}
	
	/**
	 * Checks if a function is declared as abstract
	 * @return
	 */
	public boolean isAbstract() {
		return isAbstract;
	}
	
	/**
	 * Checks if the function has a body
	 * @return
	 */
	public boolean hasBody() {
		return statementBlock != null;
	}

	/**
	 * Removes the name bridging scope.
	 * */
	protected void removeBridge() {
		if (bridgeScope != null) {
			bridgeScope.remove();
			bridgeScope = null;
		}
	}
	
	/**
	 * Generates common part of documentation comment text
	 * 
	 * @param indentation
	 * @param formalParameterList
	 * @param returnType
	 * @return
	 */
	public String generateCommonDocComment(final String indentation, FormalParameterList formalParameterList, Type returnType) {
		final String ind = indentation + ICommentable.LINE_START;
		final StringBuilder sb = new StringBuilder();
		sb.append(ICommentable.COMMENT_START)
			.append(ind).append(ICommentable.DESC_TAG).append("\n");

		if (formalParameterList!= null && formalParameterList.getNofParameters() > 0) {
			for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
				final FormalParameter param = formalParameterList.getParameterByIndex(i);
				sb.append(ind).append(ICommentable.PARAM_TAG).append(" ")
					.append(param.getIdentifier().getDisplayName()).append("\n");
			}
		}

		if (returnType != null) {
			sb.append(ind).append(ICommentable.RETURN_TAG).append("\n");
		}
		sb.append(indentation).append(ICommentable.COMMENT_END).append(indentation);
		return sb.toString();
	}
	
	/**
	 * Returns hover info based on document comments
	 * 
	 * @param hoverContent
	 * @param dc
	 * @param formalParameterList
	 * @return
	 */
	public Ttcn3HoverContent getHoverContentFromComment(Ttcn3HoverContent hoverContent, DocumentComment dc, FormalParameterList formalParameterList) {
		if (dc != null) {
			dc.addDescsContent(hoverContent);
			dc.addParamsContent(hoverContent, formalParameterList);
			dc.addReturnContent(hoverContent);
			dc.addVerdictsContent(hoverContent);
			dc.addRequirementsContent(hoverContent);
			dc.addStatusContent(hoverContent);
			dc.addRemarksContent(hoverContent);
			dc.addSinceContent(hoverContent);
			dc.addVersionContent(hoverContent);
			dc.addAuthorsContent(hoverContent);
			dc.addReferenceContent(hoverContent);
			dc.addSeesContent(hoverContent);
			dc.addUrlsContent(hoverContent);
		} else {
			if (formalParameterList != null && formalParameterList.getNofParameters() > 0) {
				hoverContent.addTag("Parameters");
				for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
					final FormalParameter param = formalParameterList.getParameterByIndex(i);
					String paramType = param.getFormalParamType();
					final StringBuilder sb = new StringBuilder(param.getIdentifier().getDisplayName());
					sb.append(" (").append(paramType != null ? paramType : "<?>").append(")");
					hoverContent.addIndentedText(sb.toString(), "");
				}
			}
		}
		return hoverContent;
	}
	
	/**
	 * Checks whether two functions have the same signature
	 * 
	 * @param timestamp
	 * @param other
	 * @return
	 */
	public abstract IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other);
}
