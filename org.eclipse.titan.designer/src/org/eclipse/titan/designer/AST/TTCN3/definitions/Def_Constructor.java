/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.DocumentComment;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.NamedBridgeScope;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.ReferenceFinder.Hit;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList.IsIdenticalResult;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;
import org.eclipse.ui.IEditorPart;

/**
 * Represents the constructor of a TTCN3 class 
 * 
 * @author Miklos Magyari
 *
 */
public class Def_Constructor extends Def_FunctionBase {
	private static final String KIND = "constructor";
	
	private static final String DOESNOTHAVESUPERCLASS = "Class type `{0}'' does not have a superclass";
	private static final String CONSTRUCTORREFERENCEEXPECTED = "Reference to constructor was expected instead of {0}";
	private static final String CONSTRUCTORCALLEXPECTED = "Expected call to constructor of class type `{0}'', instead of class type `{1}''";
	private static final String MISSINGSUPERCONSTRUCTORCALL = "Missing super-constructor call";
	private static final String EXTERNALWITHBODY = "The constructor of an external class cannot have a body";
	private static final String MISSINGBODY = "Missing constructor body";

	private Map<Identifier,Boolean> uninitializedMembers = new HashMap<>();
	private Reference baseRef;
	
	public Def_Constructor(Identifier identifier, FormalParameterList fpl, Reference baseCall, StatementBlock sb) {
		super(identifier, sb, false, false, false, false, fpl, null);
		baseRef = baseCall;
	}
	
	public Def_Constructor(Identifier identifier, FormalParameterList fpl, Type baseCall, StatementBlock sb) {
		this(identifier, fpl,
			baseCall != null && baseCall instanceof Referenced_Type ?
				((Referenced_Type)baseCall).getReference() : null,
			sb);
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		if (bridgeScope != null && bridgeScope.getParentScope() == scope) {
			return;
		}
		
		bridgeScope = new NamedBridgeScope();
		bridgeScope.setParentScope(scope);
		scope.addSubScope(getLocation(), bridgeScope);
		bridgeScope.setScopeMacroName(identifier.getDisplayName());
	
		super.setMyScope(bridgeScope);
		
		if (formalParameterList != null) {
			formalParameterList.setMyScope(bridgeScope);
			formalParameterList.setMyDefinition(this);
			bridgeScope.addSubScope(formalParameterList.getLocation(), formalParameterList);
		}
		if (baseRef != null) {
			baseRef.setMyScope(formalParameterList);
		}
		if (statementBlock != null) {
			statementBlock.setMyScope(formalParameterList);
		}
	}

	public synchronized void addUninitializedMember(Identifier identifier, boolean isTemplate) {
		uninitializedMembers.put(identifier, isTemplate);
	}

	@Override
	public String getProposalKind() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateSyntax(TTCN3ReparseUpdater reparser, boolean isDamaged) throws ReParseException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Assignment_type getAssignmentType() {
		return Assignment_type.A_CONSTRUCTOR;
	}

	@Override
	public String getAssignmentName() { 
		return KIND;
	}

	@Override
	public void check(CompilationTimeStamp timestamp) {
		check(timestamp, null);
	}

	@Override
	public void check(CompilationTimeStamp timestamp, IReferenceChain refChain) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}
		lastTimeChecked = timestamp;
		
		removeSyntaxDecoration();
		/** 
		 * Code minings are disabled for now
		 */
		// removeMining();
		
		final Class_Type myClass = getMyScope().getScopeClass();
		
		formalParameterList.check(timestamp, getAssignmentType());
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			final FormalParameter fp = formalParameterList.getParameterByIndex(i);
			if (fp.hasDefaultValue()) {
				//fp.getDefaultParameter().checkDefaultParameterInClass(myClass.hasDefaultConstructor(), false, false);
			}
		}
		
		final Class_Type baseClass = myClass.getBaseClass();
		if (baseRef != null) {
			if (baseClass == null) {
				baseRef.getLocation().reportSemanticError(
					MessageFormat.format(DOESNOTHAVESUPERCLASS, myClass.getTypeRefdLast(timestamp).getTypename()));
			} else {
				final Assignment baseCallAssignment = baseRef.getRefdAssignment(timestamp, true);
				if (baseCallAssignment != null) {
					if (baseCallAssignment.getAssignmentType() != Assignment_type.A_CONSTRUCTOR) {
						baseRef.getLocation().reportSemanticError(
							MessageFormat.format(CONSTRUCTORREFERENCEEXPECTED, baseCallAssignment.getAssignmentName()));
					} else {
						final Class_Type baseCallClass = baseCallAssignment.getMyScope().getScopeClass();
						if (baseCallClass != baseClass) {
							baseRef.getLocation().reportSemanticError(
								MessageFormat.format(CONSTRUCTORCALLEXPECTED, myClass.getTypename(),
								baseCallClass.getTypename()));
						} // TODO : baseRef parameters
					}
				}
			}
		} else if (baseClass != null && !myClass.isExternal()) {
			final Def_Constructor baseConstructor = baseClass.getConstructor(timestamp);
			if (baseConstructor != null && !baseConstructor.getFormalParameterList().hasOnlyDefaultValues()) {
				signatureLocation.reportSemanticError(MISSINGSUPERCONSTRUCTORCALL);
			}
		}
		
		// reset 'usage found' flag for the constructor's formal parameters with default values
	    // (usage in the base constructor call doesn't count for these)
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			final FormalParameter fp = formalParameterList.getParameterByIndex(i);
			if (fp.hasDefaultValue()) {
				fp.reset();
			}
		}
		
		if (statementBlock != null) {
			if (myClass.isExternal()) {
				statementBlock.getLocation().reportSemanticError(EXTERNALWITHBODY);
			}
			statementBlock.check(timestamp);
		} else if (!myClass.isExternal()) {
			signatureLocation.reportSemanticError(MISSINGBODY);
		}	
	}

	@Override
	public void generateCode(JavaGenData aData, boolean cleanUp) {
		
	}

	@Override
	public IsIdenticalResult isIdentical(CompilationTimeStamp timestamp, Def_FunctionBase other) {
		return null;
	}
	
	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (!super.memberAccept(v)) {
			return false;
		}
		if (formalParameterList != null && !formalParameterList.accept(v)) {
			return false;
		}
		return true;
	}
	
	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent(IEditorPart editor) {
		super.getHoverContent(editor);
		
		DocumentComment dc = null;
		if (hasDocumentComment()) {
			dc = parseDocumentComment();
			if (dc.isDeprecated()) {
				hoverContent.addDeprecated();
			}
		}
		
		hoverContent.addIcon(getOutlineIcon())
		.addStyledText(KIND, SWT.BOLD)
		.addText(" ")
		.addStyledText(getFullName(), SWT.BOLD)
		.addText("(");
		for (int i = 0; i < formalParameterList.getNofParameters(); i++) {
			final FormalParameter fp = formalParameterList.getParameterByIndex(i);
			if (i > 0) {
				hoverContent.addText(", ");
			}
			hoverContent.addText(fp.getFormalParamType()).addText(" ")
				.addText(fp.getType(lastTimeChecked).getTypename()).addText(" ")
				.addText(fp.getIdentifier().getDisplayName());
		}
		hoverContent.addText(")");
		hoverContent.closeHeader();
		
		getHoverContentFromComment(hoverContent, dc, formalParameterList);
		
		hoverContent.addContent(HoverContentType.INFO);
		return hoverContent;
	}
	
	@Override
	public String generateDocComment(final String indentation) {
		return generateCommonDocComment(indentation, formalParameterList, null);
	}
	
	@Override
	/** {@inheritDoc} */
	public void findReferences(final ReferenceFinder referenceFinder, final List<Hit> foundIdentifiers) {
		super.findReferences(referenceFinder, foundIdentifiers);
		if (formalParameterList != null) {
			formalParameterList.findReferences(referenceFinder, foundIdentifiers);
		}
		if (statementBlock != null) {
			statementBlock.findReferences(referenceFinder, foundIdentifiers);
		}
	}
}
