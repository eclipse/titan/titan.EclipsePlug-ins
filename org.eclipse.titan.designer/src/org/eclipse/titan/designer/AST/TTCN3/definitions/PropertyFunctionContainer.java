/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.definitions;

import org.eclipse.titan.designer.AST.Assignment;

public class PropertyFunctionContainer {
	/* 
	 * This indicates whether the property function (i.e. getter or setter)
     * is in an automatic property.
     * The getter and setter of an automatic property doesn't appear in the AST.
	 */
	public boolean isAutomatic;
	
	/*
	 * the getter/setter if it is not automatic;
     * set to null if the container is empty (i.e. no getter/setter was found)
	 */
	public Property_Function function;
	
	/*
	 * pointer to the property definition if it is automatic;
     * (is never null if 'is_automatic' is true)
	 */
	public Assignment definition;
}
