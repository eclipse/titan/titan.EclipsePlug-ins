/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST;

import java.util.HashMap;
import java.util.Map;

public class RecursionTracker {
	private static Map<IType,Void> types = new HashMap<>();
	private IType key;
	
	public RecursionTracker(IType key) {
		this.key = key;
		synchronized(types) {
			types.put(key, null);
		}
	}
	
	public static boolean isHappening(IType t) {
		return types.containsKey(t);
	}
	
	@Override
	protected void finalize() {
		synchronized (types) {
			types.remove(key);
		}
	}
}
