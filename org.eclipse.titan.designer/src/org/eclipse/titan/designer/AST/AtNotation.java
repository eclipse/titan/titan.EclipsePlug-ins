/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST;

import org.eclipse.titan.designer.AST.ASN1.Object.FieldName;

/**
 * Represent an ASN AtNotation
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class AtNotation extends ASTNode {

	/** number of "."s in compid.compid */
	private final int levels;
	private boolean correctionNeeded = false;

	private final FieldName componentIdentifiers;
	private Identifier objectClassFieldname;

	private IType firstComponent;
	private IType lastComponent;

	public AtNotation(final int levels, final FieldName fieldname) {
		this.levels = levels;
		componentIdentifiers = fieldname;
	}

	public int getLevels() {
		return levels;
	}

	public boolean isCorrectionNeeded() {
		return correctionNeeded;
	}

	public void setCorrectionNeeded() {
		correctionNeeded = true;
	}

	public FieldName getComponentIdentifiers() {
		return componentIdentifiers;
	}

	public Identifier getObjectClassFieldname() {
		return objectClassFieldname;
	}

	public void setObjectClassFieldname(final Identifier name) {
		objectClassFieldname = name;
	}

	public IType getFirstComponent() {
		return firstComponent;
	}

	public void setFirstComponent(final IType type) {
		firstComponent = type;
	}

	public IType getLastComponent() {
		return lastComponent;
	}

	public void setLastComponent(final IType type) {
		lastComponent = type;
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		// TODO
		return true;
	}
}
