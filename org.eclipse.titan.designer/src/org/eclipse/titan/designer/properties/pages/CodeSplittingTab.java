/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.properties.pages;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.GeneralConstants;
import org.eclipse.titan.designer.preferences.pages.ComboFieldEditor;
import org.eclipse.titan.designer.preferences.pages.FileListEditor;
import org.eclipse.titan.designer.properties.data.MakefileCreationData;
import org.eclipse.titan.designer.properties.data.ProjectBuildPropertyData;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Collects and handles the UI elements on a dedicated tab
 * @author Adam Knapp
 * */
public class CodeSplittingTab {
	private static final String INVALID_CODESPLITTING_NUMBER = "Invalid code splitting number in Code Splitting tab: ";
	private static final String INVALID_CODESPLITTING_VALUE = "Invalid code splitting value in Code Splitting tab: ";
	private static final String CODE_SPLITTING_TAB_TEXT = "Code Splitting (-U)";
	private static final String CODE_SPLITTING_TAB_TOOLTIP = "Settings controlling the code splitting option";
	private static final String ENABLE_SELECTIVE_CODE_SPLITTING_TEXT = "Enable selective code splitting";
	private static final String ENABLE_SELECTIVE_CODE_SPLITTING_TOOLTIP = "Without selective code splitting, "
			+ "all relevant files will be splitted in the project";

	private boolean makeFileGenerationEnabled = true;
	private Composite codeSplittingTabComposite;
	private Composite codeSplittingComposite;
	private ComboFieldEditor codeSplitting; //code splitting mode
	private Composite codeSplittingNumberComposite;
	private IntegerFieldEditor codeSplittingNumber; //code splitting number if codeSpitting == "number"
	private Button enableSelectiveCodeSplitting;
	private Composite codeSplittingFileListComposite;
	private FileListEditor editor;

	private TabItem codeSplittingTabItem;
	private final IProject project;
	private final PropertyPage page;

	public CodeSplittingTab(final IProject project, final PropertyPage page) {
		this.project = project;
		this.page = page;
	}

	/**
	 * Disposes the SWT resources allocated by this tab page.
	 */
	public void dispose() {
		codeSplittingComposite.dispose();
		codeSplitting.dispose();
		codeSplittingNumberComposite.dispose();
		codeSplittingNumber.dispose();
		enableSelectiveCodeSplitting.dispose();
		codeSplittingFileListComposite.dispose();
		editor.dispose();
		codeSplittingTabComposite.dispose();
		codeSplittingTabItem.dispose();
	}

	/**
	 * Creates and returns the SWT control for the customized body of this
	 * TabItem under the given parent TabFolder.
	 * <p>
	 * 
	 * @param tabFolder the parent TabFolder
	 * @return the new TabItem
	 */
	protected TabItem createContents(final TabFolder tabFolder) {
		codeSplittingTabItem = new TabItem(tabFolder, SWT.BORDER);
		codeSplittingTabItem.setText(CODE_SPLITTING_TAB_TEXT);
		codeSplittingTabItem.setToolTipText(CODE_SPLITTING_TAB_TOOLTIP);

		codeSplittingTabComposite = new Composite(tabFolder, SWT.MULTI);
		codeSplittingTabComposite.setEnabled(true);
		codeSplittingTabComposite.setLayout(new GridLayout());

		codeSplittingComposite = new Composite(codeSplittingTabComposite, SWT.NONE);
		codeSplitting = new ComboFieldEditor(MakefileCreationData.CODE_SPLITTING_PROPERTY, "Splitting mode:",
			new String[][] {
				{ "none", GeneralConstants.NONE },
				{ "type", GeneralConstants.TYPE },
				{ "number", GeneralConstants.NUMBER} },
			codeSplittingComposite);

		codeSplitting.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				updateCodeSplittingNumberEnabled();
			}
		});

		codeSplittingNumberComposite = new Composite(codeSplittingTabComposite, SWT.NONE);
		codeSplittingNumberComposite.setLayout(new GridLayout());
		final GridData codeSplittingNumberData = new GridData(GridData.FILL);
		codeSplittingNumberData.grabExcessHorizontalSpace = true;
		codeSplittingNumberData.horizontalAlignment = SWT.FILL;
		codeSplittingNumberComposite.setLayoutData(codeSplittingNumberData);
		codeSplittingNumber = new IntegerFieldEditor(
				GeneralConstants.NUMBER_DEFAULT,
				"Number (1-9999):",
				codeSplittingNumberComposite,
				5);
		codeSplittingNumber.setValidRange(1, 9999);
		codeSplittingNumber.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				checkCodeSplittingNumber();
			}
		});

		enableSelectiveCodeSplitting = new Button(codeSplittingTabComposite, SWT.CHECK);
		enableSelectiveCodeSplitting.setText(ENABLE_SELECTIVE_CODE_SPLITTING_TEXT);
		enableSelectiveCodeSplitting.setToolTipText(ENABLE_SELECTIVE_CODE_SPLITTING_TOOLTIP);
		enableSelectiveCodeSplitting.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				updateFileList();
			}
		});

		codeSplittingFileListComposite = new Composite(codeSplittingTabComposite, SWT.NONE);
		codeSplittingFileListComposite.setEnabled(true);
		codeSplittingFileListComposite.setLayout(new GridLayout());
		final GridData codeSplittingFileListData = new GridData(GridData.FILL);
		codeSplittingFileListData.grabExcessHorizontalSpace = true;
		codeSplittingFileListData.horizontalAlignment = SWT.FILL;
		codeSplittingFileListComposite.setLayoutData(codeSplittingFileListData);
		
		editor = new FileListEditor("codeSplittingFileList", "List of files to split", codeSplittingFileListComposite, project);
		editor.setPage(page);

		codeSplittingTabItem.setControl(codeSplittingTabComposite);
		return codeSplittingTabItem;
	}

	/**
	 * Handles the enabling/disabling of controls when the automatic
	 * Makefile generation is enabled/disabled.
	 * 
	 * @param value the actual state of automatic Makefile generation.
	 */
	protected void setMakefileGenerationEnabled(final boolean value) {
		makeFileGenerationEnabled = value;
		if (enableSelectiveCodeSplitting != null) {
			enableSelectiveCodeSplitting.setEnabled(value);
		}
		if (codeSplittingComposite != null) {
			codeSplittingComposite.setEnabled(value);
		}
		if (codeSplitting != null) {
			codeSplitting.setEnabled(value, codeSplittingComposite);
		}
		updateCodeSplittingNumberEnabled();
	}

	/**
	 * Copies the actual values into the provided preference storage.
	 * 
	 * @param project the actual project (the real preference store).
	 * @param tempStorage the temporal store to copy the values to.
	 */
	public void copyPropertyStore(final IProject project, final PreferenceStore tempStorage) {
		String temp = null;
		try {
			temp = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.CODE_SPLITTING_PROPERTY));
			if (temp != null) {
				tempStorage.setValue(MakefileCreationData.CODE_SPLITTING_PROPERTY, temp);
			}

			temp = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY));
			if (temp != null) {
				tempStorage.setValue(MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY, temp);
			}
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
	}

	/**
	 * Evaluates the properties on the option page, and compares them with
	 * the saved values.
	 * 
	 * @param project the actual project (the real preference store).
	 * @param tempStorage the temporal store to copy the values to.
	 * @return {@code true} if the values in the real and the temporal storage are
	 *         different (they have changed), {@code false} otherwise.
	 */
	public boolean evaluatePropertyStore(final IProject project, final PreferenceStore tempStorage) {
		boolean result = false;
		String actualValue = null;
		String copyValue = null;
		try {
			actualValue = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.CODE_SPLITTING_PROPERTY));
			copyValue = tempStorage.getString(MakefileCreationData.CODE_SPLITTING_PROPERTY);
			result |= ((actualValue != null && !actualValue.equals(copyValue)) || (actualValue == null && copyValue == null));

			actualValue = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY));
			copyValue = tempStorage.getString(MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY);
			result |= ((actualValue != null && !actualValue.equals(copyValue)) || (actualValue == null && copyValue == null));
			result |= editor.isChanged();
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
		return result;
	}

	/**
	 * Checks the properties of this page for errors.
	 * 
	 * @param page the property page to report the errors to.
	 * @return {@code true} if no error was found, {@code false} otherwise.
	 */
	public boolean checkProperties(final ProjectBuildPropertyPage page) {
		if (GeneralConstants.NUMBER.equals(codeSplitting.getActualValue())){
			try {
				codeSplittingNumber.getIntValue();
			} catch (NumberFormatException e) {
				page.setErrorMessage(INVALID_CODESPLITTING_NUMBER + codeSplittingNumber.getStringValue());
				return false;
			}
		}
		return true;
	}

	/**
	 * Performs special processing when the ProjectBuildProperty page's
	 * Defaults button has been pressed.
	 */
	protected void performDefaults() {
		codeSplitting.setSelectedValue(MakefileCreationData.CODE_SPLITTING_DEFAULT_VALUE);
		codeSplittingNumber.setStringValue(GeneralConstants.NUMBER_DEFAULT);
		
		updateCodeSplittingNumberEnabled();
		editor.loadDefault();
	}

	/**
	 * Loads the properties from the property storage, into the user interface elements.
	 * @param project the project to load the properties from.
	 */
	public void loadProperties(final IProject project) {
		try {
			final String codeSplittingType = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.CODE_SPLITTING_PROPERTY));
			if (codeSplittingType == null || codeSplittingType.length() == 0) {
				codeSplitting.setSelectedValue(GeneralConstants.NONE);
				codeSplittingNumber.setStringValue(GeneralConstants.NUMBER_DEFAULT);
			} else if (codeSplittingType.equals(GeneralConstants.NONE) || codeSplittingType.equals(GeneralConstants.TYPE)){
				codeSplitting.setSelectedValue(codeSplittingType);
				codeSplittingNumber.setStringValue(GeneralConstants.NUMBER_DEFAULT);
			} else {
				//number supposed:
				codeSplittingNumber.setStringValue(codeSplittingType);
				try {
					codeSplittingNumber.getIntValue();
					codeSplitting.setSelectedValue(GeneralConstants.NUMBER);
				} catch (final NumberFormatException e) {
					//not number is set:
					codeSplitting.setSelectedValue(GeneralConstants.NONE);
					codeSplittingNumber.setStringValue(GeneralConstants.NUMBER_DEFAULT);
					ErrorReporter.INTERNAL_ERROR(INVALID_CODESPLITTING_NUMBER + codeSplittingNumber.getStringValue());
				}
			}
			final String temp = project.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY));
			enableSelectiveCodeSplitting.setSelection(Boolean.parseBoolean(temp));
			updateCodeSplittingNumberEnabled();
			editor.load();
		} catch (CoreException e) {
			codeSplitting.setSelectedValue(MakefileCreationData.CODE_SPLITTING_DEFAULT_VALUE);
			codeSplittingNumber.setStringValue(GeneralConstants.NUMBER_DEFAULT);
			enableSelectiveCodeSplitting.setSelection(MakefileCreationData.SELECTIVE_CODE_SPLITTING_DEFAULT_VALUE);
			setMakefileGenerationEnabled(false);
		}
	}

	/**
	 * Saves the properties to the property storage, from the user interface elements.
	 * @param project the project to save the properties to.
	 * @return true if the save was successful, false otherwise.
	 */
	public boolean saveProperties(final IProject project) {
		try {
			final String codeSplittingActualValue = codeSplitting.getActualValue();

			if (codeSplittingActualValue.equals(GeneralConstants.NONE) || codeSplittingActualValue.equals(GeneralConstants.TYPE)) {
				project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, MakefileCreationData.CODE_SPLITTING_PROPERTY), codeSplittingActualValue);
			} else if (codeSplittingActualValue.equals(GeneralConstants.NUMBER)) {
				try {
					codeSplittingNumber.getIntValue();
					project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, MakefileCreationData.CODE_SPLITTING_PROPERTY), codeSplittingNumber.getStringValue());
				} catch( NumberFormatException e)  {
					ErrorReporter.logError(INVALID_CODESPLITTING_NUMBER + codeSplittingNumber.getStringValue());
				}
			} else {
				project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, MakefileCreationData.CODE_SPLITTING_PROPERTY), GeneralConstants.NONE);
				ErrorReporter.INTERNAL_ERROR(INVALID_CODESPLITTING_VALUE +  codeSplittingActualValue);
			}

			project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY), Boolean.toString(enableSelectiveCodeSplitting.getSelection()));
			editor.store();
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace(e);
			return false;
		}
		return true;
	}

	private void checkCodeSplittingNumber() {
		if(!codeSplittingNumber.isValid()) {
			page.setErrorMessage(INVALID_CODESPLITTING_NUMBER + codeSplittingNumber.getStringValue());
		} else {
			page.setErrorMessage(null);
		}
	}

	private void updateCodeSplittingNumberEnabled() {
		if (makeFileGenerationEnabled && GeneralConstants.NUMBER.equals(codeSplitting.getActualValue())) {
			codeSplittingNumber.setEnabled(true, codeSplittingNumberComposite);
			checkCodeSplittingNumber();
			enableSelectiveCodeSplitting.setEnabled(true);
			updateFileList();
		} else if (makeFileGenerationEnabled && GeneralConstants.TYPE.equals(codeSplitting.getActualValue())) {
			codeSplittingNumber.setEnabled(false, codeSplittingNumberComposite);
			page.setErrorMessage(null);
			enableSelectiveCodeSplitting.setEnabled(true);
			updateFileList();
		} else {
			codeSplittingNumber.setEnabled(false, codeSplittingNumberComposite);
			page.setErrorMessage(null);
			enableSelectiveCodeSplitting.setEnabled(false);
			updateFileList();
		}
	}

	private void updateFileList() {
		if (makeFileGenerationEnabled && enableSelectiveCodeSplitting.getSelection()
				&& enableSelectiveCodeSplitting.getEnabled()) {
			editor.setEnabled(true, codeSplittingFileListComposite);
		} else {
			editor.setEnabled(false, codeSplittingFileListComposite);
			page.setErrorMessage(null);
		}
	}
}
