/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.properties.pages;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.MarkerHandler;
import org.eclipse.titan.designer.core.TITANBuilder;
import org.eclipse.titan.designer.parsers.GlobalProjectStructureTracker;
import org.eclipse.titan.designer.properties.PropertyNotificationManager;
import org.eclipse.titan.designer.properties.data.FileBuildPropertyData;
import org.eclipse.titan.designer.properties.data.MakefileCreationData;
import org.eclipse.titan.designer.properties.data.ProjectBuildPropertyData;
import org.eclipse.titan.designer.properties.data.ProjectFileHandler;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class FileBuildPropertyPage extends PropertyPage {
	private Composite pageComposite = null;
	private Label headLabel = null;
	private ConfigurationManagerControl configurationManager;
	private String firstConfiguration;

	private Button excludeFromBuildButton = null;
	private static final String EXCLUDE_DISPLAY_TEXT = "Excluded from build.";

	private Button enableCodeSplittingButton = null;
	private static final String ENABLE_CODE_SPLITTING_DISPLAY_TEXT = "Enable code splitting";
	private static final String ENABLE_CODE_SPLITTING_TOOLTIP = "Selective code splitting must be enabled on project level";

	private IFile fileResource;
	private boolean hasSupportedFileExtension;

	public FileBuildPropertyPage() {
		super();
	}

	@Override
	public void dispose() {
		pageComposite.dispose();
		headLabel.dispose();

		super.dispose();
	}

	/**
	 * Handles the change of the active configuration. Sets the new
	 * configuration to be the active one, and loads its settings.
	 * 
	 * @param configuration the name of the new configuration.
	 */
	public void changeConfiguration(final String configuration) {
		configurationManager.changeActualConfiguration();

		loadProperties();

		PropertyNotificationManager.firePropertyChange(fileResource.getProject());
	}

	@Override
	protected Control createContents(final Composite parent) {
		fileResource = (IFile) getElement();

		pageComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		pageComposite.setLayout(layout);
		final GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		pageComposite.setLayoutData(data);
		if (TITANBuilder.isBuilderEnabled(fileResource.getProject())) {
			headLabel = new Label(pageComposite, SWT.NONE);
			headLabel.setText(ProjectBuildPropertyPage.BUILDER_IS_ENABLED);
		} else {
			headLabel = new Label(pageComposite, SWT.NONE);
			headLabel.setText(ProjectBuildPropertyPage.BUILDER_IS_NOT_ENABLED);
		}

		configurationManager = new ConfigurationManagerControl(pageComposite, fileResource.getProject());
		configurationManager.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				if (configurationManager.hasConfigurationChanged()) {
					changeConfiguration(configurationManager.getActualSelection());
				}
			}
		});
		firstConfiguration = configurationManager.getActualSelection();

		excludeFromBuildButton = new Button(pageComposite, SWT.CHECK);
		excludeFromBuildButton.setText(EXCLUDE_DISPLAY_TEXT);
		excludeFromBuildButton.setEnabled(true);

		hasSupportedFileExtension = FileBuildPropertyData.hasSupportedFileExtension(fileResource);
		if (hasSupportedFileExtension) {
			enableCodeSplittingButton = new Button(pageComposite, SWT.CHECK);
			enableCodeSplittingButton.setText(ENABLE_CODE_SPLITTING_DISPLAY_TEXT);
			enableCodeSplittingButton.setToolTipText(ENABLE_CODE_SPLITTING_TOOLTIP);
			updateEnableCodeSplittingButton();
		}

		boolean mode = FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.EXCLUDE_FROM_BUILD_PROPERTY);
		excludeFromBuildButton.setSelection(mode);

		if (hasSupportedFileExtension) {
			mode = FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY);
			enableCodeSplittingButton.setSelection(mode);
		}

		return null;
	}

	@Override
	public void setVisible(final boolean visible) {
		if (!visible) {
			return;
		}

		if (configurationManager != null) {
			configurationManager.refresh();
		}

		super.setVisible(visible);
	}

	@Override
	protected void performDefaults() {
		excludeFromBuildButton.setSelection(false);
		if (hasSupportedFileExtension) {
			enableCodeSplittingButton.setSelection(false);
			updateEnableCodeSplittingButton();
		}

		configurationManager.saveActualConfiguration();
	}

	@Override
	public boolean performCancel() {
		configurationManager.clearActualConfiguration();
		loadProperties();
		return super.performCancel();
	}

	@Override
	public boolean performOk() {
		final IProject project = fileResource.getProject();

		final boolean excludeFromBuildChanged = excludeFromBuildButton.getSelection() !=
				FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.EXCLUDE_FROM_BUILD_PROPERTY);			

		boolean codeSplittingChanged = hasSupportedFileExtension && enableCodeSplittingButton.getSelection() !=
				FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY);

		final boolean configurationChanged = !firstConfiguration.equals(configurationManager.getActualSelection());

		if (configurationChanged || excludeFromBuildChanged || codeSplittingChanged) {
			FileBuildPropertyData.setPropertyValue(fileResource,
					FileBuildPropertyData.EXCLUDE_FROM_BUILD_PROPERTY, excludeFromBuildButton.getSelection());

			if (codeSplittingChanged) {
				FileBuildPropertyData.setPropertyValue(fileResource,
						FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY, enableCodeSplittingButton.getSelection());
			}

			configurationManager.saveActualConfiguration();
			ProjectFileHandler projectFileHandler = new ProjectFileHandler(project);
			projectFileHandler.saveProjectSettings();
			GlobalProjectStructureTracker.projectChanged(project);

			MarkerHandler.markAllMarkersForRemoval(fileResource);

			PropertyNotificationManager.firePropertyChange(fileResource);
		}

		return true;
	}

	/**
	 * Loads the properties from the resource into the user interface elements.
	 */
	private void loadProperties() {
		boolean temp = FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.EXCLUDE_FROM_BUILD_PROPERTY);
		excludeFromBuildButton.setSelection(temp);

		if (hasSupportedFileExtension) {
			temp = FileBuildPropertyData.getPropertyValue(fileResource, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY);
			enableCodeSplittingButton.setSelection(temp);
			updateEnableCodeSplittingButton();
		}
	}

	private void updateEnableCodeSplittingButton() {
		try {
			final String temp = fileResource.getProject().getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
					MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY));
			enableCodeSplittingButton.setEnabled(Boolean.parseBoolean(temp));
		} catch (CoreException ce) {
			ErrorReporter.logExceptionStackTrace(ce);
		}
	}
}
