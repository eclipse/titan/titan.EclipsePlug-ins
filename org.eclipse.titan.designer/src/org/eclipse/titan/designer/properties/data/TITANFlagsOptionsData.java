/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.properties.data;

import java.util.HashSet;
import java.util.TreeMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Handles the property settings of projects related to the flags TITAN should
 * be called with. Also loading, saving of these properties from and into
 * external formats.
 * 
 * @author Kristof Szabados
 * */
public final class TITANFlagsOptionsData {
	private static final String EMPTY = "";
	private static final String FALSE = String.valueOf(false);

	public static final String DISABLE_ATTRIBUTE_VALIDATION_PROPERTY = "disableAttributeValidation";

	public static final String DISABLE_BER_PROPERTY = "disableBER";
	public static final String DISABLE_RAW_PROPERTY = "disableRAW";
	public static final String DISABLE_TEXT_PROPERTY = "disableTEXT";
	public static final String DISABLE_XER_PROPERTY = "disableXER";
	public static final String DISABLE_JSON_PROPERTY = "disableJSON";
	public static final String DISABLE_OER_PROPERTY = "disableOER";

	public static final String FORCE_XER_IN_ASN1_PROPERTY = "forceXERinASN.1";
	public static final String DEFAULT_AS_OMIT_PROPERTY = "defaultasOmit";
	public static final String FORCE_OLD_FUNC_OUT_PAR_PROPERTY = "forceOldFuncOutParHandling";
	public static final String GCC_MESSAGE_FORMAT_PROPERTY = "gccMessageFormat";
	public static final String LINE_NUMBERS_ONLY_IN_MESSAGES_PROPERTY = "lineNumbersOnlyInMessages";
	public static final String INCLUDE_SOURCEINFO_PROPERTY = "includeSourceInfo";
	public static final String ADD_SOURCELINEINFO_PROPERTY = "addSourceLineInfo";
	public static final String SUPPRESS_WARNINGS_PROPERTY = "suppressWarnings";

	public static final String ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY = "omitInValueList";
	public static final String WARNINGS_FOR_BAD_VARIANTS_PROPERTY = "warningsForBadVariants";
	public static final String IGNORE_UNTAGGED_ON_TOP_LEVEL_UNION_PROPERTY = "ignoreUntaggedOnTopLevelUnion";
	public static final String ACTIVATE_DEBUGGER_PROPERTY = "activateDebugger";
	public static final String QUIETLY_PROPERTY = "quietly";
	public static final String ENABLE_LEGACY_ENCODING_PROPERTY ="enableLegacyEncoding";
	public static final String DISABLE_USER_INFORMATION_PROPERTY ="disableUserInformation";

	public static final String ENABLE_REALTIME = "enableRealtimeTesting";
	public static final String DISABLE_SUBTYPE_CHECKING_PROPERTY = "disableSubtypeChecking";
	public static final String FORCE_GEN_SEOF = "forceGenSeof";
	public static final String ENABLE_OOP = "enableOOP";

	

	//The order of items of the next array defines the order of items in the tpd file within the "MakefileSettings".
	//It should be according to the TPD.xsd otherwise the generated tpd will not be valid and the import will fail.
	public static final String[] PROPERTIES = {
			DISABLE_ATTRIBUTE_VALIDATION_PROPERTY,
			DISABLE_BER_PROPERTY, DISABLE_RAW_PROPERTY, DISABLE_TEXT_PROPERTY, DISABLE_XER_PROPERTY, DISABLE_JSON_PROPERTY, DISABLE_OER_PROPERTY,
			FORCE_XER_IN_ASN1_PROPERTY, DEFAULT_AS_OMIT_PROPERTY, FORCE_OLD_FUNC_OUT_PAR_PROPERTY, GCC_MESSAGE_FORMAT_PROPERTY, LINE_NUMBERS_ONLY_IN_MESSAGES_PROPERTY,
			INCLUDE_SOURCEINFO_PROPERTY, ADD_SOURCELINEINFO_PROPERTY, SUPPRESS_WARNINGS_PROPERTY, ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY, WARNINGS_FOR_BAD_VARIANTS_PROPERTY,
			IGNORE_UNTAGGED_ON_TOP_LEVEL_UNION_PROPERTY, ACTIVATE_DEBUGGER_PROPERTY, QUIETLY_PROPERTY, ENABLE_LEGACY_ENCODING_PROPERTY, DISABLE_USER_INFORMATION_PROPERTY,
			ENABLE_REALTIME, DISABLE_SUBTYPE_CHECKING_PROPERTY, FORCE_GEN_SEOF, ENABLE_OOP /*insert here the next new item*/};
	
			//TODO:is  NAMING_RULES obsolete? 
			
	public static final String[] TAGS = PROPERTIES;
	public static final String[] DEFAULT_VALUES = {
		FALSE,
		FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
		FALSE, FALSE, FALSE, FALSE, FALSE,
		FALSE, FALSE, FALSE, FALSE, FALSE,
		FALSE, FALSE, FALSE, FALSE, FALSE,
		FALSE, FALSE, FALSE, FALSE, FALSE /*insert here the next new item*/};

	private TITANFlagsOptionsData() {
		// Do nothing
	}

	public static String getTITANFlags(final IProject project, final boolean useRuntime2) {
		final StringBuilder builder = new StringBuilder(30);

		if (useRuntime2) {
			builder.append('R');
		}

		append(builder, project, TITANFlagsOptionsData.DISABLE_BER_PROPERTY, "b");
		append(builder, project, TITANFlagsOptionsData.DISABLE_RAW_PROPERTY, "r");
		append(builder, project, TITANFlagsOptionsData.DISABLE_TEXT_PROPERTY, "x");
		append(builder, project, TITANFlagsOptionsData.DISABLE_XER_PROPERTY, "X");
		append(builder, project, TITANFlagsOptionsData.DISABLE_JSON_PROPERTY, "j");
		append(builder, project, TITANFlagsOptionsData.DISABLE_OER_PROPERTY, "O");
		append(builder, project, TITANFlagsOptionsData.FORCE_XER_IN_ASN1_PROPERTY, "a");
		append(builder, project, TITANFlagsOptionsData.DEFAULT_AS_OMIT_PROPERTY, "d");
		append(builder, project, TITANFlagsOptionsData.FORCE_OLD_FUNC_OUT_PAR_PROPERTY, "Y");
		append(builder, project, TITANFlagsOptionsData.GCC_MESSAGE_FORMAT_PROPERTY, "g");
		append(builder, project, TITANFlagsOptionsData.LINE_NUMBERS_ONLY_IN_MESSAGES_PROPERTY, "i");
		append(builder, project, TITANFlagsOptionsData.INCLUDE_SOURCEINFO_PROPERTY, "l");
		append(builder, project, TITANFlagsOptionsData.ADD_SOURCELINEINFO_PROPERTY, "L");
		append(builder, project, TITANFlagsOptionsData.SUPPRESS_WARNINGS_PROPERTY, "w");
		append(builder, project, TITANFlagsOptionsData.QUIETLY_PROPERTY, "q");
		append(builder, project, TITANFlagsOptionsData.DISABLE_SUBTYPE_CHECKING_PROPERTY, "y");
		append(builder, project, TITANFlagsOptionsData.DISABLE_ATTRIBUTE_VALIDATION_PROPERTY, "0");
		append(builder, project, TITANFlagsOptionsData.ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY, "M");
		append(builder, project, TITANFlagsOptionsData.WARNINGS_FOR_BAD_VARIANTS_PROPERTY, "E");
		append(builder, project, TITANFlagsOptionsData.IGNORE_UNTAGGED_ON_TOP_LEVEL_UNION_PROPERTY, "N");
		append(builder, project, TITANFlagsOptionsData.ENABLE_LEGACY_ENCODING_PROPERTY, "e");
		append(builder, project, TITANFlagsOptionsData.DISABLE_USER_INFORMATION_PROPERTY, "D");
		append(builder, project, TITANFlagsOptionsData.ENABLE_REALTIME, "I");
		append(builder, project, TITANFlagsOptionsData.FORCE_GEN_SEOF, "F");
		append(builder, project, TITANFlagsOptionsData.ENABLE_OOP, "k");
		append(builder, project, TITANFlagsOptionsData.ACTIVATE_DEBUGGER_PROPERTY, "n");

		if (builder.length() > 0) {
			builder.insert(0, '-');
		}

		return builder.toString();
	}

	public static String getTITANFlagComments(final IProject project, final boolean useRuntime2) {
		final StringBuilder builder = new StringBuilder();

		if (useRuntime2) {
			builder.append("function test runtime");
		} else {
			builder.append("load test runtime");
		}

		append(builder, project, TITANFlagsOptionsData.DISABLE_BER_PROPERTY, " + disable BER");
		append(builder, project, TITANFlagsOptionsData.DISABLE_RAW_PROPERTY, " + disable RAW");
		append(builder, project, TITANFlagsOptionsData.DISABLE_TEXT_PROPERTY, " + disable TEXT");
		append(builder, project, TITANFlagsOptionsData.DISABLE_XER_PROPERTY, " + disable XER");
		append(builder, project, TITANFlagsOptionsData.DISABLE_JSON_PROPERTY, " + disable JSON");
		append(builder, project, TITANFlagsOptionsData.DISABLE_OER_PROPERTY, " + disable OER");
		append(builder, project, TITANFlagsOptionsData.FORCE_XER_IN_ASN1_PROPERTY, " + enable basic XER in ASN.1");
		append(builder, project, TITANFlagsOptionsData.DEFAULT_AS_OMIT_PROPERTY, " + treat default fields as omit");
		append(builder, project, TITANFlagsOptionsData.FORCE_OLD_FUNC_OUT_PAR_PROPERTY,
				" + enforce legacy behaviour for \"out\" parameters");
		append(builder, project, TITANFlagsOptionsData.GCC_MESSAGE_FORMAT_PROPERTY,
				" + emulate GCC error/warning message format");
		append(builder, project, TITANFlagsOptionsData.LINE_NUMBERS_ONLY_IN_MESSAGES_PROPERTY,
				" + line numbers only in messages");
		append(builder, project, TITANFlagsOptionsData.INCLUDE_SOURCEINFO_PROPERTY, " + include source info");
		append(builder, project, TITANFlagsOptionsData.ADD_SOURCELINEINFO_PROPERTY,
				" + add source line info for logging");
		append(builder, project, TITANFlagsOptionsData.SUPPRESS_WARNINGS_PROPERTY, " + suppress warnings");
		append(builder, project, TITANFlagsOptionsData.QUIETLY_PROPERTY, " + suppress all messages");
		append(builder, project, TITANFlagsOptionsData.DISABLE_SUBTYPE_CHECKING_PROPERTY," + disable subtype checking");
		append(builder, project, TITANFlagsOptionsData.DISABLE_ATTRIBUTE_VALIDATION_PROPERTY,
				" + disable attribute validation");
		append(builder, project, TITANFlagsOptionsData.ALLOW_OMIT_IN_VALUELIST_TEMPLATE_PROPERTY,
				" + allow omit in template value lists");
		append(builder, project, TITANFlagsOptionsData.WARNINGS_FOR_BAD_VARIANTS_PROPERTY,
				" + warnings for unrecognized encodings");
		append(builder, project, TITANFlagsOptionsData.IGNORE_UNTAGGED_ON_TOP_LEVEL_UNION_PROPERTY,
				" + ignore UNTAGGED encoding instruction on top level unions");
		append(builder, project, TITANFlagsOptionsData.ENABLE_LEGACY_ENCODING_PROPERTY, " + enable legacy encoding");
		append(builder, project, TITANFlagsOptionsData.DISABLE_USER_INFORMATION_PROPERTY,
				" + disable user information and timestamp in header");
		append(builder, project, TITANFlagsOptionsData.ENABLE_REALTIME, " + enable realtime testing feature");
		append(builder, project, TITANFlagsOptionsData.FORCE_GEN_SEOF, " + force generation of setofs");
		append(builder, project, TITANFlagsOptionsData.ENABLE_OOP, " + enable OOP");
		append(builder, project, TITANFlagsOptionsData.ACTIVATE_DEBUGGER_PROPERTY, " + activate debugger");

		return builder.toString();
	}

	/**
	 * Remove the TITAN provided attributes from a project.
	 * @param project the project to remove the attributes from.
	 */
	public static void removeTITANAttributes(final IProject project) {
		for (final String property : PROPERTIES) {
			try {
				project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, property), null);
			} catch (CoreException e) {
				ErrorReporter.logExceptionStackTrace("While removing attributes of `" + project.getName() + "'", e);
			}
		}
	}

	/**
	 * Loads and sets the Makefile related settings contained in the XML
	 * tree for this project.
	 * 
	 * @see ProjectFileHandler#loadProjectSettings()
	 * @see ProjectBuildPropertyData#loadMakefileSettings(Node, IProject,
	 *      HashSet)
	 * 
	 * @param root the root of the subtree containing Makefile related attributes
	 * @param project the project to set the found attributes on.
	 */
	public static void loadMakefileSettings(final Node root, final IProject project) {
		final NodeList resourceList = root.getChildNodes();

		final String[] newValues = new String[TAGS.length];
		System.arraycopy(DEFAULT_VALUES, 0, newValues, 0, TAGS.length);

		for (int i = 0, size = resourceList.getLength(); i < size; i++) {
			final String name = resourceList.item(i).getNodeName();
			for (int j = 0; j < TAGS.length; j++) {
				if (TAGS[j].equals(name)) {
					newValues[j] = resourceList.item(i).getTextContent();
				}
			}
		}

		for (int i = 0; i < TAGS.length; i++) {
			final QualifiedName qualifiedName = new QualifiedName(ProjectBuildPropertyData.QUALIFIER, PROPERTIES[i]);
			try {
				final String oldValue = project.getPersistentProperty(qualifiedName);
				if (newValues[i] != null && !newValues[i].equals(oldValue)) {
					project.setPersistentProperty(qualifiedName, newValues[i]);
				}
			} catch (CoreException e) {
				ErrorReporter.logExceptionStackTrace("While loading tag `" + TAGS[i] + "' of `" + project.getName() + "'", e);
			}
		}
	}

	/**
	 * Creates an XML tree from the Makefile related settings of the project.
	 * 
	 * @see ProjectFileHandler#saveProjectSettings()
	 * @see ProjectBuildPropertyData#saveMakefileSettings(Document,
	 *      IProject)
	 * @see InternalMakefileCreationData#saveMakefileSettings(Element,
	 *      Document, IProject)
	 * 
	 * @param makefileSettings the node to use as root node
	 * @param document the document used for creating the tree nodes
	 * @param project the project to work on
	 */
	public static void saveMakefileSettings(final Element makefileSettings, final Document document, final IProject project) {
		for (int i = 0; i < TAGS.length; i++) {
			try {
				final String temp = project
						.getPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER, PROPERTIES[i]));
				if (temp != null && !DEFAULT_VALUES[i].equals(temp)) {
					final Element element = document.createElement(TAGS[i]);
					makefileSettings.appendChild(element);
					element.appendChild(document.createTextNode(temp));
				}
			} catch (CoreException e) {
				ErrorReporter.logExceptionStackTrace("While saving tag `" + TAGS[i] + "' of `" + project.getName() + "'", e);
			}
		}
	}

	/**
	 * Copies the project information related to Makefiles from the source
	 * node to the target makefileSettings node.
	 * 
	 * @see ProjectFileHandler#copyProjectInfo(Node, Node, IProject,
	 *      TreeMap, TreeMap, boolean)
	 * 
	 * @param source the node used as the source of the information.
	 * @param makefileSettings the node used as the target of the operation.
	 * @param document the document to contain the result, used to create the
	 *                XML nodes.
	 * @param saveDefaultValues whether the default values should be forced to be
	 *                added to the output.
	 */
	public static void copyMakefileSettings(final Node source, final Node makefileSettings, final Document document,
			final boolean saveDefaultValues) {
		final String[] newValues = new String[TAGS.length];
		System.arraycopy(DEFAULT_VALUES, 0, newValues, 0, TAGS.length);

		if (source != null) {
			final NodeList resourceList = source.getChildNodes();

			for (int i = 0, size = resourceList.getLength(); i < size; i++) {
				final String name = resourceList.item(i).getNodeName();
				for (int j = 0; j < TAGS.length; j++) {
					if (TAGS[j].equals(name)) {
						newValues[j] = resourceList.item(i).getTextContent();
					}
				}
			}
		}

		for (int i = 0; i < TAGS.length; i++) {
			final String temp = newValues[i];
			if (temp != null && (saveDefaultValues || !DEFAULT_VALUES[i].equals(temp))) {
				final Element node = document.createElement(TAGS[i]);
				node.appendChild(document.createTextNode(temp));

				makefileSettings.appendChild(node);
			}
		}
	}

	private static final boolean getPropertyValue(final IProject project, final String property) {
		try {
			return Boolean.parseBoolean(project.getPersistentProperty(
					new QualifiedName(ProjectBuildPropertyData.QUALIFIER, property)));
		} catch (CoreException e) {	
			ErrorReporter.logExceptionStackTrace("While getting Titan flags of `" + project.getName() + "'", e);
		}
		return false;
	}

	private static final StringBuilder append(final StringBuilder sb, final IProject project, final String property, final String flag) {
		return sb.append(getPropertyValue(project, property) ? flag : EMPTY);
	}
}
