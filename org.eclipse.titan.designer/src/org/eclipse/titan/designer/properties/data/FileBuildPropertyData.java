/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.properties.data;

import java.util.Set;
import java.util.TreeMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Handles the file specific property settings.
 * Also loading, saving of these properties from and into external formats.
 * 
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class FileBuildPropertyData {
	private static final String[] SUPPORTED_EXTENSIONS = {"ttcn", "ttcn3", "ttcnin", "ttcnpp", "asn", "asn1"};

	public static final String QUALIFIER = ProductConstants.PRODUCT_ID_DESIGNER + ".Properties.File";
	public static final String FILERESOURCEXMLNODE = "FileResource";
	public static final String FILEPATHXMLNODE = "FilePath";
	public static final String FILEPROPERTIESXMLNODE = "FileProperties";
	public static final String EXCLUDEFROMBUILDXMLNODE = "ExcludeFromBuild";
	public static final String ENABLECODESPLITTINGXMLNODE = "EnableCodeSplitting";
	public static final String EXCLUDE_FROM_BUILD_PROPERTY = "excludeFromBuild";
	public static final String ENABLE_CODE_SPLITTING_PROPERTY = "enableCodeSplitting";

	private FileBuildPropertyData() {
		// Do nothing
	}

	public static QualifiedName getQualifiedName(final String property) {
		return new QualifiedName(QUALIFIER, property);
	}

	/**
	 * Removes the TITAN related attributes from the provided file.
	 * @param file the file whose attributes are to be cleaned
	 */
	public static void removeTITANAttributes(final IFile file) {
		try {
			file.setPersistentProperty(getQualifiedName(EXCLUDE_FROM_BUILD_PROPERTY), null);
			file.setPersistentProperty(getQualifiedName(ENABLE_CODE_SPLITTING_PROPERTY), null);
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace("While removing properties of `" + file.getName() + "'", e);
		}
	}

	/**
	 * Returns the value of the provided property for the provided file.
	 * @param file the file to operate on.
	 * @param property the property to check.
	 * @return the found value of the property.
	 */
	public static boolean getPropertyValue(final IFile file, final String property) {
		try {
			return Boolean.parseBoolean(file.getPersistentProperty(getQualifiedName(property)));
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace("While getting property `" + property + "' of `" + file.getName() + "'", e);
			return false;
		}
	}

	/**
	 * Returns the value of the provided property for the provided file.
	 * @param file the file to operate on.
	 * @param property the property to check.
	 * @return the found value of the property.
	 */
	private static String getPropertyValueAsString(final IFile file, final String property) {
		return Boolean.toString(getPropertyValue(file, property));
	}

	/**
	 * Sets the provided value, on the provided resource, for the provided property.
	 * @param file the file to work on.
	 * @param property the property to change.
	 * @param value the value to set.
	 */
	public static void setPropertyValue(final IFile file, final String property, final String value) {
		setPropertyValue(file, property, Boolean.parseBoolean(value));
	}

	/**
	 * Sets the provided value, on the provided resource, for the provided property.
	 * @param file the file to work on.
	 * @param property the property to change.
	 * @param value the value to set.
	 */
	public static void setPropertyValue(final IFile file, final String property, final boolean value) {
		try {
			if (ENABLE_CODE_SPLITTING_PROPERTY.equals(property)) {
				if (hasSupportedFileExtension(file)) {
					file.setPersistentProperty(getQualifiedName(property), value ? Boolean.TRUE.toString() : null);
				}
			} else {
				file.setPersistentProperty(getQualifiedName(property), value ? Boolean.TRUE.toString() : null);
			}
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace("While setting property `" + property + "' of `" + file.getName() + "'", e);
		}
	}

	/**
	 * Checks if the provided file has it settings at the default value or not.
	 * @param file the file to check.
	 * @return {@code true} if all properties have their default values,
	 *         {@code false} otherwise.
	 */
	public static boolean hasDefaultProperties(final IFile file) {
		return !getPropertyValue(file, EXCLUDE_FROM_BUILD_PROPERTY)
				&& !getPropertyValue(file, ENABLE_CODE_SPLITTING_PROPERTY);
	}

	/**
	 * Creates an XML tree from a files properties.
	 * 
	 * @see ProjectFileHandler#saveProjectSettings()
	 * 
	 * @param document the Document to be used to create the XML nodes
	 * @param file the file whose attributes are to be used
	 * @return the root of the created XML tree
	 */
	public static Element saveFileProperties(final Document document, final IFile file) {
		final Element root = document.createElement(FILERESOURCEXMLNODE);

		final Element filePath = document.createElement(FILEPATHXMLNODE);
		filePath.appendChild(document.createTextNode(file.getProjectRelativePath().toPortableString()));
		root.appendChild(filePath);

		final Element fileProperties = document.createElement(FILEPROPERTIESXMLNODE);

		final boolean excludeFromBuildValue = getPropertyValue(file, EXCLUDE_FROM_BUILD_PROPERTY);
		if (excludeFromBuildValue) {
			final Element excludeFromBuild = document.createElement(EXCLUDEFROMBUILDXMLNODE);
			excludeFromBuild.appendChild(document.createTextNode(Boolean.TRUE.toString()));
			fileProperties.appendChild(excludeFromBuild);
		}

		final boolean enableCodeSplittingValue = getPropertyValue(file, ENABLE_CODE_SPLITTING_PROPERTY);
		if (enableCodeSplittingValue && hasSupportedFileExtension(file)) {
			final Element enableCodeSplitting = document.createElement(ENABLECODESPLITTINGXMLNODE);
			enableCodeSplitting.appendChild(document.createTextNode(Boolean.TRUE.toString()));
			fileProperties.appendChild(enableCodeSplitting);
		}

		root.appendChild(fileProperties);

		return root;
	}

	/**
	 * Load file related attributes from the provided XML tree.
	 * 
	 * @see ProjectFileHandler#loadProjectSettings()
	 * 
	 * @param node the root of the XML tree containing file attribute related informations.
	 * @param project the project to be used for finding the file
	 * @param notYetReachedFiles the set of files not yet reached. The file found at
	 *                this node will be removed from this set.
	 * @param changedResources a map of resources that changed while loading the settings
	 */
	public static void loadFileProperties(final Node node, final IProject project, final Set<IFile> notYetReachedFiles,
			final Set<IResource> changedResources) {
		final NodeList resourceList = node.getChildNodes();
		int filePathIndex = -1;
		int filePropertiesIndex = -1;
		for (int i = 0, size = resourceList.getLength(); i < size; i++) {
			final String nodeName = resourceList.item(i).getNodeName();
			if (FILEPATHXMLNODE.equals(nodeName)) {
				filePathIndex = i;
			} else if (FILEPROPERTIESXMLNODE.equals(nodeName)) {
				filePropertiesIndex = i;
			}
		}

		if (filePathIndex == -1 || filePropertiesIndex == -1) {
			return;
		}

		final String filePath = resourceList.item(filePathIndex).getTextContent();

		if (!project.exists(new Path(filePath))) {
			return;
		}

		final IFile file = project.getFile(filePath);
		if (notYetReachedFiles.contains(file)) {
			notYetReachedFiles.remove(file);
		}

		final NodeList fileProperties = resourceList.item(filePropertiesIndex).getChildNodes();
		final String oldValue = getPropertyValueAsString(file, EXCLUDE_FROM_BUILD_PROPERTY);
		final String oldValue2 = getPropertyValueAsString(file, ENABLE_CODE_SPLITTING_PROPERTY);
		boolean selectiveCodeSplitting = false;

		for (int i = 0, size = fileProperties.getLength(); i < size; i++) {
			final Node property = fileProperties.item(i);
			if (EXCLUDEFROMBUILDXMLNODE.equals(property.getNodeName())) {
				final String value = property.getTextContent();
				if (oldValue == null || !oldValue.equals(value)) {
					changedResources.add(file);
					setPropertyValue(file, EXCLUDE_FROM_BUILD_PROPERTY, value);
				}
			} else if (ENABLECODESPLITTINGXMLNODE.equals(property.getNodeName())) {
				final String value = property.getTextContent();
				if (oldValue2 == null || !oldValue2.equals(value)) {
					changedResources.add(file);
					setPropertyValue(file, ENABLE_CODE_SPLITTING_PROPERTY, value);
					selectiveCodeSplitting = true;
				}
			}
		}

		if (selectiveCodeSplitting) {
			try {
				project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
						MakefileCreationData.SELECTIVE_CODE_SPLITTING_PROPERTY), Boolean.TRUE.toString());
			} catch (CoreException e) {
				ErrorReporter.logExceptionStackTrace(e);
			}
		}
	}

	/**
	 * Sets the properties of the provided file to their default values.
	 * @param file the file whose properties should be set.
	 * @param changedResources a map of resources that changed while loading the settings.
	 */
	public static void loadDefaultProperties(final IFile file, final Set<IResource> changedResources) {
		boolean oldValue = getPropertyValue(file, EXCLUDE_FROM_BUILD_PROPERTY);
		if (!oldValue) {
			changedResources.add(file);
			setPropertyValue(file, EXCLUDE_FROM_BUILD_PROPERTY, false);
		}
		oldValue = getPropertyValue(file, ENABLE_CODE_SPLITTING_PROPERTY);
		if (!oldValue) {
			changedResources.add(file);
			setPropertyValue(file, ENABLE_CODE_SPLITTING_PROPERTY, false);
		}
	}

	/**
	 * Copies the project information related to files from the source node
	 * to the target node.
	 * 
	 * @see ProjectFileHandler#copyProjectInfo(Node, Node, IProject,
	 *      TreeMap, TreeMap, boolean)
	 * 
	 * @param sourceNode the node used as the source of the information.
	 * @param document the document to contain the result, used to create the XML nodes.
	 * @param project the project to be worked on, used to identify the folders.
	 * @param notYetReachedFiles the files not yet processed.
	 * @param saveDefaultValues whether the default values should be forced to be
	 *                added to the output.
	 * 
	 * @return the resulting target node.
	 */
	public static Node copyFileProperties(final Node sourceNode, final Document document, final IProject project,
			final Set<String> notYetReachedFiles, final boolean saveDefaultValues) {
		String filePath = null;
		boolean excludeValue = false;
		boolean splittingValue = false;

		if (sourceNode != null) {
			final NodeList resourceList = sourceNode.getChildNodes();
			int filePathIndex = -1;
			int filePropertiesIndex = -1;
			for (int i = 0, size = resourceList.getLength(); i < size; i++) {
				final String nodeName = resourceList.item(i).getNodeName();
				if (FILEPATHXMLNODE.equals(nodeName)) {
					filePathIndex = i;
				} else if (FILEPROPERTIESXMLNODE.equals(nodeName)) {
					filePropertiesIndex = i;
				}
			}

			if (filePathIndex == -1) {
				return null;
			}

			filePath = resourceList.item(filePathIndex).getTextContent();
			if (!project.exists(new Path(filePath))) {
				return null;
			}

			notYetReachedFiles.remove(filePath);
			if (filePropertiesIndex != -1) {
				final NodeList fileProperties = resourceList.item(filePropertiesIndex).getChildNodes();
				for (int i = 0, size = fileProperties.getLength(); i < size; i++) {
					final Node property = fileProperties.item(i);
					if (EXCLUDEFROMBUILDXMLNODE.equals(property.getNodeName())) {
						excludeValue = Boolean.parseBoolean(property.getTextContent());
					} else if (ENABLECODESPLITTINGXMLNODE.equals(property.getNodeName())) {
						splittingValue = Boolean.parseBoolean(property.getTextContent());
					}
				}
			}
		}

		if (!saveDefaultValues && !excludeValue && !splittingValue) {
			return null;
		}

		final Element root = document.createElement(FILERESOURCEXMLNODE);

		final Element filePathNode = document.createElement(FILEPATHXMLNODE);
		filePathNode.appendChild(document.createTextNode(filePath));
		root.appendChild(filePathNode);

		final Element fileProperties = document.createElement(FILEPROPERTIESXMLNODE);

		final Element excludeFromBuild = document.createElement(EXCLUDEFROMBUILDXMLNODE);
		excludeFromBuild.appendChild(document.createTextNode(Boolean.toString(excludeValue)));
		fileProperties.appendChild(excludeFromBuild);

		final Element enableCodeSplitting = document.createElement(ENABLECODESPLITTINGXMLNODE);
		enableCodeSplitting.appendChild(document.createTextNode(Boolean.toString(splittingValue)));
		fileProperties.appendChild(enableCodeSplitting);

		root.appendChild(fileProperties);

		return root;
	}

	/**
	 * Copies the project information related to folders from the source
	 * node to the target node. As in this special case there is no need for
	 * a source node, it purely creational operation.
	 * 
	 * @see ProjectFileHandler#copyProjectInfo(Node, Node, IProject,
	 *      TreeMap, TreeMap, boolean)
	 * 
	 * @param document the document to contain the result, used to create the XML nodes.
	 * @param file the file to use.
	 * @return the resulting target node.
	 */
	public static Node copyDefaultFileProperties(final Document document, final IFile file) {
		final Element root = document.createElement(FILERESOURCEXMLNODE);

		final Element filePath = document.createElement(FILEPATHXMLNODE);
		filePath.appendChild(document.createTextNode(file.getProjectRelativePath().toPortableString()));
		root.appendChild(filePath);

		final Element fileProperties = document.createElement(FILEPROPERTIESXMLNODE);

		final Element excludeFromBuild = document.createElement(EXCLUDEFROMBUILDXMLNODE);
		excludeFromBuild.appendChild(document.createTextNode(Boolean.FALSE.toString()));
		fileProperties.appendChild(excludeFromBuild);

		if (hasSupportedFileExtension(file)) {
			final Element enableCodeSplitting = document.createElement(ENABLECODESPLITTINGXMLNODE);
			enableCodeSplitting.appendChild(document.createTextNode(Boolean.FALSE.toString()));
			fileProperties.appendChild(enableCodeSplitting);
		}

		root.appendChild(fileProperties);

		return root;
	}

	/**
	 * Returns whether the specified file resource has supported extension or not.
	 * Supported extensions are: <code>ttcn, ttcn3, ttcnin, ttcnpp, asn, asn1</code>.
	 * @param file The file of which extension needs to be checked
	 * @return {@code true} when the extension is supported, {@code false} otherwise
	 */
	public static boolean hasSupportedFileExtension(final IFile file) {
		for (final String extension : SUPPORTED_EXTENSIONS) {
			if (extension.equals(file.getFileExtension())) {
				return true;
			}
		}
		return false;
	}
}
