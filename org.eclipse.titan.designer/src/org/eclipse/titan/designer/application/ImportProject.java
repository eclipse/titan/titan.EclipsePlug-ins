/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.application;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.wizards.TITANProjectOpener;

/**
 * Externally callable application, that takes exactly one argument:
 * the path of the project it should import to the workspace.
 * 
 * @author Adam Knapp
 * */
public final class ImportProject implements IApplication {

	private Integer closeWorkspace() {
		try {
			ResourcesPlugin.getWorkspace().save(true, null);
			return EXIT_OK;
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace("Error while closing workspace", e);
			return Integer.valueOf(-1);
		}		
	}

	@Override
	/** {@inheritDoc} */
	public Object start(IApplicationContext context) throws Exception {
		Platform.getBundle("org.eclipse.titan.designer").start();
		final String[] projectNames = (String[]) context.getArguments().get(IApplicationContext.APPLICATION_ARGS);

		if (projectNames.length != 1) {
			System.out.println("This application takes as parameter the path of the project it should import to the workspace.");
			return Integer.valueOf(-1);
		}

		IPath projectPath = new Path(projectNames[0]);
		final IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for (final IProject project : projects) {
			if (project != null && project.getName().equals(projectPath.lastSegment())) {
				System.out.println("The project with name `" + projectPath.lastSegment() + "' already exists in the workspace.");
				return closeWorkspace();
			}
		}
		try {
			final IProject project = TITANProjectOpener.open(projectPath);
			if (TITANProjectOpener.check(project)) {
				System.out.println("The project with name `" + project.getName() + "' is imported in the workspace!");
			} else {
				System.out.println("The project with name `" + project.getName() + "' is imported in the workspace, but it is not a Titan project.");
			}
		} catch (Exception e) {
			ErrorReporter.logExceptionStackTrace("Error while imporing project with name `"
					+ projectPath.lastSegment() + "' in the workspace", e);
			closeWorkspace();
			return Integer.valueOf(-1);
		}

		return closeWorkspace();
	}

	@Override
	/** {@inheritDoc} */
	public void stop() {
		// nothing to be done
	}

}
