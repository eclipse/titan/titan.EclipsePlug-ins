/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.application;

import java.net.URI;

import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.common.path.PathUtil;
import org.eclipse.titan.common.path.TITANPathUtilities;
import org.eclipse.titan.common.utils.StringUtils;
import org.eclipse.titan.designer.core.TITANJavaBuilder;
import org.eclipse.titan.designer.core.ant.AntLaunchConfigGenerator;
import org.eclipse.titan.designer.core.ant.AntScriptGenerator;
import org.eclipse.titan.designer.core.ant.CliScriptGenerator;
import org.eclipse.titan.designer.properties.data.MakefileCreationData;
import org.eclipse.titan.designer.properties.data.ProjectBuildPropertyData;

/**
 * Externally callable application, that takes exactly one argument: the name of the project it should build.
 * 
 * @author Kristof Szabados
 * */
public final class InvokeBuild implements IApplication {

	/**
	 * Turns on the automatic JAR export feature on the specified project
	 * @param project
	 */
	public void addJarBuilder(final IProject project, final String pathToJar) throws CoreException {
		String temp = pathToJar;
		final URI path = URIUtil.toURI(temp);
		final URI resolvedPath = TITANPathUtilities.resolvePathURI(temp, project.getLocation().toOSString());
		if (path.equals(resolvedPath)) {
			temp = PathUtil.getRelativePath(project.getLocation().toOSString(), temp);
		}
		project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
				MakefileCreationData.TARGET_EXECUTABLE_PROPERTY), temp);
		project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
				MakefileCreationData.DEFAULT_JAVA_TARGET_PROPERTY), MakefileCreationData.DefaultJavaTarget.EXECUTABLE.toString());
		project.setPersistentProperty(new QualifiedName(ProjectBuildPropertyData.QUALIFIER,
				MakefileCreationData.GENERATE_START_SH_SCRIPT_PROPERTY), String.valueOf(true));
		if (AntScriptGenerator.generateAndStoreBuildXML(project, false)) {
			AntLaunchConfigGenerator.createAntLaunchConfiguration(project);
			AntLaunchConfigGenerator.addAntBuilder(project);
			AntLaunchConfigGenerator.setAntBuilderEnabled(project, true);
			CliScriptGenerator.generateAndStoreScripts(project);
			project.refreshLocal(IResource.DEPTH_ONE, null);
		}
	}

	private Integer closeWorkspace() {
		try {
			ResourcesPlugin.getWorkspace().save(true, null);
			return EXIT_OK;
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace("Error while closing workspace", e);
			return Integer.valueOf(-1);
		}		
	}

	@Override
	/** {@inheritDoc} */
	public Object start(final IApplicationContext context) throws Exception {
		Platform.getBundle("org.eclipse.titan.designer").start();
		final String[] args = (String[]) context.getArguments().get(IApplicationContext.APPLICATION_ARGS);

		if (args.length % 2 != 1) {
			System.out.println("This application takes as parameter the name of the project it should build.\n"
					+ "Optionally JAR output can be specified by the `-jar path_to_jar' option.");
			return closeWorkspace();
		}

		String projectName = null, pathToJar = null;
		for (int i = 0; i < args.length; ++i) {
			if (args[i].equals("-jar") && i < args.length-1) {
				pathToJar = args[++i];
				continue;
			}
			projectName = args[i];
		}
		if (!StringUtils.isNullOrEmpty(pathToJar) && !PathUtil.isValidFilePath(pathToJar)) {
			System.out.println("The path `" + pathToJar + "' is not valid.");
			return closeWorkspace();
		}
		final IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for (final IProject project : projects) {
			if (project != null && project.getName().equals(projectName)) {
				try {
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
					if (TITANJavaBuilder.isBuilderEnabled(project)) {
						addJarBuilder(project, pathToJar);
					}
					project.build(IncrementalProjectBuilder.FULL_BUILD, new NullProgressMonitor());
				} catch (CoreException e) {
					ErrorReporter.logExceptionStackTrace(e);
				} catch (Exception e) {
					ErrorReporter.logExceptionStackTrace("Unknown error", e);
				}
				return closeWorkspace();
			}
		}

		System.out.println("The project with name `" + projectName + "' could not be found.");

		return closeWorkspace();
	}

	@Override
	/** {@inheritDoc} */
	public void stop() {
		// nothing to be done
	}
}
