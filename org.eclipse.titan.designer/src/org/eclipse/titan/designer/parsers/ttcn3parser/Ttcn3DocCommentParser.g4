parser grammar Ttcn3DocCommentParser;

/*
 ******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Parser grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 * author Adam Knapp
 */

options {
    tokenVocab=Ttcn3DocCommentLexer;
}

@header
{
    import java.lang.StringBuilder;
    import org.eclipse.core.resources.IFile;
    import org.eclipse.titan.designer.AST.*;
}

@members
{
private IFile parsedFile;
private Location commentLocation;

public void setFile(IFile file) {
    parsedFile = file;
}


public void setCommentLocation(Location location) {
    commentLocation = location;
}

/**
 * Create new location, which modified by the parser offset and line,
 * where the start and end token is the same
 * @param aToken the start and end token
 */
private Location getLocation(final Token aToken) {
	return getLocation(aToken, aToken);
}

/**
 * Create new location, which modified by the parser offset and line
 * @param aStartToken the 1st token, its line and start position will be used for the location
 *                  NOTE: start position is the column index of the tokens 1st character.
 *                        Column index starts with 0.
 * @param aEndToken the last token, its end position will be used for the location.
 *                  NOTE: end position is the column index after the token's last character.
 */
private Location getLocation(final Token aStartToken, final Token aEndToken) {
	final Token endToken = (aEndToken != null) ? aEndToken : aStartToken;
	return new Location(parsedFile, commentLocation.getLine(), commentLocation.getOffset() + aStartToken.getStartIndex(), commentLocation.getOffset() + endToken.getStopIndex() + 1);
}
}

pr_DocComment[DocumentComment documentComment]:
(   pr_BlockComment[documentComment]
|   pr_LineComments[documentComment]
)+;

pr_BlockComment[DocumentComment documentComment]:
    BLOCK_BEGIN 
    (   pr_Tag[documentComment]
    |   WS
    |   NEWLINE
    |   IDENTIFIER
    |   FREETEXT
    )*
    BLOCK_END
;

pr_LineComments[DocumentComment documentComment]:
    (   LINE
        ( pr_Tag[documentComment] )?
        NEWLINE?
    )+
;

pr_AuthorTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    AUTHOR { endcol = $AUTHOR; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addAuthor( text, getLocation($AUTHOR, endcol) );
    }
;

pr_ConfigTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    CONFIG { endcol = $CONFIG; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addConfig( text, getLocation($CONFIG, endcol) );
    }
;

pr_DescTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    DESC { endcol = $DESC; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addDesc( text, getLocation($DESC, endcol) );
    }
;

pr_ExceptionTag[DocumentComment documentComment]
@init {
    String idtext = null;
    String freetext = null;
    Token endcol = null;
}:
    EXCEPTION
    (
        WS+
        id = pr_Identifier { idtext = $id.text; endcol = $id.stop; }
    )?
    ( 
        WS+
        ft = pr_FreeText { 
            freetext = $ft.text;
            endcol = $ft.endcol;
        }
    )?
{
    documentComment.addException( idtext, freetext, getLocation($EXCEPTION, endcol) );
};

pr_MemberTag[DocumentComment documentComment]
@init {
    String idtext = null;
    String freetext = null;
    Token endcol = null;
}:
    MEMBER
    (
        WS+
        id = pr_Identifier { idtext = $id.text; endcol = $id.stop; }
    )?
    ( 
        WS+
        ft = pr_FreeText { 
            freetext = $ft.text; 
            endcol = $ft.endcol;
        }
    )?
{
    documentComment.addMember( idtext, freetext, getLocation($MEMBER, endcol) );
};

pr_ParamTag[DocumentComment documentComment]
@init {
    String idtext = null;
    String freetext = null;
    Token endcol = null;
}:
    PARAM
    (
        WS+
        id = pr_Identifier { idtext = $id.text; endcol = $id.stop; }
    )?
    (
        WS+
        ft = pr_FreeText { 
            freetext = $ft.text; 
            endcol = $ft.endcol;
        }
    )?
{
    documentComment.addParam( idtext, freetext,  getLocation($PARAM, endcol) );
};

pr_PriorityTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    PRIORITY { endcol = $PRIORITY; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addPriority( text, getLocation($PRIORITY, endcol) );
    }
;

pr_PurposeTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    PURPOSE { endcol = $PURPOSE; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addPurpose( text, getLocation($PURPOSE, endcol) );
    }
;

pr_ReferenceTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    REFERENCE { endcol = $REFERENCE; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addReference( text, getLocation($REFERENCE, endcol) );
    }
;

pr_RemarkTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    REMARK { endcol = $REMARK; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addRemark( text, getLocation($REMARK, endcol) );
    }
;

pr_RequirementTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    REQUIREMENT { endcol = $REQUIREMENT; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addRequirement( text, getLocation($REQUIREMENT, endcol) );
    }
;

pr_ReturnTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    RETURN { endcol = $RETURN; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addReturn( text, getLocation($RETURN, endcol) );
    }
;

pr_SeeTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    SEE
    (
        WS+
        id = pr_Identifier { text = $id.text; endcol = $id.stop; }
    )
    {
        documentComment.addSee( text, getLocation($SEE, endcol) );
    }
;

pr_SinceTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    SINCE { endcol = $SINCE; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addSince( text, getLocation($SINCE, endcol) );
    }
;

pr_StatusTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    STATUS { endcol = $STATUS; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addStatus( text, getLocation($STATUS, endcol) );
    }
;

pr_UrlTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    URL { endcol = $URL; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addUrl( text, getLocation($URL, endcol) );
    }
;

pr_VerdictTag[DocumentComment documentComment]
@init {
    String idtext = null;
    String freetext = null;
    Token endcol = null;
}:
    VERDICT
    (
        WS+
        id = pr_Identifier { idtext = $id.text; endcol = $id.stop; } 
    )
    ( ft = pr_FreeText {
        freetext = $ft.text;
        endcol = $ft.endcol;
    } )?
{
    documentComment.addVerdict( idtext, freetext, getLocation($VERDICT, endcol) );
};

pr_VersionTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    VERSION { endcol = $VERSION; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addVersion( text, getLocation($VERSION, endcol) );
    }
;

/*
 * Non-standard tags
 */
pr_CategoryTag[DocumentComment documentComment]
@init {
    Token endcol = null;
    String text = "";
}:
    CATEGORY { endcol = $CATEGORY; }
    ( WS+ txt = pr_FreeText { text = $txt.text; endcol = $txt.stop; })?
    { 
        documentComment.addCategory( text, getLocation($CATEGORY, endcol) );
    }
;

pr_Tag[DocumentComment documentComment]:
(   pr_AuthorTag[documentComment]
|   pr_ConfigTag[documentComment]
|   pr_DescTag[documentComment]
|   pr_ExceptionTag[documentComment]
|   pr_MemberTag[documentComment]
|   pr_ParamTag[documentComment]
|   pr_PriorityTag[documentComment]
|   pr_PurposeTag[documentComment]
|   pr_ReferenceTag[documentComment]
|   pr_RemarkTag[documentComment]
|   pr_RequirementTag[documentComment]
|   pr_ReturnTag[documentComment]
|   pr_SeeTag[documentComment]
|   pr_SinceTag[documentComment]
|   pr_StatusTag[documentComment]
|   pr_UrlTag[documentComment]
|   pr_VerdictTag[documentComment]
|   pr_VersionTag[documentComment]
// non-standard tags
|	pr_CategoryTag[documentComment]
);

pr_Identifier returns[String text]:
    (   IDENTIFIER { $text = $IDENTIFIER.getText(); }
    |   WS? NEWLINE { $text = ""; }
    )
;

pr_FreeText returns[String text, Token endcol]
@init {
    StringBuilder sb = new StringBuilder();
    $endcol = null;   
}:
    ( 
        WS?
        (   IDENTIFIER { sb.append( $IDENTIFIER.getText()); $endcol = $IDENTIFIER; } 
        |   FREETEXT { sb.append( $FREETEXT.getText()); $endcol = $FREETEXT; } 
        )
        ( WS { sb.append( $WS.getText() ); } )?
        ( NEWLINE { sb.append('\n'); })?
        ( WS { sb.append(' '); } )?
        LINE?
    )+
{
    $text = sb.toString();
}
;