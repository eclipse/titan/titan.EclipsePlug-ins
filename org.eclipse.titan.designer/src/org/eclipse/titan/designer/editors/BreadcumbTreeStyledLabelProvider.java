/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * Label provider for editor breadcrumb popup viewers
 * 
 * @author Miklos Magyari
 *
 */
public class BreadcumbTreeStyledLabelProvider extends DelegatingStyledCellLabelProvider {
	public BreadcumbTreeStyledLabelProvider() {
		super(new IStyledLabelProvider() {
			@Override
			public void addListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void dispose() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean isLabelProperty(Object element, String property) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void removeListener(ILabelProviderListener listener) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public StyledString getStyledText(Object element) {
				if (element instanceof BreadcrumbTreeItem) {
					final BreadcrumbTreeItem item = (BreadcrumbTreeItem)element;
					if (item.isDirectory()) {
						
					}
					return new StyledString(item.getText());
				}
				return new StyledString();
			}

			@Override
			public Image getImage(Object element) {
				if (element instanceof BreadcrumbTreeItem) {
					final BreadcrumbTreeItem item = (BreadcrumbTreeItem)element;
					if (item.isDirectory()) {
						return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
					}
					if (item.getText().matches(".*[.][Tt][Tt][Cc][Nn]3?$")) {
						return ImageCache.getImage("ttcn.gif");
					}
					if (item.getText().matches(".*[.][Tt][Tt][Cc][Nn][Pp][Pp]$")) {
						return ImageCache.getImage("ttcnpp.gif");
					}
					if (item.getText().matches(".*[.][Aa][Ss][Nn]1?$")) {
						return ImageCache.getImage("asn.gif");
					}
				}
				return null;
			}
			
		});
	}

	@Override
	protected void erase(final Event event, final Object element) {
		if ((event.detail & SWT.SELECTED) != 0) {
			event.detail &= ~SWT.SELECTED;

			Rectangle bounds = event.getBounds();
			final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			final RGB rgb = StringConverter.asRGB(
				store.getString(PreferenceConstants.COLOR_PREFIX + PreferenceConstants.STYLED_SELECTION_INACTIVE_BACKGROUND), null);		
			
			final Color bgColor = new Color(PlatformUI.getWorkbench().getDisplay(), rgb);
			event.gc.setBackground(bgColor);
			event.gc.fillRectangle(bounds);
		}

		super.erase(event, element);
	}

	@Override
	protected void measure(final Event event, final Object element) {
		event.detail &= ~SWT.SELECTED;
		super.measure(event, element);
	}

	@Override
	protected void paint(final Event event, final Object element) {
		event.detail &= ~SWT.SELECTED;
		super.paint(event, element);
	}
}
