/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.io.File;

/**
 * This class represents an item of the breadcrumb tree view
 *  
 * @author Miklos Magyari
 *
 */
public class BreadcrumbTreeItem {
	/** Text to be displayed for the node in the TreeView */
	private String text;
	
	/** Node-dependent data */
	private Object data;

	public BreadcrumbTreeItem(String text, Object data) {
		this.text = text;
		this.data = data;
	}
	
	public boolean isDirectory() {
		if (data instanceof File) {
			return ((File)data).isDirectory();
		}
		
		return false;
	}
	
	public String getText() {
		return text;
	}
	
	public Object getData() {
		return data;
	}
}
