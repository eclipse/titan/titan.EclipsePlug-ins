/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;

/**
 * Optionally trim trailing whitespace for the currently saved document
 * 
 * @author Miklos Magyari
 */
public class RemoveTrailingWsOnSave {
	public static void doRemove(IDocument doc) {
		if (doc == null) {
			return;
		}
		final boolean isTrim = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.REMOVETRAILINGWS);
		if (isTrim) {
			if (doc != null) {
				final String delim = TextUtilities.getDefaultLineDelimiter(doc);
				FindReplaceDocumentAdapter frAdapter = new FindReplaceDocumentAdapter(doc);
				IRegion region = null;
				do {
					try {
						region = frAdapter.find(0, "[ \t]+" + delim, true, false, false, true);
						if (region != null) {
							frAdapter.replace(delim, true);
						}
					} catch (BadLocationException e) {
						break;
					}
				} while (region != null);
			} 
		}
	}
}
