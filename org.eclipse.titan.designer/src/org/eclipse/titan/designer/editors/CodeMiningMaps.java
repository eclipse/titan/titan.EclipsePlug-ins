/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.collections.api.tuple.Triple;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.collections.impl.tuple.Tuples;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.Position;
import org.eclipse.swt.graphics.Image;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NULL_Location;

/**
 * This class keeps tracks of code mining annotations
 * 
 * @author Miklos Magyari
 *
 */
public final class CodeMiningMaps {
	public enum CodeMiningType { Header, Content, LineEnd }
	
	//* file based map of code minings
	private static Map<String,IntObjectHashMap<Triple<String,Image,Object>>> contentCodeMinings;
	private static Map<String,IntObjectHashMap<Triple<String,Image,Object>>> headerCodeMinings;
	
	static {
		contentCodeMinings = new HashMap<>();
		headerCodeMinings = new HashMap<>();
	}

	private static synchronized IntObjectHashMap<Triple<String,Image,Object>> getCodeMiningMap(CodeMiningType type, String key) {
		final IntObjectHashMap<Triple<String,Image,Object>> existing = type == CodeMiningType.Header ? headerCodeMinings.get(key) : contentCodeMinings.get(key);
		if (existing != null) {
			return existing;
		}
		IntObjectHashMap<Triple<String,Image,Object>> newmap =
				new IntObjectHashMap<Triple<String,Image,Object>>();
		if (type == CodeMiningType.Header) {
			headerCodeMinings.put(key, newmap);
		} else {
			contentCodeMinings.put(key, newmap);
		}
		
		return newmap;
	}
	
	private static synchronized void setCodeMiningMap(CodeMiningType type, String filename, IntObjectHashMap<Triple<String,Image,Object>> newmap) {
		if (type == CodeMiningType.Header) {
			headerCodeMinings.put(filename, newmap);
		} else {
			contentCodeMinings.put(filename, newmap);
		}
	}
	
	public static synchronized void addCodeMining(CodeMiningType type, String file, int offset, String text, Image image, Object data) {
		IntObjectHashMap<Triple<String,Image,Object>> miningMap = getCodeMiningMap(type, file);
		if (miningMap == null) {
			return;
		}
		miningMap.asSynchronized().put(offset, Tuples.triple(text, image, data));
	}
	
	public static synchronized IntObjectHashMap<Triple<String,Image,Object>> getCodeMinings(CodeMiningType type, final IFile file) {
		if (file == null) {
			return null;
		}
		return getCodeMiningMap(type, file.getFullPath().toOSString());
	}
	
	/**
	 * Updates the code mining maps based on the changes in the document
	 * 
	 * @param filename name of the file being updated
	 * @param event class containing the properties of the applied update
	 * @param removedNL number of newline characters removed by the update
	 */
	public static synchronized void update(String filename, DocumentEvent event, int removedNL) {
		IntObjectHashMap<Triple<String,Image,Object>> existing = contentCodeMinings.get(filename);
		final IntObjectHashMap<Triple<String,Image,Object>> tempMap =
				new IntObjectHashMap<Triple<String,Image,Object>>();
		final int change = event.getText().length() - event.fLength; 
		if (existing != null) {
			existing.forEachKeyValue((int key, Triple<String,Image,Object> value) -> {
				if (key < event.fOffset || key > event.fOffset + event.fLength) {
					tempMap.put(key < event.fOffset ? key : key + change, value);
				}
			});
		}
		setCodeMiningMap(CodeMiningType.Content, filename, tempMap);
		
		long replCount = event.fText.chars().filter(ch -> ch == '\n').count();
		if (event.fLength > 0) {
			replCount -= removedNL;
		}
		int intCount = (int)replCount;
		existing = headerCodeMinings.get(filename);
		final IntObjectHashMap<Triple<String,Image,Object>> tempMap2 =
				new IntObjectHashMap<Triple<String,Image,Object>>();
		if (existing != null) {
			existing.forEachKeyValue((int key, Triple<String,Image,Object> value) -> {
					try {
						if (event.fDocument.getLineOfOffset(event.fOffset) < key) {
							tempMap.put(key + intCount, value);
						} else {
							tempMap.put(key, value);
						}
					} catch (BadLocationException e) { }
			});
		}
		setCodeMiningMap(CodeMiningType.Header, filename, tempMap2);
	}
	
	public static void removeCodeMining(CodeMiningType type, Location location) {
		if (location == NULL_Location.INSTANCE) {
			return;
		}
		final String file = location.getFile().getFullPath().toOSString();
		removeCodeMining(type, file, location.getLine());
	}
	
	public static void removeCodeMining(CodeMiningType type, String key, int line) {
		final IntObjectHashMap<Triple<String,Image,Object>> miningMap = getCodeMiningMap(type, key);
		if (miningMap == null) {
			return;
		}
		miningMap.asSynchronized().remove(line);
	}
}
