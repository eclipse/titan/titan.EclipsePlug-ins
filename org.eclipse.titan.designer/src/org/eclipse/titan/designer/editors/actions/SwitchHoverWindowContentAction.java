/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

/**
 * Used to provide hotkey for switching between hover window content types, i.e. code comment and code peek 
 * 
 * @author Adam Knapp
 * */
public class SwitchHoverWindowContentAction extends AbstractHandler implements IEditorActionDelegate {
	private static final String COMMAND_ID = "org.eclipse.titan.designer.editors.SwitchHoverWindowContentAction";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		switchHoverWindowContent();
		return null;
	}

	public static String getCommandId() {
		return COMMAND_ID;
	}

	@Override
	public void run(IAction action) {
		switchHoverWindowContent();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// Do nothing
	}

	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// Do nothing
	}

	private void switchHoverWindowContent() {
		final HoverContentType storedType = HoverContentType.loadAsProperty();
		final HoverContentType[] hoverContentTypes = HoverContentType.asArray();
		int i = 0;
		for (; i < hoverContentTypes.length; i++) {
			if (storedType == hoverContentTypes[i]) {
				i++;
				break;
			}
		}
		if (i == hoverContentTypes.length) {
			i = 0;
		}
		HoverContentType.storeAsProperty(hoverContentTypes[i]);
	}
}
