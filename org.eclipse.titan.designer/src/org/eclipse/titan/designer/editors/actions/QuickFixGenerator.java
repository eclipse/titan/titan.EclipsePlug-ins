/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.actions;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.titan.designer.editors.controls.HoverProposal;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator;


public final class QuickFixGenerator implements IMarkerResolutionGenerator {
	public IMarkerResolution[] getResolutions(IMarker mk) {
		try {
			Object proposalList = mk.getAttribute(HoverProposal.PROPOSAL);
			if (proposalList instanceof IMarkerResolution[]) {
				return (IMarkerResolution[]) proposalList;
			} else {
				return new IMarkerResolution[0];
			}
		}
		catch (CoreException e) {
			return new IMarkerResolution[0];
		}
	}
}
