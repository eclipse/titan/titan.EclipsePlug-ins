/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;

/**
 * This class takes care of the toggling the breadcrumb
 *
 * @author Adam Knapp
 * @author Miklos Magyari
 */
public final class ToggleBreadcrumb extends AbstractHandler {
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		final boolean isEnabled = store.getBoolean(PreferenceConstants.ENABLE_BREADCRUMB);
		store.setValue(PreferenceConstants.ENABLE_BREADCRUMB, isEnabled ? false : true);
		
		return null;
	}
}
