/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jgit.api.BlameCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.diff.Edit.Type;
import org.eclipse.jgit.diff.EditList;
import org.eclipse.jgit.dircache.DirCacheIterator;
import org.eclipse.jgit.lib.Config;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.patch.FileHeader;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.FileTreeIterator;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

/**
 * Git helper utilities
 * 
 * @author Miklos Magyari
 *
 */
public class GitUtilities {
	private static Image historyIcon = null;
	
	/**
	 * Gets commit information for a given file/line (using 'git blame')
	 * 
	 * @param file
	 * @param line
	 * @return
	 */
	public static String getCommitInfo(IFile file, int line) {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();  
		final File workspaceDirectory = workspace.getRoot().getLocation().toFile();
		final String parentDir = file.getParent().getFullPath().toOSString();
		final String gitPath = workspaceDirectory.getPath() + File.separator + parentDir + File.separator + ".git";
		try {
			final Git git = Git.open(new File(gitPath));
			final Repository repository = git.getRepository();
			if (repository == null) {
				return null;
			}
			final Config config = repository.getConfig();
			final String email = config.getString("user", null, "email");
			AbstractTreeIterator treeIterator = new FileTreeIterator(repository);
			AbstractTreeIterator cacheIterator = new DirCacheIterator(repository.readDirCache());
			EditList listOfEdits = null;
			try (DiffFormatter diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE)) {
				  diffFormatter.setRepository(git.getRepository());
				  List<DiffEntry> diffEntries = diffFormatter.scan(treeIterator, cacheIterator);
				  DiffEntry currentDiffEntry = null;
				  for (int i = 0; i < diffEntries.size(); i++) {
					  if (diffEntries.get(i).getNewPath().equals(file.getName())) {
						  currentDiffEntry = diffEntries.get(i);
						  break;
					  }
				  }
				  if (currentDiffEntry != null) {
					  FileHeader fileHeader = diffFormatter.toFileHeader(currentDiffEntry);
					  listOfEdits = fileHeader.toEditList();
					  for (Edit edit : listOfEdits) {
						  if (edit.getType() == Type.REPLACE) {
							  if (line == edit.getBeginA() && line == edit.getBeginB())
								  return "You, uncommitted change";
						  }
					  }
				  }
			} catch( Exception e) {}

			BlameCommand bc = new BlameCommand(repository);
			ObjectId head = repository.resolve("HEAD");
			if (head == null) {
				return null;
			}
			bc.setStartCommit(head);
			bc.setFilePath(file.getName());
			BlameResult lines = bc.call();
			if (lines == null) {
				return null;
			}
			PersonIdent author = lines.getSourceAuthor(line);
			String modifiedBy = null;
			if (author.getEmailAddress().equals(email)) {
				modifiedBy = "You";
			} else {
				modifiedBy = author.getName();
			}
			RevCommit commit = lines.getSourceCommit(line);
			return "[" + modifiedBy + ", " + timeAgo(author.getWhen()) + "  " + commit.getShortMessage() + "]";
		} catch (IOException | GitAPIException e) {
			
		}
		return null;
	}
	
	/**
	 * Translate commit date to a more human friendly time difference
	 * (like "3 days ago" or "2 months ago"  
	 * 
	 * @param date
	 * @return
	 */
	private static String timeAgo(final Date date) {
		final LocalDate localDate = Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		final LocalDate now = LocalDate.now();
		if (localDate.isAfter(now.minusDays(1))) {
			return date.toString();
		}
		if (localDate.isAfter(now.minusDays(30))) {
			final long untilDays = localDate.until(now, ChronoUnit.DAYS);
			return untilDays + (untilDays == 1 ? " day" : " days") + " ago ";
		}
		if (localDate.isAfter(now.minusMonths(12))) {
			final long untilMonths = localDate.until(now, ChronoUnit.MONTHS);
			return untilMonths + (untilMonths == 1 ? " month" : " months") + " ago ";
		}
		
		final long untilYears = localDate.until(now, ChronoUnit.YEARS);
		return untilYears + (untilYears == 1 ? " year" : " years") + " ago ";
	}
	
	public static Image getHistoryIconImage() {
		if (historyIcon != null) {
			return historyIcon;
		}
		historyIcon = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_COPY);
		return historyIcon;
	}
}
