/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.ui.IEditorPart;

/**
 * This class tries to find out the context for the current cursor position
 * It can be used to provide more suitable content assist proposals
 *
 * @author Miklos Magyari
 * @author Adam Knapp
 * @author Arpad Lovassy
 */
public class CompletionFinder {

	// Regex fragments (used by other regexes)
	private static final String IDENTIFIER = "([A-Za-z][A-Za-z0-9_]*)";
	private static final String IDENTIFIERWITHDOT = "([A-Za-z][A-Za-z0-9_.]*)";
	private static final String ASSIGNOP = ":=";
	private static final String ATTRIBUTES = "([^;]+;)*";
	private static final String ATTRIBUTE_KEYWORD = "(encode|display|extension|variant|optional)";
	private static final String ATTRIBUTE_MODIFIER = "(override|\\@local)";
	private static final String ENCODE_DECODE_TYPE = "(BER|PER|XER|RAW|TEXT|JSON|OER)";
	private static final String DEFINITION = "\\s+" + IDENTIFIER + "\\s+" + IDENTIFIER + "\\s*" + ASSIGNOP;
	private static final String OPT_PARAMETER = "\\s*(\\([^)]*\\))?";
	// 1 group
	private static final String WITH_ATTRIBUTE_START = "with\\s*\\{\\s*" + ATTRIBUTES + "\\s*";
	// matches only to simple cases, because syntax of extension attribute can be very different
	// 3 groups
	private static final String EXTENSION_ATTRIBUTE =
									"((prototype|printing)\\s*\\(\\s*[a-z]+\\s*\\)" +
									"|transparent|internal|address|provider|done" +
									"|(encode|decode)\\s*\\(\\s*[A-Z]+\\s*(\\:\\s*[A-Z_]+\\s*)?\\)" +
									")";
	// 4 groups
	private static final String EXTENSION_ATTRIBUTES = "(" + EXTENSION_ATTRIBUTE + "\\s+)*";
	// 6 groups
	private static final String WITH_ATTRIBUTE_EXTENSION_START = WITH_ATTRIBUTE_START + "extension\\s*" +
									ATTRIBUTE_MODIFIER + "?\\s*\\\"\\s*" + EXTENSION_ATTRIBUTES;
	// 2 groups
	private static final String WITH_ATTRIBUTE_VARIANT_START = WITH_ATTRIBUTE_START + "variant\\s*" +
									ATTRIBUTE_MODIFIER + "?\\s*\\\"\\s*";

	// Regexes of the different proposal context cases
	private static final String ASSIGNMENT = "(var|const)" + DEFINITION + "\\s*(" + IDENTIFIER + ")?$";
	private static final String SINGLEASSIGNMENT = "()()\\s+" + IDENTIFIERWITHDOT + "\\s*" + ASSIGNOP + "\\s*(" + IDENTIFIERWITHDOT + ")?$";
	private static final String FRIEND = "friend[\\s]+module[\\s]*(.*)";
	private static final String IMPORT = "import[\\s]+from([\\s]*$|[\\s+]+(" + IDENTIFIER + "))";
	private static final String COMPONENT = "(system|mtc|runs[\\s]+on)((\\s+" + IDENTIFIER + "$)|\\s*)";
	private static final String PARAMETER = "\\s*\\(([^)]*$)";
	private static final String SETVERDICT = "setverdict" + PARAMETER;
	private static final String EXECUTE = "execute" + PARAMETER;
	private static final String FUNCTION_MODIFIER = "function\\s+\\@" + IDENTIFIER + "?$";
	private static final String FUNCTION_PARAM_MODIFIER = "function\\s+(\\@([a-z]+)\\s+)?" + IDENTIFIER +
														"\\s*\\(\\s*([^,]*,\\s*)*\\@" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_PROTOTYPE = WITH_ATTRIBUTE_EXTENSION_START +
									"prototype\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_PRINTING = WITH_ATTRIBUTE_EXTENSION_START +
									"printing\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE = WITH_ATTRIBUTE_EXTENSION_START +
									"(encode|decode)\\s*\\(\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE_OPTION = WITH_ATTRIBUTE_EXTENSION_START +
									"(encode|decode)\\s*\\(\\s*" + ENCODE_DECODE_TYPE + "\\s*\\:\\s*" + IDENTIFIER + "?$";
	// 4 groups
	private static final String WITH_ATTRIBUTE_VARIANT_OPTION = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER +
									"\\s*\\(\\s*" + IDENTIFIER +"?$";
	// 4 groups
	private static final String WITH_ATTRIBUTE_VARIANT_COLON = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER +
									"\\s*\\:\\s*" + IDENTIFIER +"?$";
	private static final String FUNCTION_PARAM = "(" + IDENTIFIER + OPT_PARAMETER + "\\s*\\.)+\\s*"
												+ IDENTIFIER + "\\s*\\(\\s*([^,]*,\\s*)*" + IDENTIFIER + "?$";
	private static final String PARAMETERIZED = IDENTIFIER + PARAMETER;
	private static final String CLASS_MODIFIER = "type\\s+class\\s+\\@" + IDENTIFIER + "?$";
	private static final String CLASS_EXTENDS = "type\\s+class\\s+(\\@([a-z]+)\\s+)?" + IDENTIFIER + "\\s+extends\\s+(" + IDENTIFIER + "(\\s*,\\s*" + IDENTIFIER + ")*)?";
	private static final String ALL_ANY = "(all|any)\\s*(component|port|timer)\\s*\\.\\s*" + IDENTIFIER + "?$";
	private static final String DOT_NOTATION_CHAIN = "((" + IDENTIFIER + OPT_PARAMETER + "\\s*\\.)*\\s*"
												+ IDENTIFIER + OPT_PARAMETER + ")\\s*\\.\\s*"
												+ IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_ENCODE_TYPE = WITH_ATTRIBUTE_START + "encode\\s*" + ATTRIBUTE_MODIFIER +
									"?\\s*\\\"" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_EXTENSION = WITH_ATTRIBUTE_EXTENSION_START + IDENTIFIER + "?$";
	// 3 groups
	//TODO: variant attribute van start with number, so change IDENTIFIER to something more general
	private static final String WITH_ATTRIBUTE_VARIANT = WITH_ATTRIBUTE_VARIANT_START + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_KEYWORD = "with\\s*\\{\\s*" + IDENTIFIER + "?$";
	private static final String WITH_ATTRIBUTE_MODIFIER = WITH_ATTRIBUTE_START + ATTRIBUTE_KEYWORD + "\\s*\\@?" +
									IDENTIFIER + "?$";

	private enum SpecialContext { None, SimpleComment, DocComment, StringLiteral }

	// Note: AllAny must be before Operation, because strings that matches to AllAny pattern also matches to Operation pattern
	//       FunctionParamModifier must be before Parameterized
	//       WithAttributeExtensionPrototype and WithAttributeExtensionPrinting must be before Parameterized
	private enum ContextType { Assignment, Component, Doccomment, Friend, Import, Setverdict, Execute,
		FunctionModifier, FunctionParamModifier,  WithAttributeExtensionPrototype, WithAttributeExtensionPrinting,
		WithAttributeExtensionEncodeDecode, WithAttributeExtensionEncodeDecodeOption,
		WithAttributeVariantOption, WithAttributeVariantColon,
		FunctionParam, Parameterized, ClassModifier, ClassExtends,
		AllAny, DotNotationChain, WithAttributeEncodeType, WithAttributeExtension, WithAttributeVariant,
		WithAttributeKeyword, WithAttributeModifier }

	private IEditorPart editor;
	private Module module;
	private Scope scope;
	private IDocument document;
	private IFile file;
	private int offset;

	/** collection of context tokens */
	private List<String> tokenList = new ArrayList<>();

	/** collection of whitespace strings separating context tokens */
	private List<String> wsList = new ArrayList<>();

	private static Map<Pattern,ContextType> contextMap = new LinkedHashMap<Pattern, ContextType>();

	static {
		contextMap.put(Pattern.compile(COMPONENT), ContextType.Component);
		contextMap.put(Pattern.compile(ASSIGNMENT), ContextType.Assignment);
		contextMap.put(Pattern.compile(SINGLEASSIGNMENT), ContextType.Assignment);
		contextMap.put(Pattern.compile(IMPORT), ContextType.Import);
		contextMap.put(Pattern.compile(FRIEND), ContextType.Friend);
		contextMap.put(Pattern.compile(SETVERDICT), ContextType.Setverdict);
		contextMap.put(Pattern.compile(EXECUTE), ContextType.Execute);
		contextMap.put(Pattern.compile(FUNCTION_MODIFIER), ContextType.FunctionModifier);
		contextMap.put(Pattern.compile(FUNCTION_PARAM_MODIFIER), ContextType.FunctionParamModifier);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_EXTENSION_PROTOTYPE), ContextType.WithAttributeExtensionPrototype);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_EXTENSION_PRINTING), ContextType.WithAttributeExtensionPrinting);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE), ContextType.WithAttributeExtensionEncodeDecode);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_EXTENSION_ENCODE_DECODE_OPTION), ContextType.WithAttributeExtensionEncodeDecodeOption);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_VARIANT_OPTION), ContextType.WithAttributeVariantOption);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_VARIANT_COLON), ContextType.WithAttributeVariantColon);
		contextMap.put(Pattern.compile(FUNCTION_PARAM), ContextType.FunctionParam);
		contextMap.put(Pattern.compile(PARAMETERIZED), ContextType.Parameterized);
		contextMap.put(Pattern.compile(CLASS_MODIFIER), ContextType.ClassModifier);
		contextMap.put(Pattern.compile(CLASS_EXTENDS), ContextType.ClassExtends);
		contextMap.put(Pattern.compile(ALL_ANY), ContextType.AllAny);
		contextMap.put(Pattern.compile(DOT_NOTATION_CHAIN), ContextType.DotNotationChain);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_ENCODE_TYPE), ContextType.WithAttributeEncodeType);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_EXTENSION), ContextType.WithAttributeExtension);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_VARIANT), ContextType.WithAttributeVariant);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_KEYWORD), ContextType.WithAttributeKeyword);
		contextMap.put(Pattern.compile(WITH_ATTRIBUTE_MODIFIER), ContextType.WithAttributeModifier);
	}

	public CompletionFinder(Module module) {
		this.module = module;
	}

	public ProposalContext findOnLeft(final IEditorPart editor, int offset, final IDocument document, final IFile file) {
		this.editor = editor;
		this.document = document;
		this.file = file;
		this.offset = offset;
		int ofs = offset - 1;
		if (-1 == ofs) {
			return null;
		}

		final StringBuilder left = new StringBuilder();

		StringBuilder specialStringSb = new StringBuilder();
		final SpecialContext commentContext = getSpecialContext(specialStringSb);
		switch (commentContext) {
		case SimpleComment:
			return null;
		case DocComment:
			return new DocumentCommentContext(new ProposalContextInfo(file, document, offset, specialStringSb.toString(), module, null));
		default:
			break;
		}

		try {
			char c;

			String ws = getWsStringAtOffset(ofs);
			if (ws.length() > 0) {
				wsList.add(ws);
				ofs -= ws.length();
				left.insert(0, ' ');
			}

			final StringBuilder token = new StringBuilder();
			while (ofs >= 0) {
				c = document.getChar(ofs);
				if (Character.isAlphabetic(c) || Character.isDigit(c) ||
					c == ',' || c == ':' || c == '=' || c == '.' || c == '_' || c == '(' || c == ')' || c == '@' ||
					c == '{' || c == '"') {
					token.insert(0, c);
					ofs--;
					if (-1 == ofs) {
						return getContext(left.toString());
					}
				} else if (isWhiteSpace(c)) {
					ws = getWsStringAtOffset(ofs);
					if (ws.length() > 0) {
						wsList.add(ws);
						ofs -= ws.length();

						left.insert(0, token.toString());
						left.insert(0, " ");
						token.setLength(0);
					}
				} else {
					break;
				}
			}
		} catch (BadLocationException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}

		return getContext(left.toString());
	}

	private String getWsStringAtOffset(int offset) {
		final StringBuilder wslist = new StringBuilder();

		for (;;) {
			char c;
			try {
				c = document.getChar(offset);
			} catch (BadLocationException e) {
				return wslist.toString();
			}
			if (isWhiteSpace(c)) {
				wslist.append(c);
				offset--;
				if (offset == -1) {
					return wslist.toString();
				}
			} else {
				break;
			}
		}
		return wslist.toString();
	}

	private ProposalContext getContext(String s) {
		if (s == null || s.isEmpty()) {
			return null;
		}

		for (Map.Entry<Pattern,ContextType> e : contextMap.entrySet()) {
			Pattern p = e.getKey();
			Matcher m = p.matcher(s);
			if (m.find()) {
				final ProposalContextInfo info = new ProposalContextInfo(file, document, offset, s, module, m);
				switch (e.getValue()) {
				case Assignment:
					return new AssignmentContext(info);
				case Component:
					return new ComponentContext(info);
				case Friend:
					return new FriendContext(info);
				case Import:
					return new ImportContext(info);
				case Setverdict:
					return new SetverdictContext(info);
				case Execute:
					return new ExecuteContext(info);
				case FunctionModifier:
					return new FunctionModifierContext(info);
				case FunctionParamModifier:
					return new FunctionParamModifierContext(info);
				case FunctionParam:
					return new FunctionParamContext(info);
				case Parameterized:
					return new ParameterizedContext(info);
				case ClassModifier:
					return new ClassModifierContext(info);
				case ClassExtends:
					return new ClassExtendsContext(info);
				case AllAny:
					return new AllAnyContext(info);
				case DotNotationChain:
					return new DotNotationChainContext(info);
				case WithAttributeEncodeType:
					return new WithAttributeEncodeTypeContext(info);
				case WithAttributeExtension:
					return new WithAttributeExtensionContext(info);
				case WithAttributeExtensionPrototype:
					return new WithAttributeExtensionPrototypeContext(info);
				case WithAttributeExtensionPrinting:
					return new WithAttributeExtensionPrintingContext(info);
				case WithAttributeExtensionEncodeDecode:
					return new WithAttributeExtensionEncodeDecodeContext(info);
				case WithAttributeExtensionEncodeDecodeOption:
					return new WithAttributeExtensionEncodeDecodeOptionContext(info);
				case WithAttributeVariant:
					return new WithAttributeVariantContext(info);
				case WithAttributeVariantOption:
					return new WithAttributeVariantOptionContext(info);
				case WithAttributeVariantColon:
					return new WithAttributeVariantColonContext(info);
				case WithAttributeKeyword:
					return new WithAttributeKeywordContext(info);
				case WithAttributeModifier:
					return new WithAttributeModifierContext(info);
				default:
					return null;
				}
			}
		}

		return null;
	}

	private static boolean isWhiteSpace(char c) {
		return c == ' ' || c == '\t' || c == '\n' || c == '\r';
	}

	/**
	 * Calculates if the current offset is inside a comment block or a string literal
	 *
	 * @param
	 * @return
	 */
	private SpecialContext getSpecialContext(final StringBuilder specialStringSb) {
		boolean inComment = false;
		boolean inString = false;
		boolean isLineComment = false;
		boolean isDocComment = false;
		int lastStartOffset = -1;
		String docText = null;
		try {
			docText = document.get(0, offset + 1);
		} catch (BadLocationException e1) {
			return SpecialContext.None;
		}
		for (int i = 0; i < offset; i++) {
			char c, c2, c3;
			c = docText.charAt(i);
			boolean isEof = false;
			if (inString) {
				do {
					c2 = docText.charAt(i++);
					if (c2 == '\\') {
						i++;
					}
					if (i >= offset) {
						isEof = true;
					}
				} while (c2 != '"' && i < offset);
				if (isEof == false) {
					inString = false;
					i--;
				}
			} else if (inComment) {
				if (isLineComment) {
					do {
						c2 = docText.charAt(i++);
						if (i >= offset) {
							isEof = true;
						}
					} while (c2 != '\n' && i < offset);
				} else {
					do {
						c2 = docText.charAt(i++);
						if (i >= offset) {
							isEof = true;
						}
						c3 = docText.charAt(i);
					} while (i < offset && ! (c2 == '*' && c3 == '/'));
				}
				if (isEof == false) {
					inComment = false;
					i--;
				}
			} else {
				if (c == '/') {
					lastStartOffset = i;
					c2 = docText.charAt(i + 1);
					if (c2 == '*') {
						inComment = true;
						isLineComment = false;
						i++;
					} else if (c2 == '/') {
						inComment = true;
						isLineComment = true;
						i++;
					}
					if (inComment) {
						c2 = docText.charAt(i + 1);
						if (c2 == '*') {
							isDocComment = true;
							i++;
						} else {
							isDocComment = false;
						}
					}
				} else if (c == '"') {
					lastStartOffset = i;
					inString = true;
				}
			}
		}
		if (lastStartOffset != -1) {
			specialStringSb.append(docText.substring(lastStartOffset, offset));
		}
		if (inComment) {
			if (isDocComment) {
				return SpecialContext.DocComment;
			}
			return SpecialContext.SimpleComment;
		}
		if (inString) {
			return SpecialContext.StringLiteral;
		}

		return SpecialContext.None;
	}

	public List<String> getTokenList() {
		return tokenList;
	}

	public List<String> getWsList() {
		return wsList;
	}
}
