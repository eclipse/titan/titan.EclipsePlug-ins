/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute encode type
 * @author Arpad Lovassy
 */
public class WithAttributeEncodeTypeContext extends ProposalContext {

	public WithAttributeEncodeTypeContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		addProposal("BER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("PER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("XER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("RAW", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("TEXT", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("JSON", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("OER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
	}
}	
