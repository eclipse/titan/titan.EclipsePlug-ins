/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.Arrays;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for documentation comment context
 * 
 * @author Adam Knapp 
 * @author Arpad Lovassy
 */
public class DocumentCommentContext extends ProposalContext {

	private static final String[] HTML_TAGS = { "<b>", "<code>", "<em>", "<li>", "<ol>", "<p>", "<pre>",
			"<strong>", "<ul>" };
	private static final String LINE_BREAK = "<br>";
	
	public DocumentCommentContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = getPrefix();
		if (!prefix.isEmpty()) {
			if (prefix.startsWith("<")) {
				addHtmlTagProposals(propCollector, prefix);
			} else if (prefix.startsWith(ICommentable.AT)) {
				addDocCommentTagProposals(propCollector, prefix);
			} else {
				String tag = getTagBeforePrefix();
				if (Arrays.asList(ICommentable.DOC_COMMENT_TAGS).contains(tag)) {
					addDocCommentTagParamProposals(propCollector, tag, prefix);
				}
			}
		} else {
			String tag = getTagBeforeSpace();
			if (Arrays.asList(ICommentable.DOC_COMMENT_TAGS).contains(tag)) {
				addDocCommentTagParamProposals(propCollector, tag, "");
			}
		}
	}

	/**
	 * Returns the tag before the prefix based on the context information. Never returns {@code null}.
	 * 
	 * @return the tag before the prefix based on the context information
	 */
	private String getTagBeforeSpace() {
		if (proposalContextInfo.context != null && !proposalContextInfo.context.isEmpty()) {
			int i = proposalContextInfo.context.length()-1;
			for (; i >= 0; i--) {
				final char c = proposalContextInfo.context.charAt(i);
				if (c != ' ') {
					break;
				}
			}
			final StringBuilder sb = new StringBuilder(proposalContextInfo.context.length());
			for (; i >= 0; i--) {
				final char c = proposalContextInfo.context.charAt(i);
				if (!isPrefixChar(c)) {
					break;
				}
				sb.insert(0, c);
			}
			return sb.toString();
		} else {
			return "";
		}
	}

	/**
	 * Returns the tag before the spaces based on the context information. Never returns {@code null}.
	 * 
	 * @return the tag before the spaces based on the context information
	 */
	private String getTagBeforePrefix() {
		if (proposalContextInfo.context != null && !proposalContextInfo.context.isEmpty()) {
			int i = proposalContextInfo.context.length()-1;
			for (; i >= 0; i--) {
				final char c = proposalContextInfo.context.charAt(i);
				if (!isPrefixChar(c)) {
					break;
				}
			}
			for (; i >= 0; i--) {
				final char c = proposalContextInfo.context.charAt(i);
				if (c != ' ') {
					break;
				}
			}
			final StringBuilder sb = new StringBuilder(proposalContextInfo.context.length());
			for (; i >= 0; i--) {
				final char c = proposalContextInfo.context.charAt(i);
				if (!isPrefixChar(c)) {
					break;
				}
				sb.insert(0, c);
			}
			return sb.toString();
		} else {
			return "";
		}
	}


	@Override
	protected boolean isPrefixChar(final char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_' || c == '@' || c == '<';
	}

	private void addDocCommentTagProposals(ProposalCollector propCollector, String prefix) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		for (String tag : ICommentable.DOC_COMMENT_TAGS) {
			final CompletionProposal proposal = new CompletionProposal(tag, replacementOffset, replacementLength, 
				tag.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(tag), null, new Ttcn3HoverContent(tag + " tag"), tag.startsWith(prefix) ? PREFIX_MATCHES : OTHER);

			propCollector.addProposal(proposal);
		}

		propCollector.sortAll();
	}

	/**
	 * Adds proposals for doc comment tag parameter
	 * @param propCollector list of proposals
	 * @param tag doc comment tag
	 * @param prefix complete comment tag
	 */
	private void addDocCommentTagParamProposals(ProposalCollector propCollector, String tag, String prefix) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		switch (tag) {
		case ICommentable.STATUS_TAG:
			final String deprecatedStr = "deprecated";
			final CompletionProposal proposal = new CompletionProposal(deprecatedStr, replacementOffset, replacementLength, 
					deprecatedStr.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
					new StyledString(deprecatedStr), null, new Ttcn3HoverContent(tag + " " + deprecatedStr), deprecatedStr.startsWith(prefix) ? PREFIX_MATCHES : OTHER);
				propCollector.addProposal(proposal);
		}
		propCollector.sortAll();
	}

	private void addHtmlTagProposals(ProposalCollector propCollector, String prefix) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		for (String tag : HTML_TAGS) {
			final CompletionProposal proposal = new CompletionProposal(tag + getHtmlClosingTag(tag),
				replacementOffset, replacementLength, tag.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(tag), null, new Ttcn3HoverContent(tag + " html tag"), tag.startsWith(prefix) ? PREFIX_MATCHES : OTHER);

			propCollector.addProposal(proposal);
		}
		final CompletionProposal proposal = new CompletionProposal(LINE_BREAK, replacementOffset, replacementLength, 
				LINE_BREAK.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(LINE_BREAK), null, new Ttcn3HoverContent(LINE_BREAK + "html tag"), LINE_BREAK.startsWith(prefix) ? PREFIX_MATCHES : OTHER);
		propCollector.addProposal(proposal);

		propCollector.sortAll();
	}

	private String getHtmlClosingTag(final String tag) {
		final StringBuilder sb = new StringBuilder(tag);
		return sb.insert(1, '/').toString();
	}
}
