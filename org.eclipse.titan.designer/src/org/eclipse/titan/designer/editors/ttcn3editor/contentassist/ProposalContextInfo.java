/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;

/**
 * Helper class for storing common information
 * 
 * @author Adam Knapp 
 *
 */
public class ProposalContextInfo {
	/** File in which proposals are requested */
	public IFile file;
	/** Document in which proposals are requested */
	public IDocument document;
	/** Offset in the document, where proposals are requested */
	public int offset;
	/** The text between the offset and the last instruction */
	public String context;
	/** Module in which proposals are requested */
	public Module module;
	/** Scope in which proposals are requested */
	public Scope scope;
	/** Dot context */
	public boolean isDotContext;
	/** matcher for the regex that matched */
	public Matcher matcher;

	/** Create an unfilled object */
	public ProposalContextInfo() {}

	/**
	 * Fills the fields according to the input parameters
	 * @param file File in which proposals are requested
	 * @param document Document in which proposals are requested
	 * @param offset Offset in the document, where proposals are requested
	 * @param context The text between the offset and the last instruction
	 * @param module Module in which proposals are requested
	 */
	public ProposalContextInfo(final IFile file, final IDocument document, final int offset,
			final String context, final Module module, final Matcher matcher) {
		this.file = file;
		this.document = document;
		this.offset = offset;
		this.context = context;
		this.module = module;
		this.matcher = matcher;
		if (module != null) {
			this.scope = module.getSmallestEnclosingScope(offset); 
		}
		if (context != null) {
			isDotContext = context.endsWith(".");
		}
	}
}
