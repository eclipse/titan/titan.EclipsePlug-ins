/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Port;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Timer;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.PortTypeBody;
import org.eclipse.titan.designer.AST.TTCN3.types.PortTypeBody.OperationModes;
import org.eclipse.titan.designer.AST.TTCN3.types.Port_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Set_Seq_Choice_BaseType;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;

/**
 * Collects proposals for TTCN-3 dot notation chain of
 * <ul>
 *   <li> component operations, e.g. MyComp.create/done/stop etc.
 *   <li> port operations, e.g. MyPort.start/stop/send/receive etc.
 *   <li> timer operations, e.g. MyTimer.start/stop/read/running/timeout
 *   <li> structured data type (union/record/set) fields
 *   <li> class member variables
 *   <li> class member functions
 * </ul>
 * @author Arpad Lovassy
 */
public class DotNotationChainContext extends ProposalContext {

	public DotNotationChainContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String varName = matcher.group(1);
		// matcher.group(1) is the a structured var name with possible dots
		// matcher.group(5) is the beginning of the operation or field name, same as prefix 

		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		final Reference reference = TTCN3ReferenceAnalyzer.parseForCompletion(proposalContextInfo.file, varName);
		if (reference == null || proposalContextInfo.scope == null) {
			return;
		}

		final Assignment assignment = proposalContextInfo.scope.getAssBySRef(timestamp, reference);
		if (assignment == null) {
			return;
		}

		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		if (assignment instanceof Def_Timer) {
			// timer is not a type
			addProposal("start", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("stop", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("read", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("running", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("timeout", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("start", "start( ${seconds} );", "start( seconds )", "", propCollector, replacementOffset, replacementLength, prefix);
			propCollector.sortAll();
			return;
		}

		IType type = assignment.getType(timestamp);
		if (assignment instanceof Def_Var ||
			assignment instanceof Def_Function) {
			if (type != null) {
				if (reference.getSubreferences().size() > 1) {
					// gets the type of a dot separated structured expression
					type = type.getFieldType(timestamp, reference, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null, false);
				}
			}
			if (type instanceof Referenced_Type) {
				type = type.getTypeRefdLast(timestamp);
			}
			if (type instanceof Component_Type) {
				addItemsByType(type, propCollector, prefix, null);
			} else if (type instanceof TTCN3_Set_Seq_Choice_BaseType) {
				// union, record, set
				addItemsByType(type, propCollector, prefix, null);
			} else if (type instanceof Class_Type) {
				final Class_Type classType = (Class_Type)type;
				addClassProposals(classType, propCollector, replacementOffset, replacementLength, prefix);
			}
		} else if (assignment instanceof Def_Port) {
			if (type instanceof Port_Type) {
				final Port_Type portType = (Port_Type)type;
				addPortProposals(propCollector, prefix, portType.getPortBody());
				addItemsByType(null, Type_type.TYPE_PORT, propCollector, prefix, null);
			}
		}
	}
	
	/**
	 * Adds the port related proposals.
	 *
	 * handles the following proposals:
	 *
	 * <ul>
	 * <li>in message mode:
	 * <ul>
	 * <li>send(template), receive, trigger
	 * </ul>
	 * <li>in procedure mode:
	 * <ul>
	 * <li>call, getcall, reply, raise, getreply, catch
	 * </ul>
	 * <li>general:
	 * <ul>
	 * <li>check
	 * <li>clear, start, stop, halt
	 * </ul>
	 * </ul>
	 *
	 * @param propCollector the proposal collector.
	 * @param prefix the prefix based on the context information
	 * @param portTypeBody
	 */
	private void addPortProposals(final ProposalCollector propCollector, final String prefix, final PortTypeBody portTypeBody) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();

		final OperationModes operationMode = portTypeBody.getOperationMode();
		if (OperationModes.OP_Message.equals(operationMode) || OperationModes.OP_Mixed.equals(operationMode)) {
			addTemplateProposal("send", "send( ${templateInstance} );","send( templateInstance )", "",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("send", "send( ${templateInstance} ) to ${location};", "send( templateInstance ) to location", "",
					propCollector, replacementOffset, replacementLength, prefix);

			addReceiveProposals(propCollector, prefix, replacementOffset, replacementLength);
			addTriggerProposals(propCollector, prefix, replacementOffset, replacementLength);
		}

		if (OperationModes.OP_Procedure.equals(operationMode) || OperationModes.OP_Mixed.equals(operationMode)) {
			addTemplateProposal("call", "call( ${templateInstance} );", "call( templateInstance )", "",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("call", "call( ${templateInstance} , ${callTimer} );", "call( templateInstance , callTimer )", "with timer",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("call", "call( ${templateInstance} ) to ${location};", "call( templateInstance ) to location", "with to clause",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("call", "call( ${templateInstance} , ${callTimer} ) to ${location};", "call( templateInstance , callTimer ) to location", "with timer and to clause",
					propCollector, replacementOffset, replacementLength, prefix);

			addGetcallProposals(propCollector, prefix, replacementOffset, replacementLength);

			addTemplateProposal("reply", "reply( ${templateInstance} );","reply( templateInstance )", "",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("reply", "reply( ${templateInstance} ) to ${location};", "reply( templateInstance ) to location", "",
					propCollector, replacementOffset, replacementLength, prefix);

			addGetreplyProposals(propCollector, prefix, replacementOffset, replacementLength);

			addTemplateProposal("raise", "raise( ${signature}, ${templateInstance} );", "raise( signature, templateInstance )", "",
					propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("raise", "raise( ${signature}, ${templateInstance} ) to ${location};", "raise( signature, templateInstance ) to location", "with to clause",
					propCollector, replacementOffset, replacementLength, prefix);

			addCatchProposals(propCollector, prefix, replacementOffset, replacementLength);
		}

		addCheckProposals(propCollector, prefix, replacementOffset, replacementLength);

		addProposal("clear;", "clear", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("start;", "start", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("stop;", "stop", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("halt;", "halt", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds the "receive" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addReceiveProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("running", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("receive", "receive", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive -> value ${myVar};", "receive -> value myVar", "value redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive -> sender ${myPeer};", "receive -> sender myPeer", "sender redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive -> value ${myVar} sender ${myPeer};", "receive -> value myVar sender myPeer", "value and sender redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive from ${myPeer};", "receive from myPeer", "from clause",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive from ${myPeer} -> value ${myVar};", "receive from myPeer -> value myVar", "from clause with value redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${template} );", "receive( templateInstance )", "",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${templateInstance} ) -> value ${myVar};", "receive( templateInstance ) -> value myVar", "value redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${templateInstance} ) -> sender ${myPeer};", "receive( templateInstance ) -> sender myPeer", "sender redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${templateInstance} ) -> value ${myVar} sender ${myPeer};", "receive( templateInstance ) -> value myVar sender myPeer",
				"value and sender redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${templateInstance} ) from ${myPeer};", "receive( templateInstance ) from myPeer", "from clause",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("receive", "receive( ${templateInstance} ) from ${myPeer} -> value ${myVar};", "receive( templateInstance ) from myPeer -> value myVar",
				"from clause with value redirect",
				propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds the "trigger" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addTriggerProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("trigger", "trigger", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger -> value ${myVar};", "trigger -> value myVar", "value redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger -> sender ${myPeer};", "trigger -> sender myPeer", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger -> value ${myVar} sender ${myPeer};", "trigger -> value myVar sender myPeer", "value and sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger from ${myPeer};", "trigger from myPeer", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger from ${myPeer} -> value ${myVar};", "trigger from myPeer -> value myVar", "from clause with value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} );", "trigger( templateInstance )", "", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} ) -> value ${myVar};", "trigger( templateInstance ) -> value myVar", "value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} ) -> sender ${myPeer};", "trigger( templateInstance ) -> sender myPeer", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} ) -> value ${myVar} sender ${myPeer};", "trigger( templateInstance ) -> value myVar sender myPeer", "value and sender redirect",
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} ) from ${myPeer};", "trigger( templateInstance ) from myPeer", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("trigger", "trigger( ${templateInstance} ) from ${myPeer} -> value ${myVar};", "trigger( templateInstance ) from myPeer -> value myVar", "from clause with value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);

	}

	/**
	 * Adds the "getcall" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addGetcallProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("getcall", "getcall", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall from ${myPartner};", "getcall from myPartner", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall -> sender ${myPartnerVar};", "getcall -> sender myPartnerVar", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} );", "getcall( templateInstance )", "", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} ) from ${myPartner};", "getcall( templateInstance ) from myPartner", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} ) -> sender ${myPartnerVar};", "getcall( templateInstance ) -> sender myPartnerVar", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} ) -> param( ${parameters} );", "getcall( templateInstance ) -> param(parameters)", "parameters", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} ) from ${myPartner} -> param( ${parameters} );", "getcall( templateInstance ) from myPartner -> param(parameters)", "from clause and parameters", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getcall", "getcall( ${templateInstance} ) -> param( ${parameters} ) sender ${mySenderVar};", "getcall( templateInstance ) -> param(parameters) sender mySenderVar", "parameters and sender clause", 
				propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds the "getreply" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addGetreplyProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("getreply", "getreply", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getreply", "getreply from ${myPartner};", "getreply from myPartner", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("getreply", "getreply( ${templateInstance} ) -> value ${myReturnValue} param( ${parameters} ) sender ${mySenderVar};", "getreply( templateInstance ) -> value myReturnValue param(parameters) sender mySenderVar", "value, parameters and sender clause", 
				propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds the "catch" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addCatchProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("catch", "catch", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch -> value ${myVar};", "catch -> value myVar", "value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch -> sender ${myPeer};", "catch -> sender myPeer", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch -> value ${myVar} sender ${myPeer};", "catch -> value myVar sender myPeer", "value and sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch from ${myPeer};", "catch from myPeer", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch from ${myPeer} -> value ${myVar};", "catch from myPeer -> value myVar", "from clause with value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} );", "catch( signature, templateInstance )", "", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} ) -> value ${myVar};", "catch( signature, templateInstance ) -> value myVar", "value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} ) -> sender ${myPeer};", "catch( signature, templateInstance ) -> sender myPeer", "sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} ) -> value ${myVar} sender ${myPeer};", "catch( signature, templateInstance ) -> value myVar sender myPeer", "value and sender redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} ) from ${myPeer};", "catch( signature, templateInstance ) from myPeer", "from clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( ${signature}, ${templateInstance} ) from ${myPeer} -> value ${myVar};", "catch( signature, templateInstance ) from myPeer -> value myVar", "from clause with value redirect", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("catch", "catch( timeout );", "catch( timeout )", "", 
				propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds the "check" related operations of port types, to the completion
	 * list.
	 *
	 * @param propCollector the proposal collector
	 */
	public static void addCheckProposals(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("check", "check", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("check", "check( ${portOperation} );", "check( portOperation )", "", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("check", "check( from ${myPeer});", "check( from myPeer)", "form clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("check", "check( from ${myPeer} -> value ${myVar});", "check( from myPeer -> value myVar)", "form and value clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("check", "check( -> value ${myVar} );", "check( -> value myVar)", "value clause", 
				propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("check", "check( -> value ${myVar} sender ${myPeer} );", "check( -> value myVar sender myPeer)", "value and sender clause", 
				propCollector, replacementOffset, replacementLength, prefix);
	}
}
