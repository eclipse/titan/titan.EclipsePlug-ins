/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute extension encode/decode option
 * NOTE: encode_type : encode_option ([ß|] encode_option)*
 *       format is not used, so it is not supported.
 * @author Arpad Lovassy
 */
public class WithAttributeExtensionEncodeDecodeOptionContext extends ProposalContext {

	public WithAttributeExtensionEncodeDecodeOptionContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String encodeOrDecode = matcher.group(8);
		final String encodeType = matcher.group(9);
		
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		if ("BER".equals(encodeType)) {
			addProposal("BER_ENCODE_CER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ENCODE_DER", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ACCEPT_SHORT", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ACCEPT_LONG", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ACCEPT_INDEFINITE", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ACCEPT_DEFINITE", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BER_ACCEPT_ALL", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			
		} else if ("XER".equals(encodeType)) {
			addProposal("XER_EXTENDED", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		}
	}
}	
