/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.definitions.IParameterisedAssignment;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;

/**
 * Collects proposals for a parameter list context
 * 
 * @author Miklos Magyari
 *
 */
public class ParameterizedContext extends ProposalContext {
	public ParameterizedContext(ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String id = matcher.group(1);
		final String[] existingPars = matcher.group(2).split(",");
		final int actualPar = existingPars.length;
		
		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		final Reference reference = TTCN3ReferenceAnalyzer.parseForCompletion(proposalContextInfo.file, id);
				
		if (proposalContextInfo.scope == null) {
			return;
		}
		Assignment assignment = proposalContextInfo.scope.getAssBySRef(timestamp, reference);
		if (assignment == null) {
			return;
		}
		if (assignment instanceof IParameterisedAssignment) {
			IParameterisedAssignment pa = (IParameterisedAssignment)assignment;
			final FormalParameterList fpl = pa.getFormalParameterList();
			if (fpl.getNofParameters() < actualPar) {
				return;
			}
			final FormalParameter fp = fpl.getParameterByIndex(actualPar - 1);
			final IType type = fp.getType(timestamp);
			if (type == null) {
				return;
			}
			final Type_type typeType = type.getTypetypeTtcn3();
			addItemsByType(type, typeType, propCollector, null, null);
		}
	}
}
