/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute extension
 * @author Arpad Lovassy
 */
public class WithAttributeExtensionContext extends ProposalContext {

	public WithAttributeExtensionContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		addTemplateProposal("prototype", "prototype( ${prototypeSetting} )", "prototype( prototypeSetting )", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("transparent", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("encode", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("decode", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("errorbehavior", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("printing", "printing( ${printingSetting} )", "printing( printingSetting )", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("internal", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("address", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("provider", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("user", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("extends", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("anytype", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("in", "in( ${typeMappingList} )", "in( typeMappingList )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("out", "out( ${typeMappingList} )", "out( typeMappingList )", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("done", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("version", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("requires", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("requiresTITAN", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
	}
}	
