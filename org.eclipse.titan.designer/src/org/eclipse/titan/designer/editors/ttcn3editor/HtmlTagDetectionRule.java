/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

/**
 * An IPredicateRule implementation for detecting html tags in document comments
 * 
 * @author Miklos Magyari
 *
 */
public class HtmlTagDetectionRule implements IPredicateRule {
	private final IToken token;
	
	public HtmlTagDetectionRule(final IToken token) {
		this.token = token;
	}
	
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		return evaluate(scanner, false);
	}

	@Override
	public IToken getSuccessToken() {
		return token;
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner, boolean resume) {
		int count = 0;
		
		int c = scanner.read();
		if (c != '<') {
			scanner.unread();
			return Token.UNDEFINED;
		}
		count++;
		for (;;) {
			c = scanner.read();
			count++;
			if (c == ICharacterScanner.EOF) {
				break;
			}
			if (c == '>') {
				return token;
			}
			if (c == '<') {
				break;
			}
		}
		
		for (int i = 0; i < count; i++) {
			scanner.unread();
		}
		return Token.UNDEFINED;
	}
}
