/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute variant option (2nd level after the bracket)
 *   with { variant "attribute_keyword( option
 * @author Arpad Lovassy
 */
public class WithAttributeVariantOptionContext extends ProposalContext {

	public WithAttributeVariantOptionContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String attributeKeyword = matcher.group(3);

		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();

		switch (attributeKeyword) {
		// RAW, based on pr_XSingleRAWEncodingDef
		case "PADDING":
		case "PREPADDING":
			addProposal("yes", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("no", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposalsXBitsOctets(propCollector, prefix, replacementOffset, replacementLength);
			break;
		case "PADDING_PATTERN":
			addItemsByType(null, Type_type.TYPE_BITSTRING, propCollector, prefix, null);
			break;
		case "EXTENSION_BIT":
		case "EXTENSION_BIT_GROUP":
			addProposal("yes", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("no", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("reverse", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "LENGTHTO":
			//TODO: 'LENGTHTO' '(' IDENTIFIER ( ',' IDENTIFIER )* ')' ( ( '+' | '-' ) NUMBER )?
			break;
		case "POINTERTO":
			//TODO: 'POINTERTO' '(' IDENTIFIER ')'
			break;
		case "UNIT":
		case "PTRUNIT":
			addProposalsXBitsOctets(propCollector, prefix, replacementOffset, replacementLength);
			break;
		case "LENGTHINDEX":
			//TODO: 'LENGTHINDEX' '(' pr_XStructFieldRef? ')'
			break;
		//TODO: ( 'TAG' | 'CROSSTAG' ) '(' pr_XAssocList ';'? ')'
		//TODO: 'PRESENCE' '(' ( '{' pr_XMultiKeyId '}' | pr_XMultiKeyId ) ';'? ')'
		//TODO: 'FORCEOMIT' '(' pr_XStructFieldRef ( ',' pr_XStructFieldRef )* ')'
		case "FIELDLENGTH":
			addProposalsXNumber(propCollector, prefix, replacementOffset, replacementLength);
			break;
		case "PTROFFSET":
			// 'PTROFFSET' '(' ( NUMBER | IDENTIFIER ) ')'
			addItemsByType(null, Type_type.TYPE_INTEGER, propCollector, prefix, null);
			break;
		case "ALIGN":
			addProposal("left", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("right", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "COMP":
			addProposal("unsigned", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("2scompl", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("signbit", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "BYTEORDER":
			addProposal("first", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("last", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "FIELDORDER":
		case "BITORDER":
		case "BITORDERINFIELD":
		case "BITORDERINOCTET":
			addProposal("msb", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("lsb", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "HEXORDER":
			addProposal("low", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("high", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "REPEATABLE":
			addProposal("yes", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("no", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		case "TOPLEVEL":
			// 'TOPLEVEL' '(' 'BITORDER' '(' ( 'msb' | 'lsb' ) ')' ( ',' 'BITORDER' '(' ( 'msb' | 'lsb' ) ')' )* ')'
			addProposal("BITORDER( msb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("BITORDER( lsb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			break;
		}
	}

	/**
	 * proposals for pr_XBitsOctets
	 */
	private void addProposalsXBitsOctets(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("bit", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("bits", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposalsXOctets(propCollector, prefix, replacementOffset, replacementLength);
		addProposalsXNumber(propCollector, prefix, replacementOffset, replacementLength);
	}

	/**
	 * proposals for pr_XOctets
	 */
	private static void addProposalsXOctets(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("octet", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("octets", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("nibble", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("word16", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("dword32", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("elements", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * proposals for pr_XNumber
	 */
	private void addProposalsXNumber(final ProposalCollector propCollector,
			final String prefix, final int replacementOffset, final int replacementLength) {
		addProposal("variable", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("null_terminated", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("IEEE754", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("IEEE754", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		// no proposals for constant NUMBER
	}
}
