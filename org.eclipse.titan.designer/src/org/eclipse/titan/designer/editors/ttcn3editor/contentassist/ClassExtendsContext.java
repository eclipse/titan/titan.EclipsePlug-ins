/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Collects proposals for "class extends" statement
 * 
 * @author Arpad Lovassy
 */
public class ClassExtendsContext extends ProposalContext {

	public ClassExtendsContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}
	
	@Override
	public void getProposals(ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String className = matcher.group(3);
		final String[] existingExtends = matcher.group(4) != null ? matcher.group(4).split(",") : null;
		final List<String> existingExtendsList = existingExtends != null ? Arrays.asList(existingExtends) : new ArrayList<>(); 
		
		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();

		// list of class names which will be excluded from the proposal list
		final List<String> excludeNames = new ArrayList<String>();
		// don't propose the class itself
		excludeNames.add(className);
		// true if a non-trait class is already listed, in this case only trait classes are proposed
		boolean nonTraitListed = false;
		// list of non-trait class names, which are not listed. If we find a listed non-trait classes,
		// all of these classes will be excluded
		final List<String> nonTraitClassNames = new ArrayList<String>();
		final List<Assignment> classes = getAvailableAssignmentsByType(Type_type.TYPE_CLASS);
		for (final Assignment cItem : classes) {
			final IType type = cItem.getType(timestamp);
			if (type instanceof Class_Type) {
				final Class_Type classType = (Class_Type)type;
				final String cName = cItem.getGenName();
				// don't propose classes that are already listed in the 'extends' list
				if (existingExtendsList.contains(cName)) {
					excludeNames.add(cName);
					if (!classType.isTrait()) {
						nonTraitListed = true;
					}
				} else {
					if (!classType.isTrait()) {
						nonTraitClassNames.add(cName);
					}
				}
				// don't propose final classes that cannot be extended
				if (classType.isFinal()) {
					excludeNames.add(cName);
				}
			} else {
				//TODO: program error: assignments with TYPE_CLASS must have Class_Type type
			}
			// if a non-trait class is already listed, only propose trait classes
			if (nonTraitListed) {
				excludeNames.addAll(nonTraitClassNames);
			}
		}
		String prefix = getPrefix();
		addItemsByType(null, Type_type.TYPE_CLASS, propCollector, prefix, excludeNames);
	}
}
