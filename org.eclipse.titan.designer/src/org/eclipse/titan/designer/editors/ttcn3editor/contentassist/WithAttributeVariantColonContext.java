/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute variant option (2nd level after the colon)
 *   with { variant "attribute_keyword : option
 * @author Arpad Lovassy
 */
public class WithAttributeVariantColonContext extends ProposalContext {

	public WithAttributeVariantColonContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String attributeKeyword = matcher.group(3);

		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();

		switch (attributeKeyword) {
		case "JSON":
			// based on pr_XJsonAttribute
			addProposal("omit as null", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("name as", "name as ${jsonAlias}", "name as jsonAlias", "", propCollector, replacementOffset, replacementLength, prefix);
			addProposal("as value", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("default", "default ${json_value} ", "default json_value", "", propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("extend", "extend ${json_value} : ${json_value}", "extend json_value : json_value", "", propCollector, replacementOffset, replacementLength, prefix);
			addProposal("metainfo for unbound", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("as number", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

			// pr_XTypeIndicator:
			final String[] TYPEINDICATOR = {
					"number",
					"integer",
					"string",
					"array",
					"object",
					"objectMember",
					"literal"
			};

			for (String s : TYPEINDICATOR) {
				addProposal(s, Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			}
			break;

		case "XSD":
			// based on pr_xsddata
			final String[] XSDDATA = {
					"base64Binary",
					"decimal",
					"hexBinary",
					"QName",
					"short",
					"long",
					"string",
					"normalizedString",
					"token",
					"Name",
					"NMTOKEN",
					"NCName",
					"ID",
					"IDREF",
					"ENTITY",
					"anyURI",
					"language",
					"integer",
					"positiveInteger",
					"nonPositiveInteger",
					"negativeInteger",
					"nonNegativeInteger",
					"unsignedLong",
					"int",
					"unsignedInt",
					"unsignedShort",
					"byte",
					"unsignedByte",
					"float",
					"double",
					"duration",
					"dateTime",
					"time",
					"date",
					"gYearMonth",
					"gYear",
					"gMonthDay",
					"gDay",
					"gMonth",
					"NMTOKENS",
					"IDREFS",
					"ENTITIES",
					"boolean",
					"anySimpleType",
					"anyType"
			};

			for (String s : XSDDATA) {
				addProposal(s, Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			}
			break;
		}
	}
}
