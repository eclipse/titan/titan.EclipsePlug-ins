/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Module.module_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FriendModule;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for "friend module" statement
 * 
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public class FriendContext extends ProposalContext {
	private List<String> onTheFlyModules;
	private char endsWith;

	/**
	 * 
	 * @param module
	 * @param file
	 * @param scope
	 * @param modules list of modules that are already listed after 'friend module ...'
	 * @param needsComma whether we need to also insert a leading comma
	 */
	public FriendContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.onTheFlyModules = parseModuleList();
		this.doFallback = false;
		this.endsWith = proposalContextInfo.context.charAt(proposalContextInfo.context.length()-1);
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = null;
		if (endsWith != ',' && onTheFlyModules.size() > 0) {
			prefix = onTheFlyModules.get(onTheFlyModules.size() - 1);
		}
		
		final Set<String> knownModuleNames = sourceParser.getKnownModuleNames();
		boolean needsComma = knownModuleNames.contains(prefix); 
		knownModuleNames.remove(proposalContextInfo.module.getIdentifier().getDisplayName());

		final List<FriendModule> friendModules = ((TTCN3Module) proposalContextInfo.module).getFriendModules();		
		for (final FriendModule friendModule : friendModules) {
			knownModuleNames.remove(friendModule.getIdentifier().getDisplayName());
		}
		knownModuleNames.removeAll(onTheFlyModules);

		for (final String knownModuleName : knownModuleNames) {
			final Module tempModule = sourceParser.getModuleByName(knownModuleName);
			if (tempModule.getModuletype() != module_type.TTCN3_MODULE) {
				continue;
			}
				
			String replacementString;
			if (onTheFlyModules.size() == 0 && endsWith == 'e') {
				replacementString = " " + knownModuleName;
			} else {
				replacementString = knownModuleName;
			}
			int relevance = OTHER;
			int offset = proposalContextInfo.offset;
			int cursorPos;
			int replacementLen = 0;
			if (needsComma) {
				replacementString = ", " + replacementString;
				cursorPos = replacementString.length();
			} else if (prefix == null) { 
				cursorPos = replacementString.length();
			} else {
				offset = offset - prefix.length();
				replacementLen = prefix.length();
				cursorPos = prefix.length() + replacementString.length() - replacementLen;
				if (knownModuleName.toLowerCase().startsWith(prefix.toLowerCase())) {
					relevance = PREFIX_MATCHES;
				}
			} 
			
			final CompletionProposal proposal = new CompletionProposal(replacementString, offset, replacementLen, 
					cursorPos, ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(knownModuleName), null, tempModule.getHoverContent(null), relevance);
			propCollector.addProposal(proposal);
		}
		propCollector.sortAll();
	}

	private List<String> parseModuleList() {
		final List<String> mList = new ArrayList<String>();
		
		final String modules = proposalContextInfo.matcher.group(1);
		if (modules.length() == 0) {
			return mList;
		}
		final String[] friendModules = modules.split("[,\\s]");
		for (int i = 0; i < friendModules.length; i++) {
			mList.add(friendModules[i].trim());
		}
		return mList;
	}
}
