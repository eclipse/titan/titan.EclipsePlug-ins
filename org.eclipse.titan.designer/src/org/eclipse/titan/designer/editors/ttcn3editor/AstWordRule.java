/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.editors.AstSyntaxHighlightTokens;
import org.eclipse.titan.designer.editors.DocumentTracker;
import org.eclipse.titan.designer.editors.IDocumentGetter;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;

/**
 * Custom scanner rule based on WordRule
 * 
 * After identifying a word, the rule queries AST for special syntax elements. If found,
 * special styles will be used for the word, if not, the default style is applied
 *  
 * @author Miklos Magyari
 * @author Adam Knapp
 *
 */
public class AstWordRule extends WordRule {
	private static boolean isSemanticHighlightingEnabled;
	private static boolean isBracketColoringEnabled;
	
	private StringBuilder fBuffer = new StringBuilder();

	static {
		final IPreferencesService prefs = Platform.getPreferencesService();
		isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
		isBracketColoringEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.BRACKET_COLORING_ENABLED, false, null);

		Activator.getDefault().getPreferenceStore().addPropertyChangeListener((event) -> {
			final String property = event.getProperty();
			if (PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING.equals(property) ||
				PreferenceConstants.BRACKET_COLORING_ENABLED.equals(property)) {
				isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
				isBracketColoringEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.BRACKET_COLORING_ENABLED, false, null);
			}
		});
	}

	public AstWordRule(IWordDetector detector) {
		super(detector);
	}

	public AstWordRule(IWordDetector detector, IToken defaultToken) {
		super(detector, defaultToken);
	}

	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		boolean isEof = false;

		int c= scanner.read();
		final boolean isBracket = isBracket(c);
		if (c != ICharacterScanner.EOF && (fDetector.isWordStart((char) c) || isBracket)) {
			if (fColumn == UNDEFINED || (fColumn == scanner.getColumn() - 1)) {
				fBuffer.setLength(0);
				if (isBracket) {
					fBuffer.append((char) c);
				} else {
					do {
						fBuffer.append((char) c);
						c= scanner.read();
					} while (c != ICharacterScanner.EOF && fDetector.isWordPart((char) c));
					scanner.unread();
					if (c == ICharacterScanner.EOF) {
						isEof = true;
					}
				}

				String buffer= fBuffer.toString();				
				if ((isSemanticHighlightingEnabled && !isBracket) ||
					(isBracketColoringEnabled && isBracket)) {
					final IDocumentGetter codeScanner = (IDocumentGetter)scanner;
					final Module module = DocumentTracker.get(codeScanner.getDocument());
					IToken decoration = null;
					if (module != null) {
						final String filename = module.getLocation().getFile().getFullPath().toOSString();
						decoration = AstSyntaxHighlightTokens.getSyntaxToken(filename, codeScanner.getTokenOffset());
					} else if (scanner instanceof CodeScanner) {
						final Location peekLocation = ((CodeScanner)scanner).getPeekLocation();
						if (peekLocation != null) { 
							final Location calcLocation = new Location(peekLocation);
							calcLocation.setOffset(calcLocation.getOffset() + ((CodeScanner)scanner).getTokenOffset());
							decoration = AstSyntaxHighlightTokens.getSyntaxToken(calcLocation);
						}
					}
					if (decoration != null) {
						return decoration;
					}
				}

				IToken token= fWords.get(buffer);

				if (token != null)
					return token;

				if (fDefaultToken.isUndefined())
					unreadBuffer(scanner);

				return isEof ? Token.UNDEFINED : fDefaultToken;
			}
		}

		scanner.unread();
		return Token.UNDEFINED;
	}

	private boolean isBracket(int c) {
		switch(c) {
		case '(':
		case ')':
		case '{':
		case '}':
			return true;
		}
		return false;
	}
}
