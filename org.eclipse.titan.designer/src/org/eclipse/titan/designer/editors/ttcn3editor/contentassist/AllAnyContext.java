/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for all/any component/port/timer operation statements
 * @author Arpad Lovassy
 */
public class AllAnyContext extends ProposalContext {

	public AllAnyContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		final String anyOrAll = matcher.group(1);
		final String componentPortOrTimer = matcher.group(2);
		// matcher.group(3) is the beginning of the operator name, same as prefix 

		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		if ("component".equals(componentPortOrTimer)) {
			if ("all".equals(anyOrAll)) {
				addProposal("stop;", "stop", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
				addProposal("kill;", "kill", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
			}
			addProposal("running", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("alive", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("done", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("killed", Type_type.TYPE_COMPONENT, propCollector, replacementOffset, replacementLength, prefix);
		} else if ("port".equals(componentPortOrTimer)) {
			if ("all".equals(anyOrAll)) {
				addProposal("clear;", "clear", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
				addProposal("start;", "start", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
				addProposal("stop;", "stop", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
				addProposal("halt;", "halt", Type_type.TYPE_PORT, propCollector, replacementOffset, replacementLength, prefix);
			} else {
				// any
				DotNotationChainContext.addReceiveProposals(propCollector, prefix, replacementOffset, replacementLength);
				DotNotationChainContext.addTriggerProposals(propCollector, prefix, replacementOffset, replacementLength);
				DotNotationChainContext.addGetcallProposals(propCollector, prefix, replacementOffset, replacementLength);
				DotNotationChainContext.addGetreplyProposals(propCollector, prefix, replacementOffset, replacementLength);
				DotNotationChainContext.addCatchProposals(propCollector, prefix, replacementOffset, replacementLength);
				DotNotationChainContext.addCheckProposals(propCollector, prefix, replacementOffset, replacementLength);
			}
		} else if ("timer".equals(componentPortOrTimer)) {
			if ("all".equals(anyOrAll)) {
				addProposal("stop;", "stop", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			} else {
				// any
				addProposal("running", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
				addProposal("timeout", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
			}
		}
	}
}	
