/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.PatternRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.editors.WhiteSpaceDetector;
import org.eclipse.titan.designer.preferences.PreferenceConstants;

/**
 *
 * @author Miklos Magyari
 *
 */
public final class CommentScanner extends RuleBasedScanner {

	public CommentScanner(final ColorManager colorManager) {
		final List<IRule> rules = new ArrayList<IRule>();
		final IToken keyword = colorManager.createTokenFromPreference(PreferenceConstants.COLOR_COMMENT_TAG);
		final IToken comment = colorManager.createTokenFromPreference(PreferenceConstants.COLOR_COMMENTS);
		final IToken htmltag = colorManager.createTokenFromPreference(PreferenceConstants.COLOR_HTML_TAG); 
		
		final WordRule wordRule = new WordRule(new CommentTagDetector(), comment);
		for (String element : ICommentable.DOC_COMMENT_TAGS) {
			wordRule.addWord(element, keyword);
		}
		rules.add(wordRule);
		rules.add(new HtmlTagDetectionRule(htmltag));
		rules.add(new WhitespaceRule(new WhiteSpaceDetector()));
		final WordRule commentTextRule = new WordRule(new CommentTextDetector(), comment);
		rules.add(commentTextRule);
		setRules(rules.toArray(new IRule[rules.size()]));
	}
}
