/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.codemining.ICodeMiningProvider;
import org.eclipse.jface.text.codemining.LineHeaderCodeMining;
import org.eclipse.search.ui.ISearchQuery;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.ReferenceFinder;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.editors.referenceSearch.ReferenceSearchQuery;

/**
 * Class for line header code mining. Supports icon + text
 * 
 * @author Miklos Magyari
 *
 */
public class HeaderCodeMining extends LineHeaderCodeMining {
	private final String text;
	private final Image image;
	private final Object data; 
	
	//* text only constructor
	public HeaderCodeMining(int beforeLineNumber, IDocument document, ICodeMiningProvider provider, Object data, String text)
			throws BadLocationException {
		super(beforeLineNumber - 1, document, provider);
		this.data = data;
		this.text = text;
		this.image = null;
	}
	
	//* constructor for image and text
	public HeaderCodeMining(int beforeLineNumber, IDocument document, ICodeMiningProvider provider, Object data, String text, Image image)
			throws BadLocationException {
		super(beforeLineNumber - 1, document, provider);
		this.data = data;
		this.text = text;
		this.image = image;
	}

	@Override
	protected CompletableFuture<Void> doResolve(ITextViewer viewer, IProgressMonitor monitor) {
		return CompletableFuture.runAsync(() -> {
			super.setLabel(text);
		});
	}
	
	private final Consumer<MouseEvent> action = new Consumer<MouseEvent>() {
		@Override
		public void accept(MouseEvent t) {
			/** 
			 *  Handle click on reference count
			 *  For now, we simply run the reference finder
			 *  
			 *  TODO: Assignments already have the Set of references so it would be better to use that in a custom dialog
			 */
			if (data instanceof Definition) { 
				final ReferenceFinder finder = new ReferenceFinder((Assignment)data);
				final Definition definition = (Definition)data;
				final ReferenceSearchQuery query = new ReferenceSearchQuery(finder, null, definition.getLocation().getFile().getProject());
				for (final ISearchQuery runningQuery : NewSearchUI.getQueries()) {
					NewSearchUI.cancelQuery(runningQuery);
				}
				NewSearchUI.runQueryInBackground(query);
			}
		}
	};
	
	/**
	 * Renders the mining.
	 * 
	 * @param gc the graphics context
	 * @param textWidget the text widget to draw on
	 * @param color the color of the line
	 * @param x the x position of the annotation
	 * @param y the y position of the annotation
	 * @return the size of the draw of mining.
	 */
	@Override
	public Point draw(GC gc, StyledText textWidget, Color color, int x, int y) {
		FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
		fontData.setHeight(8);
		final Font miningFont = new Font(Display.getDefault(), fontData); 
		gc.setFont(miningFont);
		String title= getLabel() != null ? getLabel() : "no command";
		if (image != null) {
			gc.drawImage(image, x, y + 4);			
		}
		gc.drawString(title, image != null ? x + 20 : x, y, true);
		Point extent = gc.stringExtent(title);
		if (image != null) {
			extent.x += image.getBounds().width;
			extent.y += image.getBounds().height;
		}
		
		miningFont.dispose();
		return extent;
	}
	
	@Override
	public Consumer<MouseEvent> getAction() {
		return action;
	}
}
