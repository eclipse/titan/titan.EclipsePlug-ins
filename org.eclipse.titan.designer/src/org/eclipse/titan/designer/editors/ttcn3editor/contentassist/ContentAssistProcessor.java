/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ContextInformation;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Timer;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.PortTypeBody;
import org.eclipse.titan.designer.consoles.TITANDebugConsole;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.ttcn3editor.PartitionScanner;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3CodeSkeletons;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Editor;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Keywords;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3ReferenceParser;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.preferences.PreferenceConstantValues;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public final class ContentAssistProcessor implements IContentAssistProcessor {
	private static final String STANDARD_ERROR_MESSAGE = "No completions available.";

	private final AbstractDecoratedTextEditor editor;
	private final String sortingpolicy;
	private int proposalListSize;
	private int numberOfComputedResults = 0;
	private String errorMessage = null;
	private final String partition;

	public ContentAssistProcessor(final AbstractDecoratedTextEditor editor, final String partition) {
		this.editor = editor;
		this.partition = partition;
		sortingpolicy = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.CONTENTASSISTANT_PROPOSAL_SORTING);
		proposalListSize = Activator.getDefault().getPreferenceStore().getInt(PreferenceConstants.CONTENTASSISTANT_PROPOSALLIST_SIZE);
	}

	// FIXME add semantic check guard on project level.
	@Override
	public ICompletionProposal[] computeCompletionProposals(final ITextViewer viewer, final int offset) {
		if (editor == null) {
			return new ICompletionProposal[] {};
		}

		final IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
		if (file == null) {
			return new ICompletionProposal[] {};
		}
		
		/**
		 * Reanalyze the current file to be able to provide correct proposals
		 * The analyzer threads are joined to wait until the analysis is finished
		 */
		if (editor instanceof TTCN3Editor) {
			TTCN3Editor t3editor = (TTCN3Editor)editor;
			BusyIndicator.showWhile(t3editor.getSite().getShell().getDisplay(), new Runnable() {
				@Override
				public void run() {
					(t3editor).analyzeCurrentFile(file);
				}
			});
		}

		final IDocument doc = viewer.getDocument();
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		if (tempModule == null) {
			return null;
		}
		
		final CompletionFinder cf = new CompletionFinder(tempModule);
		final TemplateContextType contextType = new TemplateContextType(TTCN3CodeSkeletons.CONTEXT_IDENTIFIER, TTCN3CodeSkeletons.CONTEXT_NAME);
        final ProposalContext onleft = cf.findOnLeft(editor, offset, doc, file);
        if (onleft != null) {
        	final String prefix = onleft.getPrefix();
    		final int replacementOffset = onleft.proposalContextInfo.offset - prefix.length();
    		final ProposalCollector propCollector = new ProposalCollector(contextType, Identifier_type.ID_TTCN, doc, prefix, replacementOffset);
            onleft.getProposals(propCollector);
            if (!onleft.doFallback()) {
            	final ICompletionProposal[] proposals = propCollector.getCompletitions(proposalListSize); 
            	numberOfComputedResults = proposals.length;
            	return proposals;
            }
        }
		
		final TTCN3ReferenceParser refParser = new TTCN3ReferenceParser(true);

		Reference ref = refParser.findReferenceForClassMemberCompletion(file, offset, doc);
		Reference oldRef = refParser.findReferenceForCompletion(file, offset, doc);

		if (((ref == null || ref.getSubreferences().isEmpty()) && oldRef == null)) {
			return new ICompletionProposal[] {};
		}

		Scope scope = null;
		String moduleName = null;
		if (tempModule != null) {
			moduleName = tempModule.getName();
			scope = tempModule.getSmallestEnclosingScope(refParser.getReplacementOffset());
			if (ref != null) {
				ref.setMyScope(scope);
				ref.detectModid();
			}
			if (oldRef != null) {
				oldRef.setMyScope(scope);
				oldRef.detectModid();
			}
		} 

		final IPreferencesService prefs = Platform.getPreferencesService();
		if (prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER, PreferenceConstants.DISPLAYDEBUGINFORMATION, true, null)) {
			TITANDebugConsole.println("parsed the reference: " + ref);
		}

		final ProposalCollector propCollector;
		if (ref != null) {
			propCollector = new ProposalCollector(Identifier_type.ID_TTCN, TTCN3CodeSkeletons.CONTEXT_IDENTIFIER, contextType,
				doc, ref, refParser.getReplacementOffset());
		} else {
			propCollector = new ProposalCollector(Identifier_type.ID_TTCN, TTCN3CodeSkeletons.CONTEXT_IDENTIFIER, contextType,
				doc, oldRef, refParser.getReplacementOffset());
		}

		final CompilationTimeStamp timestamp = tempModule.getLastCompilationTimeStamp();
		if (timestamp == null) {
			return null;
		}

		if (ref != null) {
			Assignment assignment = scope.getAssBySRef(timestamp, ref);
			if (assignment == null) {
				return null;
			}
			IType type = assignment.getType(timestamp);
			if (type != null) {
				if (ref.getSubreferences().size() > 1) {
					type = type.getFieldType(timestamp, ref, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null, false);
				} else {
					type = type.getTypeRefdLast(timestamp);
				}
				if (type instanceof Class_Type) {
					getClassProposals(tempModule, ref, scope, propCollector, offset);
				}
			}
		}
		
		ref = oldRef;
		
		propCollector.setProjectParser(projectSourceParser);
		if (moduleName == null) {
			// rootless behavior
			if (ref.getModuleIdentifier() == null) {
				final Set<String> moduleNames = projectSourceParser.getKnownModuleNames();
				Module module;
				for (final String name : moduleNames) {
					module = projectSourceParser.getModuleByName(name);
					if (module != null) {
						propCollector.addProposal(name, name, ImageCache.getImage("ttcn.gif"), Module.MODULE);
						module.getAssignments().addProposal(propCollector);
					}
				}
			} else {
				final Module module = projectSourceParser.getModuleByName(ref.getModuleIdentifier().getName());
				if (module != null) {
					module.getAssignments().addProposal(propCollector);
				}
			}
		} else {
			/*
			 * search for the best scope in the module's scope
			 * hierarchy and call proposal adding function on the
			 * found scope instead of what can be found here
			 */
			if (scope != null) {
				scope.addProposal(propCollector);
			}
		}

		propCollector.sortTillMarked();
		propCollector.markPosition();

		if (ref.getSubreferences().size() != 1) {
			if (PreferenceConstantValues.SORT_ALPHABETICALLY.equals(sortingpolicy)) {
				propCollector.sortAll();
			}
			final ICompletionProposal[] proposals = propCollector.getCompletitions(); 
        	numberOfComputedResults = proposals.length;
        	return proposals;
		}

		final Set<String> knownModuleNames = projectSourceParser.getKnownModuleNames();
		for (final String knownModuleName : knownModuleNames) {
			final Identifier tempIdentifier = new Identifier(Identifier_type.ID_NAME, knownModuleName);
			final Module tempModule2 = projectSourceParser.getModuleByName(knownModuleName);
			propCollector.addProposal(tempIdentifier, ImageCache.getImage(tempModule2.getOutlineIcon()), "module");
		}
		propCollector.sortTillMarked();
		propCollector.markPosition();
		
		if (ref.getModuleIdentifier() == null) {
			if (scope == null) {
				TTCN3CodeSkeletons.addSkeletonProposals(doc, refParser.getReplacementOffset(), propCollector);
			} else {
				scope.addSkeletonProposal(propCollector);
			}

			propCollector.addTemplateProposal("refers",
					new Template("refers( function/altstep/testcase name )", "", propCollector.getContextIdentifier(),
							"refers( ${fatName} );", false), TTCN3CodeSkeletons.SKELETON_IMAGE);
			propCollector.addTemplateProposal("derefers", new Template("derefers( function/altstep/testcase name )(parameters)", "",
					propCollector.getContextIdentifier(), "derefers( ${fatName} ) ( ${parameters} );", false),
					TTCN3CodeSkeletons.SKELETON_IMAGE);

			propCollector.sortTillMarked();
			propCollector.markPosition();

			TTCN3CodeSkeletons.addPredefinedSkeletonProposals(doc, refParser.getReplacementOffset(), propCollector);

			if (scope == null) {
				TTCN3Keywords.addKeywordProposals(propCollector);
			} else {
				scope.addKeywordProposal(propCollector);
			}

			propCollector.sortTillMarked();
			propCollector.markPosition();
		} else {
			if (scope == null || !(scope instanceof StatementBlock)) {
				if (PreferenceConstantValues.SORT_ALPHABETICALLY.equals(sortingpolicy)) {
					propCollector.sortAll();
				}
				final ICompletionProposal[] proposals = propCollector.getCompletitions(); 
            	numberOfComputedResults = proposals.length;
            	return proposals;
			}

			final String fakeModule = ref.getModuleIdentifier().getName();
			if ("any component".equals(fakeModule) || "all component".equals(fakeModule)) {
				Component_Type.addAnyorAllProposal(propCollector, 0);
			} else if ("any port".equals(fakeModule) || "all port".equals(fakeModule)) {
				PortTypeBody.addAnyorAllProposal(propCollector, 0);
			} else if ("any timer".equals(fakeModule) || "all timer".equals(fakeModule)) {
				Timer.addAnyorAllProposal(propCollector, 0);
			}
		}

		if (PreferenceConstantValues.SORT_ALPHABETICALLY.equals(sortingpolicy)) {
			propCollector.sortAll();
		}

		final ICompletionProposal[] proposals = propCollector.getCompletitions(); 
    	numberOfComputedResults = proposals.length;
    	return proposals;
	}

	@Override
	public IContextInformation[] computeContextInformation(final ITextViewer viewer, final int offset) {
		return new ContextInformation[] {};
	}

	@Override
	public char[] getCompletionProposalAutoActivationCharacters() {
		if (partition.equals(PartitionScanner.TTCN3_COMMENT)) {
			return new char[] { '@', '<' };
		}
		return new char[] { '.' };
	}

	@Override
	public char[] getContextInformationAutoActivationCharacters() {
		return new char[] {};
	}

	@Override
	public IContextInformationValidator getContextInformationValidator() {
		return null;
	}

	@Override
	public String getErrorMessage() {
		if (errorMessage != null) {
			return errorMessage;
		}
		if (numberOfComputedResults > 0) {
			return null;
		}
		return STANDARD_ERROR_MESSAGE;
	}

	private void getClassProposals(Module module, Reference ref, Scope scope, ProposalCollector propCollector, int offset) {
		if (ref != null && scope != null) {
			final CompilationTimeStamp timestamp = module.getLastCompilationTimeStamp();
			if (timestamp == null) {
				return;
			}
			Assignment assignment = scope.getAssBySRef(timestamp, ref);
			if (assignment instanceof Def_Var) {
				IType type = assignment.getType(timestamp);
				if (type != null ) {
					IType refd = type.getTypeRefdLast(timestamp);
					if (refd instanceof Class_Type) {
						Class_Type ct = (Class_Type)refd;
						final IType fieldType = ct.getFieldType(timestamp, ref, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null, false);
						if (fieldType instanceof Class_Type) {
							ct = (Class_Type)fieldType;
							if (ct.getClassTypeBody() == null) {
								return;
							}
							for (Map.Entry<String, Definition> entry : ct.getDefinitionMap().getDefinitionMap().entrySet()) {
								final Definition d = entry.getValue();
								final String memberName = d.getIdentifier().getDisplayName();
								if (d instanceof Def_Function) {
									memberName.concat("()");
								}
								final IType memberType = d.getType(timestamp);
								final String memberTypeName = memberType != null ? memberType.getTypename() : "";
								String visibility;
								Stylers.ColoredStyler styler = null;
								switch (d.getVisibilityModifier()) {
								case Private:
									styler = new Stylers.ColoredStyler(Stylers.PrivateColor);
									visibility = "private";
									break;
								case Public:
									visibility = "public";
									styler = new Stylers.ColoredStyler(Stylers.PublicColor);
									break;
								default:
									visibility = "protected";
									styler = new Stylers.ColoredStyler(Stylers.ProtectedColor);
								}
								final String context = "<i>" + visibility + "</i> <b>" + memberTypeName + "</b> " + memberName;
								CompletionProposal cp = new CompletionProposal(memberName, offset, 0, memberName.length(),
										ImageCache.getImage(d.getOutlineIcon()), new StyledString(memberName), null, new Ttcn3HoverContent(context), 0);
								cp.getStyledDisplayString().append(" \u25fc", styler);
								propCollector.addProposal(cp);
							}
							propCollector.sortAll();
						}
					}
				}
			}
		}
	}
}
