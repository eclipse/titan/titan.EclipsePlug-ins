/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

/**
 * Partition scanner supporting multiple document partitions
 * 
 * @author Kristof Szabados
 * @author Miklos Magyari
 * 
 **/
public final class PartitionScanner extends RuleBasedPartitionScanner {
	public static final String TTCN3_PARTITIONING = "__ttcn3_partitioning";
	public static final String TTCN3_COMMENT = "_comment_partition_comment_type"; 

	public static final String[] PARTITION_TYPES = new String[] { IDocument.DEFAULT_CONTENT_TYPE, TTCN3_COMMENT };

	public PartitionScanner() {
		IToken commentToken = new Token(TTCN3_COMMENT);
		IPredicateRule[] rules = new IPredicateRule[3];
		rules[0] = new TTCN3StringDetectionPatternRule(new Token(""));
		rules[1] = new MultiLineRule("/*", "*/", commentToken, '\0', true);
		rules[2] = new EndOfLineRule("//", commentToken);
		setPredicateRules(rules);
	}
}
