/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for with attribute variant 1st level (e.g. keywords)
 * @author Arpad Lovassy
 */
public class WithAttributeVariantContext extends ProposalContext {

	public WithAttributeVariantContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();

		// BER
		// based on pr_XBERAttributes

		addTemplateProposal("length accept", "length accept ${ber_decode_param}", "length accept ber_decode_param", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("length accept short", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("length accept long", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("length accept indefinite", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("length accept definite", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// XER
		// based on pr_XERAttribute

		addProposal("abstract", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		//	( 'anyAttributes' | 'anyElement' ) ( ( 'from' | 'except' ) pr_urilist )?
		addProposal("anyAttributes", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("anyElement", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		addProposal("attribute", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("attributeFromQualified", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("block", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("controlNamespace", "controlNamespace ${str1} prefix ${str2}", "controlNamespace str1 prefix str2", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'defaultForEmpty' 'as' ( IDENTIFIER ( '.' IDENTIFIER )? | XSTRING )
		addProposal("defaultForEmpty as", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		addProposal("element", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("elementFormQualified", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("embedValues", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("form as unqualified", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("form as qualified", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("fractionDigits", "fractionDigits ${unsigned_int}", "fractionDigits unsigned_int", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("list", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		//	'name' 'as' pr_newNameOrKeyword
		addTemplateProposal("name as", "name as ${newNameOrKeyword}", "name as newNameOrKeyword", "", propCollector, replacementOffset, replacementLength, prefix);

		//	'namespace' 'as' XSTRING ( 'prefix' XSTRING )?
		addTemplateProposal("namespace as", "namespace as ${string}", "namespace as string", "", propCollector, replacementOffset, replacementLength, prefix);

		//	'text' ( ( XSTRING | 'all' ) 'as' pr_newNameOrKeyword )?
		addProposal("text", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		addProposal("untagged", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("useNil", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("useNumber", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("useOrder", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("useUnion", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("useType", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("whitespace preserve", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("whitespace replace", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("whitespace collapse", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("XSD", "XSD : ${xsddata}", "XSD : xsddata", "", propCollector, replacementOffset, replacementLength, prefix);

		// JSON attribute
		// based on pr_JSONAttribute
		addProposal("omit as null", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("asValue", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("default", "default ${json_value} ", "default json_value", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("extend", "extend ${json_value} : ${json_value}", "extend json_value : json_value", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("metainfo for unbound", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("as number", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("chosen", "chosen( ${list} )", "chosen( list )", "", propCollector, replacementOffset, replacementLength, prefix);
		addProposal("as map", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("escape as short", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("escape as usi", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("escape as transparent", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// RAW
		// based on pr_XSingleRAWEncodingDef
		// 'PADDING' | 'PREPADDING' ) '(' ( 'yes' | 'no' | pr_XBitsOctets ) ')'
		addTemplateProposal("PADDING", "PADDING( ${param} )", "PADDING( param )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("PREPADDING", "PREPADDING( ${param} )", "PREPADDING( param )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'PADDING_PATTERN' '(' BSTRING ')'
		addTemplateProposal("PADDING_PATTERN", "PADDING_PATTERN( ${bitstring} )", "PADDING_PATTERN( bitstring )", "", propCollector, replacementOffset, replacementLength, prefix);

		addProposal("PADDALL", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("EXTENSION_BIT( yes )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("EXTENSION_BIT( no )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("EXTENSION_BIT( reverse )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// 'EXTENSION_BIT_GROUP' '(' ( 'yes' | 'no' | 'reverse' ) ',' from = IDENTIFIER ',' to = IDENTIFIER ')'
		addTemplateProposal("EXTENSION_BIT_GROUP",
				"EXTENSION_BIT_GROUP( ${param1},  ${first_field_name}, ${last_field_name} )",
				"EXTENSION_BIT_GROUP( ${param1},  first_field_name, last_field_name )", "",
				propCollector, replacementOffset, replacementLength, prefix);

		// 'LENGTHTO' '(' IDENTIFIER ( ',' IDENTIFIER )* ')' ( ( '+' | '-' ) NUMBER )?
		addTemplateProposal("LENGTHTO", "LENGTHTO( ${field_name} )", "LENGTHTO( field_name )", "", propCollector, replacementOffset, replacementLength, prefix);

		addTemplateProposal("POINTERTO", "POINTERTO( ${field_name} )", "POINTERTO( field_name )", "", propCollector, replacementOffset, replacementLength, prefix);

		// ( 'UNIT' | 'PTRUNIT' ) '(' pr_XBitsOctets ')'
		addTemplateProposal("UNIT", "UNIT( ${bitsOctets} )", "UNIT( bitsOctets )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("PTRUNIT", "PTRUNIT( ${bitsOctets} )", "PTRUNIT( bitsOctets )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'LENGTHINDEX' '(' pr_XStructFieldRef? ')'
		addTemplateProposal("LENGTHINDEX", "LENGTHINDEX( ${structFieldRef} )", "LENGTHINDEX( structFieldRef )", "", propCollector, replacementOffset, replacementLength, prefix);

		// ( 'TAG' | 'CROSSTAG' ) '(' pr_XAssocList ';'? ')'
		addTemplateProposal("TAG", "TAG( ${assocList} )", "TAG( assocList )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("CROSSTAG", "CROSSTAG( ${assocList} )", "CROSSTAG( assocList )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'PRESENCE' '(' ( '{' pr_XMultiKeyId '}' | pr_XMultiKeyId ) ';'? ')'
		addTemplateProposal("PRESENCE", "PRESENCE( ${keyIdList} )", "PRESENCE( keyIdList )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'FORCEOMIT' '(' pr_XStructFieldRef ( ',' pr_XStructFieldRef )* ')'
		addTemplateProposal("FORCEOMIT", "FORCEOMIT( ${structFieldRef} )", "FORCEOMIT( structFieldRef )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'FIELDLENGTH' '(' pr_XNumber ')'
		addTemplateProposal("FIELDLENGTH", "FIELDLENGTH( ${number} )", "FIELDLENGTH( number )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'PTROFFSET' '(' ( NUMBER | IDENTIFIER ) ')'
		addTemplateProposal("PTROFFSET", "PTROFFSET( ${param} )", "PTROFFSET( param )", "", propCollector, replacementOffset, replacementLength, prefix);

		addProposal("ALIGN( left )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("ALIGN( right )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("COMP( unsigned )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("COMP( 2scompl )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("COMP( signbit )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BYTEORDER( first )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BYTEORDER( last )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("FIELDORDER( msb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("FIELDORDER( lsb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDER( msb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDER( lsb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDERINFIELD( msb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDERINFIELD( lsb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDERINOCTET( msb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("BITORDERINOCTET( lsb )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("HEXORDER( low )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("HEXORDER( high )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("REPEATABLE( yes )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("REPEATABLE( no )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// 'TOPLEVEL' '(' 'BITORDER' '(' ( 'msb' | 'lsb' ) ')' ( ',' 'BITORDER' '(' ( 'msb' | 'lsb' ) ')' )* ')'
		addProposal("TOPLEVEL( BITORDER(( msb ) )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("TOPLEVEL( BITORDER(( lsb ) )", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		addProposal("IntX", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// 'unsigned'? NUMBER ( 'bit' | 'bits' ) -> there is no good proposal for this case

		addProposal("UTF-8", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("UTF-16", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("IEEE754 float", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("IEEE754 double", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("CSN.1 L/H", Type_type.TYPE_UNDEFINED, propCollector, replacementOffset, replacementLength, prefix);

		// TEXT
		// based on pr_XSingleTEXTEncodingDef

		// ( 'BEGIN' | 'END' | 'SEPARATOR' ) '(' pr_XEncodeToken ( ',' pr_XMatchDef ( ',' pr_XModifierDef )? )? ')'
		addTemplateProposal("BEGIN", "BEGIN( ${encodeToken} )", "BEGIN( encodeToken )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("END", "END( ${encodeToken} )", "END( encodeToken )", "", propCollector, replacementOffset, replacementLength, prefix);
		addTemplateProposal("SEPARATOR", "SEPARATOR( ${encodeToken} )", "SEPARATOR( encodeToken )", "", propCollector, replacementOffset, replacementLength, prefix);

		// 'TEXT_CODING' '(' ( pr_XAttrList | pr_XTokenDefList )? ( ',' pr_XDecodingRule ( ',' pr_XMatchDef ( ',' pr_XModifierDef )? )? )? ')'
		addTemplateProposal("TEXT_CODING", "TEXT_CODING( ${param} )", "TEXT_CODING( param )", "", propCollector, replacementOffset, replacementLength, prefix);

		// JSON
		// based on pr_XJsonDef
		addTemplateProposal("JSON", "JSON : ${jsonAttribute}", "JSON : jsonAttribute", "", propCollector, replacementOffset, replacementLength, prefix);
	}
}
