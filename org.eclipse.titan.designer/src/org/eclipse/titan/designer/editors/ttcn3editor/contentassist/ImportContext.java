/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for "import from" statement
 * 
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public class ImportContext extends ProposalContext {

	public ImportContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final Set<String> knownModuleNames = sourceParser.getKnownModuleNames();
		knownModuleNames.remove(proposalContextInfo.module.getIdentifier().getDisplayName());

		final List<Module> importedModules = proposalContextInfo.module.getImportedModules();
		for (final Module importedModule : importedModules) {
			knownModuleNames.remove(importedModule.getIdentifier().getDisplayName());
		}

		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		for (final String knownModuleName : knownModuleNames) {
			final Module tempModule = sourceParser.getModuleByName(knownModuleName);
			final String tempModuleName = tempModule.getIdentifier().getDisplayName();
			final CompletionProposal proposal = new CompletionProposal(tempModuleName, replacementOffset, replacementLength, 
					tempModuleName.length(), ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(tempModuleName), null, tempModule.getHoverContent(null),
					tempModuleName.toLowerCase().startsWith(prefix.toLowerCase()) ? PREFIX_MATCHES : OTHER);
			propCollector.addProposal(proposal);

			final String replacementString = tempModuleName + " all";
			final CompletionProposal proposal2 = new CompletionProposal(replacementString, replacementOffset, replacementLength, 
					replacementString.length(), ImageCache.getImage(tempModule.getOutlineIcon()), 
					new StyledString(replacementString), null, tempModule.getHoverContent(null),
					tempModuleName.toLowerCase().startsWith(prefix.toLowerCase()) ? PREFIX_MATCHES : OTHER);
			propCollector.addProposal(proposal2);
		}
		propCollector.sortAll();
	}
}
