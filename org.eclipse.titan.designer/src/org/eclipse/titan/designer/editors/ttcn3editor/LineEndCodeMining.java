/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.codemining.ICodeMiningProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Special content code mining placed at the end of a line
 * @author Miklos Magyari
 *
 */
public class LineEndCodeMining extends ContentCodeMining {
	private static int LINEENDMARGIN = 20;
	
	public LineEndCodeMining(Position position, ICodeMiningProvider provider, String text) {
		super(position, provider);
		this.text = text;
		this.image = null;
	}
	
	public LineEndCodeMining(Position position, ICodeMiningProvider provider, String text, Image image) {
		super(position, provider);
		this.text = text;
		this.image = image;
	}
	
	@Override
	protected int getLeftMargin() {
		return LINEENDMARGIN;
	}
}
