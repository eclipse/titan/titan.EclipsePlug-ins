/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for class function modifier in the function declaration
 * (type function @abstract/@final/@deterministic)
 * or for module function modifier
 * (type function @control@deterministic)
 * 
 * @author Arpad Lovassy
 */
public class FunctionParamModifierContext extends ProposalContext {

	public FunctionParamModifierContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}
	
	@Override
	public void getProposals(ProposalCollector propCollector) {
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		addProposal("@lazy", Type_type.TYPE_FUNCTION, propCollector, replacementOffset, replacementLength, prefix);
		addProposal("@fuzzy", Type_type.TYPE_FUNCTION, propCollector, replacementOffset, replacementLength, prefix);
	}
}
