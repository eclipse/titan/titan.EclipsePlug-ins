/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.concurrent.CompletableFuture;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.codemining.ICodeMiningProvider;
import org.eclipse.jface.text.codemining.LineContentCodeMining;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.editors.ColorManager.Theme;
import org.eclipse.ui.PlatformUI;

public class ContentCodeMining extends LineContentCodeMining {
	protected String text;
	protected Image image;
	
	public ContentCodeMining(Position position, ICodeMiningProvider provider) {
		super(position, provider);
		this.text = "";
		this.image = null;
	}

	public ContentCodeMining(Position position, ICodeMiningProvider provider, String text) {
		super(position, provider);
		this.text = text;
		this.image = null;
	}
	
	public ContentCodeMining(Position position, ICodeMiningProvider provider, String text, Image image) {
		super(position, provider);
		this.text = text;
		this.image = image;
	}
	
	@Override
	protected CompletableFuture<Void> doResolve(ITextViewer viewer, IProgressMonitor monitor) {
		return CompletableFuture.runAsync(() -> {
			super.setLabel(text);
		});
	}
	
	/**
	 * Renders the mining.
	 * 
	 * @param gc the graphics context
	 * @param textWidget the text widget to draw on
	 * @param color the color of the line
	 * @param x the x position of the annotation
	 * @param y the y position of the annotation
	 * @return the size of the draw of mining.
	 */
	@Override
	public Point draw(GC gc, StyledText textWidget, Color color, int x, int y) {
		final Theme theme = ColorManager.getColorTheme();
		final Display display = PlatformUI.getWorkbench().getDisplay();
		if (theme == Theme.Dark) {
			gc.setForeground(new Color(display, 200, 200, 200));
		} else {
			gc.setForeground(new Color(display, 50, 50, 50));
		}
		final String title= getLabel() != null ? getLabel() : "<?>";
		final Point extent = gc.stringExtent(title);
		extent.x += 20;
		if (image != null) {
			extent.x += image.getBounds().width;
			if (image.getBounds().height > extent.y) {
				extent.y = image.getBounds().height;
			}
		}
		
		final FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
		fontData.setHeight(8);
		final Font miningFont = new Font(display, fontData); 
		gc.setFont(miningFont);
		final int leftmargin = getLeftMargin();
		if (image != null) {
			gc.drawImage(image, x + leftmargin, y + 4);			
		}
		gc.drawString(title, image != null ? x + leftmargin + 20 : x + leftmargin + 10, y, true);
		
		miningFont.dispose();
		return extent;
	}
	
	protected int getLeftMargin() {
		return 0;
	}
}
