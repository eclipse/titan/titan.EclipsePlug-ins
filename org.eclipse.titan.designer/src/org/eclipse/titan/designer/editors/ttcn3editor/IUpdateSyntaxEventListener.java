/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.EventListener;

/**
 * Interface for incremental syntax update event listeners
 *  
 * @author Miklos Magyari
 *
 */
public interface IUpdateSyntaxEventListener extends EventListener {
	/**
	 * Called before an incremental update of ttcn3 source code is performed
	 * @param e
	 */
	public void beforeSyntaxUpdate(UpdateSyntaxEvent e);

	/**
	 * Called after an incremental update of ttcn3 source code is performed
	 * @param e
	 */
	public void afterSyntaxUpdate(UpdateSyntaxEvent e);
}
