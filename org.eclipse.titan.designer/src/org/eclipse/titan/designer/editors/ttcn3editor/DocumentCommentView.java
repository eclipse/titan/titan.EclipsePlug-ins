/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.ui.part.ViewPart;

/**
 * View for displaying document comments and AST information
 * Content is updated when navigating in the source editor
 *  
 * @author Miklos Magyari
 *
 */
public class DocumentCommentView extends ViewPart {
	public static final String DOCUMENT_COMMENT_VIEW_ID = "org.eclipse.titan.designer.editors.ttcn3editor.DocumentCommentView";
	
	private Browser browser;
	
	public DocumentCommentView() {
		super();
	}
	
	@Override
	public void createPartControl(Composite parent) {
		if (browser == null) {
			try {
				browser = new Browser(parent, SWT.NONE);
			} catch (SWTError e) {
				ErrorReporter.INTERNAL_ERROR("Could not instantiate SWT Browser: " + e.getMessage());
				return;
			}
			final Ttcn3HoverContent content = new Ttcn3HoverContent();

			content.addContent(HoverContentType.INFO, "Navigate in the source to show document comment info");
			content.closeHeader();
			browser.setText(content.getText(HoverContentType.INFO, true));
			browser.setVisible(true);
		}
	}

	@Override
	public void setFocus() {
		browser.setFocus();
	}
	
	public void setContent(Ttcn3HoverContent content) {
		String text = null;
		if (content.hasText(HoverContentType.INFO)) {
			text = content.getText(HoverContentType.INFO, true);
		} else if (content.hasText(HoverContentType.SOURCE)) {
			text = content.getText(HoverContentType.SOURCE, true);
		}
		if (text != null) {
			browser.setText(text);
			browser.setVisible(true);
		};
	}
}
