/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignments;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.attributes.Types;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_AbsFunction;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Component_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.EnumItem;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Set_Seq_Choice_BaseType;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3CodeSkeletons;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;

/**
 * Base class for different proposal contexts
 * 
 * @author Miklos Magyari
 * @author Arpad Lovassy
 *
 */
public abstract class ProposalContext {
	public final static int HIGHEST = 16;
	public final static int TYPEANDPREFIX_MATCHES = 8;
	public final static int TYPE_MATCHES = 4;
	public final static int PREFIX_MATCHES = 2;
	public final static int OTHER = 1;

	protected ProjectSourceParser sourceParser;
	protected ProposalContextInfo proposalContextInfo;
	protected boolean doFallback;
	private CompilationTimeStamp timestamp;

	public ProposalContext(final ProposalContextInfo proposalContextInfo) {
		sourceParser = GlobalParser.getProjectSourceParser(proposalContextInfo.file.getProject());
		this.proposalContextInfo = proposalContextInfo;
		timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		doFallback = true;
	}

	/**
	 * Adds context-specific proposals to the given collector
	 * 
	 * @param propCollector
	 */
	public abstract void getProposals(ProposalCollector propCollector);

	/**
	 * Collects the list of available definitions in the current scope
	 * 
	 * @param typeType
	 * @return
	 */
	protected List<Definition> getAvailableDefsByType(final Type_type typeType) {
		List<Definition> definitions = new ArrayList<Definition>();
		Map<String, Definition> map = null;

		final Scope sc = getScope();
		if (sc != null) {
			if (sc instanceof StatementBlock) {
				map = ((StatementBlock)sc).getDefinitionMap(); 
			} else if (sc instanceof Definitions) {
				map = ((Definitions)sc).getDefinitionMap();
			}
			
			if (map == null) {
				return definitions;
			}
			
			for (Map.Entry<String, Definition> e : map.entrySet()) {
				final Definition def = e.getValue();
				final IType deftype = def.getType(timestamp);
				if (deftype != null && deftype.getTypetype() == typeType) {
					definitions.add(def);
				}
			}
		}

		final List<Module> importedModules = proposalContextInfo.module.getImportedModules();
		for (final Module importEntry : importedModules) {
			final Scope moduleScope = importEntry.getModuleScope();
			if (moduleScope instanceof TTCN3Module) {
				final TTCN3Module module = (TTCN3Module)moduleScope;
				final Definitions importDefinitions = module.getDefinitions();
				for (int i = 0; i < importDefinitions.getNofAssignments(); i++) {
					final Definition definitionEntry = importDefinitions.getAssignmentByIndex(i);
					final IType entryType = definitionEntry.getType(timestamp);
					if (entryType instanceof Referenced_Type) {
						if (((Referenced_Type)entryType).getTypeRefdLast(timestamp).getTypetypeTtcn3() == typeType) {
							definitions.add(definitionEntry);
						}
					} else if (entryType != null && entryType.getTypetype() == typeType) {
						definitions.add(definitionEntry);
					}
				}
			}
		}
		return definitions;
	}

	protected List<Assignment> getAvailableAssignmentsByType(final Type_type typeType) {
		List<Assignment> assignments = new ArrayList<Assignment>();

		final Scope sc = getScope();
		if (sc == null) {
			return assignments; 
		}

		final Assignments asses = proposalContextInfo.scope.getAssignmentsScope();
		final int nrAss = asses.getNofAssignments();
		for (int i = 0; i < nrAss; i++) {
			Assignment ass = asses.getAssignmentByIndex(i);
			IType aType = ass.getType(timestamp);
			if (aType != null) {
				if (ass.getType(timestamp).getTypetype() == typeType) {
					assignments.add(ass);
				}
			}
		}
	
		return assignments;
	}

	private Scope getScope() {
		// FIXME : timestamp should not be null; this check is to avoid stack overflow
		if (timestamp == null) {
			return null;
		}

		Scope sc = proposalContextInfo.module.getSmallestEnclosingScope(proposalContextInfo.offset - getPrefix().length());
		while (sc != null) {
			if (sc instanceof Definitions || sc instanceof StatementBlock) {
				break;
			}
			sc = sc.getParentScope();
		}
		return sc;		
	}

	/**
	 * Returns whether fallback is needed to the old proposal list compilation
	 * @return Whether fallback is needed to the old proposal list compilation
	 */
	public boolean doFallback() {
		return doFallback;
	}

	/**
	 * Returns the prefix based on the context information. Never returns {@code null}.
	 * @return the prefix based on the context information
	 */
	protected String getPrefix() {
		if (proposalContextInfo.context != null && !proposalContextInfo.context.isEmpty()) {
			final StringBuilder sb = new StringBuilder(proposalContextInfo.context.length());
			char c;
			for (int i = proposalContextInfo.context.length()-1; i >= 0; i--) {
				c = proposalContextInfo.context.charAt(i);
				if (!isPrefixChar(c)) {
					break;
				}
				sb.insert(0, c);
			}
			return sb.toString();
		} else {
			return "";
		}
	}

	/**
	 * Returns whether the specified char is considered as part of a prefix.
	 * Sub-classes may override this method.
	 * @param c
	 * @return whether the specified char is considered as part of a prefix
	 */
	protected boolean isPrefixChar(final char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_' || c == '@';
	}

	/**
	 * Adds available items of the specified type to the proposal list
	 *  
	 * @param type
	 * @param propCollector
	 * @param excludeNames list of names to be excluded
	 */
	protected void addItemsByType(IType type, ProposalCollector propCollector, String prefixIn, List<String> excludeNames) {
		addItemsByType(type, type.getTypetype(), propCollector, prefixIn, excludeNames);
	}

	/**
	 * Adds available items of the specified type to the proposal list
	 *  
	 * @param type
	 * @param ttype
	 * @param propCollector
	 * @param excludeNames list of names to be excluded
	 */
	protected void addItemsByType(IType type, Type_type ttype, ProposalCollector propCollector, String prefixIn, List<String> excludeNames) {
		final String prefix = prefixIn != null ? prefixIn : "";
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		
		switch (ttype) {
		case TYPE_TTCN3_ENUMERATED:
			TTCN3_Enumerated_Type etype = (TTCN3_Enumerated_Type)type;
			for (EnumItem item : etype.getEnumItems()) {
				final String enumString = item.getId().getDisplayName();
				final Ttcn3HoverContent enumItemHover = etype.getItemHoverContent(item.getId());
				final CompletionProposal enumProposal = new CompletionProposal(enumString, replacementOffset, replacementLength, 
						enumString.length(), ImageCache.getImageByType(ttype), new StyledString(enumString, new Stylers.ItalicStyler()), 
						null, enumItemHover, enumString.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
				propCollector.addProposal(enumProposal);
			}
			List<Definition> enumDefList = getAvailableDefsByType(ttype);
			for (Definition def : enumDefList) {
				if (def.getType(timestamp) instanceof Referenced_Type) {
					final Referenced_Type enumref = (Referenced_Type)def.getType(timestamp);
					if (enumref.getTypeRefdLast(timestamp) instanceof TTCN3_Enumerated_Type) {
						final String ename = def.getIdentifier().getDisplayName();
						Ttcn3HoverContent enumHover = null;
						if (def instanceof ICommentable) {
							enumHover = ((ICommentable)def).getHoverContent(null);
						}
						final CompletionProposal enumProposal2 = new CompletionProposal(ename, replacementOffset, replacementLength, 
							ename.length(), ImageCache.getImageByType(ttype), new StyledString(ename), 
							null, enumHover , ename.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
							propCollector.addProposal(enumProposal2);
					}
				}
			}
			break;
		case TYPE_TTCN3_CHOICE:
		case TYPE_TTCN3_SEQUENCE:
		case TYPE_TTCN3_SET:
			// union, record, set
			final TTCN3_Set_Seq_Choice_BaseType choiceType = (TTCN3_Set_Seq_Choice_BaseType)type;
			for (int i = 0; i < choiceType.getNofComponents(); i++) {
				final String name = choiceType.getComponentIdentifierByIndex(i).getName();
				addProposal(name, name, type.getTypetype(), propCollector, replacementOffset, replacementLength, prefix);
			}
			break;
		case TYPE_BOOL:
			addProposal("true", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("false", ttype, propCollector, replacementOffset, replacementLength, prefix);
			/** no break on purpose */
		case TYPE_BITSTRING:
		case TYPE_CHARSTRING:
		case TYPE_HEXSTRING:
		case TYPE_INTEGER:
		case TYPE_OCTETSTRING:
		case TYPE_REAL:
		case TYPE_UCHARSTRING:
		case TYPE_TESTCASE:
		case TYPE_CLASS:
			addItems(ttype, propCollector, excludeNames, replacementOffset, replacementLength, prefix);
			break;
		case TYPE_VERDICT:
			addProposal("none", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("pass", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("inconc", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("fail", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addProposal("error", ttype, propCollector, replacementOffset, replacementLength, prefix);
			addItems(ttype, propCollector, excludeNames, replacementOffset, replacementLength, prefix);
			break;
		case TYPE_COMPONENT:
			for (final String proposalName : Component_Type.SIMPLE_COMPONENT_PROPOSALS) {
				addProposal(proposalName, ttype, propCollector, replacementOffset, replacementLength, prefix);
			}
			addTemplateProposal("create", "create( ${name} );", "create( name )", "", propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("create", "create( ${name} ) alive;", "create( name ) alive", "", propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("create", "create( ${name}, ${location} );", "create( name, location )", "", propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("create", "create( ${name}, ${location} ) alive;", "create( name, location ) alive", "", propCollector, replacementOffset, replacementLength, prefix);
			addTemplateProposal("start", "start( ${functionName} );", "start( function name )", "", propCollector, replacementOffset, replacementLength, prefix);
			addItems(ttype, propCollector, excludeNames, replacementOffset, replacementLength, prefix);
			break;
		case TYPE_PORT:
			addItems(ttype, propCollector, excludeNames, replacementOffset, replacementLength, prefix);
			break;
		default:
		}
		propCollector.sortAll();
	}
	
	/**
	 * Adds proposals for class type.
	 * RECURSIVE
	 * @param classType
	 * @param propCollector
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	protected void addClassProposals(final Class_Type classType, final ProposalCollector propCollector,
			final int replacementOffset, final int replacementLength, String prefix) {
		final Map<String, Def_AbsFunction> absFunctions = classType.getAbstractFunctions();
		for (Def_AbsFunction absFunction : absFunctions.values()) {
			final String absFunctionName = absFunction.getIdentifier().getTtcnName();
			final FormalParameterList fpl = absFunction.getFormalParameterList();
			addFunctionProposal(absFunctionName, fpl, propCollector, replacementOffset, replacementLength, prefix);
		}
		final Definitions members = classType.getMembers();
		final Map<String, Definition> membersMap = members.getDefinitionMap();
		for (Definition member : membersMap.values()) {
			final String memberName = member.getIdentifier().getTtcnName();
			if (member instanceof Def_Var) {
				addProposal(memberName, member.getType(timestamp).getTypetype(), propCollector, replacementOffset, replacementLength, prefix);
			} else if (member instanceof Def_Function) {
				final Def_Function f = (Def_Function)member;
				final FormalParameterList fpl = f.getFormalParameterList();
				addFunctionProposal(memberName, fpl, propCollector, replacementOffset, replacementLength, prefix);
			}
		}

		final Class_Type baseClassType = classType.getBaseClass();
		if (baseClassType != null) {
			addClassProposals(baseClassType, propCollector, replacementOffset, replacementLength, prefix);
		}
		final Types traitClasses = classType.getBaseTraits();
		for (int i = 0; i < traitClasses.getNofTypes(); i++) {
			IType type = traitClasses.getType(i);
			if ( type instanceof Referenced_Type) {
				type = type.getTypeRefdLast(timestamp);
			}
			if ( type instanceof Class_Type) {
				final Class_Type traitClassType = (Class_Type)type;
				addClassProposals(traitClassType, propCollector, replacementOffset, replacementLength, prefix);
			}
		}
	}

	/**
	 * Adds proposal of a function or abstract function type.
	 * @param functionName
	 * @param fpl formal parameter list of the function 
	 * @param propCollector
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	private static void addFunctionProposal(final String functionName, final FormalParameterList fpl, final ProposalCollector propCollector,
			final int replacementOffset, final int replacementLength, String prefix) {
		if (fpl == null || fpl.getNofParameters() == 0) {
			addProposal(functionName + "()", Type_type.TYPE_FUNCTION, propCollector, replacementOffset, replacementLength, prefix);
		} else {
			final StringBuilder candidateString = new StringBuilder(functionName);
			candidateString.append('(');
			final StringBuilder visibleString = new StringBuilder(functionName);
			visibleString.append('(');
			for ( int i = 0; i < fpl.getNofParameters(); i++ ) {
				final FormalParameter fp = fpl.getParameterByIndex(i);
				final String parameterName = fp.getIdentifier().getTtcnName();
				if (i > 0) {
					candidateString.append(',');
					visibleString.append(',');
				}
				candidateString.append(" ${");
				candidateString.append(parameterName);
				candidateString.append('}');
				visibleString.append(' ');
				visibleString.append(parameterName);
			}
			candidateString.append(" )");
			visibleString.append(" )");
			addTemplateProposal(functionName, candidateString.toString(), visibleString.toString(), "",
					propCollector, replacementOffset, replacementLength, prefix);
		}
	}

	/**
	 * Adds single proposal to the proposal list.
	 * In this version the candidate string is the same as the visible string.
	 * @param proposalString proposal string to add
	 * @param ttype
	 * @param propCollector
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	protected static void addProposal(final String proposalString, Type_type ttype, ProposalCollector propCollector,
			final int replacementOffset, final int replacementLength, String prefix) {
		addProposal(proposalString, proposalString, ttype, propCollector, replacementOffset, replacementLength, prefix);
	}

	/**
	 * Adds single proposal to the proposal list.
	 * @param candidateString proposal candidate string to add
	 * @param visibleString the visible string of the proposal
	 * @param ttype
	 * @param propCollector
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	protected static void addProposal(final String candidateString,final String visibleString, final Type_type ttype,
			final ProposalCollector propCollector,
			final int replacementOffset, final int replacementLength, final String prefix) {
		final CompletionProposal proposal = new CompletionProposal(candidateString, replacementOffset, replacementLength, 
				candidateString.length(), ImageCache.getImageByType(ttype), new StyledString(visibleString, new Stylers.ItalicStyler()), 
				null, null, candidateString.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
		propCollector.addProposal(proposal);
	}

	/**
	 * Adds proposal to the list.
	 * General version for basic TTCN-3 types
	 * @param ttype
	 * @param propCollector
	 * @param excludeName
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	private void addItems(Type_type ttype, ProposalCollector propCollector, List<String> excludeNames,
						  int replacementOffset, int replacementLength, String prefix) {
		List<Definition> defList = getAvailableDefsByType(ttype);
		for (Definition a : defList) {
			final String name = a.getIdentifier().getDisplayName();
			if (excludeNames == null || ! excludeNames.contains(name)) {
				final Ttcn3HoverContent doc = a.getHoverContent(null);
				final CompletionProposal proposal = new CompletionProposal(name, replacementOffset, replacementLength, 
					name.length(), ImageCache.getImageByType(ttype), new StyledString(name), 
					null, doc, name.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
				propCollector.addProposal(proposal);
			}
		}
		List<Assignment> aList = getAvailableAssignmentsByType(ttype);
		for (Assignment a : aList) {
			switch (a.getAssignmentType()) {
			case A_TYPE:
				break;
			default:
				Ttcn3HoverContent doc = null;
				if (a instanceof ICommentable) {						
					final ICommentable d = (ICommentable)a;
					doc = d.getHoverContent(null);
				}
				final String name = a.getIdentifier().getDisplayName();
				final CompletionProposal proposal = new CompletionProposal(name, replacementOffset, replacementLength, 
					name.length(), ImageCache.getImageByType(ttype), new StyledString(name), 
					null, doc, name.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
				propCollector.addProposal(proposal);
			}
		}
	}

	/**
	 * Adds template proposal to the proposal list
	 * @param prefixString the prefix of TemplateProposal, using which we can decide if the proposal is valid in the given context.
	 * @param candidateString proposal candidate string to add (template pattern)
	 * @param visibleString the visible string of the proposal
	 * @param description
	 * @param ttype
	 * @param propCollector
	 * @param replacementOffset
	 * @param replacementLength
	 * @param prefix
	 */
	protected static void addTemplateProposal(final String prefixString, final String candidateString,
			final String visibleString, final String description, final ProposalCollector propCollector,
			final int replacementOffset, final int replacementLength, final String prefix) {
		try {
			propCollector.addTemplateProposal(prefixString, new Template(visibleString, description, propCollector.getContextIdentifier(),
					candidateString, false), TTCN3CodeSkeletons.SKELETON_IMAGE);
		} catch (Exception e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
	}

	/**
	 * @param name reference name
	 * @return type of the referenced name
	 */
	protected IType getType(final String name) {
		if (proposalContextInfo.scope == null) {
			return null;
		}

		final Reference reference = TTCN3ReferenceAnalyzer.parseForCompletion(proposalContextInfo.file, name);
		if (reference == null) {
			return null;
		}
		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		final Assignment assignment = proposalContextInfo.scope.getAssBySRef(timestamp, reference);
		if (assignment == null) {
			return null;
		}
		final IType type = assignment.getType(timestamp);
		return type;
	}
}
