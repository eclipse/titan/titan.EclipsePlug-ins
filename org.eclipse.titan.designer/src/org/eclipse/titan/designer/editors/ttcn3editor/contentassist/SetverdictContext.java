/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;

/**
 * Collects proposals for setverdict parameter
 * 
 * @author Arpad Lovassy
 *
 */
public class SetverdictContext extends ProposalContext {
	public SetverdictContext(ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = getPrefix();
		addItemsByType(null, Type_type.TYPE_VERDICT, propCollector, prefix, null);
	}
}
