/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.IOutlineElement;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Extfunction;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Template;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var_Template;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Group;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ImportModule;
import org.eclipse.titan.designer.editors.OutlineViewSorter;
import org.eclipse.titan.designer.editors.controls.OutlineHoverManager;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.parsers.ProjectSourceSyntacticAnalyzer;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.views.contentoutline.ContentOutline;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class OutlinePage extends ContentOutlinePage implements IUpdateSyntaxEventListener, IPropertyChangeListener {	
	private final AbstractTextEditor editor;
	private TreeViewer viewer;
	private ViewerFilter hideFunctionsFilter;
	private ViewerFilter hideTemplatesFilter;
	private ViewerFilter hideTypesFilter;
	private ViewerFilter hideImportsFilter;
	private ViewerFilter groupFilter;
	private ViewerFilter underGroupFilter;
	private ViewerFilter hideUncategorizedFilter;
	private static final String ACTION_SORT = "Sort";
	private static final String USE_CATEGORIES = "Categorise";
	private static final String ACTION_TOGGLE_GROUP_MODE = "Toggle Group Mode";
	private static final String ACTION_HIDE_FUNCTIONS = "Hide Functions";
	private static final String ACTION_HIDE_TEMPLATES = "Hide Templates";
	private static final String ACTION_HIDE_TYPES = "Hide Types";
	private static final String ACTION_HIDE_IMPORTS = "Hide Imports";
	private static final String ACTION_SHOW_UNCATEGORIZED = "Show uncategorized elements";
	
	private OutlineHoverManager fTextHoverManager;
	
	private HashSet<String> categories = new HashSet<>();
	private boolean showUncategorized = true;
	private Map<String, Boolean> enabledCategories = new HashMap<>();

	public OutlinePage(final AbstractTextEditor editor) {
		this.editor = editor;
	}
	
	@Override
	public void createControl(final Composite parent) {
		super.createControl(parent);

		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		viewer = getTreeViewer();
		viewer.setContentProvider(new OutlineContentProvider());
		//viewer.setLabelProvider(new OutlineLabelProvider());
		viewer.setLabelProvider(new OutlineStyledLabelProvider());

		final OutlineViewSorter comperator = new OutlineViewSorter();
		comperator.setSortByName(store.getBoolean(PreferenceConstants.OUTLINE_SORTED));
		comperator.setCategorizing(store.getBoolean(PreferenceConstants.OUTLINE_CATEGORISED));
		viewer.setComparator(comperator);

		final ViewerFilter filterGroup = getGroupFilter();
		final ViewerFilter filterUnderGroup = getUnderGroupFilter();
		if (store.getBoolean(PreferenceConstants.OUTLINE_GROUPED)) {
			viewer.removeFilter(filterGroup);
			viewer.addFilter(filterUnderGroup);
		} else {
			viewer.removeFilter(filterUnderGroup);
			viewer.addFilter(filterGroup);
		}
		ViewerFilter filter = getHideFunctionsFilter();
		if (store.getBoolean(PreferenceConstants.OUTLINE_HIDE_FUNCTIONS)) {
			viewer.addFilter(filter);
		} else {
			viewer.removeFilter(filter);
		}

		filter = getHideTemplatesFilter();
		if (store.getBoolean(PreferenceConstants.OUTLINE_HIDE_TEMPLATES)) {
			viewer.addFilter(filter);
		} else {
			viewer.removeFilter(filter);
		}

		filter = getHideTypesFilter();
		if (store.getBoolean(PreferenceConstants.OUTLINE_HIDE_TYPES)) {
			viewer.addFilter(filter);
		} else {
			viewer.removeFilter(filter);
		}

		viewer.setAutoExpandLevel(2);
		viewer.setInput(getModule());
		viewer.addSelectionChangedListener(this);

		final IActionBars bars = getSite().getActionBars();
		final Action sortToggler = new Action(ACTION_SORT) {
			@Override
			public void run() {
				ViewerComparator comperator = viewer.getComparator();
				if (comperator == null) {
					comperator = new OutlineViewSorter();
					viewer.setComparator(comperator);
				}

				if (comperator instanceof OutlineViewSorter) {
					store.setValue(PreferenceConstants.OUTLINE_SORTED, isChecked());
					((OutlineViewSorter) comperator).setSortByName(isChecked());
				}

				viewer.refresh(false);
			}
		};
		sortToggler.setImageDescriptor(ImageCache.getImageDescriptor("sort_alphabetically.gif"));
		sortToggler.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_SORTED));
		bars.getToolBarManager().add(sortToggler);

		final Action categorise = new Action(USE_CATEGORIES) {
			@Override
			public void run() {
				ViewerComparator comperator = viewer.getComparator();

				if (comperator == null) {
					comperator = new OutlineViewSorter();
					viewer.setComparator(comperator);
				}

				if (comperator instanceof OutlineViewSorter) {
					store.setValue(PreferenceConstants.OUTLINE_CATEGORISED, isChecked());
					((OutlineViewSorter) comperator).setCategorizing(isChecked());
				}

				viewer.refresh(false);
			}
		};
		categorise.setImageDescriptor(ImageCache.getImageDescriptor("categorize.gif"));
		categorise.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_CATEGORISED));
		bars.getToolBarManager().add(categorise);

		final Action toggleGroupMode = new Action(ACTION_TOGGLE_GROUP_MODE) {
			@Override
			public void run() {
				ViewerFilter filterGroup = getGroupFilter();
				ViewerFilter filterUnderGroup = getUnderGroupFilter();
				store.setValue(PreferenceConstants.OUTLINE_GROUPED, isChecked());
				if (isChecked()) {
					viewer.removeFilter(filterGroup);
					viewer.addFilter(filterUnderGroup);
				} else {
					viewer.removeFilter(filterUnderGroup);
					viewer.addFilter(filterGroup);
				}
				viewer.refresh(false);
			}
		};
		toggleGroupMode.setImageDescriptor(ImageCache.getImageDescriptor("outline_group.gif"));
		toggleGroupMode.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_GROUPED));
		bars.getToolBarManager().add(toggleGroupMode);

		final Action hideFunctions = new Action(ACTION_HIDE_FUNCTIONS) {
			@Override
			public void run() {
				ViewerFilter filter = getHideFunctionsFilter();
				store.setValue(PreferenceConstants.OUTLINE_HIDE_FUNCTIONS, isChecked());
				if (isChecked()) {
					viewer.addFilter(filter);
				} else {
					viewer.removeFilter(filter);
				}
				viewer.refresh(false);
			}
		};
		hideFunctions.setImageDescriptor(ImageCache.getImageDescriptor("filter_functions.gif"));
		hideFunctions.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_HIDE_FUNCTIONS));
		bars.getToolBarManager().add(hideFunctions);

		final Action hideTemplates = new Action(ACTION_HIDE_TEMPLATES) {
			@Override
			public void run() {
				ViewerFilter filter = getHideTemplatesFilter();
				store.setValue(PreferenceConstants.OUTLINE_HIDE_TEMPLATES, isChecked());
				if (isChecked()) {
					viewer.addFilter(filter);
				} else {
					viewer.removeFilter(filter);
				}
				viewer.refresh(false);
			}
		};
		hideTemplates.setImageDescriptor(ImageCache.getImageDescriptor("filter_templates.gif"));
		hideTemplates.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_HIDE_TEMPLATES));
		bars.getToolBarManager().add(hideTemplates);

		final Action hideTypes = new Action(ACTION_HIDE_TYPES) {
			@Override
			public void run() {
				ViewerFilter filter = getHideTypesFilter();
				store.setValue(PreferenceConstants.OUTLINE_HIDE_TYPES, isChecked());
				if (isChecked()) {
					viewer.addFilter(filter);
				} else {
					viewer.removeFilter(filter);
				}
				viewer.refresh(false);
			}
		};
		hideTypes.setImageDescriptor(ImageCache.getImageDescriptor("filter_types.gif"));
		hideTypes.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_HIDE_TYPES));
		bars.getToolBarManager().add(hideTypes);
		
		final Action hideImports = new Action(ACTION_HIDE_IMPORTS) {
			@Override
			public void run() {
				ViewerFilter filter = getHideImportsFilter();
				store.setValue(PreferenceConstants.OUTLINE_HIDE_IMPORTS, isChecked());
				if (isChecked()) {
					viewer.addFilter(filter);
				} else {
					viewer.removeFilter(filter);
				}
				viewer.refresh();
			}
		};
		hideImports.setImageDescriptor(ImageCache.getImageDescriptor("filter_imports.gif"));
		hideImports.setChecked(store.getBoolean(PreferenceConstants.OUTLINE_HIDE_IMPORTS));
		bars.getToolBarManager().add(hideImports);
		
		final Action showUncategorizedItems = new Action(ACTION_SHOW_UNCATEGORIZED) {
			@Override
			public void run() {
				showUncategorized = !showUncategorized;
				setChecked(showUncategorized);
				viewer.refresh();
			} 
		};
		showUncategorizedItems.setChecked(showUncategorized);
		viewer.addFilter(getHideUncategorizedFilter());
		bars.getMenuManager().add(showUncategorizedItems);

		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(this);
		ProjectSourceSyntacticAnalyzer.addSyntaxUpdateListener(this);
		
		viewer.getTree().addMouseTrackListener(new MouseTrackListener() {
			@Override
			public void mouseHover(MouseEvent event) {
				final IPreferencesService prefs = Platform.getPreferencesService();
				final boolean isHoveringEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.OUTLINE_ENABLE_HOVER_TOOLTIP, false, null);
				if (!isHoveringEnabled) {
					return;
				}
				final Tree tree = viewer.getTree();
				TreeItem item = tree.getItem(new Point(event.x, event.y));
				if (item != null && item.getData() instanceof Definition) {
					Ttcn3HoverInfoControl control = new Ttcn3HoverInfoControl(getControl().getShell());
					final Definition definition = (Definition)item.getData(); 
					control.setInput(definition.getHoverContent(null));
					fTextHoverManager = new OutlineHoverManager((IInformationControlCreator) control, viewer.getControl(), event);		
					fTextHoverManager.presentInformation();
				}
			}
			
			@Override
			public void mouseExit(MouseEvent e) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void mouseEnter(MouseEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	private ViewerFilter getGroupFilter() {
		if (groupFilter == null) {
			groupFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof Group) {
						return false;
					}
					return true;
				}
			};
		}
		return groupFilter;
	}

	private ViewerFilter getUnderGroupFilter() {
		if (underGroupFilter == null) {
			underGroupFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof Definition && ((Definition) element).getParentGroup() != null && !(parentElement instanceof Group)) {
						return false;
					}
					if (element instanceof ImportModule && ((ImportModule) element).getParentGroup() != null
							&& !(parentElement instanceof Vector<?>)) {
						return false;
					}
					return true;
				}
			};
		}
		return underGroupFilter;
	}

	private ViewerFilter getHideFunctionsFilter() {
		if (hideFunctionsFilter == null) {
			hideFunctionsFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof Def_Function || element instanceof Def_Extfunction) {
						return false;
					}
					return true;
				}
			};
		}
		return hideFunctionsFilter;
	}

	private ViewerFilter getHideTemplatesFilter() {
		if (hideTemplatesFilter == null) {
			hideTemplatesFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof Def_Template || element instanceof Def_Var_Template) {
						return false;
					}
					return true;
				}
			};
		}
		return hideTemplatesFilter;
	}

	private ViewerFilter getHideTypesFilter() {
		if (hideTypesFilter == null) {
			hideTypesFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof Def_Type) {
						return false;
					}
					return true;
				}
			};
		}
		return hideTypesFilter;
	}
	
	private ViewerFilter getHideImportsFilter() {
		if (hideImportsFilter == null) {
			hideImportsFilter = new ViewerFilter() {
				@Override
				public boolean select(Viewer viewer, Object parentElement, Object element) {
					if (element instanceof ImportModule) {
						return false;
					}
					return true;
				}
			};
		}
		return hideImportsFilter;
	}
	
	private ViewerFilter getHideUncategorizedFilter() {
		if (hideUncategorizedFilter == null) {
			hideUncategorizedFilter = new ViewerFilter() {
				@Override
				public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
					if (element instanceof ICommentable) {
						if (element instanceof Definitions) {
							return true;
						}
						final ICommentable commentable = (ICommentable)element;
						if (commentable.hasDocumentComment()) {
							if (commentable.getDocumentComment().hasCategory()) {
								final Boolean enabled = enabledCategories.get(commentable.getDocumentComment().getCategory());
								return enabled != null && enabled;
							}
						}						
					}
					if (element instanceof Definition) {
						final Object[] children = ((Definition)element).getOutlineChildren();
						if (children != null && children.length > 0) {
							for (final Object child : children) {
								if (child instanceof ICommentable) {
									final ICommentable cchild = (ICommentable)child;
									if (cchild.hasDocumentComment()) {
										if (cchild.getDocumentComment().hasCategory()) {
											final Boolean enabled = enabledCategories.get(cchild.getDocumentComment().getCategory());
											return enabled != null && enabled;
										}
									}
								}
							}
						}
					}
					return showUncategorized;
				}
			};
		}
		return hideUncategorizedFilter;
	}

	public void update() {
		final Control control = getControl();
		if (control != null && !control.isDisposed()) {
			control.setRedraw(false);
			getTreeViewer().setInput(getModule());
			control.setRedraw(true);
		}
		
		// collect categories
		final HashSet<String> existing = new HashSet<>(categories);
		
		final TreeItem[] items = getTreeViewer().getTree().getItems();
		if (items.length == 0) {
			return;
		}
		final TreeItem root = items[0];
		if (root == null) {
			return;
		}
		if (root.getData() instanceof Definitions) {
			categories = new HashSet<>();
			final Definitions defs = (Definitions)root.getData();
			for (int i = 0; i < defs.getNofAssignments(); i++) {
				final Definition elemDef = defs.getAssignmentByIndex(i);
				collectDefinitionCategory(elemDef);
				final Object[] children = elemDef.getOutlineChildren();
				if (children != null && children.length > 0) {
					for (final Object child : children) {
						if (child instanceof Definition) {
							final Definition childDef = (Definition)child;
							collectDefinitionCategory(childDef);
						}
					}
				}
			}
		}
		final IActionBars bars = getSite().getActionBars();
		final IMenuManager menumanager = bars.getMenuManager();
		for (String s : existing) {
			if (!categories.contains(s)) {
				menumanager.remove(s);
				menumanager.update();
				enabledCategories.remove(s);
			}
		}
		for (String s : categories) {
			if (!existing.contains(s)) {
				final Action action = new Action(s) {
					@Override
					public void run() {
						Boolean isChecked = enabledCategories.get(s);
						if (isChecked == null) {
							return;
						}
						isChecked = !isChecked;
						enabledCategories.put(s, isChecked);
						setChecked(isChecked);
						viewer.refresh();
					}
				};
				action.setChecked(true);
				enabledCategories.put(s, true);
				action.setId(s);
				menumanager.add(action);
				menumanager.update();
			}
		}
	}
	
	private void collectDefinitionCategory(Definition definition) {
		if (definition instanceof ICommentable) {
			ICommentable commentable = (ICommentable)definition;
			if (commentable.hasDocumentComment() && commentable.getDocumentComment().hasCategory()) {
				 final String category = commentable.getDocumentComment().getCategory();
				 if (!categories.contains(category)) {
					 categories.add(category);
				 }
			}
		}
	}

	public void refresh() {
		final Control control = getControl();
		if (control == null || control.isDisposed()) {
			return;
		}

		control.setRedraw(false);
		final Module module = getModule();
		if (getTreeViewer().getInput() == module) {
			getTreeViewer().refresh();
			getTreeViewer().expandToLevel(2);
		} else {
			getTreeViewer().setInput(getModule());
		}
		control.setRedraw(true);
	}

	@Override
	public void selectionChanged(final SelectionChangedEvent event) {
		super.selectionChanged(event);

		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		if (page == null) {
			return;
		}
		final IWorkbenchPart part = page.getActivePart();
		final ISelection selection = event.getSelection();
		if (part instanceof ContentOutline) {
			if (selection.isEmpty()) {
				return;
			}
			
			final Object selectedElement = ((IStructuredSelection) selection).getFirstElement();
			Identifier identifier = null;
			if (selectedElement instanceof IOutlineElement) {
				identifier = ((IOutlineElement) selectedElement).getIdentifier();
			}
	
			if (identifier == null || identifier.getLocation() == null) {
				return;
			}
	
			final Location location = identifier.getLocation();	
			editor.selectAndReveal(location.getOffset(), location.getEndOffset() - location.getOffset());
		} else if (selection instanceof TreeSelection) {
			final TreeSelection treeSelection = (TreeSelection)selection;
			final TreePath[] paths = treeSelection.getPaths();
			final TreePath tp = paths.length > 0 ? paths[0] : null;
			if (tp != null) {
				getTreeViewer().setExpandedState(tp, true);
			}
		}
	}

	private Module getModule() {
		final IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);

		if (file == null) {
			return null;
		}

		final ProjectSourceParser sourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module module = sourceParser.containedModule(file);
		if (module == null || module.getLastCompilationTimeStamp() == null) {
			return null;
		}

		return module;
	}
	
	@Override
	public void dispose() {
		ProjectSourceSyntacticAnalyzer.removeUpdateSyntaxListener(this);
		Activator.getDefault().getPreferenceStore().removePropertyChangeListener(this);
		super.dispose();
	}

	@Override
	public void afterSyntaxUpdate(UpdateSyntaxEvent e) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				update();
			}
		});
	}

	@Override
	public void beforeSyntaxUpdate(UpdateSyntaxEvent e) {
		// Do nothing
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		final String property = event.getProperty();
		if (PreferenceConstants.OUTLINE_ENABLE_SEMANTIC_HIGHLIGHTING.equals(property)) {
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					update();
				}
			});
		}
	}
}
