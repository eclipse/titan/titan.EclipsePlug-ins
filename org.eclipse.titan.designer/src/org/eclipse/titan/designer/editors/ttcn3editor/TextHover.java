/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.designer.editors.BaseTextHover;
import org.eclipse.titan.designer.editors.IReferenceParser;
import org.eclipse.titan.designer.editors.controls.HoverProposal;
import org.eclipse.titan.designer.editors.controls.MarkerHoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.MarkerAnnotation;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public final class TextHover extends BaseTextHover implements ITextHoverExtension, ITextHoverExtension2 {
	private final ISourceViewer sourceViewer;
	private final IEditorPart editor;

	public TextHover(final ISourceViewer sourceViewer, final IEditorPart editor) {
		this.sourceViewer = sourceViewer;
		this.editor = editor;
	}

	@Override
	protected ISourceViewer getSourceViewer() {
		return sourceViewer;
	}

	@Override
	protected IEditorPart getTargetEditor() {
		return editor;
	}

	@Override
	protected IReferenceParser getReferenceParser() {
		return new TTCN3ReferenceParser(false);
	}

	/**
	 * Ttcn3 editor hover support
	 *  
	 * @param textViewer
	 * @param hoverRegion
	 * @return
	 */
	@Override
	public Object getHoverInfo2(final ITextViewer textViewer, final IRegion hoverRegion) {
		final boolean enableCodeHoverPopups = Platform.getPreferencesService().getBoolean(
				ProductConstants.PRODUCT_ID_DESIGNER, PreferenceConstants.ENABLE_CODE_HOVER_POPUPS, true, null);

		final IFile file = editor.getEditorInput().getAdapter(IFile.class);
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		if (tempModule == null) {
			return "";
		}

		final IAnnotationModel annotationModel = sourceViewer.getAnnotationModel();
		if (annotationModel != null) {
			final Iterator<?> iterator = annotationModel.getAnnotationIterator();
			StringBuilder markerInfo = new StringBuilder();
			List<HoverProposal> proposals = new ArrayList<HoverProposal>(); 
			while (iterator.hasNext()) {
				final Object o = iterator.next();
				if (o instanceof MarkerAnnotation) {
					final MarkerAnnotation actualMarker = (MarkerAnnotation) o;
					final Position markerPosition = annotationModel.getPosition(actualMarker);
					if (markerPosition.getOffset() <= hoverRegion.getOffset()
							&& markerPosition.getOffset() + markerPosition.getLength() >= hoverRegion.getOffset()) {
						String message = actualMarker.getText();
						if (message == null) {
							ErrorReporter.INTERNAL_ERROR("The marker at " + markerPosition.getOffset() + " does not seem to have any text");
						}
						if (markerInfo == null) {
							// FIXME error handling
						}
						markerInfo.append(message);
						Object attr = null;
						try {
							attr = actualMarker.getMarker().getAttribute(HoverProposal.PROPOSAL);
						} catch (CoreException e) {
							// TODO Auto-generated catch block
						}
						if (attr instanceof HoverProposal[]) {
							HoverProposal[] propsInMarker = (HoverProposal[])attr;
							for (int i = 0; i < propsInMarker.length; i++) {
								HoverProposal propEntry = propsInMarker[i];
								propEntry.setMarker(actualMarker.getMarker(), actualMarker);
								proposals.add(propEntry);
							}
						}
					}
				}
				if (markerInfo.length() > 0) {
					HoverProposal[] propArr = {};
					propArr = proposals.toArray(propArr);
					return new MarkerHoverContent(markerInfo.toString(), propArr);
				}
			}
		}

		if (! enableCodeHoverPopups) {
			return "";		
		}
		Ttcn3HoverContent info = null;
		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(hoverRegion.getOffset());
		tempModule.accept(visitor);
		final Declaration declaration = visitor.getReferencedDeclaration();
		if (declaration == null) {
			return "";
		}

		final ICommentable commentable = declaration.getCommentable();
		if (commentable == null) {
			return "";
		}

		info = commentable.getHoverContent(editor);
		if (info == null) {
			return "";
		}

		return info;
	}

	/**
	 * Legacy interface method implemented for completeness
	 * 
	 * getHoverInfo2 is used instead
	 */
	@Deprecated
	@Override
	public String getHoverInfo(final ITextViewer textViewer, final IRegion hoverRegion) {
		return null;
	}

	@Override
	public IInformationControlCreator getHoverControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(Shell parent) {
				return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString());
			}
		};
	}
}
