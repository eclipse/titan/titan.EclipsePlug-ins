/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.List;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for component context
 * 
 * @author Miklos Magyari
 *
 */
public class ComponentContext extends ProposalContext {
	public ComponentContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		List<Definition> defList = getAvailableDefsByType(Type_type.TYPE_COMPONENT);
		for (Definition def : defList) {
			final String id = def.getIdentifier().getDisplayName();
			final CompletionProposal proposal = new CompletionProposal(id, replacementOffset, replacementLength, 
					id.length(), ImageCache.getImageByType(Type_type.TYPE_COMPONENT), 
					new StyledString(id), null, def.getHoverContent(null),
					id.toLowerCase().startsWith(prefix.toLowerCase()) ? PREFIX_MATCHES : OTHER);
			propCollector.addProposal(proposal);
		}
		propCollector.sortAll();
	}
}
