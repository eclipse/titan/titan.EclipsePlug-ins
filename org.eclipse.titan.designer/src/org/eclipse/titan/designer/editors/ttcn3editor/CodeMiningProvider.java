/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.eclipse.collections.api.tuple.Triple;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.codemining.AbstractCodeMiningProvider;
import org.eclipse.jface.text.codemining.ICodeMining;
import org.eclipse.swt.graphics.Image;
import org.eclipse.titan.designer.editors.CodeMiningMaps;
import org.eclipse.titan.designer.editors.CodeMiningMaps.CodeMiningType;
import org.eclipse.titan.designer.editors.DocumentTracker;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.texteditor.ITextEditor;

public class CodeMiningProvider extends AbstractCodeMiningProvider {

	@Override
	public CompletableFuture<List<? extends ICodeMining>> provideCodeMinings(ITextViewer viewer,
			IProgressMonitor monitor) {
		return CompletableFuture.supplyAsync(() -> {
			List<ICodeMining> minings = new ArrayList<>();
			final IPreferencesService prefs = Platform.getPreferencesService();
			final boolean isMiningsEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER, 
					PreferenceConstants.ENABLE_CODE_MININGS, false, null);
			if (!isMiningsEnabled) {
				return minings;
			}
			final IDocument document = viewer.getDocument();
			final ITextEditor textEditor = super.getAdapter(ITextEditor.class);
			if (textEditor instanceof TTCN3Editor) {
				final IFile file = DocumentTracker.getFile(document);
				if (file != null) {
					final IntObjectHashMap<Triple<String,Image,Object>> headerMinings = CodeMiningMaps.getCodeMinings(CodeMiningType.Header, file);
					headerMinings.forEachKeyValue((int key, Triple<String,Image,Object> value) -> {
						try {
							minings.add(new HeaderCodeMining(key, document, this, value.getThree(), value.getOne(), value.getTwo()));
						} catch (BadLocationException e) {
							e.printStackTrace();
						}
					});
					final IntObjectHashMap<Triple<String,Image,Object>> contentMinings = CodeMiningMaps.getCodeMinings(CodeMiningType.Content, file);
					contentMinings.forEachKeyValue((int key, Triple<String,Image,Object> value) -> {
						final Position pos = new Position(key, value.getOne().length());
						if (value.getThree() instanceof Boolean) {
							minings.add(new LineEndCodeMining(pos, this, value.getOne(), value.getTwo()));
						} else {
							minings.add(new ContentCodeMining(pos, this, value.getOne(), value.getTwo()));
						}
					});						
				}
				return minings;
			}
			return null;
		});
	}
}
