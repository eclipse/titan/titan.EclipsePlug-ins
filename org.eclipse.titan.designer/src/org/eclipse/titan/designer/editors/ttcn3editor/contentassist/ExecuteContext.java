/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.Map;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Testcase;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for execute parameter
 * 
 * @author Arpad Lovassy
 *
 */
public class ExecuteContext extends ProposalContext {
	public ExecuteContext(ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = getPrefix();
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		
		if (proposalContextInfo.module instanceof TTCN3Module) {
			TTCN3Module ttcn3module = (TTCN3Module)proposalContextInfo.module;
			Map<String, Definition> defs = ttcn3module.getDefinitions().getDefinitionMap();
			if (defs == null) {
				return;
			}
			for (Definition def : defs.values()) {
				if (def instanceof Def_Testcase) {
					Def_Testcase tc = (Def_Testcase) def;
					final String tcname = tc.getGenName();
					final CompletionProposal tcProposal = new CompletionProposal(tcname, replacementOffset, replacementLength, 
							tcname.length(), ImageCache.getImageByType(Type_type.TYPE_TESTCASE), new StyledString(tcname, new Stylers.ItalicStyler()), 
							null, null, tcname.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
					propCollector.addProposal(tcProposal);
				}
			}
		}
	}
}
