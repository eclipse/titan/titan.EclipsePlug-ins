/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.attributes.Types;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_AbsFunction;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_FunctionBase;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Var;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.types.Class_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;

/**
 * Collects proposals for a function parameter.
 * The function call is a dot notation chain.
 * @author Arpad Lovassy
 */
public class FunctionParamContext extends ProposalContext {

	public FunctionParamContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		this.doFallback = false;
	}

	@Override
	public void getProposals(final ProposalCollector propCollector) {
		final Matcher matcher = proposalContextInfo.matcher;
		// the dot notation chain before the function, it contains a "." at the end if it's not empty
		final String chain = matcher.group(1);
		if (chain.isEmpty()) {
			return;
		}
		// last object in the chain
		final String lastObject = chain.replaceAll("\\.$", "");
		final String functionName = matcher.group(4);
		// existing parameters before the current parameter, it contains a "," at the end if it's not empty
		final String parameters = matcher.group(5);
		// number of parameters before current, which is the parameter index
		final int index = countMatches(parameters, ',');
		// matcher.group(6) is the beginning of the operation or field name, same as prefix
		
		final String fullFunctionName = chain + functionName;

		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		final Reference reference = TTCN3ReferenceAnalyzer.parseForCompletion(proposalContextInfo.file, lastObject);
		if (reference == null || proposalContextInfo.scope == null) {
			return;
		}

		final Assignment assignment = proposalContextInfo.scope.getAssBySRef(timestamp, reference);
		if (assignment == null) {
			return;
		}

		final String prefix = getPrefix();

		IType type = assignment.getType(timestamp);
		if (assignment instanceof Def_Var ||
			assignment instanceof Def_Function) {
			if (type != null) {
				if (reference.getSubreferences().size() > 1) {
					// gets the type of a dot separated structured expression
					type = type.getFieldType(timestamp, reference, 1, Expected_Value_type.EXPECTED_DYNAMIC_VALUE, null, false);
				}
			}
			if (type instanceof Referenced_Type) {
				type = type.getTypeRefdLast(timestamp);
			}
			if (type instanceof Class_Type) {
				final Class_Type classType = (Class_Type)type;
				final List<Def_FunctionBase> result = new ArrayList<Def_FunctionBase>();
				getClassFunction(classType, functionName, timestamp, result);
				if (result.size() > 0) {
					final Def_FunctionBase f = result.get(0);
					final FormalParameterList fpl = f.getFormalParameterList();
					if (index < fpl.getNofParameters()) {
						final FormalParameter fp = fpl.getParameterByIndex(index);
						final Type paramType = fp.getType(timestamp);
						addItemsByType(paramType, propCollector, prefix, null);
					}
				}
			}
		}
	}

	private static int countMatches(String s, char c) {
		if (s == null) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
		    if (s.charAt(i) == c) {
		        count++;
		    }
		}
		return count;
	}

	private static void getClassFunction(final Class_Type classType, final String functionName,
			final CompilationTimeStamp timestamp, final List<Def_FunctionBase> result) {
		final Map<String, Def_AbsFunction> absFunctions = classType.getAbstractFunctions();
		for (Def_AbsFunction absFunction : absFunctions.values()) {
			final String absFunctionName = absFunction.getIdentifier().getTtcnName();
			if (functionName.equals(absFunctionName)) {
				result.add(absFunction);
			}
		}
		final Definitions members = classType.getMembers();
		final Map<String, Definition> membersMap = members.getDefinitionMap();
		for (Definition member : membersMap.values()) {
			final String memberName = member.getIdentifier().getTtcnName();
			if (functionName.equals(memberName)) {
				if (member instanceof Def_Function) {
					final Def_Function f = (Def_Function)member;
					result.add(f);
				}
			}
		}

		final Class_Type baseClassType = classType.getBaseClass();
		if (baseClassType != null) {
			getClassFunction(baseClassType, functionName, timestamp, result);
		}
		final Types traitClasses = classType.getBaseTraits();
		for (int i = 0; i < traitClasses.getNofTypes(); i++) {
			IType type = traitClasses.getType(i);
			if ( type instanceof Referenced_Type) {
				type = type.getTypeRefdLast(timestamp);
			}
			if ( type instanceof Class_Type) {
				final Class_Type traitClassType = (Class_Type)type;
				getClassFunction(traitClassType, functionName, timestamp, result);
			}
		}
	}
}
