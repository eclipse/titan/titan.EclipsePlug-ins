/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;

/**
 * This class implements a rule to perform bracket coloring
 * 
 * @author Miklos Magyari
 *
 */
public class BracketRule implements IRule {
	private static String[] bracketColorCodes = new String[] { 
		PreferenceConstants.BRACKETCOLOR0,
		PreferenceConstants.BRACKETCOLOR1,
		PreferenceConstants.BRACKETCOLOR2,
		PreferenceConstants.BRACKETCOLOR3,
		PreferenceConstants.BRACKETCOLOR4,
	};
	
	private int depth = 0;
	
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		final IPreferencesService prefs = Platform.getPreferencesService();
		final boolean isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER, PreferenceConstants.BRACKET_COLORING_ENABLED, 
				false, null);
		
		if (isEnabled == false) {
			return Token.UNDEFINED;
		}
		
		final ColorManager colorManager = new ColorManager();	
		final char c = (char)scanner.read();
		
		switch (c) {
		case '(':
		case ')':
		case '{':
		case '}':
			break;
		default:			
			scanner.unread();
			return Token.UNDEFINED;
		}

		if ((c == ')' || c == '}') && depth > 0) {
			depth--;
		}

		IToken token = Token.UNDEFINED;
		if (depth < bracketColorCodes.length) {
			token = colorManager.createTokenFromPreference(bracketColorCodes[depth]);
		}

		if (c == '(' || c == '{') {
			depth++;
		}
		
		return token;
	}
	
	public void setBracketDepth(int depth) {
		this.depth = depth;
	}
}