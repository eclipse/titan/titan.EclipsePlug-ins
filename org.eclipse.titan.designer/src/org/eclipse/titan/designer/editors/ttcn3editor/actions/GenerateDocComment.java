/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.RewriteSessionEditProcessor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.designer.editors.EditorUtils;
import org.eclipse.titan.designer.editors.IEditorWithCarretOffset;
import org.eclipse.titan.designer.editors.ISemanticTITANEditor;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Editor;
import org.eclipse.titan.designer.editors.ttcnppeditor.TTCNPPEditor;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

/**
 * Action code for documentation comment generation feature.
 * This action is used for both {@link TTCN3Editor} and {@link TTCNPPEditor}
 * 
 * @author Adam Knapp
 * @author Miklos Magyari
 *
 */
public class GenerateDocComment extends AbstractHandler implements IEditorActionDelegate {
	@Override
	public void run(IAction action) {
		generateDocComment();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// Do nothing
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		generateDocComment();
		return null;
	}

	@Override
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// Do nothing
	}

	private void generateDocComment() {
		final IEditorPart targetEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		if (targetEditor == null || !(targetEditor instanceof TTCN3Editor) &&
				!(targetEditor instanceof TTCNPPEditor)) {
			return;
		}

		final IDocument document = ((ISemanticTITANEditor) targetEditor).getDocument();
		final ISelection selection = TextSelection.emptySelection();
		final IFile file = (IFile) targetEditor.getEditorInput().getAdapter(IFile.class);

		int offset;
		if (selection instanceof TextSelection && !selection.isEmpty() && !((TextSelection) selection).getText().isEmpty()) {
			final TextSelection tSelection = (TextSelection) selection;
			offset = tSelection.getOffset() + tSelection.getLength();
		} else {
			offset = ((IEditorWithCarretOffset) targetEditor).getCarretOffset();
		}

		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module module = projectSourceParser.containedModule(file);
		if (module == null) {
			return;
		}

		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(offset);
		module.accept(visitor);

		final Declaration declaration = visitor.getReferencedDeclaration();
		if (declaration == null) {
			return;
		}

		final ICommentable commentable = declaration.getCommentable();
		final Location location = declaration.getLocation();
		if (commentable == null || commentable.hasDocumentComment() ||
				location == null) {
			return;
		}

		final String declFilePath = ((IFile) location.getFile()).getFullPath().toString();
		if (file.getFullPath().toString().equals(declFilePath)) {
			if (location.containsOffset(offset)) {
				final int startOffset = location.getOffset();
				final StringBuilder sb = new StringBuilder();
				final int realOffset = EditorUtils.getLeadingWhitespace(document, startOffset, sb);

				final MultiTextEdit multiEdit = new MultiTextEdit(realOffset, 0);
				final RewriteSessionEditProcessor processor = new RewriteSessionEditProcessor(document,
						multiEdit, TextEdit.UPDATE_REGIONS | TextEdit.CREATE_UNDO);

				final String docComment = commentable.generateDocComment(sb.toString());
				multiEdit.addChild(new InsertEdit(realOffset, docComment));
				try {
					processor.performEdits();
					((ISemanticTITANEditor) targetEditor).analyzeCurrentFile(file);
				} catch (BadLocationException e) {
					ErrorReporter.logExceptionStackTrace(e);
				}
			}
		}
	}
}
