/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.referenceSearch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.search.ui.text.AbstractTextSearchViewPage;
import org.eclipse.search.ui.text.Match;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.editors.ColorManager.Theme;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @author Szabolcs Beres
 * @author Miklos Magyari
 * */
public class ReferenceSearchResultView extends AbstractTextSearchViewPage {
	private static class ReferenceSearchLabelProvider extends StyledCellLabelProvider {
		private static final String ICON_MATCH = "match.gif";
		private final Image matchIcon;
		private List<StyleRange> ranges = new ArrayList<>();

		public ReferenceSearchLabelProvider() {
			super();
			matchIcon = ImageCache.getImage(ICON_MATCH);
		}

		@Override public void update(ViewerCell cell) {
			Object element = cell.getElement();
			if (element instanceof IResource) {
				cell.setText(WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider().getText(element));
			} else if (element instanceof ReferenceSearchMatch) {
				final ReferenceSearchMatch refSearchMatch = (ReferenceSearchMatch) element;
				final Location location = refSearchMatch.getId().getLocation();
				final Integer line = location.getLine();
				final Path path = Path.of(location.getFile().getLocation().toOSString());
				try {
					final String codeText = Files.readAllLines(path).get(line - 1).stripLeading();
					cell.setText(line + ":   " + codeText);
					final StyleRange lineRange = new StyleRange(0, line.toString().length(), getLineNumberColor(), null);
					ranges.add(lineRange);
				} catch (IOException e) {
					
				}
			} else {
				cell.setText("Unexpected element");
			}

			if (element instanceof Match) {
				cell.setImage(matchIcon); 
			}

			cell.setImage(WorkbenchLabelProvider.getDecoratingWorkbenchLabelProvider().getImage(element));
			cell.setStyleRanges(ranges.toArray(new StyleRange[0]));
		}
	}

	private ReferenceSearchContentProvider contentProvider;

	public ReferenceSearchResultView() {
		super(AbstractTextSearchViewPage.FLAG_LAYOUT_TREE);
	}

	@Override
	protected void elementsChanged(final Object[] objects) {
		if (contentProvider != null) {
			getViewer().refresh();
		}
	}

	@Override
	protected void clear() {
		getViewer().refresh();
	}

	@Override
	protected void configureTreeViewer(final TreeViewer viewer) {
		contentProvider = new ReferenceSearchContentProvider(this);

		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(new ReferenceSearchLabelProvider());
		viewer.setInput(ResourcesPlugin.getWorkspace().getRoot());

		getViewer().addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(final SelectionChangedEvent event) {
				try {
					final Object selectedElement = ((TreeSelection) event.getSelection()).getFirstElement();
					if (selectedElement instanceof ReferenceSearchMatch) {
						final ReferenceSearchMatch match = (ReferenceSearchMatch) selectedElement;
						showMatch(match, match.getOffset(), match.getLength(), false);
						return;
					}
				} catch (PartInitException e) {
					ErrorReporter.logExceptionStackTrace(e);
				}
			}
		});
	}

	@Override
	protected void configureTableViewer(final TableViewer viewer) {
		// Do nothing
		// Not supported
	}

	@Override
	protected void showMatch(final Match match, final int currentOffset, final int currentLength) throws PartInitException {
		if (!(match instanceof ReferenceSearchMatch)) {
			return;
		}

		final IStructuredSelection selection = (IStructuredSelection) getViewer().getSelection();
		if (selection != null && !selection.toList().contains(match)) {
			getViewer().setSelection(new StructuredSelection(match));
		}

		final IFile file = (IFile) match.getElement();
		final IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
		if (desc == null) {
			return;
		}

		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		final IEditorPart part = page.openEditor(new FileEditorInput(file), desc.getId(), false);
		if (part != null && part instanceof ITextEditor) {
			final ITextEditor textEditor = (ITextEditor) part;
			textEditor.selectAndReveal(currentOffset, currentLength);
		}
	}

	private static Color getLineNumberColor() {
		return new Color(PlatformUI.getWorkbench().getDisplay(),
				ColorManager.getColorTheme() == Theme.Light ? new RGB(51, 56, 152) : new RGB(238, 137, 133));
	}
}
