/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.asn1editor;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHoverExtension;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.declarationsearch.IdentifierFinderVisitor;
import org.eclipse.titan.designer.editors.BaseTextHover;
import org.eclipse.titan.designer.editors.IReferenceParser;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.EditorsUI;

/**
 * @author Kristof Szabados
 * @author Miklos Magyari 
 * */
public final class TextHover extends BaseTextHover implements ITextHoverExtension, ITextHoverExtension2 {
	private final ISourceViewer sourceViewer;
	private final IEditorPart editor;

	public TextHover(final ISourceViewer sourceViewer, final ASN1Editor editor) {
		this.sourceViewer = sourceViewer;
		this.editor = editor;
	}

	@Override
	protected ISourceViewer getSourceViewer() {
		return sourceViewer;
	}

	@Override
	protected IEditorPart getTargetEditor() {
		return editor;
	}

	@Override
	protected IReferenceParser getReferenceParser() {
		return new ASN1ReferenceParser();
	}
	
	/**
	 * ASN1 editor hover support
	 *  
	 * @param textViewer
	 * @param hoverRegion
	 * @return
	 */
	@Override
	public Object getHoverInfo2(final ITextViewer textViewer, final IRegion hoverRegion) {
		final boolean enableCodeHoverPopups = Platform.getPreferencesService().getBoolean(
				ProductConstants.PRODUCT_ID_DESIGNER, PreferenceConstants.ENABLE_CODE_HOVER_POPUPS, true, null);
		if (! enableCodeHoverPopups) {
			return "";		
		}
		final IFile file = editor.getEditorInput().getAdapter(IFile.class);
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		if (tempModule == null) {
			return "";
		}

		Ttcn3HoverContent info = null;
		final IdentifierFinderVisitor visitor = new IdentifierFinderVisitor(hoverRegion.getOffset());
		tempModule.accept(visitor);
		final Declaration declaration = visitor.getReferencedDeclaration();
		if (declaration == null) {
			return "";
		}

		final ICommentable commentable = declaration.getCommentable();
		if (commentable == null) {
			return "";
		}

		info = commentable.getHoverContent(editor);
		if (info == null) {
			return "";
		}

		return info;
	}

	/**
	 * Legacy interface method implemented for completeness
	 * 
	 * getHoverInfo2 is used instead
	 */
	@Deprecated
	@Override
	public String getHoverInfo(final ITextViewer textViewer, final IRegion hoverRegion) {
		return null;
	}

	@Override
	public IInformationControlCreator getHoverControlCreator() {
		return new IInformationControlCreator() {
			@Override
			public IInformationControl createInformationControl(Shell parent) {
				return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString());
			}
		};
	}
}
