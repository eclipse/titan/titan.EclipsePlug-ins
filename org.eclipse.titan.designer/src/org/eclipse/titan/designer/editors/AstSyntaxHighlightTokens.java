/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.collections.api.map.sorted.MutableSortedMap;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.collections.impl.map.sorted.mutable.TreeSortedMap;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;

/**
 * This class handles the map of offsets used for AST based semantic decoration
 * and also keeps track of bracket depths at certain offsets. 
 * 
 * @author Miklos Magyari
 * @author Arpad Lovassy
 *
 */
public final class AstSyntaxHighlightTokens {
	public enum SyntaxDecoration {
		Constant, 						/* TTCN3 constant */
		DefType, 						/* own type defined with the 'type' keyword */
		Deprecated,						/* object marked as 'deprecated' in the status field of document comment */
		Variable,						/* TTCN3 variable */
		Unused,							/* definition without any references */
		Bracket0,
		Bracket1,
		Bracket2,
		Bracket3,
		Bracket4,
		Bracket5,
		Bracket6,
		Bracket7,
	}
	
	private static Map<String, IntObjectHashMap<EnumSet<SyntaxDecoration>>> offsetMaps = new HashMap<>();
	private static Map<String, MutableSortedMap<Integer,Integer>> depthMaps = new HashMap<>();
	
	private static final int MAX_BRACKET_COLORS = 8;
	private static List<SyntaxDecoration> bracketcolors = new ArrayList<>(List.of(
			SyntaxDecoration.Bracket0,
			SyntaxDecoration.Bracket1,
			SyntaxDecoration.Bracket2,
			SyntaxDecoration.Bracket3,
			SyntaxDecoration.Bracket4,
			SyntaxDecoration.Bracket5,
			SyntaxDecoration.Bracket6,
			SyntaxDecoration.Bracket7
	));
	
	/** Map of style tokens associated with special AST-based syntax elements */ 
	private static Map<SyntaxDecoration, IToken> tokenMap = new HashMap<>();
	
	private static boolean isUnused;
	
	static {
		final IPreferencesService prefs = Platform.getPreferencesService();
		final boolean isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
		if (isSemanticHighlightingEnabled) {
			addTokens();
		}
		isUnused = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
			PreferenceConstants.COLOR_AST_UNUSED + PreferenceConstants.ENABLED, false, null);
		
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener((event) -> {
			final String property = event.getProperty();
			if ((PreferenceConstants.COLOR_AST_UNUSED + PreferenceConstants.ENABLED).equals(property)) {
				isUnused = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.COLOR_AST_UNUSED + PreferenceConstants.ENABLED, false, null);
			}
		});
	}
	
	public static void updateTokens() {
		final IPreferencesService prefs = Platform.getPreferencesService();
		tokenMap.clear();
		final boolean isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
		if (isEnabled) {
			addTokens();
		}
	}
	
	private static void addTokens() {
		final ColorManager colorManager = new ColorManager();
		final IPreferencesService prefs = Platform.getPreferencesService();
		boolean isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.COLOR_AST_CONSTANT + PreferenceConstants.ENABLED, true, null);
		if (isEnabled) {
			tokenMap.put(SyntaxDecoration.Constant, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_CONSTANT));
		}
		
		isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.COLOR_AST_DEFTYPE + PreferenceConstants.ENABLED, true, null);
		if (isEnabled) {
			tokenMap.put(SyntaxDecoration.DefType, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_DEFTYPE));
		}
		
		isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.COLOR_AST_DEPRECATED + PreferenceConstants.ENABLED, true, null);
		if (isEnabled) {
			tokenMap.put(SyntaxDecoration.Deprecated, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_DEPRECATED));
		}
		
		isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.COLOR_AST_VARIABLE + PreferenceConstants.ENABLED, true, null);
		if (isEnabled) {
			tokenMap.put(SyntaxDecoration.Variable, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_VARIABLE));
		}
		
		isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.COLOR_AST_UNUSED + PreferenceConstants.ENABLED, true, null);
		if (isEnabled) {
			tokenMap.put(SyntaxDecoration.Unused, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_UNUSED));
		}
		tokenMap.put(SyntaxDecoration.Bracket0, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR0));
		tokenMap.put(SyntaxDecoration.Bracket1, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR1));
		tokenMap.put(SyntaxDecoration.Bracket2, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR2));
		tokenMap.put(SyntaxDecoration.Bracket3, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR3));
		tokenMap.put(SyntaxDecoration.Bracket4, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR4));
		tokenMap.put(SyntaxDecoration.Bracket5, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR5));
		tokenMap.put(SyntaxDecoration.Bracket6, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR6));
		tokenMap.put(SyntaxDecoration.Bracket7, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR7));
	}
	
	/** no instantiation */
	private AstSyntaxHighlightTokens() {
		
	}
	
	public static synchronized IntObjectHashMap<EnumSet<SyntaxDecoration>> getOffsetMap(String moduleName) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> existingMap = offsetMaps.get(moduleName);
		if (existingMap != null) {
			return existingMap;
		} else {
			IntObjectHashMap<EnumSet<SyntaxDecoration>> newOffsetMap = new IntObjectHashMap<EnumSet<SyntaxDecoration>>();
			offsetMaps.put(moduleName, newOffsetMap);
			return newOffsetMap;
		}
	}
	
	public static synchronized MutableSortedMap<Integer,Integer> getDepthMap(String moduleName) {
		final MutableSortedMap<Integer,Integer> existingMap = depthMaps.get(moduleName);
		if (existingMap != null) {
			return existingMap;
		} else {
			final MutableSortedMap<Integer,Integer> newDepthMap = new TreeSortedMap<Integer,Integer>();
			depthMaps.put(moduleName, newDepthMap);
			return newDepthMap;
		}
	}
	
	public static synchronized void setOffsetMap(String module, IntObjectHashMap<EnumSet<SyntaxDecoration>> offsetMap) {
		offsetMaps.put(module, offsetMap);
	}
	
	public static synchronized void addBracket(String module, IFile actualFile, int offset, int depth) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> tokens = getOffsetMap(actualFile.getFullPath().toOSString());
		final MutableSortedMap<Integer,Integer> depths = getDepthMap(actualFile.getFullPath().toOSString());
		
		if (depth < 0) {
			tokens.remove(offset);
			depths.remove(offset);
			return;
		}
		
		tokens.asSynchronized().put(offset, bracketcolors.size() > 0 ? EnumSet.of(bracketcolors.get(depth % MAX_BRACKET_COLORS)) : null);		
		depths.asSynchronized().put(offset, depth);
	}
	
	/**
	 * Gets bracket depth at the given offset of a file. It is needed for incremental reparsing to know
	 * how deep the reparsed code is located in the bracket structure 
	 * 
	 * @param file
	 * @param offset
	 * @return
	 */
	public static int getDepthAtOffset(IFile file, int offset) {
		final MutableSortedMap<Integer,Integer> map = getDepthMap(file.getFullPath().toOSString());
		int depth = 0;
		
		for (Pair<Integer, Integer> pair : map.keyValuesView()) {
			if (pair.getOne() < offset) {
				depth = pair.getTwo();
			} else {
				break;
			}
		}
		return depth;
	}
	
	/** Sets a special syntax token at the given location */
	public static void addSyntaxDecoration(Location location, SyntaxDecoration decoration) {
		final IFile file = (IFile)location.getFile();
		if (file != null) {			
			addSyntaxDecoration(file.getFullPath().toOSString(), location.getOffset(), decoration);
		}
	}
	
	public static void addSyntaxDecoration(String key, int offset, SyntaxDecoration decoration) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(key);
		if (syntaxMap == null) {
			return;
		}
		
		EnumSet<SyntaxDecoration> existing = syntaxMap.asSynchronized().get(offset);
		if (existing == null) {
			existing = EnumSet.of(decoration);
		} else {
			existing.add(decoration);
		}
		syntaxMap.asSynchronized().put(offset, existing);
	}

	public static void removeSyntaxDecoration(Location location) {
		final IFile file = (IFile)location.getFile();
		if (file != null) {
			removeSyntaxDecoration(file.getFullPath().toOSString(), location.getOffset());
		}
	}
	
	public static void removeSyntaxDecoration(String key, int offset) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(key);
		if (syntaxMap == null) {
			return;
		}
		syntaxMap.asSynchronized().remove(offset);
	}
	
	public static void removeSpecificSyntaxDecoration(Location location, SyntaxDecoration decoration) {
		final IFile file = (IFile)location.getFile();
		if (file != null) {
			removeSpecificSyntaxDecoration(file.getFullPath().toOSString(), location.getOffset(), decoration);
		}
	}
	
	public static void removeSpecificSyntaxDecoration(String key, int offset, SyntaxDecoration decoration) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(key);
		if (syntaxMap == null || syntaxMap.get(offset) == null) {
			return;
		}
		
		if (syntaxMap.get(offset).contains(decoration)) {
			final EnumSet<SyntaxDecoration> existing = syntaxMap.asSynchronized().get(offset);
			existing.remove(decoration);
			if (existing.size() > 0) {
				syntaxMap.asSynchronized().put(offset, existing);
			} else {
				syntaxMap.asSynchronized().remove(offset);
			}
		}
	}

	/**
	 * Returns the special syntax token at a given offset, if exists 
	 * 
	 * @param offset
	 * @return
	 */
	public static IToken getSyntaxToken(String key, int offset) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(key);
		if (syntaxMap == null) {
			return null;
		}
		final EnumSet<SyntaxDecoration> existing = syntaxMap.get(offset);
		if (existing == null) { 
			return null;
		}
		if (existing.contains(SyntaxDecoration.Unused) && isUnused) {
			return tokenMap.get(SyntaxDecoration.Unused);
		} else {
			Iterator<SyntaxDecoration> iterator = existing.iterator();
			while (iterator.hasNext()) {
				SyntaxDecoration decoration = iterator.next();
				if (decoration != SyntaxDecoration.Unused) {
					return tokenMap.get(decoration);
				}
			}
		}
		return null;
	}
	
	public static IToken getSyntaxToken(Location location) {
		if (location == null) {
			return null;
		}
		final IFile file = (IFile)location.getFile();
		if (file != null) {
			return getSyntaxToken(file.getFullPath().toOSString(), location.getOffset());
		}
		return null;
	}

	/**
	 * Updates highlighting offset map, based on the change in the document
	 *  
	 * @param filename
	 * @param event
	 */
	public static synchronized void update(String filename, DocumentEvent event) {
		final IntObjectHashMap<EnumSet<SyntaxDecoration>> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(filename);
		if (syntaxMap == null) {
			return;
		}

		int change = event.getText().length() - event.fLength; 

		IntObjectHashMap<EnumSet<SyntaxDecoration>> tempMap = new IntObjectHashMap<>();
		syntaxMap.forEachKeyValue((int key, EnumSet<SyntaxDecoration> value) -> {
			if (key < event.fOffset || key > event.fOffset + event.fLength) {
				tempMap.put(key < event.fOffset ? key : key + change, value);
			}
		});

		AstSyntaxHighlightTokens.setOffsetMap(filename, tempMap);
	}
	
	public static boolean isSemanticHighlightingPreferenceChanged(String property) {
		return 
			property.contains(PreferenceConstants.COLOR_AST_CONSTANT) ||
			property.contains(PreferenceConstants.COLOR_AST_DEFTYPE) ||
			property.contains(PreferenceConstants.COLOR_AST_DEPRECATED) ||
			property.contains(PreferenceConstants.COLOR_AST_VARIABLE) ||
			property.contains(PreferenceConstants.COLOR_AST_UNUSED);
	}
}
