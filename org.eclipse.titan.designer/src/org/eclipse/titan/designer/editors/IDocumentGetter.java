/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.ITokenScanner;

/**
 * Interface to get IDocument in a generic way.
 * @author Adam Knapp
 *
 */
public interface IDocumentGetter extends ITokenScanner {
	/**
	 * Returns the contained {@link IDocument} or {@code null}
	 * @return the contained {@link IDocument} or {@code null}
	 */
	public IDocument getDocument();
}
