/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcnppeditor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;

/**
 * @author Kristof Szabados
 * */
public final class PartitionScanner extends RuleBasedPartitionScanner {
	public static final String TTCNPP_PARTITIONING = "__ttcnpp_partitioning";

	static final String[] PARTITION_TYPES = new String[] { IDocument.DEFAULT_CONTENT_TYPE,
			org.eclipse.titan.designer.editors.ttcn3editor.PartitionScanner.TTCN3_COMMENT };

	public PartitionScanner() {
		IToken commentToken = new Token(org.eclipse.titan.designer.editors.ttcn3editor.PartitionScanner.TTCN3_COMMENT);
		IPredicateRule[] rules = new IPredicateRule[2];
		rules[0] = new MultiLineRule("/*", "*/", commentToken, '\0', true);
		rules[1] = new EndOfLineRule("//", commentToken);
		setPredicateRules(rules);
	}
}
