/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ITreeContentProvider;

/**
 * TreeViewer content provider for editor breadcrumb popup viewers 
 * 
 * @author Miklos Magyari
 *
 */
public class BreadcrumbTreeContentProvider implements ITreeContentProvider {
	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof Path) {
			final Path path = (Path)inputElement;
			final File file = path.toFile();
			if (file.isDirectory()) {
				File[] listing = file.listFiles();
				List<BreadcrumbTreeItem> treeItems = new ArrayList<>();
				for (File f : listing) {
					if (f.isHidden()) {
						continue;
					}
					if (f.isDirectory()) {
						if (f.getName().startsWith(".")) {
							continue;
						}
						treeItems.add(new BreadcrumbTreeItem(f.getName(), f));
					}
					final String filename = f.getName();
					if (filename.matches(".*[.][Tt][Tt][Cc][Nn]3?$") ||
						filename.matches(".*[.][Tt][Tt][Cc][Nn][Pp][Pp]$") ||
						filename.matches(".*[.][Aa][Ss][Nn]1?$")) {
						treeItems.add(new BreadcrumbTreeItem(f.getName(), f));
					}
				}
				return treeItems.toArray();
			}
		}
		return new Object[] {};
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof BreadcrumbTreeItem) {
			final BreadcrumbTreeItem item = (BreadcrumbTreeItem)parentElement;			
			if (item.getData() instanceof File) {
				final File file = (File)item.getData();
				return getElements(new Path(file.getAbsolutePath()));
			}
		}
		return new Object[] {};
	}

	@Override
	public Object getParent(Object element) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof BreadcrumbTreeItem) {
			final BreadcrumbTreeItem item = (BreadcrumbTreeItem)element;
			return item.isDirectory();
		}
		return false;
	}
}
