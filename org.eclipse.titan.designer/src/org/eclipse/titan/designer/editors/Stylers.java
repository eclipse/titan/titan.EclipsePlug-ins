/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.designer.editors;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.widgets.Display;

/**
 * This class contains different predefined Styler classes used for
 * decorated code completion proposals.
 * 
 * @author Miklos Magyari
 *
 */
public class Stylers {
	private Stylers() { }
	
	public static final Color PublicColor = new Color(Display.getCurrent(), 59, 179, 0);
	public static final Color ProtectedColor = new Color(Display.getCurrent(), 255, 140, 25);
	public static final Color PrivateColor = new Color(Display.getCurrent(), 227,38,54);
	
	public static class BlueBoldStyler extends Styler {
		private static Font f;
		
		public BlueBoldStyler() {
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(SWT.BOLD);
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.foreground = new Color(Display.getCurrent(), 16, 66, 255);
			textStyle.font = f;
		}
	}

	public static class BoldStyler extends Styler {
		private static Font f;
		
		public BoldStyler() {
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(SWT.BOLD);
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.font = f;
		}
	}
	
	public static class BlackItalicStyler extends Styler {
		private static Font f;
		
		public BlackItalicStyler() {
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(SWT.ITALIC);
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.foreground = new Color(Display.getCurrent(), 0, 0, 0);
			textStyle.font = f;
		}
	}

	public static class ItalicStyler extends Styler {
		private static Font f;
		
		public ItalicStyler() {
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(SWT.ITALIC);
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.font = f;
		}
	}
	
	public static class StrikethroughStyler extends Styler {
		private static Font f;
		
		public StrikethroughStyler() {
			if (f == null) {				
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.font = f;
			textStyle.strikeout = true;
		}
	}
	
	public static class ColoredStyler extends Styler {
		private final Color color;
		private int style;
		private static Font f;
		
		public ColoredStyler(Color color) {
			this.color = color;
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(style);
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		public void setStyle(int style) {
			this.style = style;
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.foreground = color;
			textStyle.font = f;
		}
	}
	
	public static class TextAttributeStyler extends Styler {
		private final TextAttribute attribute;
		private static Font f;
		
		public TextAttributeStyler(TextAttribute attribute) {
			this.attribute = attribute;
			if (f == null) {
				FontData fontData = JFaceResources.getDefaultFont().getFontData()[0];
				fontData.setStyle(attribute.getStyle());
				f = new Font(Display.getDefault(), fontData);
			}
		}
		
		@Override
		public void applyStyles(TextStyle textStyle) {
			textStyle.background = attribute.getBackground();
			textStyle.foreground = attribute.getForeground();
			textStyle.font = f;
			textStyle.strikeout = (attribute.getStyle() & TextAttribute.STRIKETHROUGH) > 0;			
		}
	}
}
