/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

/**
 * This class provides text editor breadcrumbs
 * 
 * @author Miklos Magyari
 */
public class Breadcrumb {
	private static final int BUTTON_MARGIN = 5;
	private static final int BUTTON_TEXT_MARGIN = 3;
	
	private static Image FolderImage;
	private static Image TTCN3Image;
	private static Image TTCNPPImage;
	private static Image ASN1Image;
	
	private final Composite parent;
	private final Composite composite;
	private PaintListener paintListener;
	
	private final List<Control> dynamicElements;
	private boolean isEnabled;
	private GridData layoutData;
	private Shell shell;
	private TreeViewer treeViewer;
	
	public Breadcrumb(Composite parent) {
		composite = new Composite(parent, SWT.NONE);
		final RowLayout rowlayout = new RowLayout();
		rowlayout.marginHeight = 0;
		rowlayout.marginWidth = 0;
		rowlayout.marginLeft = 0;
		rowlayout.marginRight = 0;
		composite.setLayout(rowlayout);
		layoutData = new GridData(SWT.FILL, SWT.FILL, true, false);
		composite.setLayoutData(layoutData);
		
		/**
		 * lay out children on paint event (like window resize)
		 */
		composite.addPaintListener(e -> {
			final Point point = composite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			point.x = parent.getSize().x;
			composite.setBackground(ColorManager.getBreadcrumbColor());
			composite.layout();
			composite.setSize(point);
		});
		
		dynamicElements = new ArrayList<Control>();
		this.parent = parent;
		
		paintListener = new PaintListener() {
			@Override
			public void paintControl(PaintEvent e) {
				e.gc.setBackground(composite.getBackground());
				e.gc.setForeground(composite.getForeground());
				e.gc.fillRectangle(e.x, e.y, e.width, e.height);
				final Button button = (Button)e.getSource();
				final Image image = button.getImage();
				final String text = button.getText();
				final int fontSize = e.gc.textExtent(text).y;
				final int imageHeight = image != null ? image.getBounds().height : 0;
				final int imageWidth = image != null ? image.getBounds().width : 0;
				final int buttonHeight = button.getBounds().height;
				if (image != null) {
					e.gc.drawImage(image, BUTTON_MARGIN, (buttonHeight - imageHeight) / 2);
				}
				if (text != null && text.length() != 0) {
					e.gc.drawText(text, imageWidth + BUTTON_MARGIN + (image != null ? BUTTON_TEXT_MARGIN : 0), (buttonHeight - fontSize) / 2);
				}
			}
		};
	}
	
	static {
		final ISharedImages shared = PlatformUI.getWorkbench().getSharedImages();
		FolderImage = shared.getImage(ISharedImages.IMG_OBJ_FOLDER);
		TTCN3Image = ImageCache.getImage("ttcn.gif");
		TTCNPPImage = ImageCache.getImage("ttcnpp.gif");
		ASN1Image = ImageCache.getImage("asn.gif");
	}
	
	/** 
	 * Changes the SourceViewer layout to be able to add extra composites
	 * @param parent
	 */
	public void changeParentViewerLayout() {
		final GridLayout layout = new GridLayout(1, false);
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		layout.marginLeft = 0;
		layout.marginRight = 0;
		parent.setLayout(layout);
	}
	
	/**
	 * Updates the source viewer layout after adding the breadcrumb composite.
	 * This is needed to make sure the code editor and the breadcrumb do not overlap.
	 */
	public void updateSourceViewerLayout() {
		if (parent.isDisposed()) {
			return;
		}
		final Control[] children = parent.getChildren();
	    if (children.length < 2) {
	    	return;
	    }
	    for (final Control child : children) {
	        if (child != composite) {
	            child.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
	        } else {
	        	layoutData.exclude = ! isEnabled;
	        }
	    }
	    
	    composite.layout();
	    parent.layout();
	}
	
	private Button addButton(final String text, String imageName) {
		return addButton(text, ImageCache.getImage(imageName));
	}
	
	private Button addButton(final String text, final Image image) {
		return addButton(text, image, null);
	}
	
	private Button addButton(final String text, final Image image, Object data) {
		Button button = new Button(composite, SWT.PUSH);
		button.setData(data);
		button.addPaintListener(paintListener);
		if (image != null) {
			button.setImage(image);
		}
		button.setText(text);
		final Point size = button.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		button.setSize(size);
		button.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(MouseEvent e) {
				final Rectangle buttonPos = button.getBounds();
				Point realLocation = parent.getDisplay().map(parent, null, buttonPos.x, buttonPos.y);
				shell = new Shell(composite.getShell(), SWT.RESIZE | SWT.TOOL | SWT.ON_TOP);
				GridLayout layout= new GridLayout(1, false);
				layout.marginHeight= 0;
				layout.marginWidth= 0;
				shell.setLayout(layout);

				Composite fcomposite= new Composite(shell, SWT.NONE);
				fcomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				GridLayout gridLayout= new GridLayout(1, false);
				gridLayout.marginHeight= 0;
				gridLayout.marginWidth= 0;
				fcomposite.setLayout(gridLayout);

				treeViewer= new TreeViewer(fcomposite, SWT.MULTI| SWT.H_SCROLL | SWT.V_SCROLL);
				treeViewer.setContentProvider(new BreadcrumbTreeContentProvider());
				treeViewer.setLabelProvider(new BreadcumbTreeStyledLabelProvider());
				treeViewer.setInput(button.getData());
				treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {				
					@Override
					public void selectionChanged(SelectionChangedEvent event) {
						if (event.getSelection() instanceof TreeSelection) {
							final TreeSelection selection = (TreeSelection)event.getSelection();
							if (selection.getFirstElement() instanceof BreadcrumbTreeItem) {
								final BreadcrumbTreeItem item = (BreadcrumbTreeItem)selection.getFirstElement();
								if (! item.isDirectory()) {
									if (item.getData() instanceof File) {
										final File selected = (File)item.getData();
										final URI uri = selected.toURI();
										IFile files[] = ResourcesPlugin.getWorkspace().getRoot().findFilesForLocationURI(uri);
										if (files.length > 0) {
											final IEditorPart targetEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
											final IWorkbenchPage page = targetEditor.getSite().getPage();
											final IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(files[0].getName());
											
											try {
												final IEditorPart editorPart = page.openEditor(new FileEditorInput(files[0]), desc.getId());
											} catch (PartInitException e) {
												
											}
										}
									}
								}
							}
						}
					}
				});
				
				final Tree tree= (Tree) treeViewer.getControl();
				tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				shell.setBounds(new Rectangle(realLocation.x, realLocation.y + buttonPos.height, 200, 200));
				shell.setVisible(true);
				
				installCloser(shell);
			}
			
			@Override
			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		return button;
	}
	
	private Button addTextButton(final String text) {
		Button button = new Button(composite, SWT.PUSH);
		button.addPaintListener(paintListener);
		button.setText(text);
		final Point size = button.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		button.setSize(size);
		return button;
	}
	
	/**
	 * Adds a text only item to the breadcrumb
	 * @param text
	 */
	public void addBaseElement(String text) {
		addBaseElement(text, null, null);
	}

	/**
	 * Adds a new item to the breadcrumb with icon and text. This part of the breadcrumb is fixed
	 * and not removed by <code>clearDynamicElements()</code>.
	 * 
	 * @param text
	 * @param image
	 */
	public void addBaseElement(String text, Image image, Object data) {
		addArrowButton();
		addButton(text, image, data);
	}
	
	/**
	 * Removes all dyncamic elements from the breadcrumb. The elements are disposed, but the UI is not updated.
	 */
	public void clearDynamicElements() {
		for (Control dynbutton : dynamicElements) {
			dynbutton.dispose();
		}
		dynamicElements.clear();
	}
	
	/**
	 * Adds a dynamic element to the breadcrumb. When <code>clearDynamicElements()</code> is called,
	 * this element is removed.
	 * @param declaration
	 */
	public void addDynamicElement(Declaration declaration) {
		final List<Scope> tree = new ArrayList<>();
		final Assignment assignment = declaration.getAssignment();
		if (assignment == null) {
			return;
		}
		Scope scope = assignment.getMyScope();
		while (scope != null && !(scope instanceof Module)) {
			tree.add(scope);
			scope = scope.getParentScope();
		}
		
		for (int i = tree.size(); i > 0; i--) {
			final Scope parentScope = tree.get(i - 1);
			final Assignment info = parentScope.getScopeBreadcrumbDefinition(); 
			if (info != null) {
				addArrowButton(true);
				final Button parentButton = addButton(info.getIdentifier().getDisplayName(), info.getOutlineIcon());
				dynamicElements.add(parentButton);
			}
		}
		
		final Point point = composite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		composite.layout();
		composite.setSize(point);
	}
	
	private void addArrowButton() {
		addArrowButton(false);
	}
	
	private void addArrowButton(boolean isDynamic) {
		final Button arrowButton = addTextButton(">");
		if (isDynamic) {
			dynamicElements.add(arrowButton);
		}
	}
	/**
	 * Builds a list of breadcrumb items from an IFile
	 * 
	 * @param input
	 */
	public void buildFilePath(IEditorInput input) {
		StringBuilder pathpart = new StringBuilder();
		final IFile file = input.getAdapter(IFile.class);
		final IProject project = file.getProject();
		final IPath container = project.getLocation();
		final IPath fileLocation = file.getLocation();
		final int containerSegments = container.segmentCount();
		addBaseElement(project.getName(), ImageCache.getImage("titan.gif"), null);
		pathpart.append(container);
		pathpart.append(Path.SEPARATOR);
		final IPath path = file.getFullPath();
		for (int i = containerSegments; i < fileLocation.segmentCount(); i++) {
			final String segment = fileLocation.segment(i);
			pathpart.append(segment);
			pathpart.append(Path.SEPARATOR);
			final Path elementPath = new Path(pathpart.toString());
			if (i != fileLocation.segmentCount() - 1) {
				addBaseElement(segment, FolderImage, elementPath);
			} else {
				if (segment.matches(".*[.][Tt][Tt][Cc][Nn]3?$")) {
					addBaseElement(segment, TTCN3Image, elementPath);
				} else if (segment.matches(".*[.][Tt][Tt][Cc][Nn][Pp][Pp]$")) {
					addBaseElement(segment, TTCNPPImage, elementPath);
				} else if (segment.matches(".*[.][Aa][Ss][Nn]1?$")) {
					addBaseElement(segment, ASN1Image, elementPath);
				}
			}
		}
	}
	
	/**
	 * Sets the enabled state of the breadcrumb
	 * @param isEnabled
	 */
	public void setEnabled(boolean isEnabled) {
		if (composite.isDisposed()) {
			return;
		}
		composite.setVisible(isEnabled);
		this.isEnabled = isEnabled;
	}
	
	private void installCloser(Shell shell) {
		final Listener focusListener = event -> {
			Widget focusedElement = event.widget;
			boolean isFocused = focusedElement == shell || (focusedElement instanceof Tree);
			
			switch (event.type ) {
			case SWT.FocusIn:
				if (! isFocused) {
					shell.close();
				}
			case SWT.FocusOut:
				if (event.display.getActiveShell() == null) {
					shell.close();
				}
			}
		};
		
		final Display display = shell.getDisplay();
		display.addFilter(SWT.FocusIn, focusListener);
		display.addFilter(SWT.FocusOut, focusListener);
		
		shell.addDisposeListener(event -> {
			display.removeFilter(SWT.FocusIn, focusListener);
			display.removeFilter(SWT.FocusOut, focusListener);
		});
	}
}
