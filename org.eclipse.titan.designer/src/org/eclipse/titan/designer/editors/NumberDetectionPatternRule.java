/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

/**
 * Scanner rule to detect integers and floats
 * 
 * @author Miklos Magyari
 * 
 */
public class NumberDetectionPatternRule implements IRule {
	private final IToken integerToken;
	private final IToken floatToken;
	private int nrRead;
	
	public NumberDetectionPatternRule(final IToken integerToken, final IToken floatToken) {
		this.integerToken = integerToken;
		this.floatToken = floatToken;
	}
	
	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		StringBuffer sb = new StringBuffer();
		nrRead = 0;
		boolean isDot = false;
		boolean isE = false;
		char lastChar = 0;
		int c = scanner.read();
		if (isNumber((char)c) || (char)c == '-') {
			sb.append((char)c);
			do {
				nrRead++;
				c = scanner.read();				
				if ((char)c == '.') {
					if (lastChar == '.') {
						// assuming range operator '..'
						scanner.unread();
						scanner.unread();
						sb.deleteCharAt(sb.length() - 1);
						final String num = sb.toString();
						return (num.contains(".") || num.contains("E")) ? floatToken : integerToken; 
					}
				} else if ((char)c == 'E') {
					if (isE) {
						// malformed number, roll back
						return unreadScanner(scanner);
					}
					isE = true;
				}
				lastChar = (char)c;
				sb.append((char)c);
			} while (isNumber((char)c) || (char)c == '.' || (char)c == 'E' || (char)c == '-');
			
			scanner.unread();
			final String num = sb.toString();
			return (num.contains(".") || num.contains("E")) ? floatToken : integerToken; 
		}
		
		scanner.unread();
		return Token.UNDEFINED;
	}

	private boolean isNumber(char c) {
		return c >= '0' && c <= '9';
	}
	
	/** 
	 * Reset the scanner to the position where the evaluation started.
	 * Used when the number is obviously malformed
	 *  
	 * @param scanner
	 * @return Always returns <b>Token.UNDEFINED</b>
	 */
	private IToken unreadScanner(ICharacterScanner scanner) {
		for (int i = 0; i < nrRead; i++) {
			scanner.unread();
		}
		
		return Token.UNDEFINED;
	}
}
