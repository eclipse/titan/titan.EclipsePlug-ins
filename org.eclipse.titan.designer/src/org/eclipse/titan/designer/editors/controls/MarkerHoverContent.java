/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

/**
 * This class represents information to be displayed for marker hovers
 * 
 * Used for ttcn3 editor annotation hover support returned by getHoverInfo as an 'object'
 *
 * @author Miklos Magyari
 */
public class MarkerHoverContent {
	private String text;
	private HoverProposal[] proposals;
	
	public MarkerHoverContent(final String text, final HoverProposal[] proposals) {
		this.text = text;
		this.proposals = proposals;
	}

	public String getText() {
		return text == null ? "" : text;
	}

	public void setText(final String text) {
		this.text = text;
	}

	public HoverProposal[] getProposals() {
		return proposals;
	}
	
	public HoverProposal getProposal(final int index) {
		return proposals == null ? null : proposals[index];
	}

	public void setProposals(final HoverProposal[] proposals) {
		this.proposals = proposals;
	}
	
	public int nrOfProposals() {
		return proposals == null ? 0 : proposals.length;
	}
}
