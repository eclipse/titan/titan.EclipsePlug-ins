/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.preferences.PreferenceHandler;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * This class represents a categorized collection of strings (containing html)
 * 
 * Used for ttcn3 editor hover support returned by getHoverInfo2 as an 'object'
 *
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class Ttcn3HoverContent {
	private static final String LINK_PATTERN = "<a href=\"{0}\">{0}</a>";
	private static final String ICON_PATTERN = "<div class=\"title-with-icon\"><img src=\"{0}\"><span style=\"margin-left: 5px\">";
	private static final String DIV_END = "</div>";
	private static final String SPAN_END = "</span>";
	private static final String DIV_TAG = "<div class=\"tag\">";
	private static final String DIV_INDENTED = "<div class=\"indented-block\">";

	private StringBuilder sb = new StringBuilder();
	private StringBuilder bufferSb = new StringBuilder();

	private Map<HoverContentType, String> map = new HashMap<HoverContentType, String>();

	private boolean hasIcon = false;

	/**
	 * Default constructor
	 */
	public Ttcn3HoverContent() {
		// Do nothing
	}

	/**
	 * Adds the specified text as {@link HoverContentType#INFO} to the content
	 * @param text text to add as {@link HoverContentType#INFO}
	 */
	public Ttcn3HoverContent(final String text) {
		this(HoverContentType.INFO, text);
	}

	/**
	 * Adds the specified text to the specified content type
	 * @param type content type
	 * @param text text to add
	 */
	public Ttcn3HoverContent(final HoverContentType type, final String text) {
		addContent(type, text);
	}

	/**
	 * Adds the specified text to the specified content type
	 * @param type content type
	 * @param text text to add
	 */
	public void addContent(final HoverContentType type, final String text) {
		if (text == null || text.isEmpty()) {
			map.put(type, text);
			return;
		}
		sb = new StringBuilder(text);
		addContent(type);
	}

	public void addContent(final HoverContentType type) {
		map.put(type, sb.toString());
		sb.setLength(0);
	}

	public boolean hasText(final HoverContentType type) {
		return map.get(type) != null;
	}

	public String getText(final HoverContentType type) {
		return getText(type, false);
	}
	
	public String getText(final HoverContentType type, boolean isDocView) {
		String url = null;
		switch (ColorManager.getColorTheme()) {
		case Dark:
			url = getStyleSheetUrl("HoverInfoDark.css");
			break;
		case Light:
		default:
			url = getStyleSheetUrl("HoverInfo.css");
			break;
		}
		StringBuilder styled = new StringBuilder();
		if (url != null) {
			styled.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
			styled.append(url);
			styled.append("\">");
			
		} else {
			styled.append(sb);
		}
		Color background = null;
		if (isDocView) {
			background = PreferenceHandler.getEditorBackgroundColor();
		} else {
			background = ColorManager.getInformationBackgroundColor();
		}
		
		styled.append("<style>body { background-color: rgb(");
		styled.append(background.getRed()).append(", ").append(background.getGreen()).append(", ")
			.append(background.getBlue()).append("); }</style>");
		
		final String text = styled.append(map.get(type)).toString();
		if (text != null) {
			return text.replace("@@", "@");
		}
		return null;
	}

	public Ttcn3HoverContent addText(final String text) {
		sb.append(text);
		return this;
	}
	
	public Ttcn3HoverContent addTag(final String text) {
		sb.append(DIV_TAG)
			.append(text)
			.append(DIV_END);
		return this;
	}

	public Ttcn3HoverContent addTagWithText(final String tag, final String text) {
		sb.append(DIV_TAG)
			.append(tag)
			.append("<span class=\"tagtext\">")
			.append(text)
			.append(SPAN_END).append(DIV_END);
		return this;
	}

	public Ttcn3HoverContent addStyledText(final String text) {
		bufferSb.append(text);
		return this;
	}

	public Ttcn3HoverContent addStyledText(final String text, final int style, final boolean isIndented) {
		boolean isItalic = false;
		boolean isBold = false;
		
		if ((style & SWT.ITALIC) != 0) {
			isItalic = true;
		}
		
		if ((style & SWT.BOLD) != 0) {
			isBold= true;
		}
		
		if (isIndented) {
			sb.append(DIV_INDENTED);
		}
		if (isItalic) {
			sb.append("<i>");
		}
		if (isBold) {
			sb.append("<b>");
		}
		sb.append(bufferSb).append(text);
		if (isBold) {
			sb.append("</b>");
		}
		if (isItalic) {
			sb.append("</i>");
		}
		if (isIndented) {
			sb.append(DIV_END);
		}
		bufferSb.setLength(0);
		return this;
	}
	
	public Ttcn3HoverContent addStyledText(final String text, final int style) {
		return addStyledText(text, style, false);
	}

	public Ttcn3HoverContent addIndentedText(final String text) {
		return addIndentedText(text, 0);
	}

	public Ttcn3HoverContent addIndentedText(final String text, final int style) {
		return addStyledText(text, style, true);
	}

	public Ttcn3HoverContent addIndentedText(final String indentTo, final String textToIndent) {
		if (indentTo == null || textToIndent == null) {
			return this;
		}
		sb.append("<div class=\"list-title\">")
			.append(indentTo)
			.append(DIV_END)
			.append(DIV_INDENTED)
			.append(textToIndent)
			.append(DIV_END);
		
		return this;
	}

	public Ttcn3HoverContent addUrlAsLink(final String url) {
		return addUrlAsLink(url, 0);
	}

	public Ttcn3HoverContent addUrlAsLink(final String url, final int style) {
		if (url == null || url.isEmpty()) {
			return this;
		}
		return addIndentedText(MessageFormat.format(LINK_PATTERN, url), style);
	}

	/**
	 * Adds the specified icon from the icons directory to header. If it is used, the header <b>MUST</b> be closed.
	 * @param iconUri the name of the gif file from the icons directory
	 * @return a reference to this object
	 */
	public Ttcn3HoverContent addIcon(final String iconUri) {
		if (iconUri == null || iconUri.isEmpty()) {
			return this;
		}
		hasIcon = true;
		return addText(MessageFormat.format(ICON_PATTERN, getIconUrl(iconUri)));
	}
	
	public Ttcn3HoverContent addDeprecated() {
		return addStyledText("This object is deprecated<br><br>", SWT.ITALIC);
	}

	public boolean hasIcon() {
		return hasIcon;
	}

	/**
	 * If icon is added to the header, this will close the header section.
	 * @return a reference to this object
	 */
	public Ttcn3HoverContent closeHeader() {
		if (hasIcon) {
			sb.append(SPAN_END).append(DIV_END);
		}
		return this;
	}

	private String getStyleSheetUrl(final String css) {
		return getUrl("css", css);
	}

	private String getIconUrl(final String icon) {
		return getUrl("icons", icon);
	}

	private String getUrl(final String dir, final String filename) {
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		Path path = new Path(dir + "/" + filename);
		URL fileURL = FileLocator.find(bundle, path , null);
		URL fileURL2 = null;
		try {
			fileURL2 = FileLocator.toFileURL(fileURL);
		} catch (IOException e) {
			return null;
		}
		return "file:" + fileURL2.getFile();
	}
}
