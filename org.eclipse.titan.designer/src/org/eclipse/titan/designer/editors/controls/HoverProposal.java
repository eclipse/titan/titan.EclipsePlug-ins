/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.RewriteSessionEditProcessor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.AST.TTCN3.types.ClassTypeBody;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Editor;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.MarkerAnnotation;
import org.eclipse.ui.views.markers.WorkbenchMarkerResolution;

/**
 * This class handles proposal data for markers
 * 
 * @author Miklos Magyari
 *
 */
public class HoverProposal extends WorkbenchMarkerResolution {
	public static final String PROPOSAL = "proposal";
	
	private String label;
	private String image;	
	private IEditorPart editor = null;
	private Location location;
	private int offset;
	private int indent;
	private IMarker marker;
	private MarkerAnnotation model;
	private Object data;
	
	/**
	 * Map of offset/string pairs to be inserted into the document
	 *
	 */
	private Map<Integer, String> inserts = new HashMap<Integer, String>();

	
	public HoverProposal(String label, String image) {
		this.label = label;
		this.image = image;
	}
	
	/**
	 * Creates a proposal with one string to be inserted at a certain location
	 * 
	 * @param label Text that will be shown in the list of available proposals
	 * @param offset Offset of the document position where the proposed text should be inserted
	 * @param textToInsert Text to be inserted
	 */
	public HoverProposal(String label, String image, Location location, int offset, String textToInsert) {
		this.label = label;
		this.image = image;
		this.location = location;
		this.inserts.put(offset, textToInsert);
	}	
	
	/**
	 * Creates a proposal with multiple strings to be inserted at different locations
	 * 
	 * @param label Text that will be shown in the list of available proposals
	 * @param textToInsert Map of positions and texts to be inserted
	 */
	public HoverProposal(String label, String image, Map<Integer, String> textsToInsert) {
		this.label = label;
		this.image = image;
		this.inserts = textsToInsert;
	}
	
	/**
	 * Adds a proposal with multiple strings to be inserted at different locations
	 * 
	 * @param offset Offset of the document position where the proposed text should be inserted
	 * @param textToInsert Map of positions and texts to be inserted
	 * 
	 */
	public void addProposal(int offset, String textToInsert) {
		this.inserts.put(offset, textToInsert);
	}
	
	public void setMarker(IMarker marker, MarkerAnnotation model) {
		this.marker = marker;
		this.model = model;
	}
	
	@Override
	public String getLabel() {
		return label;
	}

	@Override
	/**
	 * Default action for the proposal
	 * 
	 * If not overridden, it inserts all texts at their corresponding offsets
	 * 
	 */
	public void run(IMarker marker) {		
		insertText();
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Image getImage() {
		return image != null ? PlatformUI.getWorkbench().getSharedImages().getImage(image) : null;
	}

	@Override
	public IMarker[] findOtherMarkers(IMarker[] markers) {
		// FIXME
		return new IMarker[] {};
	}
	
	protected void insertText() {
		for (Map.Entry<Integer, String> insert : inserts.entrySet()) {
			insertText(insert.getValue(), insert.getKey());
		}
	}
	
	/**
	 * Inserts text at the end of definition section of the current module or at the end of class definitions
	 * 
	 * In this case, offset is not an offset in the document, but acts as a key to look up the string in the map of inserts
	 * 
	 * @param offsetKey index of string to be inserted
	 */
	public void insertTextAtDefs(int offsetKey) {
		if (data == null) {
			final int offset = getDefinitionsLocation();
			insertText(inserts.get(offsetKey), offset);
		} else {
			if (data instanceof ClassTypeBody) {
				ClassTypeBody body = (ClassTypeBody)data;
				insertText(inserts.get(offsetKey), body.getDefinitionsLocation());
			}
		}
	}
	
	private void getActiveEditor() {
		editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
	}
	
	// FIXME this is completely unfinished
	protected void calculateIndent() {
		getActiveEditor();
		IDocument doc = ((TTCN3Editor) editor).getDocument();
		int indent = 0;
		try {
			while (offset > 0) {
				char c = doc.getChar(offset - indent);
				if (c == '\t') {
					indent += 4;
				} else if (c == ' ') {
					indent++;
				} else {
					break;
				}
			}
		} catch (BadLocationException e) {
			
		}
		this.indent = indent;
	}
	
	private int getDefinitionsLocation() {
		getActiveEditor();
		final IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module actualModule = projectSourceParser.containedModule(file);

		return ((TTCN3Module) actualModule).getAssignmentsScope().getLocation().getOffset() + 1;
	}
	
	protected void insertText(String text, int newOffset) {
		getActiveEditor();
		
		StringBuilder indentedText = new StringBuilder();
		for (int i = 0; i < indent / 4; i++) {
			indentedText.append('\t');
		}
		for (int i = 0; i < indent % 4; i++) {
			indentedText.append(' ');
		}
		indentedText.append('\t');
		indentedText.append(text);
		final MultiTextEdit multiEdit = new MultiTextEdit(newOffset, 0);
		final RewriteSessionEditProcessor processor = new RewriteSessionEditProcessor(((TTCN3Editor) editor).getDocument(),
				multiEdit, TextEdit.UPDATE_REGIONS | TextEdit.CREATE_UNDO);
		multiEdit.addChild(new InsertEdit(newOffset, indentedText.toString()));

		try {
			processor.performEdits();
			if (editor instanceof TTCN3Editor) {
				final IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
				((TTCN3Editor)editor).analyzeCurrentFile(file);
			}
		} catch (BadLocationException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
