/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.util.Locale;

import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;

/**
 * @author Adam Knapp
 * */
public enum HoverContentType {
	/** Information, e.g. documentation comments */
	INFO,
	/** Source code */
	SOURCE;

	public static HoverContentType[] asArray() {
		return new HoverContentType[] { INFO, SOURCE };
	}

	/**
	 * This works like {@link #valueOf(String)} except it is case
	 * insensitive. This and {@link #toString()} should be kept
	 * consistent.
	 */
	public static HoverContentType createInstance(final String str) {
		return HoverContentType.valueOf(str.toUpperCase());
	}

	public static HoverContentType getDefault() {
		return INFO;
	}

	public static String[][] getDisplayNamesAndValues() {
		return new String[][] { { "code comment and info", INFO.toString() },
				{ "code peek", SOURCE.toString() } };
	}

	public static void storeAsProperty(final HoverContentType type) {
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.HOVER_WINDOW_CONTENT, type.toString());
	}

	public static HoverContentType loadAsProperty() {
		final String storedType = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.HOVER_WINDOW_CONTENT);
		return storedType.isEmpty() ? getDefault() : createInstance(storedType);
	}

	@Override
	public String toString() {
		return name().toLowerCase(Locale.ENGLISH);
	}
}
