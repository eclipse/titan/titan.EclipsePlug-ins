/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import org.eclipse.jface.text.AbstractHoverInformationControlManager;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * 
 * @author Miklos Magyari
 *
 */
public class OutlineHoverManager extends AbstractHoverInformationControlManager {
	private Control parent;
	private int width = 600;
	private int height = 400;
	
	public OutlineHoverManager(IInformationControlCreator creator, Control control, MouseEvent e) {
		super(creator);
		install(control);
		getInformationControl();
		setCustomInformationControlCreator(creator);
		setAnchor(ANCHOR_TOP);
		parent = control;
		
		Point realLocation = control.getDisplay().map(control, null, e.x, e.y);
		
		final Ttcn3HoverInfoControl infoControl = (Ttcn3HoverInfoControl)creator;
		infoControl.setSize(width, height);
		infoControl.setLocation(calculateTopLeft(realLocation));
	}

	@Override
	protected void computeInformation() {
		setInformation(null, null);
	}
	
	@Override
	public void presentInformation() {
		Rectangle rect = parent.getDisplay().getBounds();
		rect.x = width;
		rect.y = height;
		showInformationControl(rect);
	}
	
	private Point calculateTopLeft(Point mouse) {
		Rectangle screen = parent.getDisplay().getBounds();
		int x = mouse.x;
		int y = mouse.y;
		if (screen.width < x + width) {
			x -= width + 20;
		} else {
			x += 20;
		}
		if (screen.height < y + height) {
			y -= height + 20;
		} else {
			y += 20;
		}
		
		return new Point(x, y);
	}
}
