/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.wizards;

import java.io.FileNotFoundException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.titan.designer.core.TITANNature;

/**
 * Helper class to open an already existing and configured Titan project that is not part of 
 * the actual workspace, i.e. helps to bring in the project into the current workspace context. 
 * The intention is to provide simplified functionality of "Import projects from File System or Archive" wizard.
 * 
 * @author Adam Knapp
 * */
public final class TITANProjectOpener {

	/**
	 * Checks whether the specified project is a TITAN project or not
	 * @param project Project to check
	 * @return Whether the specified project is a TITAN project or not
	 */
	public static boolean check(final IProject project) {
		if (project == null || !project.isAccessible()) {
			return false;
		}
		if (!TITANNature.hasTITANNature(project)) {
			return false;
		}
		if (!TITANNature.hasTITANBuilder(project) && !TITANNature.hasTITANJavaBuilder(project)) {
			return false;
		}
		return true;
	}

	/**
	 * Creates a new project in the actual workspace according to the project description on the specified path 
	 * @param path Path of the folder that contains the project with the project description file (".project")
	 * @return If a project with the same name already exists in the workspace then the handler of this project
	 *         is returned, otherwise the project on the specified path is created in the workspace.
	 *         {@code null} is returned upon empty or null path
	 * @throws CoreException, FileNotFoundException
	 */
	public static IProject open(final IPath path) throws CoreException, FileNotFoundException {
		if (path == null || path.isEmpty()) {
			return null;
		}
		final String projectName = path.lastSegment();
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IProject[] projects = workspace.getRoot().getProjects();
		for (final IProject project : projects) {
			if (project.getName().equals(projectName)) {
				return project;
			}
		}
		IPath toProjectFile = path.append(".project");
		if (!toProjectFile.isAbsolute()) {
			toProjectFile = workspace.getRoot().getLocation().append(toProjectFile);
		}
		if (!toProjectFile.toFile().exists()) {
			throw new FileNotFoundException("Project description file (.project) was not found "
					+ "on the specified path: `" + toProjectFile.toOSString() + "'");
		}
		final IProjectDescription description = workspace.loadProjectDescription(toProjectFile);
		for (final IProject project : projects) {
			if (project.getName().equals(description.getName())) {
				return project;
			}
		}
		IProject newProject = workspace.getRoot().getProject(description.getName());
		newProject.create(description, null);
		newProject.open(IResource.FORCE, null);
		newProject.refreshLocal(IResource.DEPTH_INFINITE, null);
		return newProject;
	}

	/**
	 * Creates a new project in the actual workspace according to the project description on the specified path
	 * @param pathString Path of the folder that contains the project with the project description file (".project")
	 * @return If a project with the same name already exists in the workspace then the handler of this project
	 *         is returned, otherwise the project on the specified path is created in the workspace.
	 *         {@code null} is returned upon empty or null path
	 * @throws CoreException, FileNotFoundException
	 */
	public static IProject open(final String pathString) throws CoreException, FileNotFoundException {
		return open(new Path(pathString));
	}
}
