/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.PlatformUI;
import org.osgi.service.prefs.Preferences;

/**
 * Helper class for handling different preferences
 * 
 * @author Miklos Magyari
 */
public final class PreferenceHandler {
	private static final String EDITOR_BACKGROUND_NODE = "org.eclipse.ui.editors";
	private static final String EDITOR_BACKGROUND_ENTRY = "AbstractTextEditor.Color.Background";
	
	public static String getPreferenceEntry(final String node, final String entry) {
		Preferences preferences = InstanceScope.INSTANCE.getNode(node);
		if (preferences == null) {
			return null;
		}
		String value = preferences.get(entry, null);
		if (value == null) {
			preferences = DefaultScope.INSTANCE.getNode(node);
			value= preferences.get(entry, null);
		}
		return value;
	}
	
	public static Color getEditorBackgroundColor() {
		final String color = getPreferenceEntry(EDITOR_BACKGROUND_NODE, EDITOR_BACKGROUND_ENTRY);
		if (color == null) {
			return null;
		}
		
		return new Color(PlatformUI.getWorkbench().getDisplay(), StringConverter.asRGB(color));
	}
}
