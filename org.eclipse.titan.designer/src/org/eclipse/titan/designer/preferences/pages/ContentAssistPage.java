/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.editors.controls.HoverContentType;
import org.eclipse.titan.designer.preferences.PreferenceConstantValues;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * @author Kristof Szabados
 * */
public final class ContentAssistPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	private static final String DESCRIPTION = "Preferences for the editor's content assist";
	private static final String[][] SORTING_OPTIONS = new String[][] {
			{ PreferenceConstantValues.SORT_BY_RELEVANCE, PreferenceConstantValues.SORT_BY_RELEVANCE },
			{ PreferenceConstantValues.SORT_ALPHABETICALLY, PreferenceConstantValues.SORT_ALPHABETICALLY } };

	private Composite pageComposite;

	private Group insertionGroup;
	private Composite insertionComposite;
	private BooleanFieldEditor insertSingleProposal;
	private BooleanFieldEditor insertCommonPrefixes;
	private Composite sortingComposite;
	private Group sortingGroup;
	private ComboFieldEditor sorting;
	private IntegerFieldEditor proposalListSize;
	private Group autoActivationGroup;
	private Composite autoActivationComposite;
	private BooleanFieldEditor enableAutoActivation;
	private IntegerFieldEditor autoActivationDelay;
	private Group hoverWindowContentGroup;
	private Composite hoverWindowContentComposite;
	private BooleanFieldEditor enableCodeHoverPopups;
	private ComboFieldEditor hoverWindowContentCombo;

	public ContentAssistPage() {
		super(GRID);
	}

	@Override
	public void init(final IWorkbench workbench) {
		setDescription(DESCRIPTION);
	}

	@Override
	protected void createFieldEditors() {
		pageComposite = new Composite(getFieldEditorParent(), SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		pageComposite.setLayout(layout);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		pageComposite.setLayoutData(gridData);

		insertionGroup = new Group(pageComposite, SWT.SHADOW_ETCHED_OUT);
		insertionGroup.setText("Insertion");
		insertionGroup.setLayout(new GridLayout(2, false));
		insertionGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		insertionComposite = new Composite(insertionGroup, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.horizontalIndent = 3;
		insertionComposite.setLayoutData(gridData);

		insertSingleProposal = new BooleanFieldEditor(PreferenceConstants.CONTENTASSISTANT_SINGLE_PROPOSAL_INSERTION,
				"Insert single proposals automatically", insertionComposite);
		addField(insertSingleProposal);

		insertCommonPrefixes = new BooleanFieldEditor(PreferenceConstants.CONTENTASSISTANT_COMMON_PREFIX_INSERTION,
				"Insert common prefixes automatically", insertionComposite);
		addField(insertCommonPrefixes);

		sortingGroup = new Group(pageComposite, SWT.SHADOW_ETCHED_OUT);
		sortingGroup.setText("Sorting");
		sortingGroup.setLayout(new GridLayout(2, false));
		sortingGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		sortingComposite = new Composite(sortingGroup, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.horizontalIndent = 3;
		sortingComposite.setLayoutData(gridData);

		sorting = new ComboFieldEditor(PreferenceConstants.CONTENTASSISTANT_PROPOSAL_SORTING, "Sort proposals", SORTING_OPTIONS,
				sortingComposite);
		final Label text = sorting.getLabelControl(sortingComposite);
		text.setToolTipText("Sort proposals");
		addField(sorting);

		proposalListSize = new IntegerFieldEditor(PreferenceConstants.CONTENTASSISTANT_PROPOSALLIST_SIZE,
				"Maximum amount of proposals to show", sortingComposite, 4);
		proposalListSize.setValidRange(0, 9999);
		proposalListSize.setErrorMessage("Value of 'Maximum amount of proposals to show' must be an Integer between 0 and 9,999");
		final Label proposalListSizeText = proposalListSize.getLabelControl(sortingComposite);
		proposalListSizeText.setToolTipText("Due to performance consideration, this preference allows to "
				+ "limit the amount of proposals displayed at once. Zero means unlimited.");
		addField(proposalListSize);

		autoActivationGroup = new Group(pageComposite, SWT.SHADOW_ETCHED_OUT);
		autoActivationGroup.setText("Auto-Activation");
		autoActivationGroup.setLayout(new GridLayout(2, false));
		autoActivationGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		autoActivationComposite = new Composite(autoActivationGroup, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.horizontalIndent = 3;
		autoActivationComposite.setLayoutData(gridData);

		enableAutoActivation = new BooleanFieldEditor(PreferenceConstants.CONTENTASSISTANT_AUTO_ACTIVATION, "Enable auto activation",
				autoActivationComposite);
		addField(enableAutoActivation);

		autoActivationDelay = new IntegerFieldEditor(PreferenceConstants.CONTENTASSISTANT_AUTO_ACTIVATION_DELAY, "Auto activation delay:",
				autoActivationComposite);
		autoActivationDelay.setValidRange(10, 1000);
		autoActivationDelay.setTextLimit(10);
		autoActivationDelay.setEnabled(doGetPreferenceStore().getBoolean(PreferenceConstants.CONTENTASSISTANT_AUTO_ACTIVATION),
				autoActivationComposite);
		addField(autoActivationDelay);
		
		hoverWindowContentGroup = new Group(pageComposite, SWT.SHADOW_ETCHED_OUT);
		hoverWindowContentGroup.setText("Content of hover window");
		hoverWindowContentGroup.setLayout(new GridLayout(2, false));
		hoverWindowContentGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		hoverWindowContentComposite = new Composite(hoverWindowContentGroup, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.horizontalIndent = 3;
		hoverWindowContentComposite.setLayoutData(gridData);
		
		enableCodeHoverPopups = new BooleanFieldEditor(PreferenceConstants.ENABLE_CODE_HOVER_POPUPS, "Enable code hover popups",
				hoverWindowContentComposite);
		addField(enableCodeHoverPopups);
		hoverWindowContentCombo = new ComboFieldEditor(PreferenceConstants.HOVER_WINDOW_CONTENT,
				"Hover window content:", HoverContentType.getDisplayNamesAndValues(), hoverWindowContentComposite);
		hoverWindowContentCombo.setEnabled(doGetPreferenceStore().getBoolean(PreferenceConstants.ENABLE_CODE_HOVER_POPUPS),
				hoverWindowContentComposite);
		final Label text2 = hoverWindowContentCombo.getLabelControl(hoverWindowContentComposite);
		text2.setToolTipText("What do you want to see as hover tooltip");
		addField(hoverWindowContentCombo);
	}

	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	@Override
	public void dispose() {
		pageComposite.dispose();
		enableAutoActivation.dispose();
		insertionGroup.dispose();
		insertionComposite.dispose();
		insertSingleProposal.dispose();
		insertCommonPrefixes.dispose();
		sortingComposite.dispose();
		sortingGroup.dispose();
		sorting.dispose();
		proposalListSize.dispose();
		autoActivationGroup.dispose();
		autoActivationComposite.dispose();
		enableAutoActivation.dispose();
		autoActivationDelay.dispose();
		enableCodeHoverPopups.dispose();
		hoverWindowContentGroup.dispose();
		hoverWindowContentCombo.dispose();
		super.dispose();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent event) {
		autoActivationDelay.setEnabled(enableAutoActivation.getBooleanValue(), autoActivationComposite);
		hoverWindowContentCombo.setEnabled(enableCodeHoverPopups.getBooleanValue(), hoverWindowContentComposite);
		
		super.propertyChange(event);
	}

	@Override
	protected void performDefaults() {
		autoActivationDelay.setEnabled(true, autoActivationComposite);
		super.performDefaults();
	}

	@Override
	public boolean performOk() {
		final boolean result = super.performOk();
		final IEclipsePreferences node = InstanceScope.INSTANCE.getNode(ProductConstants.PRODUCT_ID_DESIGNER);
		if (node != null) {
			try {
				node.flush();
			} catch (Exception e) {
				ErrorReporter.logExceptionStackTrace(e);
			}
		}

		return result;
	}
}
