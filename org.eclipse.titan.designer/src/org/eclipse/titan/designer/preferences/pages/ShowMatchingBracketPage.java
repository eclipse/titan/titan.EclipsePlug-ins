/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Control;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This preference page holds the controls and functionality to set the matching
 * bracket feature related options.
 * 
 * @author Kristof Szabados
 * @author Miklos Magyari
 */
public final class ShowMatchingBracketPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	static final String DESCRIPTION = "Preferences for the editor's matching brackets";
	static final String HIGHLIGHT_MATCHING_BRACKETS = "Highlight matching brackets";
	static final String MATCHING_BRACKETS_COLORING = "Enable coloring of matching brackets (EXPERIMENTAL)";
	static final String COLOR = "Color:";
	
	private boolean isEnabled;
	private ColorFieldEditor matchingBracketColor;
	private BooleanFieldEditor enableBracketColoring;
	private ColorFieldEditor bracketColor0;
	private ColorFieldEditor bracketColor1;
	private ColorFieldEditor bracketColor2;
	private ColorFieldEditor bracketColor3;
	private ColorFieldEditor bracketColor4;
	private ColorFieldEditor bracketColor5;
	private ColorFieldEditor bracketColor6;
	private ColorFieldEditor bracketColor7;
	
	public ShowMatchingBracketPage() {
		super(GRID);
	}

	@Override
	public void init(final IWorkbench workbench) {
		setDescription(DESCRIPTION);
	}

	@Override
	protected void createFieldEditors() {
		final BooleanFieldEditor enableMatchingBrackets = new BooleanFieldEditor(PreferenceConstants.MATCHING_BRACKET_ENABLED,
				HIGHLIGHT_MATCHING_BRACKETS, getFieldEditorParent());
		addField(enableMatchingBrackets);

		matchingBracketColor = new ColorFieldEditor(PreferenceConstants.COLOR_MATCHING_BRACKET, COLOR,
				getFieldEditorParent());
		addField(matchingBracketColor);
		enableBracketColoring = new BooleanFieldEditor(PreferenceConstants.BRACKET_COLORING_ENABLED, MATCHING_BRACKETS_COLORING, 
				getFieldEditorParent());
		final Control text = enableBracketColoring.getDescriptionControl(getFieldEditorParent());
		text.setToolTipText("The semantic highlighting must be enabled to have effect of this preference");
		addField(enableBracketColoring);
		
		bracketColor0 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_0, "Bracket color level 1",
				getFieldEditorParent());
		addField(bracketColor0);
		bracketColor1 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_1, "Bracket color level 2",
				getFieldEditorParent());
		addField(bracketColor1);
		bracketColor2 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_2, "Bracket color level 3",
				getFieldEditorParent());
		addField(bracketColor2);
		bracketColor3 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_3, "Bracket color level 4",
				getFieldEditorParent());
		addField(bracketColor3);
		bracketColor4 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_4, "Bracket color level 5",
				getFieldEditorParent());
		addField(bracketColor4);
		bracketColor5 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_5, "Bracket color level 6",
				getFieldEditorParent());
		addField(bracketColor5);
		bracketColor6 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_6, "Bracket color level 7",
				getFieldEditorParent());
		addField(bracketColor6);
		bracketColor7 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_7, "Bracket color level 8",
				getFieldEditorParent());
		addField(bracketColor7);
	}

	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}
	
	@Override
    public boolean performOk() {
		getPreferenceStore().setValue(PreferenceConstants.BRACKET_COLORING_ENABLED, isEnabled);
        return super.performOk();
    }
	
	@Override
	public void dispose() {
		matchingBracketColor.dispose();
		enableBracketColoring.dispose();
		bracketColor0.dispose();
		bracketColor1.dispose();
		bracketColor2.dispose();
		bracketColor3.dispose();
		bracketColor4.dispose();
		bracketColor5.dispose();
		bracketColor6.dispose();
		bracketColor7.dispose();
	}
}
