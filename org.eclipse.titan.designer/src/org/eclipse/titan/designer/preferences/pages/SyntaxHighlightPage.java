/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.preferences.PreferenceHandler;
import org.eclipse.titan.designer.preferences.PreferenceInitializer;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This preference page hold the controls and functionality related to syntax
 * coloring.
 * 
 * @author Kristof Szabados
 * @author Adam Knapp
 * @author Miklos Magyari
 */
public final class SyntaxHighlightPage extends PreferencePage implements IWorkbenchPreferencePage {
	private static final String TEMPLATE_CODE = 
			  "// TTCN-3 version of \"Hello, world!\"\n"
			+ "/**\n"
			+ " * @desc Demo module\n"
			+ " * @status deprecated\n"
			+ " * @url https://projects.eclipse.org/projects/tools.titan\n"
			+ " * @author AK, MM\n"
			+ " */\n"
			+ "module MyExample {\n"
			+ "type port PCOType message {\n"
			+ "  inout charstring;\n"
			+ "}\n\n"
			+ "type component MTCType {\n"
			+ "  port PCOType MyPCO_PT;\n"
			+ "}\n\n"
			+ "type record MyRecord {\n"
			+ "  integer x1,\n"
			+ "  charstring c2\n"
			+ "}\n\n"
			+ "type enumerated WorkDays { Monday, Tuesday, Wednesday, Thursday, Friday }\n\n"
			+ "modulepar integer modPar := 9;\n"
			+ "const integer c_integer := 33;\n\n"
			+ "public function myFunction(integer x, charstring y) return integer {\n"
			+ "  return x;\n"
			+ "}\n\n"
			+ "type class BaseClass {\n"
			+ "  private var integer f1;\n"
			+ "  const integer myConst;\n"
			+ "  public var MyRecord myRecord;\n"
			+ "}\n\n"
			+ "testcase tc_HelloW() runs on MTCType system MTCType {\n"
			+ "  timer TL_T := 15.0;\n"
			+ "  map(mtc:MyPCO_PT, system:MyPCO_PT);\n"
			+ "  MyPCO_PT.send(\"Hello, world!\");\n"
			+ "  TL_T.start;\n"
			+ "  alt {\n"
			+ "    [] MyPCO_PT.receive(\"Hello, TTCN-3!\") { TL_T.stop; setverdict(pass); }\n"
			+ "    [] TL_T.timeout { setverdict(inconc); }\n"
			+ "    [] MyPCO_PT.receive { TL_T.stop; setverdict(fail); }\n"
			+ "  }\n"
			+ "}\n\n"
			+ "control {\n"
			+ "  execute(tc_HelloW());\n"
			+ "}\n"
			+ "}\n";
	private Map<String,List<StyleRange>> rangeMap = new HashMap<>();
	private static final String ENABLE_SEMANTIC_HIGHLIGHTING = "Enable semantic highlighting (EXPERIMENTAL)";

	private Composite pageComposite;
	private Composite upperHalfComposite;
	private Composite colorEditorsComposite;
	private ColorFieldEditor foregroundColorEditor;
	private ColorFieldEditor backgroundColorEditor;
	private BooleanFieldEditor isEnabled;
	private BooleanFieldEditor useBackgroundColor;
	private BooleanFieldEditor isBold;
	private BooleanFieldEditor isItalic;
	private BooleanFieldEditor isStrikethrough;
	private BooleanFieldEditor isUnderline;
	private TreeViewer treeViewer;
	private StyledText textViewer;
	private SyntaxhighlightLabelProvider labelProvider;
	private Label previewLabel;
	private BooleanFieldEditor enableSemanticHighlighting;

	private PreferenceStore tempstore;
	private final Map<String, String> possiblyChangedPreferences = new HashMap<String, String>();

	private ColorManager colorManager = new ColorManager();
	private SyntaxHighlightColoringElement element = null;
	private SyntaxHighlightColoringGroup ttcn3SemanticGroup = null;

	private final ISelectionChangedListener treeListener = new ISelectionChangedListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ISelectionChangedListener#
		 * selectionChanged
		 * (org.eclipse.jface.viewers.SelectionChangedEvent)
		 */
		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			if (event.getSelection().isEmpty()) {
				return;
			}

			if (event.getSelection() instanceof IStructuredSelection) {
				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection.size() == 1) {
					if (selection.getFirstElement() instanceof SyntaxHighlightColoringElement) {
						element = (SyntaxHighlightColoringElement) selection.getFirstElement();
						
						switch(element.getBasePreferenceKey()) {
						case PreferenceConstants.COLOR_AST_CONSTANT:
						case PreferenceConstants.COLOR_AST_DEFTYPE:
						case PreferenceConstants.COLOR_AST_DEPRECATED:
						case PreferenceConstants.COLOR_AST_UNUSED:
						case PreferenceConstants.COLOR_AST_VARIABLE:
							isEnabled.setEnabled(true, colorEditorsComposite);
							break;
						default:
							isEnabled.setEnabled(false, colorEditorsComposite);
							break;
						}
						
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.ENABLED);
						isEnabled.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.ENABLED);
						isEnabled.load();

						foregroundColorEditor.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.FOREGROUND);
						foregroundColorEditor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.FOREGROUND);
						foregroundColorEditor.load();

						useBackgroundColor.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.USEBACKGROUNDCOLOR);
						useBackgroundColor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.USEBACKGROUNDCOLOR);
						useBackgroundColor.load();

						backgroundColorEditor.setEnabled(useBackgroundColor.getBooleanValue(), colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.BACKGROUND);
						backgroundColorEditor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.BACKGROUND);
						backgroundColorEditor.load();

						isBold.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.BOLD);
						isBold.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.BOLD);
						isBold.load();

						isItalic.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.ITALIC);
						isItalic.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.ITALIC);
						isItalic.load();

						isStrikethrough.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.STRIKETHROUGH);
						isStrikethrough.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.STRIKETHROUGH);
						isStrikethrough.load();

						isUnderline.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.UNDERLINE);
						isUnderline.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.UNDERLINE);
						isUnderline.load();

						updateStyleRanges(); 
						return;
					}
				}
			}

			isEnabled.setEnabled(false, colorEditorsComposite);
			foregroundColorEditor.setEnabled(false, colorEditorsComposite);
			backgroundColorEditor.setEnabled(false, colorEditorsComposite);
			useBackgroundColor.setEnabled(false, colorEditorsComposite);
			isBold.setEnabled(false, colorEditorsComposite);
			isItalic.setEnabled(false, colorEditorsComposite);
			isStrikethrough.setEnabled(false, colorEditorsComposite);
			isUnderline.setEnabled(false, colorEditorsComposite);
		}
	};

	final class TempPreferenceInitializer extends PreferenceInitializer {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.titan.designer.preferences.PreferenceInitializer
		 * #getPreference()
		 */
		@Override
		public IPreferenceStore getPreference() {
			return tempstore;
		}
	}

	private void loadIntoTemp(final String preferenceName) {
		if (!possiblyChangedPreferences.containsKey(preferenceName)) {
			tempstore.setValue(preferenceName, getPreferenceStore().getString(preferenceName));
		}
	}

	private void storeIntoFinal(final String preferenceName) {
		getPreferenceStore().setValue(preferenceName, tempstore.getString(preferenceName));
	}

	@Override
	public void init(final IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		tempstore = new PreferenceStore();
		final TempPreferenceInitializer initializer = new TempPreferenceInitializer();
		initializer.initializeDefaultPreferences();
	}

	@Override
	public boolean performOk() {
		for (final String changedKey : possiblyChangedPreferences.keySet()) {
			storeIntoFinal(changedKey);
		}
		return super.performOk();
	}

	@Override
	protected void performDefaults() {
		final String[] preferenceNames = tempstore.preferenceNames();
		for (final String preferenceName : preferenceNames) {
			tempstore.setValue(preferenceName, tempstore.getDefaultString(preferenceName));
			possiblyChangedPreferences.put(preferenceName, null);
		}
		if (isEnabled != null) {
			isEnabled.loadDefault();
		}
		if (foregroundColorEditor != null) {
			foregroundColorEditor.loadDefault();
		}
		if (backgroundColorEditor != null) {
			backgroundColorEditor.loadDefault();
		}
		if (useBackgroundColor != null) {
			useBackgroundColor.loadDefault();
		}
		if (isBold != null) {
			isBold.loadDefault();
		}
		if (isItalic != null) {
			isItalic.loadDefault();
		}
		if (isStrikethrough != null) {
			isStrikethrough.loadDefault();
		}
		if (isUnderline != null) {
			isUnderline.loadDefault();
		}
		enableSemanticHighlighting.loadDefault();		
		super.performDefaults();
	}

	@Override
	public void dispose() {
		isEnabled.dispose();
		foregroundColorEditor.dispose();
		backgroundColorEditor.dispose();
		useBackgroundColor.dispose();
		isBold.dispose();
		isItalic.dispose();
		isStrikethrough.dispose();
		isUnderline.dispose();
		previewLabel.dispose();
		textViewer.dispose();
		labelProvider.dispose();
		colorEditorsComposite.dispose();
		upperHalfComposite.dispose();
		enableSemanticHighlighting.dispose();
		pageComposite.dispose();
		super.dispose();
	}

	private void createTreeViewer(final Composite parent) {
		final GridLayout treeLayout = new GridLayout();
		treeLayout.numColumns = 2;
		final GridData treeData = new GridData();
		treeData.horizontalAlignment = GridData.FILL;
		treeData.verticalAlignment = SWT.FILL;
		treeData.grabExcessHorizontalSpace = true;
		treeData.grabExcessVerticalSpace = true;

		treeViewer = new TreeViewer(parent);
		treeViewer.getControl().setLayoutData(treeData);
		treeViewer.setContentProvider(new SyntaxHighlightContentProvider());
		labelProvider = new SyntaxhighlightLabelProvider();
		treeViewer.setLabelProvider(labelProvider);

		treeViewer.setInput(initialInput());
		treeViewer.addSelectionChangedListener(treeListener);
	}

	private void createColorEditors(final Composite parent) {
		colorEditorsComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		colorEditorsComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		colorEditorsComposite.setLayoutData(gridData);

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ENABLED);
		isEnabled = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ENABLED, "Enable semantic highlighting for this type", SWT.CHECK,
				colorEditorsComposite);
		isEnabled.setEnabled(false, colorEditorsComposite);
		isEnabled.setPreferenceStore(tempstore);
		isEnabled.setPage(this);
		isEnabled.load();
		isEnabled.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isEnabled.getPreferenceName(), null);
				isEnabled.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});
		
		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.FOREGROUND);
		foregroundColorEditor = new ColorFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.FOREGROUND,
				"Color:", colorEditorsComposite);
		foregroundColorEditor.setEnabled(false, colorEditorsComposite);
		foregroundColorEditor.setPreferenceStore(tempstore);
		foregroundColorEditor.setPage(this);
		foregroundColorEditor.load();
		foregroundColorEditor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(foregroundColorEditor.getPreferenceName(), null);
				foregroundColorEditor.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.USEBACKGROUNDCOLOR);
		useBackgroundColor = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.USEBACKGROUNDCOLOR,
				"Enable background color", SWT.CHECK, colorEditorsComposite);
		useBackgroundColor.setEnabled(false, colorEditorsComposite);
		useBackgroundColor.setPreferenceStore(tempstore);
		useBackgroundColor.setPage(this);
		useBackgroundColor.load();
		useBackgroundColor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(useBackgroundColor.getPreferenceName(), null);
				useBackgroundColor.store();
				backgroundColorEditor.setEnabled(useBackgroundColor.getBooleanValue(), colorEditorsComposite);
				updateStyleRanges();
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BACKGROUND);
		backgroundColorEditor = new ColorFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BACKGROUND,
				"Background color:", colorEditorsComposite);
		backgroundColorEditor.setEnabled(false, colorEditorsComposite);
		backgroundColorEditor.setPreferenceStore(tempstore);
		backgroundColorEditor.setPage(this);
		backgroundColorEditor.load();
		backgroundColorEditor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(backgroundColorEditor.getPreferenceName(), null);
				backgroundColorEditor.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BOLD);
		isBold = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BOLD, "Bold", SWT.CHECK,
				colorEditorsComposite);
		isBold.setEnabled(false, colorEditorsComposite);
		isBold.setPreferenceStore(tempstore);
		isBold.setPage(this);
		isBold.load();
		isBold.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isBold.getPreferenceName(), null);
				isBold.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ITALIC);
		isItalic = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ITALIC, "Italic", SWT.CHECK,
				colorEditorsComposite);
		isItalic.setEnabled(false, colorEditorsComposite);
		isItalic.setPreferenceStore(tempstore);
		isItalic.setPage(this);
		isItalic.load();
		isItalic.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isItalic.getPreferenceName(), null);
				isItalic.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.STRIKETHROUGH);
		isStrikethrough = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.STRIKETHROUGH, "Striketrough", SWT.CHECK,
				colorEditorsComposite);
		isStrikethrough.setEnabled(false, colorEditorsComposite);
		isStrikethrough.setPreferenceStore(tempstore);
		isStrikethrough.setPage(this);
		isStrikethrough.load();
		isStrikethrough.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isStrikethrough.getPreferenceName(), null);
				isStrikethrough.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.UNDERLINE);
		isUnderline = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.UNDERLINE, "Underline", SWT.CHECK,
				colorEditorsComposite);
		isUnderline.setEnabled(false, colorEditorsComposite);
		isUnderline.setPreferenceStore(tempstore);
		isUnderline.setPage(this);
		isUnderline.load();
		isUnderline.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isUnderline.getPreferenceName(), null);
				isUnderline.store();
				if (element != null) {
					updateStyleRanges();
				}
			}
		});

		GridData wordsGridData = new GridData();
		wordsGridData.horizontalAlignment = GridData.FILL;
		wordsGridData.verticalAlignment = GridData.FILL;
		wordsGridData.horizontalSpan = 2;

		previewLabel = new Label(colorEditorsComposite, SWT.SINGLE);
		previewLabel.setText("Preview: ");
		previewLabel.setVisible(true);
		previewLabel.setLayoutData(wordsGridData);

		wordsGridData = new GridData();
		wordsGridData.horizontalAlignment = GridData.FILL;
		wordsGridData.verticalAlignment = GridData.FILL;
		wordsGridData.horizontalSpan = 2;
		wordsGridData.grabExcessHorizontalSpace = true;
		wordsGridData.grabExcessVerticalSpace = true;

		textViewer = new StyledText(colorEditorsComposite, SWT.V_SCROLL | SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.READ_ONLY);
		textViewer.setEditable(false);
		textViewer.setLayoutData(wordsGridData);
		final Color bgColor = PreferenceHandler.getEditorBackgroundColor();
		if (bgColor != null) {
			textViewer.setBackground(bgColor);
		}
		textViewer.setFont(JFaceResources.getTextFont());
		fillTemplateText();
		
		final IPreferenceStore defaultStore = Activator.getDefault().getPreferenceStore();
		for (Map.Entry<String,List<StyleRange>> entry : rangeMap.entrySet()) {
			final String key = entry.getKey();
			tempstore.setValue(key + PreferenceConstants.ENABLED, defaultStore.getString(key + PreferenceConstants.ENABLED));
			tempstore.setValue(key + PreferenceConstants.FOREGROUND, defaultStore.getString(key + PreferenceConstants.FOREGROUND));
			tempstore.setValue(key + PreferenceConstants.BACKGROUND, defaultStore.getString(key + PreferenceConstants.BACKGROUND));
			tempstore.setValue(key + PreferenceConstants.USEBACKGROUNDCOLOR, defaultStore.getBoolean(key + PreferenceConstants.USEBACKGROUNDCOLOR));
			tempstore.setValue(key + PreferenceConstants.BOLD, defaultStore.getBoolean(key + PreferenceConstants.BOLD));
			tempstore.setValue(key + PreferenceConstants.ITALIC, defaultStore.getBoolean(key + PreferenceConstants.ITALIC));
			tempstore.setValue(key + PreferenceConstants.STRIKETHROUGH, defaultStore.getBoolean(key + PreferenceConstants.STRIKETHROUGH));
			tempstore.setValue(key + PreferenceConstants.UNDERLINE, defaultStore.getBoolean(key + PreferenceConstants.UNDERLINE));
		}
	}

	private void createSemanticHighlightingCheckbox(final Composite parent) {
		loadIntoTemp(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING);
		enableSemanticHighlighting = new BooleanFieldEditor(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING,
				ENABLE_SEMANTIC_HIGHLIGHTING, SWT.CHECK, parent);
		enableSemanticHighlighting.setEnabled(true, parent);
		enableSemanticHighlighting.setPreferenceStore(tempstore);
		enableSemanticHighlighting.setPage(this);
		enableSemanticHighlighting.load();
		enableSemanticHighlighting.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(enableSemanticHighlighting.getPreferenceName(), null);
				enableSemanticHighlighting.store();
				if (ttcn3SemanticGroup != null) {
					ttcn3SemanticGroup.setEnabled(enableSemanticHighlighting.getBooleanValue());
					treeViewer.refresh();
				}
				updateStyleRanges();
			}
		});
	}

	private void createUpperHalf(final Composite parent) {
		upperHalfComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		upperHalfComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		upperHalfComposite.setLayoutData(gridData);

		createTreeViewer(upperHalfComposite);
		createColorEditors(upperHalfComposite);
	}

	@Override
	protected Control createContents(final Composite parent) {
		pageComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		pageComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		pageComposite.setLayoutData(gridData);

		createSemanticHighlightingCheckbox(pageComposite);
		createUpperHalf(pageComposite);

		//updateStyleRanges();
		
		return pageComposite;
	}

	private SyntaxHighlightColoringGroup initialInput() {
		final SyntaxHighlightColoringGroup root = new SyntaxHighlightColoringGroup("root");

		final SyntaxHighlightColoringGroup generalGroup = new SyntaxHighlightColoringGroup("General");
		generalGroup.add(new SyntaxHighlightColoringElement("Plain text", PreferenceConstants.COLOR_NORMAL_TEXT));
		generalGroup.add(new SyntaxHighlightColoringElement("Strings", PreferenceConstants.COLOR_STRINGS));
		generalGroup.add(new SyntaxHighlightColoringElement("Integers", PreferenceConstants.COLOR_INTEGER));
		generalGroup.add(new SyntaxHighlightColoringElement("Floats", PreferenceConstants.COLOR_FLOAT));
		root.add(generalGroup);

		final SyntaxHighlightColoringGroup asn1Group = new SyntaxHighlightColoringGroup("ASN.1 specific");
		asn1Group.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_ASN1_KEYWORDS));
		asn1Group.add(new SyntaxHighlightColoringElement("CMIP verbs", PreferenceConstants.COLOR_CMIP_VERB));
		asn1Group.add(new SyntaxHighlightColoringElement("compare types", PreferenceConstants.COLOR_COMPARE_TYPE));
		asn1Group.add(new SyntaxHighlightColoringElement("Status", PreferenceConstants.COLOR_STATUS));
		asn1Group.add(new SyntaxHighlightColoringElement("Tags", PreferenceConstants.COLOR_TAG));
		asn1Group.add(new SyntaxHighlightColoringElement("Storage", PreferenceConstants.COLOR_STORAGE));
		asn1Group.add(new SyntaxHighlightColoringElement("Modifier", PreferenceConstants.COLOR_MODIFIER));
		asn1Group.add(new SyntaxHighlightColoringElement("Access types", PreferenceConstants.COLOR_ACCESS_TYPE));
		root.add(asn1Group);

		final SyntaxHighlightColoringGroup configGroup = new SyntaxHighlightColoringGroup("Configuration specific");
		configGroup.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_CONFIG_KEYWORDS));
		configGroup.add(new SyntaxHighlightColoringElement("Section title", PreferenceConstants.COLOR_SECTION_TITLE));
		configGroup.add(new SyntaxHighlightColoringElement("File and control mask options",
				PreferenceConstants.COLOR_FILE_AND_CONTROL_MASK_OPTIONS));
		configGroup.add(new SyntaxHighlightColoringElement("External command types", PreferenceConstants.COLOR_EXTERNAL_COMMAND_TYPES));
		root.add(configGroup);

		final SyntaxHighlightColoringGroup ttcn3Group = new SyntaxHighlightColoringGroup("TTCN-3 specific");
		ttcn3Group.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_TTCN3_KEYWORDS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Preprocessor", PreferenceConstants.COLOR_PREPROCESSOR));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Visibility modifiers", PreferenceConstants.COLOR_VISIBILITY_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Template match", PreferenceConstants.COLOR_TEMPLATE_MATCH));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Type", PreferenceConstants.COLOR_TYPE));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Timer operators", PreferenceConstants.COLOR_TIMER_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Port operators", PreferenceConstants.COLOR_PORT_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Config operators", PreferenceConstants.COLOR_CONFIG_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Verdict operators", PreferenceConstants.COLOR_VERDICT_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("System under test related operators", PreferenceConstants.COLOR_SUT_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Function operators", PreferenceConstants.COLOR_FUNCTION_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Predefined operators", PreferenceConstants.COLOR_PREDEFINED_OP));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Boolean consts", PreferenceConstants.COLOR_BOOLEAN_CONST));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Verdict consts", PreferenceConstants.COLOR_TTCN3_VERDICT_CONST));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Other consts", PreferenceConstants.COLOR_OTHER_CONST));
		root.add(ttcn3Group);
		
		final SyntaxHighlightColoringGroup commentGroup = new SyntaxHighlightColoringGroup("Comments");
		commentGroup.add(new SyntaxHighlightColoringElement("Comment default", PreferenceConstants.COLOR_COMMENTS));
		commentGroup.add(new SyntaxHighlightColoringElement("Document comment tag", PreferenceConstants.COLOR_COMMENT_TAG));
		commentGroup.add(new SyntaxHighlightColoringElement("Nested HTML tag", PreferenceConstants.COLOR_HTML_TAG));
		root.add(commentGroup);

		ttcn3SemanticGroup = new SyntaxHighlightColoringGroup("TTCN-3 semantic specific");
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Constants",
				PreferenceConstants.COLOR_AST_CONSTANT));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Deprecated elements",
				PreferenceConstants.COLOR_AST_DEPRECATED));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Type definitions",
				PreferenceConstants.COLOR_AST_DEFTYPE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Variables",
				PreferenceConstants.COLOR_AST_VARIABLE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Unused definitions",
				PreferenceConstants.COLOR_AST_UNUSED));
		ttcn3SemanticGroup.setEnabled(enableSemanticHighlighting.getBooleanValue());
		root.add(ttcn3SemanticGroup);

		return root;
	}
	
	private void addTemplateText(String key, String text) {
		addTemplateText(key, text, null);
	}
	
	private void addTemplateText(String key, String text, String optWhiteSpace) {
		int start = textViewer.getText().length();
		textViewer.append(text);
		final StyleRange range = colorManager.createStyleRangeFromPreference(key, Activator.getDefault().getPreferenceStore(), start, text.length());
		
		textViewer.setStyleRange(range);
		List<StyleRange> list = rangeMap.containsKey(key) ? rangeMap.get(key) : new ArrayList<StyleRange>();
		list.add(range);
		rangeMap.put(key, list);
		
		if (optWhiteSpace != null) {
			textViewer.append(optWhiteSpace);
		}
	}
	
	private void fillTemplateText() {
		addTemplateText(PreferenceConstants.COLOR_COMMENTS, "/**", "\n ");
		addTemplateText(PreferenceConstants.COLOR_COMMENTS, " *", " ");
		addTemplateText(PreferenceConstants.COLOR_COMMENT_TAG, "@desc", " ");
		addTemplateText(PreferenceConstants.COLOR_COMMENTS, "Syntax highlighting preview");
		addTemplateText(PreferenceConstants.COLOR_HTML_TAG, "<br>", "\n");
		addTemplateText(PreferenceConstants.COLOR_COMMENTS, " */", "\n");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "module", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "MyExample", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "{", "\n");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "type", " ");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "port", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_DEFTYPE, "PCOType", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "message", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "{", "\n\t");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "inout", " ");
		addTemplateText(PreferenceConstants.COLOR_TYPE, "charstring");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ";", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "}", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "type", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "component", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "MTCType", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "{", "\n\t");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "port", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_DEFTYPE, "PCOType", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "MyPCO_PT");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ";", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "}", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "type", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "record", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_DEFTYPE, "MyRecord", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "{", "\n\t");
		addTemplateText(PreferenceConstants.COLOR_TYPE, "integer", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_VARIABLE, "x1");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ",", "\n\t");
		addTemplateText(PreferenceConstants.COLOR_TYPE, "charstring", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_VARIABLE, "c2", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "}", "\n");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "type", " ");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "enumerated", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_DEFTYPE, "WorkDays", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "{", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "Monday");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ",", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "Tuesday");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ",", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "Wednesday");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ",", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "Thursday");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ",", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "Friday", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "}", "\n");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "const", " ");
		addTemplateText(PreferenceConstants.COLOR_TYPE, "integer", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_CONSTANT, "c_integer", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ":=", " ");
		addTemplateText(PreferenceConstants.COLOR_INTEGER, "33");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ";", "\n");
		addTemplateText(PreferenceConstants.COLOR_TTCN3_KEYWORDS, "var", " ");
		addTemplateText(PreferenceConstants.COLOR_TYPE, "float", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_VARIABLE, "dep", " ");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ":=", " ");
		addTemplateText(PreferenceConstants.COLOR_AST_DEPRECATED, "deprecated_function()");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, ";", "\n");
		addTemplateText(PreferenceConstants.COLOR_NORMAL_TEXT, "}");
		updateStyleRanges();
	}
		
	private void updateStyleRanges() {
		try {
			final boolean isSemantic = enableSemanticHighlighting.getBooleanValue();
			for (Map.Entry<String,List<StyleRange>> entry : rangeMap.entrySet()) {
				String base = entry.getKey();
				if (! isSemantic) {
					switch(entry.getKey()) {
					case PreferenceConstants.COLOR_AST_CONSTANT:
					case PreferenceConstants.COLOR_AST_VARIABLE:
					case PreferenceConstants.COLOR_AST_DEFTYPE:
					case PreferenceConstants.COLOR_AST_DEPRECATED:
					case PreferenceConstants.COLOR_AST_UNUSED:
						base = PreferenceConstants.COLOR_NORMAL_TEXT;
						break;
					default:
					}
				} 
				for (StyleRange oldRange : entry.getValue()) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							base, tempstore, oldRange.start, oldRange.length));
				}
			}
		} catch (Exception e) {

		}
	}
}
