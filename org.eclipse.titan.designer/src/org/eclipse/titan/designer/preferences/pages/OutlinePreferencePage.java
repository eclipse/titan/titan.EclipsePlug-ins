/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * Preferences page for the outline view
 * 
 * @author Miklos Magyari
 *
 */
public class OutlinePreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	private static final String DESCRIPTION = "Preferences for the outline page";
	
	private Composite pageComposite;
	private Group prefsGroup;
	private Composite prefsComposite;
	private BooleanFieldEditor enableSemanticHighlighting;
	private BooleanFieldEditor enableHoverTooltip;
	
	public OutlinePreferencePage() {
		super(GRID);
	}
	
	@Override
	public void init(IWorkbench workbench) {
		setDescription(DESCRIPTION);
	}

	@Override
	protected void createFieldEditors() {
		pageComposite = new Composite(getFieldEditorParent(), SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		pageComposite.setLayout(layout);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		pageComposite.setLayoutData(gridData);

		prefsGroup = new Group(pageComposite, SWT.SHADOW_ETCHED_OUT);
		prefsGroup.setText("Outline preferences");
		prefsGroup.setLayout(new GridLayout(2, false));
		prefsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		prefsComposite = new Composite(prefsGroup, SWT.NONE);
		gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		gridData.horizontalIndent = 3;
		prefsComposite.setLayoutData(gridData);

		enableSemanticHighlighting = new BooleanFieldEditor(PreferenceConstants.OUTLINE_ENABLE_SEMANTIC_HIGHLIGHTING,
				"Enable semantic highlighting", prefsComposite);
		addField(enableSemanticHighlighting);

		enableHoverTooltip = new BooleanFieldEditor(PreferenceConstants.OUTLINE_ENABLE_HOVER_TOOLTIP,
				"Enable hover tooltips", prefsComposite);
		addField(enableHoverTooltip);
	}
	
	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	@Override
	public void dispose() {
		pageComposite.dispose();
		prefsGroup.dispose();
		prefsComposite.dispose();
		enableSemanticHighlighting.dispose();
		enableHoverTooltip.dispose();
	}

	@Override
	protected void performDefaults() {
		super.performDefaults();
	}

	@Override
	public boolean performOk() {
		final boolean result = super.performOk();
		final IEclipsePreferences node = InstanceScope.INSTANCE.getNode(ProductConstants.PRODUCT_ID_DESIGNER);
		if (node != null) {
			try {
				node.flush();
			} catch (Exception e) {
				ErrorReporter.logExceptionStackTrace(e);
			}
		}

		return result;
	}
}
