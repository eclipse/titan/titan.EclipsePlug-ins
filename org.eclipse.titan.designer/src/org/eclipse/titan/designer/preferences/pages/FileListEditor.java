package org.eclipse.titan.designer.preferences.pages;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.properties.data.FileBuildPropertyData;

/**
 * Handles the list of files related to the selective code splitting feature 
 * @author Adam Knapp
 * */
public class FileListEditor extends ExcludeRegexpEditor {
	private static final String[] FILTER_EXTENSIONS = {"*.*", "*.ttcn;*.ttcn3;*.ttcnpp", "*.asn;*.asn1"};
	private static final String[] FILTER_NAMES = {"All", "TTCN files", "ASN.1 files"};

	private final IProject project;
	private final Set<IFile> fileSet = new HashSet<>();
	private final Set<IFile> fileSetWc = new HashSet<>();

	public FileListEditor(final String name, final String labelText,
			final Composite parent, final IProject project) {
		super(name, labelText, parent);
		this.project = project;
	}

	@Override
	protected void createButtons(Composite box) {
		addButton = createPushButton(box, "Add");
		editButton = createPushButton(box, "Edit");
		removeButton = createPushButton(box, "Remove");
	}

	@Override
	protected String[] getNewInputObjects() {
		clearMessage();
		final FileDialog dialog = new FileDialog(getShell(), SWT.OPEN | SWT.MULTI);
		dialog.setText("File(s) to split");
		dialog.setFilterExtensions(FILTER_EXTENSIONS);
		dialog.setFilterNames(FILTER_NAMES);
		dialog.setFilterPath(project.getLocation().toOSString());
		dialog.open();

		String[] selection = dialog.getFileNames();
		if (selection.length == 0) {
			return null;
		}

		final String path = Path.fromOSString(dialog.getFilterPath())
				.makeRelativeTo(project.getLocation()).toOSString();

		if (selection.length == 1) {
			selection[0] = addToFileSet(path, selection[0]);
			return selection[0] == null ? null : selection;  
		}

		final List<String> selectionList = new ArrayList<>(selection.length);
		for (int i = 0; i < selection.length; i++) {
			final String filePath = addToFileSet(path, selection[i]);
			if (filePath != null) {
				selectionList.add(filePath);
			}
		}
		return selectionList.toArray(String[]::new);
	}

	public boolean isChanged() {
		return !fileSet.equals(fileSetWc);
	}

	@Override
	protected String getEditInputObject(final String original) {
		clearMessage();
		final FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
		dialog.setText("File to split");
		dialog.setFilterExtensions(FILTER_EXTENSIONS);
		dialog.setFilterNames(FILTER_NAMES);
		dialog.open();
		final String selection = dialog.getFileName();
		if (selection == null) {
			return original;
		}
		final String path = Path.fromOSString(dialog.getFilterPath())
				.makeRelativeTo(project.getLocation()).toOSString();
		final String filePath = addToFileSet(path, selection);
		if (filePath == null) {
			return original;
		}
		fileSetWc.removeIf(f -> f.getProjectRelativePath().toOSString().equals(original));
		return filePath;
	}

	@Override
	public void loadDefault() {
		clearMessage();
		list.removeAll();
		fileSetWc.clear();
	}

	@Override
	public void load() {
		final ResourceVisitor rv = new ResourceVisitor();
		try {
			project.accept(rv);
		} catch (CoreException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}
		fileSet.addAll(rv.fileSet);
		fileSetWc.addAll(rv.fileSet);
		list.removeAll();
		for (final IFile file : fileSet) {
			list.add(file.getProjectRelativePath().toOSString());
		}
	}

	@Override
	public void store() {
		// Files on which the code splitting should be disabled
		final Set<IFile> diff1 = new HashSet<>(fileSet);
		diff1.removeAll(fileSetWc);
		for (final IFile file : diff1) {
			FileBuildPropertyData.setPropertyValue(file, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY, false);
		}
		// Files on which the code splitting should be enabled
		final Set<IFile> diff2 = new HashSet<>(fileSetWc);
		diff2.removeAll(fileSet);
		for (final IFile file : diff2) {
			FileBuildPropertyData.setPropertyValue(file, FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY, true);
		}
		fileSet.clear();
		fileSet.addAll(fileSetWc);
	}

	@Override
	protected boolean removeObject(final String toBeRemoved) {
		clearMessage();
		return fileSetWc.removeIf(f -> f.getProjectRelativePath().toOSString().equals(toBeRemoved));
	}

	private String addToFileSet(final String path, final String fileName) {
		final String filePath = getFilePath(path, fileName);
		final IFile file = (IFile)project.findMember(filePath);
		if (file == null) {
			showMessage("File not processed, no change performed");
			return null;
		}
		final boolean modified = fileSetWc.add(file);
		if (!modified) {
			showMessage("Duplicate elements have been removed");
			return null;
		}
		return filePath;
	}

	private String getFilePath(final String path, final String fileName) {
		final StringBuilder sb = new StringBuilder(path);
		return sb.append(File.separatorChar).append(fileName).toString();
	}

	/**
	 * Collects the files from the project on which the code splitting property is enabled.
	 */
	private static final class ResourceVisitor implements IResourceVisitor {
		final Set<IFile> fileSet = new HashSet<>();

		@Override
		public boolean visit(IResource resource) throws CoreException {
			if (resource != null && resource.isAccessible() && resource.getType() == IResource.FILE &&
					FileBuildPropertyData.getPropertyValue((IFile)resource,
							FileBuildPropertyData.ENABLE_CODE_SPLITTING_PROPERTY)) {
				fileSet.add((IFile)resource);
			}
			return true;
		}
	}
}
