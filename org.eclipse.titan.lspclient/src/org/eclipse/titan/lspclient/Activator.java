/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lspclient;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in's life cycle.
 * 
 * @author Adam Knapp
 */
public final class Activator extends AbstractUIPlugin {
	public static final String TITAN = "titan";
	public static final String TITAN_PREFIX = "org.eclipse." + TITAN;
	public static final String PRODUCT_ID_LSPCLIENT = TITAN_PREFIX + ".lspclient";
	/** The plug-in ID */
	public static final String PLUGIN_ID = PRODUCT_ID_LSPCLIENT;
	/** Language server ID from the plugin.xml */
	public static final String SERVER_ID = PRODUCT_ID_LSPCLIENT + ".ttcn.server";
	/** Marker type of LSP Client */
	public static final String TITAN_LSP_MARKER_TYPE = "org.eclipse.titan.lspclient.marker";
	/** Marker type of LSP Client for syntactic problems, used only during testing */
	public static final String TITAN_LSP_MARKER_TYPE_SYNTACTIC = TITAN_LSP_MARKER_TYPE + ".syntax";
	/** Marker type of LSP Client for semantic problems, used only during testing */
	public static final String TITAN_LSP_MARKER_TYPE_SEMANTIC = TITAN_LSP_MARKER_TYPE + ".semantic";
	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
		// Do nothing
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
}
