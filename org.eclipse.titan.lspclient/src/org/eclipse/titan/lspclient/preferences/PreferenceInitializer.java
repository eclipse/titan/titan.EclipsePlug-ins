/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lspclient.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.titan.lspclient.Activator;

/**
 * This class is used for initializing the internal values to their default state.
 * Currently, the values are static, i.e. not configurable via the UI.
 * 
 * @author Adam Knapp
 */
public final class PreferenceInitializer extends AbstractPreferenceInitializer {
	private static final String IGNORE = "ignore";
	private static final String WARNING = "warning";
	private static final String ERROR = "error";

	public static final String PREFIX = Activator.PRODUCT_ID_LSPCLIENT + ".";

	// LSP Client specific preferences
	public static final String  LS_DEBUG = PREFIX + "lsDebug";

	// copy of the configuration options of the LS  
	public static final String DISPLAY_DEBUG_INFORMATION = PREFIX + "displayDebugInformation";
	public static final String REPORT_IGNORED_PREPROCESSOR_DIRECTIVES = PREFIX + "reportIgnoredPreprocessorDirectives";
	public static final String LIMIT_ALL_THREAD_CREATION = PREFIX + "limitAllThreadCreation";
	public static final String PROCESSING_UNITS_TO_USE = PREFIX + "processingUnitsToUse";
	public static final String ENABLE_TITANIUM = PREFIX + "enableTitanium";
	public static final String ENABLE_INLAY_HINTS = PREFIX + "enableInlayHints";

	public static final String ON_THE_FLY_PREFERENCES = PREFIX + "onTheFlyChecker";
	public static final String USE_ON_THE_FLY_PARSING = ON_THE_FLY_PREFERENCES + ".useOnTheFlyParsing"; 
	public static final String USE_INCREMENTAL_PARSING = ON_THE_FLY_PREFERENCES + ".useIncrementalParsing";
	public static final String ENABLE_OOP_EXTENSION = ON_THE_FLY_PREFERENCES + ".enableOop";
	public static final String ENABLE_REALTIME_EXTENSION = ON_THE_FLY_PREFERENCES + ".enableRealtime";
	public static final String ENABLE_DOC_COMMENT_ON_THE_FLY_CHECK = ON_THE_FLY_PREFERENCES + ".enableDocComments";
	public static final String REPORT_NAMING_CONVENTION_PROBLEMS = ON_THE_FLY_PREFERENCES + ".enableNamingConventionCheck";
	public static final String NAMING_CONVENTION_SEVERITY = ON_THE_FLY_PREFERENCES + ".namingConventionSeverity";
	public static final String REPORT_STRICT_CONSTANTS = ON_THE_FLY_PREFERENCES + ".reportStrictConstants";
	public static final String REPORT_DOC_COMMENT_INCONSISTENCY = ON_THE_FLY_PREFERENCES + ".reportDocumentCommentIncosistency";
	public static final String REPORT_MODULE_NAME_REUSE = ON_THE_FLY_PREFERENCES + ".reportModuleNameReuse";
	public static final String REPORT_ERRORS_IN_EXTENSION_SYNTAX = ON_THE_FLY_PREFERENCES + ".reportErrorsInExtensionSyntax";
	public static final String REPORT_KEYWORD_USED_AS_IDENTIFIER = ON_THE_FLY_PREFERENCES + ".reportKeywordUsedAsidentifier";
	public static final String REPORT_DUPLICATED_UNIQUE_FIELD_VALUE = ON_THE_FLY_PREFERENCES + ".reportDuplicatedUniqueFieldValue";
	public static final String ON_THE_FLY_CHECKER_TEST_MODE = ON_THE_FLY_PREFERENCES + ".onTheFlyCheckerTestMode";

	public static final String OUTLINE = PREFIX + "outline";
	public static final String OUTLINE_SHOW_FUNCTIONS = OUTLINE + ".showFunctions";
	public static final String OUTLINE_SHOW_TESTCASES = OUTLINE + ".showTestcases";
	public static final String OUTLINE_SHOW_CLASSES = OUTLINE + ".showClasses";

	public static final String NAMING_CONVENTION = PREFIX + "namingConvention";
	public static final String NAMING_CONVENTION_TESTCASE = NAMING_CONVENTION + ".testcase";

	public static final String COMPILER_PREFERENCES = PREFIX + "compiler";
	public static final String ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE = COMPILER_PREFERENCES + ".analyzeOnlyWhenTpdIsActive";
	public static final String ACTIVATED_TPD = COMPILER_PREFERENCES + ".activatedTpd";
	public static final String LOG_PARSE_TREE = COMPILER_PREFERENCES + ".logParseTree";
	public static final String TITAN_INSTALLATION_PATH = COMPILER_PREFERENCES + ".titanInstallationPath";
	public static final String LICENSE_FILE_PATH = COMPILER_PREFERENCES + ".licenseFilePath";

	@Override
	public final void initializeDefaultPreferences() {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();

		// LSP Client specific preferences
		store.setDefault(LS_DEBUG, true);

		// LS specific preferences
		store.setDefault(DISPLAY_DEBUG_INFORMATION, false);
		PreferenceUtils.getInstance().updateConfiguration(DISPLAY_DEBUG_INFORMATION, false);
		store.setDefault(REPORT_IGNORED_PREPROCESSOR_DIRECTIVES, WARNING);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_IGNORED_PREPROCESSOR_DIRECTIVES, WARNING);
		store.setDefault(LIMIT_ALL_THREAD_CREATION, true);
		PreferenceUtils.getInstance().updateConfiguration(LIMIT_ALL_THREAD_CREATION, true);
		final int availableProcessors = Runtime.getRuntime().availableProcessors();
		store.setDefault(PROCESSING_UNITS_TO_USE, availableProcessors);
		PreferenceUtils.getInstance().updateConfiguration(PROCESSING_UNITS_TO_USE, availableProcessors);
		store.setDefault(ENABLE_TITANIUM, false);
		PreferenceUtils.getInstance().updateConfiguration(ENABLE_TITANIUM, false);
		store.setDefault(ENABLE_INLAY_HINTS, false);
		PreferenceUtils.getInstance().updateConfiguration(ENABLE_INLAY_HINTS, false);

		store.setDefault(USE_ON_THE_FLY_PARSING, true);
		PreferenceUtils.getInstance().updateConfiguration(USE_ON_THE_FLY_PARSING, true);
		store.setDefault(USE_INCREMENTAL_PARSING, false);
		PreferenceUtils.getInstance().updateConfiguration(USE_INCREMENTAL_PARSING, false);
		store.setDefault(ENABLE_OOP_EXTENSION, false);
		PreferenceUtils.getInstance().updateConfiguration(ENABLE_OOP_EXTENSION, false);
		store.setDefault(ENABLE_REALTIME_EXTENSION, false);
		PreferenceUtils.getInstance().updateConfiguration(ENABLE_REALTIME_EXTENSION, false);
		store.setDefault(ENABLE_DOC_COMMENT_ON_THE_FLY_CHECK, false);
		PreferenceUtils.getInstance().updateConfiguration(ENABLE_DOC_COMMENT_ON_THE_FLY_CHECK, false);
		store.setDefault(REPORT_NAMING_CONVENTION_PROBLEMS, false);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_NAMING_CONVENTION_PROBLEMS, false);
		store.setDefault(NAMING_CONVENTION_SEVERITY, WARNING);
		PreferenceUtils.getInstance().updateConfiguration(NAMING_CONVENTION_SEVERITY, WARNING);
		store.setDefault(REPORT_STRICT_CONSTANTS, WARNING);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_STRICT_CONSTANTS, WARNING);
		store.setDefault(REPORT_DOC_COMMENT_INCONSISTENCY, IGNORE);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_DOC_COMMENT_INCONSISTENCY, IGNORE);
		store.setDefault(REPORT_MODULE_NAME_REUSE, ERROR);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_MODULE_NAME_REUSE, ERROR);
		store.setDefault(REPORT_ERRORS_IN_EXTENSION_SYNTAX, WARNING);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_ERRORS_IN_EXTENSION_SYNTAX, WARNING);
		store.setDefault(REPORT_KEYWORD_USED_AS_IDENTIFIER, false);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_KEYWORD_USED_AS_IDENTIFIER, false);
		store.setDefault(REPORT_DUPLICATED_UNIQUE_FIELD_VALUE, IGNORE);
		PreferenceUtils.getInstance().updateConfiguration(REPORT_DUPLICATED_UNIQUE_FIELD_VALUE, IGNORE);
		store.setDefault(ON_THE_FLY_CHECKER_TEST_MODE, false);
		PreferenceUtils.getInstance().updateConfiguration(ON_THE_FLY_CHECKER_TEST_MODE, false);

		store.setDefault(OUTLINE_SHOW_FUNCTIONS, false);
		PreferenceUtils.getInstance().updateConfiguration(OUTLINE_SHOW_FUNCTIONS, false);
		store.setDefault(OUTLINE_SHOW_TESTCASES, false);
		PreferenceUtils.getInstance().updateConfiguration(OUTLINE_SHOW_TESTCASES, false);
		store.setDefault(OUTLINE_SHOW_CLASSES, false);
		PreferenceUtils.getInstance().updateConfiguration(OUTLINE_SHOW_CLASSES, false);

		store.setDefault(NAMING_CONVENTION_TESTCASE, "tc_*");
		PreferenceUtils.getInstance().updateConfiguration(NAMING_CONVENTION_TESTCASE, "tc_*");

		store.setDefault(ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE, false);
		PreferenceUtils.getInstance().updateConfiguration(ANALYZE_ONLY_WHEN_TPD_IS_ACTIVE, false);
		store.setDefault(ACTIVATED_TPD, "");
		PreferenceUtils.getInstance().updateConfiguration(ACTIVATED_TPD, "");
		store.setDefault(LOG_PARSE_TREE, false);
		PreferenceUtils.getInstance().updateConfiguration(LOG_PARSE_TREE, false);

		final String ttcn3Dir = System.getenv("TTCN3_DIR");
		if (ttcn3Dir != null) {
			store.setDefault(TITAN_INSTALLATION_PATH, ttcn3Dir);
			PreferenceUtils.getInstance().updateConfiguration(TITAN_INSTALLATION_PATH, ttcn3Dir);
		}

		final String licenseFile = System.getenv("TTCN3_LICENSE_FILE");
		if (licenseFile != null) {
			store.setDefault(LICENSE_FILE_PATH, licenseFile);
			PreferenceUtils.getInstance().updateConfiguration(LICENSE_FILE_PATH, licenseFile);
		}
	}
}
