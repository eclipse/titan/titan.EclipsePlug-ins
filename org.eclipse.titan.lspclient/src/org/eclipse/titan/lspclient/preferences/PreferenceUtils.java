/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lspclient.preferences;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.lsp4e.IConfigurationHandler;
import org.eclipse.titan.lspclient.Activator;

/**
 * Utility function for preference handling
 * @author Adam Knapp
 * */
@SuppressWarnings("restriction")
public final class PreferenceUtils implements IConfigurationHandler {

	private final IPropertyChangeListener propertyChangeListener = new IPropertyChangeListener() {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			PreferenceUtils.getInstance().updateConfiguration(event.getProperty(), event.getNewValue());
		}
	};

	/** Copy of the content of the preference store. The prefix of the keys is "titan".  */
	private static final Map<String, Object> prefStoreCopy = new ConcurrentHashMap<>();

	private static PreferenceUtils PREFERENCE_UTILS_INSTANCE = new PreferenceUtils();

	public PreferenceUtils() {
		getPreferenceStore().addPropertyChangeListener(propertyChangeListener);
	}

	public static PreferenceUtils getInstance() {
		return PREFERENCE_UTILS_INSTANCE;
	}

	/**
	 * Returns all the preferences related to the plugin as an immutable {@link Map}.
	 * Use {@link PreferenceUtils.updatePreference()} to add/update a preference.
	 * @return all the preferences stored in the plugin's preference store
	 */
	@Override
	public Map<String, Object> getConfiguration() {
		return PREFERENCE_UTILS_INSTANCE.getConfiguration(true);
	}

	/**
	 * Returns all the preferences related to the plugin as an immutable {@link Map}.
	 * Use {@link PreferenceUtils.updatePreference()} to add/update a preference.
	 * @return all the preferences stored in the plugin's preference store
	 */
	public Map<String, Object> getConfiguration(final boolean needPrefix) {
		if (!needPrefix) {
			return Collections.unmodifiableMap(prefStoreCopy);
		}
		Set<Entry<String, Object>> entries = prefStoreCopy.entrySet();
		final HashMap<String, Object> copy = (HashMap<String, Object>) entries.stream()
				.collect(Collectors.toMap(
						(entry) -> { return Activator.TITAN + "." + entry.getKey(); },
						Map.Entry::getValue));
		return copy;
	}

	/**
	 * Returns the specified preference as an immutable {@link Map}.
	 * @param preference The name/key of the preference
	 * @return the specified preference as an immutable {@link Map}
	 */
	@Override
	public Map<String, Object> getConfiguration(final String prefName) {
		final String temp = removeConfigurationPrefix(prefName);
		return Collections.singletonMap(Activator.TITAN + "." + temp, prefStoreCopy.get(temp));
	}

	/**
	 * Updates or adds a preference to the preference map.
	 * @param preference The name/key of the preference
	 * @param newValue The (new) value of the preference
	 */
	public void updateConfiguration(final String preference, final Object newValue) {
		prefStoreCopy.put(removeConfigurationPrefix(preference), newValue);
	}

	@Override
	public IPreferenceStore getPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}

	/**
	 * Replaces the "org.eclipse.titan.lspclient" prefix used by the preference store
	 * of the plugin with the "titan" prefix used by the language server
	 * @param preference The name of the preference
	 * @return
	 */
	private String removeConfigurationPrefix(final String preference) {
		return preference.replace(PreferenceInitializer.PREFIX, "");
	}
}
