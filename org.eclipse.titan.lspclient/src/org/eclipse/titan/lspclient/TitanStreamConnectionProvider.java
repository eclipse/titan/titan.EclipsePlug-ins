/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lspclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.lsp4e.server.StreamConnectionProvider;
import org.eclipse.titan.lspclient.preferences.PreferenceInitializer;
import org.osgi.framework.Bundle;

public final class TitanStreamConnectionProvider implements StreamConnectionProvider {
	private static final String OS;
	private static final boolean IS_WINDOWS;
	private static final boolean IS_LINUX;

	/** Command line options to enable debugging of the LS */
	private static final String DEBUG_OPTIONS = "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=127.0.0.1:54321";

	// path to java binary
	private static final String pathToJava;

	// path to the language server jar file
	private static final String pathToLspServer = "resources/org.eclipse.titan.lsp.jar";

	// path of log file
	private static final String pathToLogFile = ".metadata/lspclient.log";

	private Process process;

	static {
		// collect OS information
		final String tmp = System.getProperty("os.name"); 
		OS = tmp == null ? "" :	tmp;
		IS_WINDOWS = OS.startsWith("Windows");
		IS_LINUX = OS.contains("nix") || OS.contains("nux") || OS.contains("aix") || OS.contains("mac") || OS.contains("BSD");
		if (!IS_WINDOWS && !IS_LINUX) {
			System.err.println("Unsupported OS detected");
			System.exit(-1);
		}

		// find Java executable
		Path tempPath = null;
		String temp = System.getenv("JAVA_HOME");
		if (temp != null) {
			tempPath = Paths.get(temp, "bin", IS_WINDOWS ? "java.exe" : "java");
		} else {
			temp = System.getenv("PATH");
			if (temp != null) {
				boolean found = false;
				for (String string : temp.split(IS_WINDOWS ? ";" : ":")) {
					if (string.contains("java")) {
						tempPath = Paths.get(string);
						found = true;
						break;
					}
				}
				if (!found) {
					System.err.println("No Java detected");
					System.exit(-1);
				}
			} else {
				System.err.println("No Java detected");
				System.exit(-1);
			}
		}
		if (!Files.isExecutable(tempPath)) {
			System.err.println("The detected Java is not executable: '" + tempPath.toString() + "'");
			System.exit(-1);
		}
		pathToJava = tempPath.toString();
	}

	@Override
	public InputStream getInputStream() {
		return process.getInputStream();
	}

	@Override
	public OutputStream getOutputStream() {
		return process.getOutputStream();
	}

	@Override
	public void start() throws IOException {
		// Setting up language server path
		final Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		URL fileURL = FileLocator.find(bundle, new org.eclipse.core.runtime.Path(pathToLspServer));
		String finalPathToLspServer = null;
		try {
			final URL fileURL2 = FileLocator.toFileURL(fileURL);
			finalPathToLspServer = Paths.get(fileURL2.toURI()).toAbsolutePath().toString();
		} catch (IOException | URISyntaxException e) {
			System.err.println("Language Server not found");
			System.exit(-1);
		}

		// Setting up log file path
		Path finalLogFilePath = null;
		try {
			finalLogFilePath = Paths.get(ResourcesPlugin.getWorkspace().getRoot().getLocationURI()).resolve(pathToLogFile);
		} catch (Exception e) {
			System.err.println("Cannot determine the default log file location");
			finalLogFilePath = Paths.get(pathToLogFile);
		}

		final boolean debug = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceInitializer.LS_DEBUG);
		ProcessBuilder builder = new ProcessBuilder(pathToJava, debug ? DEBUG_OPTIONS : "", "-jar", finalPathToLspServer);
		//Redirect redirect;
		builder.redirectError(Redirect.to(finalLogFilePath.toFile()));
		process = builder.start();
	}

	@Override
	public void stop() {
		process.destroy();
		process = null;
	}

	@Override
	public InputStream getErrorStream() {
		return process.getErrorStream();
	}
}
