/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lspclient;

import org.eclipse.ui.IStartup;

/**
 * This class is only responsible triggering the early activation of the plugin
 * @author Adam Knapp
 * */
public final class TitanLspStartup implements IStartup {

	@Override
	public void earlyStartup() {
		// Do nothing, this triggers the activation of the plugin
	}

}
