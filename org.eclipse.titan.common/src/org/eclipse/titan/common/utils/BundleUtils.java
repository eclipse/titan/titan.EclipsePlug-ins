/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.common.utils;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;

/**
 * Provides functions to get information about bundles.
 * 
 * @author Adam Knapp
 */
public final class BundleUtils {
	private static final String JFACE_BUNDLE = "org.eclipse.jface";
	
	/**
	 * Returns the version of the specified bundle. If this bundle is not found or does not have a
	 * specified version then {@link Version#emptyVersion} is returned.
	 * 
	 * @param bundleName symbolic name of the bundle
	 * @return the version of the specified bundle
	 */
	public static Version getBundleVersion(final String bundleName) {
		Bundle bundle = Platform.getBundle(bundleName);
		if (bundle != null) {
			return bundle.getVersion();
		}
		return Version.emptyVersion;
	}
	
	/**
	 * Returns the version of {@code org.eclipse.jface} bundle
	 * @return the version of {@code org.eclipse.jface} bundle
	 */
	public static Version getJFaceVersion() {
		return getBundleVersion(JFACE_BUNDLE);
	}
}
