/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.regressiontests.designer.statictests.Basic_tests;

import java.util.ArrayList;
import org.eclipse.core.resources.IMarker;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.regressiontests.designer.Designer_plugin_tests;
import org.eclipse.titan.regressiontests.library.MarkerToCheck;
import org.junit.BeforeClass;
import org.junit.Test;

/** 
 * @author Miklos Magyari
 * @author Adam Knapp
 */
public class OOP_Semantic_tests {
	private static final String DIR_PATH = "src/Basic_tests/";
	private static final String OOP_NEGATIVE_BASIC_SEMANTIC = "/OopNegativeSemanticTest.ttcn";
	private static final String OOP_BASIC_SEMANTIC_WARNING = "/OopWarningSemanticTest.ttcn";

	private static boolean parseOOP;

	@BeforeClass
	public static void setUp() throws Exception {
		parseOOP = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.ENABLEOOPEXTENSION);
		if (parseOOP)
			return;

		Designer_plugin_tests.ignoreMarkersOnFile(DIR_PATH + OOP_NEGATIVE_BASIC_SEMANTIC);
		Designer_plugin_tests.ignoreMarkersOnFile(DIR_PATH + OOP_BASIC_SEMANTIC_WARNING);
		for (String testFile : OOP_Syntax_tests.testFilesNegative) {
			Designer_plugin_tests.ignoreMarkersOnFile(OOP_Syntax_tests.OOP_DIR_PATH + "negative/" + testFile);
		}
	}

	@Test
	public void OOPNegativeSemantic_Test() throws Exception {
		if (parseOOP) {
			// check for semantic errors
			Designer_plugin_tests.checkSemanticMarkersOnFile(oopNegative_ttcn_initializer(), DIR_PATH + OOP_NEGATIVE_BASIC_SEMANTIC);
			
			// check for semantic warnings
			Designer_plugin_tests.checkSemanticMarkersOnFile(oopWarning_ttcn_initializer(), DIR_PATH + OOP_BASIC_SEMANTIC_WARNING);
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_50101_top_level_002_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_50101_top_level_002.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_50101_top_level_007_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_50101_top_level_007.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_50101_top_level_008_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_50101_top_level_008.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_50101_top_level_010_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_50101_top_level_010.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_50101_top_level_011_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_50101_top_level_011.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_5010102_abstractClasses_001_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_5010102_abstractClasses_001.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_5010109_Visibility_001_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_5010109_Visibility_001.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_5010109_Visibility_002_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_5010109_Visibility_002.ttcn");
			
			Designer_plugin_tests.checkSemanticMarkersOnFile(NegSem_5010109_Visibility_004_ttcn_initializer(), 
					OOP_Syntax_tests.OOP_DIR_PATH + "negative/NegSem_5010109_Visibility_004.ttcn");
		}
	}

	private ArrayList<MarkerToCheck> NegSem_50101_top_level_002_ttcn_initializer() {
		//NegSem_50101_top_level_002.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(2);
		int lineNum = 16;
		markersToCheck.add(new MarkerToCheck("Duplicate field name `v_i' was first declared here",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Duplicate field name `v_i' was declared here again",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_50101_top_level_007_ttcn_initializer() {
		//NegSem_50101_top_level_007.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 21;
		markersToCheck.add(new MarkerToCheck("Class type cannot be the contained value of an any type value",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_50101_top_level_008_ttcn_initializer() {
		//NegSem_50101_top_level_008.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(3);
		int lineNum = 16;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `runs on` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have an `mtc` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `system` clause",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_50101_top_level_010_ttcn_initializer() {
		//NegSem_50101_top_level_010.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 30;
		markersToCheck.add(new MarkerToCheck("Class `@NegSem_50101_top_level_010.t_subclass' is not runs on compatible with parent `@NegSem_50101_top_level_010.t_superclass_with_incompatible'",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_50101_top_level_011_ttcn_initializer() {
		//NegSem_50101_top_level_011.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(2);
		int lineNum = 26;
		markersToCheck.add(new MarkerToCheck("Class `@NegSem_50101_top_level_011.t_subclass_system' is not system compatible with parent `@NegSem_50101_top_level_011.t_superclass_with_incompatible_system'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("Class `@NegSem_50101_top_level_011.t_subclass_mtc' is not mtc compatible with parent `@NegSem_50101_top_level_011.t_superclass_with_incompatible_mtc'",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_5010102_abstractClasses_001_ttcn_initializer() {
		//NegSem_5010102_abstractClasses_001.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 27;
		markersToCheck.add(new MarkerToCheck("An abstract class cannot be instantiated",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_5010109_Visibility_001_ttcn_initializer() {
		//NegSem_5010109_Visibility_001.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 15;
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_5010109_Visibility_002_ttcn_initializer() {
		//NegSem_5010109_Visibility_002.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 21;
		markersToCheck.add(new MarkerToCheck("A field of any visibility cannot be overriden by a subclass",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> NegSem_5010109_Visibility_004_ttcn_initializer() {
		//NegSem_5010109_Visibility_004.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 20;
		markersToCheck.add(new MarkerToCheck("Private member is inaccessible due to its protection level",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> oopWarning_ttcn_initializer() {
		//OopWarningSemanticTest.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(2);
		int lineNum = 36;
		markersToCheck.add(new MarkerToCheck("Control never reaches this statement",  lineNum, IMarker.SEVERITY_WARNING));
		lineNum += 12;
		markersToCheck.add(new MarkerToCheck("Unnecessary cast: class is cast to itself",  lineNum, IMarker.SEVERITY_WARNING));

		return markersToCheck;
	}
	
	private ArrayList<MarkerToCheck> oopNegative_ttcn_initializer() {
		//oopNegativeSemanticTest.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(107);
		int lineNum = 33;
		markersToCheck.add(new MarkerToCheck("class type expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 10;
		int i = 0;
		for (i = 0; i < 2; i++) {
			markersToCheck.add(new MarkerToCheck("A class can only extend one non-trait class", lineNum, IMarker.SEVERITY_ERROR));
		}
		markersToCheck.add(new MarkerToCheck("A final class cannot be extended",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("A trait class can only extend trait classes",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("A final class cannot be extended",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("A class can only extend one non-trait class",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Duplicate reference to class `MinimalClass' was declared here again",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Duplicate reference to class `MinimalClass' was first declared here",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 9;
		markersToCheck.add(new MarkerToCheck("An external class cannot be extended by a non-external class",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("Class `@classesNegativeSemantic.RunsonClass3' is not runs on compatible with parent `@classesNegativeSemantic.RunsonClass1'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("Class `@classesNegativeSemantic.MtcClass3' is not mtc compatible with parent `@classesNegativeSemantic.MtcClass1'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Class `@classesNegativeSemantic.SystemClass2' is not system compatible with parent `@classesNegativeSemantic.SystemClass1'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("A class cannot extend itself neither directly nor indirectly",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("A class can only extend one non-trait class",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Duplicate reference to class `DD' was declared here again",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Duplicate reference to class `DD' was first declared here",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 1;
		for (i = 0; i < 4; i++) {
			markersToCheck.add(new MarkerToCheck("A class cannot extend itself neither directly nor indirectly", lineNum++, IMarker.SEVERITY_ERROR));
		}
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Trait classes can only declare methods",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  ++lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Trait classes can only declare methods",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Trait classes cannot have a constructor",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("Trait classes can only declare abstract methods",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Trait method cannot have a function body",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Abstract function can only be declared in an abstract or trait class",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Abstract function can only be declared in an abstract or trait class",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 15;
		markersToCheck.add(new MarkerToCheck("Class must implement abstract method `f__abs1' inherited from class `@classesNegativeSemantic.ClassWithAbstract4'",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Class must implement abstract method `f__abs2' inherited from class `@classesNegativeSemantic.ClassWithAbstract4'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("Reference to non-existent field `nonexist' in union template for type `@classesNegativeSemantic.MyUnion'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("A field of any visibility cannot be overriden by a subclass",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Method is overridden with a different return type",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		for (i = 0; i < 2; i++) {
			markersToCheck.add(new MarkerToCheck("All formal parameters of a constructor shall be `in` parameters", lineNum, IMarker.SEVERITY_ERROR));
		}
		lineNum += 11;
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("integer value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		for (i = 0; i < 2; i++) {
			markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected", lineNum++, IMarker.SEVERITY_ERROR));
		}
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 15;
		markersToCheck.add(new MarkerToCheck("Constant can only be initialized in a constructor",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("Constant value can only be assigned once",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 10;
		markersToCheck.add(new MarkerToCheck("integer value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Constant must be initialized",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 12;
		markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Trait classes can only declare abstract methods",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("A trait class should not define a finally block",  ++lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("There is no visible definition with name `m_const' in module `classesNegativeSemantic'",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Return statement cannot be used in a class destructor",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("An absract class cannot be final",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("A trait class cannot be final",  ++lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("A class cannot be both abstract and trait",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		for (i = 0; i < 2; i++) {
			markersToCheck.add(new MarkerToCheck("Class field visibility must be private or protected", lineNum++, IMarker.SEVERITY_ERROR));
		}
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `charstring' was expected instead of `integer'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `integer' was expected instead of `float'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 18;
		markersToCheck.add(new MarkerToCheck("A class can only be cast to a superclass or subclass",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 5;
		markersToCheck.add(new MarkerToCheck("`super' reference is only valid inside class bodies",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Return type differs from the overridden method's return type",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `integer' was expected instead of `charstring'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `charstring' was expected instead of `integer'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 19;
		markersToCheck.add(new MarkerToCheck("Definition is overridden with different formal parameters",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 12;
		markersToCheck.add(new MarkerToCheck("Method is overridden with a different return type",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 11;
		for (i = 0; i < 2; i++) {
			markersToCheck.add(new MarkerToCheck("A field of any visibility cannot be overriden by a subclass", lineNum++, IMarker.SEVERITY_ERROR));
		}
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `charstring' was expected instead of `float'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("Formal parameter list differs from previous definition",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Public method can only be overriden by a public method",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Protected method can only be overriden by a public or protected method",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Return type differs from the overridden method's return type",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `integer' was expected instead of `float'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `runs on` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have an `mtc` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `system` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `runs on` clause",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Class functions cannot have a `system` clause",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Class functions cannot have an `mtc` clause",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 18;
		markersToCheck.add(new MarkerToCheck("Reference to `value` is only allowed inside a dynamic template or property setter",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 11;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `charstring' was expected instead of `integer'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 9;
		markersToCheck.add(new MarkerToCheck("A property without a setter cannot have an initial value",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("An empty property body is not allowed",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Return statement cannot be used in a property setter",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("Statement block of a property getter should have a `return' statement",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("Property getter cannot be both abstract and final",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Abstract getter should not have a body",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 7;
		markersToCheck.add(new MarkerToCheck("octetstring value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("Abstract properties shall contain a property body",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 14;
		markersToCheck.add(new MarkerToCheck("Raise statement cannot be used in a finally block",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("The prototype of method `toString is not identical to that of the method inherited from the object class",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("The prototype of method `equals is not identical to that of the method inherited from the object class",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 6;
		markersToCheck.add(new MarkerToCheck("`variable `@classesNegativeSemantic.ObjClass2' shadows a method inherited from the object class",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("The prototype of method `toString is not identical to that of the method inherited from the object class",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 10;
		markersToCheck.add(new MarkerToCheck("Unknown field reference",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `charstring' was expected instead of `integer'",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 8;
		markersToCheck.add(new MarkerToCheck("integer value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 4;
		markersToCheck.add(new MarkerToCheck("Type mismatch: a value of type `octetstring' was expected instead of `charstring'",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("A trait class cannot be instantiated",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("An abstract class cannot be instantiated",  ++lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("Private member is inaccessible due to its protection level",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 2;
		markersToCheck.add(new MarkerToCheck("Character string value was expected",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("Too few parameters: 13 was expected instead of 10",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("Cannot assign a value to a property without a setter",  lineNum, IMarker.SEVERITY_ERROR));
		lineNum += 3;
		markersToCheck.add(new MarkerToCheck("Too few parameters: 5 was expected instead of 3",  lineNum, IMarker.SEVERITY_ERROR));
		markersToCheck.add(new MarkerToCheck("float value was expected",  ++lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
}
