/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.regressiontests.designer;

import java.util.Locale;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.regressiontests.MainTestSuite;
import org.eclipse.titan.regressiontests.common.CommonTestSuite;
import org.eclipse.titan.regressiontests.designer.dynamictests.ChangeTests;
import org.eclipse.titan.regressiontests.designer.statictests.StaticTests;
import org.eclipse.titan.regressiontests.designer.unittest.DesignerUnitTestSuite;
import org.eclipse.titan.regressiontests.library.WorkspaceHandlingLibrary;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ChangeTests.class,
	StaticTests.class,
	DesignerUnitTestSuite.class,
	CommonTestSuite.class })
public class Designer_plugin_tests_with_OOP extends Designer_plugin_tests {

	@BeforeClass
	public static void setUp() throws Exception {
		Locale.setDefault(new Locale("en", "EN")); // the number format used is the English one

		/**
		 * The options that could be set can be found in the Designer plug-in.
		 * Those options which would be assigned their default value, should not be set, but left as they are initialized.
		 * */
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.LICENSE_FILE_PATH, MainTestSuite.LICENSE_FILE);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.REPORTUNSUPPORTEDCONSTRUCTS, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.REPORTTYPECOMPATIBILITY, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.REPORTNAMINGCONVENTIONPROBLEMS, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.REPORT_STRICT_CONSTANTS, "error");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.DISPLAYDEBUGINFORMATION, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.DEBUG_CONSOLE_LOG_TO_SYSOUT, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.ENABLEOOPEXTENSION, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.REPORT_MODULE_NAME_REUSE, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false);

		WorkspaceHandlingLibrary.setAutoBuilding(false);
		boolean found = false;
		IProject[] projects = WorkspaceHandlingLibrary.getProjectsInWorkspace();
		for (IProject project : projects) {
			if (PROJECT_NAME.equals(project.getName())) {
				found = true;
				break;
			}
		}

		if (!found) {
			WorkspaceHandlingLibrary.importProjectIntoWorkspace(PROJECT_NAME, URIUtil.append(MainTestSuite.getPathToWorkspace(), "Semantic_Analizer_Tests"));
		}
	}
}
