/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.runtime.core;

import org.eclipse.titan.runtime.core.Base_Type.TTCN_Typedescriptor;
import org.eclipse.titan.runtime.core.TTCN_EncDec.error_type;

/**
 * BER encoding/decoding functions
 *
 * @author Kristof Szabados
 * @author Jakab Anett
 */
public class BER {
	public static final int BER_ENCODE_CER = 1;
	public static final int BER_ENCODE_DER = 2;
	private BER() {
		// Hide constructor
	}

	public static enum ASN_TagClass {
		ASN_TAG_UNDEF,
		ASN_TAG_UNIV, /** < UNIVERSAL */
		ASN_TAG_APPL, /** < APPLICATION */
		ASN_TAG_CONT, /** < context-specific */
		ASN_TAG_PRIV /** PRIVATE */
	}

	/**
	 * @brief ASN.1 identifier
	 *
	 * Contains two thirds of the T from a TLV
	 * */
	public static final class ASN_Tag {
		/** Tag class */
		public ASN_TagClass tagclass;

		/**
		 * Tag value.
		 * For UNIVERSAL, the values are predefined.
		 *
		 * Should be handle as unsigned int
		 **/
		public int tagnumber;

		/**
		 * Constructor.
		 *
		 * @param tagclass the class of the tag.
		 * @param tagnumber the number of the tag.
		 * */
		public ASN_Tag(final ASN_TagClass tagclass, final int tagnumber) {
			this.tagclass = tagclass;
			this.tagnumber = tagnumber;
		}
		
		public String print() {
			String prefix = null;
			
			switch (tagclass) {
			case ASN_TAG_UNDEF: {
				prefix = "<UNDEF> ";
				break;
			}
			case ASN_TAG_UNIV: {
				prefix = "UNIVERSAL ";
				break;
			}
			case ASN_TAG_APPL: {
				prefix = "APPLICATION ";
				break;
			}
			case ASN_TAG_CONT: {
				prefix = "";
				break;
			}
			case ASN_TAG_PRIV: {
				prefix = "PRIVATE ";
				break;
			}
			default:
				prefix = "<ERROR> ";
			    break;
			}
			
			return (prefix + Integer.toString(tagnumber));
		}
	}

	/**
	 * Descriptor for BER encoding/decoding during runtime
	 * Originally ASN_BERdescriptor_t
	 *
	 * the number _of_tags field might disappear later, if proven unnecessary in practice.
	 */
	public static final class ASN_BERdescriptor {
		/**
		 * Number of tags.
		 *
		 * For the UNIVERSAL classes, this is usually 1 (except for CHOICE and ANY)
		 * */
		public int number_of_tags;

		/**
		 * This array contains the tags.
		 * Index 0 is the innermost tag.
		 */
		public ASN_Tag[] tags;

		/**
		 * Constructor.
		 *
		 * @param number_of_tags the number of tags.
		 * @param tags the tags themselves.
		 * */
		public ASN_BERdescriptor(final int number_of_tags, final ASN_Tag[] tags) {
			this.number_of_tags = number_of_tags;
			this.tags = tags;
		}
		
		public String printTags() {
			if(number_of_tags == 0) {
				return "<no tags>";
			} else {
				String s = "";
				
				for (int i = number_of_tags; i > 0; i--) {
					s += tags[i - 1].print();
					
					if (i != 1) {
						s += " ";
					}
				}
				
				return s;
			}
		}
	}

	/**
	 * This class represents a BER TLV.
	 *
	 * It represents a "deconstructed" (cooked) representation of a BER encoding.
	 */
	public static final class ASN_BER_TLV {
		public boolean isConstructed; /** < Primitive/Constructed bit. */
		public boolean isLenDefinite; /** < \c false for indefinite form */
		public boolean isLenShort; /** < \c true for short form (0-127) */
		public boolean isTagComplete;
		public boolean isComplete;
		public boolean isVTlvsSelected; /**< How the data is stored in \p V. If true, V part is stored in 1 or more tlvs. */

		public ASN_TagClass tagclass; // < UNIV/APP/context/PRIVATE
		public int tagnumber;

		public int Tlen; // length of T part
		public int Llen; // length of L part

		public byte[] Tstr; // encoded T part
		public byte[] Lstr; // encoded L part

		// V simple
		public int Vlen; // length of V part
		public byte[] Vstr; // encoded V part

		// V part made of multiple tlvs
		public int n_tlvs;
		public ASN_BER_TLV[] tlvs;

		
		/** Creates a new non-constructed TLV with empty T, L and V part. */
		public ASN_BER_TLV() {
			isConstructed = false;
			isLenDefinite = false;
			isLenShort = false;
			isTagComplete = false;
			isComplete = false;
			isVTlvsSelected = false;
			tagclass = ASN_TagClass.ASN_TAG_UNIV;
			tagnumber = 0;
			Tlen = 0;
			Llen = 0;
			Tstr = new byte[0];
			Lstr = new byte[0];
			Vlen = 0;
			Vstr = new byte[0];
			n_tlvs = 0;
			tlvs = new ASN_BER_TLV[0];
		}
		

		/** Creates a new non-constructed TLV with the given V-part.
		    * Tagclass and -number are set to UNIV-0. */
		public ASN_BER_TLV(int vlen, byte[] vstr) {
			isConstructed = false;
			isLenDefinite = false;
			isLenShort = false;
			isTagComplete = false;
			isComplete = false;
			isVTlvsSelected = false;
			tagclass = ASN_TagClass.ASN_TAG_UNIV;
			tagnumber = 0;
			Tlen = 0;
			Llen = 0;
			Tstr = new byte[0];
			Lstr = new byte[0];
			Vlen = vlen;
			Vstr = new byte[Vlen];
			Vstr = vstr;
			n_tlvs = 0;
			tlvs = new ASN_BER_TLV[0];
		}
		

		/** Creates a new constructed TLV which contains one TLV (\a
		    * tlv). Or, if the parameter is NULL, then an empty constructed
		    * TLV is created. */
		public ASN_BER_TLV(ASN_BER_TLV tlv) {
			isConstructed = true;
			isLenDefinite = false;
			isLenShort = false;
			isTagComplete = false;
			isComplete = false;
			isVTlvsSelected = true;
			tagclass = ASN_TagClass.ASN_TAG_UNIV;
			tagnumber = 0;
			Tlen = 0;
			Llen = 0;
			Tstr = new byte[0];
			Lstr = new byte[0];
			Vlen = 0;
			Vstr = new byte[0];
			
			if (tlv == null) {
				n_tlvs = 0;
				tlvs = new ASN_BER_TLV[0];
			} else {
				n_tlvs = 1;
				tlvs = new ASN_BER_TLV[n_tlvs];
				tlvs[0] = tlv;
			}
		}
		

		public void put_to_buf(final TTCN_Buffer buf) {
			buf.put_s(Tstr);
			buf.put_s(Lstr);
			
			if (isVTlvsSelected) {
				
				for (int i = 0; i < n_tlvs; i++) {
					tlvs[i].put_to_buf(buf);
				}
				
			} else {
				buf.put_s(Vstr);
			}
		}
		

		/**
		 * How many bits are needed minimally to represent the given (nonnegative
		 * integral) value. Returned value is greater than or equal to 1.
		 */
		private int minBitsNeeded(int num) {

			if (num == 0)
				return 1;

			int tmp = num;
			int n = 0;

			while (tmp != 0) {
				n++;
				tmp /= 2;
			}

			return n;
		}
		
		
		public void chk_constructedFlag(boolean expectedFlag) {
			if(Tlen > 0 && isConstructed != expectedFlag) {
				TTCN_EncDec_ErrorContext.error(error_type.ET_INVAL_MSG,
					       "Invalid 'constructed' flag (must be %sset).", expectedFlag?"":"un");
			}
		}
		

		/**
		 * Adds \a p_tlv to this (constructed) TLV.
		 * 
		 * @pre isConstructed is \c true
		 * @pre isVTlvsSelected is \c true
		 */
		public void add_TLV(ASN_BER_TLV p_tlv) {
			
			if (!isConstructed && !isVTlvsSelected) {
				TTCN_EncDec_ErrorContext.error_internal("ASN_BER_TLV_t::add_TLV() invoked for a non-constructed TLV.");
			}
			
			n_tlvs += 1;
			ASN_BER_TLV[] tempTLVs = tlvs;
			tlvs = new ASN_BER_TLV[n_tlvs];
			
			if (n_tlvs > 1) {
				for (int i = 0; i < n_tlvs - 1; i++) {
					tlvs[i] = tempTLVs[i];
				}
			}
			
			tlvs[n_tlvs - 1] = p_tlv;
		}
		

		/** Adds a new UNIVERSAL 0 TLV to this (constructed) TLV. */
		public void add_UNIV0_TLV() {

			ASN_BER_TLV newTlv = new ASN_BER_TLV();
			newTlv.setLenDefinite(true);
			newTlv.setLenShort(true);
			newTlv.setTlen(1);

			byte[] newTstr = new byte[1];
			newTstr[0] = 0x00;

			newTlv.setTstr(newTstr);
			newTlv.setLlen(1);

			byte[] newLstr = new byte[1];
			newLstr[0] = 0x00;

			newTlv.setLstr(newLstr);
			add_TLV(newTlv);
		}

		
		public void add_TL(int p_coding, ASN_TagClass p_tagclass, int p_tagnumber) {
			//final TTCN_EncDec_ErrorContext errorContext = new TTCN_EncDec_ErrorContext("ASN_BER_TLV_t::add_TL(): ");

			tagclass = p_tagclass;
			tagnumber = p_tagnumber;

			/* L-part */

			if (p_coding == BER_ENCODE_CER && isConstructed) {
				isLenDefinite = false;
				add_UNIV0_TLV();
			} else {
				isLenDefinite = true;
			}

			if (isLenDefinite) {
				Tlen = Llen = 0;
				Vlen = Vstr.length;

				if (Vlen > 127) {
					isLenShort = false;
					Llen = 1 + (minBitsNeeded(Vlen) + 7) / 8;
				} else {
					isLenShort = true;
					Llen = 1;
				}
			} else {
				Llen = 1;
			}

			Lstr = new byte[Llen];

			if (isLenDefinite) {
				if (isLenShort) {
					Lstr[0] = (byte) Vlen;

				} else { // long form
					int i = Llen - 1; // octets needed

					Lstr[0] = (byte) ((i & 0x7F) | 0x80);

					int tmp = Vlen;

					while (i > 0) {
						Lstr[i] = (byte) (tmp & 0xFF);
						i--;
						tmp >>>= 8;
					}
				}
			} else { // len is indefinite
				Lstr[0] = (byte) 0x80;
			}

			/* T-part */

			if (tagnumber > 30) {
				Tlen = 1 + (minBitsNeeded(tagnumber) + 6) / 7;
			} else {
				Tlen = 1;
			}

			Tstr = new byte[Tlen];

			switch (tagclass) {
			case ASN_TAG_UNIV: {
				Tstr[0] = 0x00;
				break;
			}
			case ASN_TAG_APPL: {
				Tstr[0] = 0x40;
				break;
			}
			case ASN_TAG_CONT: {
				Tstr[0] = (byte) 0x80;
				break;
			}
			case ASN_TAG_PRIV: {
				Tstr[0] = (byte) 0xC0;
				break;
			}
			case ASN_TAG_UNDEF:
			default: {
				TTCN_EncDec_ErrorContext.error_internal("Unhandled case or undefined tagclass.");
				break;
			}
			}

			if (isConstructed) { // add constructed bit
				Tstr[0] |= 0x20;
			}

			if (tagnumber < 30) {
				Tstr[0] |= (byte) tagnumber;
			} else {
				Tstr[0] |= 0x1F;

				int tmp = tagnumber;
				int i = Tlen - 1; // octets needed

				while (i > 0) {
					Tstr[i] = (byte) ((tmp & 0x7F) | 0x80);
					i--;
					tmp >>>= 7;
				}

				Tstr[Tlen - 1] &= 0x7F;
			}

			isComplete = true;
			isTagComplete = true;

		}

		public ASN_BER_TLV ASN_BER_V2TLV(TTCN_Typedescriptor p_td, int p_coding) {

			if (p_td.ber.number_of_tags == 0) {
				return this;
			}

			ASN_BER_TLV tlv2 = !(tagclass == ASN_TagClass.ASN_TAG_UNIV && tagnumber == 0) ? new ASN_BER_TLV() : this;
			ASN_BERdescriptor ber = p_td.ber;

			for (int i = 0; i < ber.number_of_tags; i++) {
				ASN_Tag tag = ber.tags[i];
				tlv2.add_TL(p_coding, tag.tagclass, tag.tagnumber);

				if (i != ber.number_of_tags - 1) {
					tlv2 = new ASN_BER_TLV(tlv2);
				}
			}
			return tlv2;
		}

		public boolean ASN_BER_str2TLV(int p_len_s, byte[] p_str, int L_form) {
			isConstructed = false;
			isLenDefinite = true;
			isLenShort = true;
			isTagComplete = false;
			isComplete = false;
			isVTlvsSelected = false;
			tagclass = ASN_TagClass.ASN_TAG_UNIV;
			tagnumber = 0;
			Tlen = 0;
			Llen = 0;
			Vlen = 0;
			Tstr = new byte[0];
			Lstr = new byte[0];
			Vstr = new byte[0];
			n_tlvs = 0;
			tlvs = new ASN_BER_TLV[0];

			if (p_len_s == 0) {
				return incomplete(this, p_len_s); // or return just false??
			}

			byte c = p_str[0];
			
			switch ((c & 0xC0) >> 6) {
			case 0: {
				tagclass = ASN_TagClass.ASN_TAG_UNIV;
				break;
			}
			case 1: {
				tagclass = ASN_TagClass.ASN_TAG_APPL;
				break;
			}
			case 2: {
				tagclass = ASN_TagClass.ASN_TAG_CONT;
				break;
			}
			case 3: {
				tagclass = ASN_TagClass.ASN_TAG_PRIV;
				break;
			}
			}

			if ((c & 0x20) > 0) {
				isConstructed = true;
			}

			byte c2 = (byte) (c & 0x1F);
			int currentPos = 0;

			if (c2 != 0x1F) {// low tag number
				tagnumber = c2;
			} else { // high tag number
				tagnumber = 0;
				boolean doIt = true;
				boolean err_repr = false;

				while (doIt) {
					currentPos++;
					
					if (currentPos >= p_len_s)
						return incomplete(this, p_len_s);
					
					c = p_str[currentPos];
					
					if (!err_repr) {
						int ASN_Tagnumber_7msb = (0x7F) << (Integer.BYTES * 8 - 8);
						
						if ((tagnumber & ASN_Tagnumber_7msb) != 0) { // if 7msb on
							err_repr = true;
							TTCN_EncDec_ErrorContext.error(error_type.ET_REPR,"Tag number is too big");
							tagnumber = 0;
						 
						} else {
							tagnumber <<= 7;
							tagnumber |= c & 0x7F;
						}
					} // !err_repr
					
					if ((c & 0x80) == 0) { // !(c & 0x80) ??
						doIt = false;
					}
				}
			} // high tag number
			
			isTagComplete = true;
			currentPos++;
			
			// T-part
			Tlen = currentPos;
			
			Tstr = new byte[Tlen];
			
			for (int i = 0; i < Tlen; i++) {
				Tstr[i] = p_str[i];
			}
			
			
			if (currentPos >= p_len_s) {
				return incomplete(this, p_len_s);
			}
			
			isLenDefinite = true;
			isLenShort = false;
			
			c = p_str[currentPos];
			
			if((c & 0x80) == 0) { // short form
			    Llen=1;
			    Vlen=c;
			    isLenShort=true;
			    
			    Lstr = new byte[Llen];
			    Lstr[0] = (byte) Vlen;
			    
			    if ((L_form & 0x01) == 0) {  //BER_ACCEPT_SHORT
			    	TTCN_EncDec_ErrorContext.error(error_type.ET_LEN_FORM, "Short length form is not acceptable.");
			    } // if wrong L_form
			    
			} else {
			    if (c == 0x80) { // indefinite len
				    Llen=1;
				    isLenDefinite = false;
				      
				    if ((L_form & 0x04) == 0) {  //BER_ACCEPT_INDEFINITE
				    	TTCN_EncDec_ErrorContext.error(error_type.ET_LEN_FORM, "Indefinite length form is not acceptable.");
				    } // if wrong L_form
				    
			    } else if (c == 0xFF) { // reserved len
			    	TTCN_EncDec_ErrorContext.error(error_type.ET_INVAL_MSG, 
			    			"Error in L: In the long form, the value 0xFF shall not be used (see X.690 clause 8.1.3.5.c)).");
			      // using as normal long form
			    } else { // long form
			        if((L_form & 0x02) == 0) {  //BER_ACCEPT_LONG
			        	TTCN_EncDec_ErrorContext.error(error_type.ET_LEN_FORM, "Long length form is not acceptable.");
			        } // if wrong L_form
			        
			        //L-part
			        Llen = (c & 0x7F) + 1;
			        
			        Lstr = new byte[Llen];
			        
			        for (int i = Tlen; i < Tlen + Llen; i++) {
			        	Lstr[i - Tlen] = p_str[i]; 
			        }
			        
			      
			        if(Tlen + Llen > p_len_s) {
			        	Llen = p_len_s - Tlen;
			            return incomplete(this, p_len_s);
			        }
			      
			        Vlen=0;
			        boolean err_repr = false;
			      
			        for(int i = Llen - 1; i > 0; i--) {
			            if(!err_repr) {
				        	int size_8msb = (0xFF) << (Integer.BYTES * 8 - 8); //(0xFF)<<(sizeof(size_t)*8-8)
				            if((Vlen & size_8msb) != 0) {
				            	err_repr = true;
				            	TTCN_EncDec_ErrorContext.error(error_type.ET_REPR, "In long form L: Length of V is too big.");
				            	Vlen = ~0;
				            	
				            // if 8msb on
				            } else {
				            	 Vlen <<= 8;
				            }
			            } // if !err_repr
			        
			            currentPos ++;
			        
			            if(!err_repr) {
			            	c = p_str[currentPos];
			            	Vlen|=c;
			            } // if !err_repr
			        } // for i
			    } // if long form
			}
			
			currentPos++;
			  
			if(isLenDefinite) {
			    if(Vlen > p_len_s - Tlen - Llen) {
			        return incomplete(this, p_len_s);
			    }
			// definite len 
			} else { // indefinite len for V
			    if(!isConstructed) {
			    	TTCN_EncDec_ErrorContext.error(error_type.ET_INVAL_MSG,  
			    			"A sender can use the indefinite form only if the encoding is constructed (see X.690 clause 8.1.3.2.a).");
			    } // if !isConstructed

			    ASN_BER_TLV tmpTlv = new ASN_BER_TLV();
			    int tmp_len;
			    //int i = 1;
			    boolean doit=true;
			    
			    while(doit) {
			      
			        if(!tmpTlv.ASN_BER_str2TLV(p_len_s-currentPos, subArray(p_str, currentPos, p_len_s), 0x07)) { //BER_ACCEPT_ALL
			        	return incomplete(this, p_len_s);
			        }
			      
			        tmp_len = tmpTlv.getTlen() + tmpTlv.getLlen() + tmpTlv.getVlen();  //getLen function
			        currentPos += tmp_len;
			        Vlen += tmp_len;
			      
			        if(tmpTlv.getTagclass() == ASN_TagClass.ASN_TAG_UNIV && tmpTlv.getTagnumber() == 0) {
			        	doit = false; // End-of-contents
			        }
			      
			        //i++;
			    } // while doit
			    // tlv.V.str.Vlen=&p_str[curr_pos]-tlv.V.str.Vstr;
			} // if indefinite len for V
			
			Vstr = new byte[Vlen];
			
			for (int i = currentPos; i < p_str.length; i++) {
				Vstr[i-currentPos] = p_str[i];
			}
			  
			isComplete=true;

			return true;
		}

		private boolean incomplete(ASN_BER_TLV tlv, int p_len_s) {
			return false;
		}
		
		public byte[] subArray(byte[] str, int from, int to) {
			
			byte[] sub = new byte[to - from + 1];
			System.arraycopy(str, from, sub, 0, sub.length);
			
			return sub;
		}

		public boolean isConstructed() {
			return isConstructed;
		}

		public void setConstructed(boolean isConstructed) {
			this.isConstructed = isConstructed;
		}

		public boolean isLenDefinite() {
			return isLenDefinite;
		}

		public void setLenDefinite(boolean isLenDefinite) {
			this.isLenDefinite = isLenDefinite;
		}

		public boolean isLenShort() {
			return isLenShort;
		}

		public void setLenShort(boolean isLenShort) {
			this.isLenShort = isLenShort;
		}

		public boolean isTagComplete() {
			return isTagComplete;
		}

		public void setTagComplete(boolean isTagComplete) {
			this.isTagComplete = isTagComplete;
		}

		public boolean isComplete() {
			return isComplete;
		}

		public void setComplete(boolean isComplete) {
			this.isComplete = isComplete;
		}

		public ASN_TagClass getTagclass() {
			return tagclass;
		}

		public void setTagclass(ASN_TagClass tagclass) {
			this.tagclass = tagclass;
		}

		public int getTagnumber() {
			return tagnumber;
		}

		public void setTagnumber(int tagnumber) {
			this.tagnumber = tagnumber;
		}

		public int getTlen() {
			return Tlen;
		}

		public void setTlen(int tlen) {
			Tlen = tlen;
		}

		public int getLlen() {
			return Llen;
		}

		public void setLlen(int llen) {
			Llen = llen;
		}

		public byte[] getTstr() {
			return Tstr;
		}

		public void setTstr(byte[] tstr) {
			Tstr = tstr;
		}

		public byte[] getLstr() {
			return Lstr;
		}

		public void setLstr(byte[] lstr) {
			Lstr = lstr;
		}

		public int getVlen() {
			return Vlen;
		}

		public void setVlen(int vlen) {
			Vlen = vlen;
		}

		public byte[] getVstr() {
			return Vstr;
		}

		public void setVstr(byte[] vstr) {
			Vstr = vstr;
		}


		public int getN_tlvs() {
			return n_tlvs;
		}


		public void setN_tlvs(int n_tlvs) {
			this.n_tlvs = n_tlvs;
		}


		public ASN_BER_TLV[] getTlvs() {
			return tlvs;
		}


		public void setTlvs(ASN_BER_TLV[] tlvs) {
			this.tlvs = tlvs;
		}


		public boolean isVTlvsSelected() {
			return isVTlvsSelected;
		}


		public void setVTlvsSelected(boolean isVTlvsSelected) {
			this.isVTlvsSelected = isVTlvsSelected;
		}

	}

	private static final ASN_Tag TitanInteger_tag_[] = new ASN_Tag[] { new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 2) };
	public static final ASN_BERdescriptor TitanInteger_Ber_ = new ASN_BERdescriptor(1, TitanInteger_tag_);
	
	private static final ASN_Tag TitanOctetString_tag_[] = new ASN_Tag[] { new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 4) };
	public static final ASN_BERdescriptor TitanOctetString_Ber_ = new ASN_BERdescriptor(1, TitanOctetString_tag_);
	
	private static final ASN_Tag TitanObjectDescriptor_tag_[] = new ASN_Tag[] {
			new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 7) };
	public static final ASN_BERdescriptor TitanObjectDescriptor_Ber_ = new ASN_BERdescriptor(1,
			TitanObjectDescriptor_tag_);

	private static final ASN_Tag TitanExternal_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 8)};
	public static final ASN_BERdescriptor TitanExternal_Ber_ = new ASN_BERdescriptor(1, TitanExternal_tag_);

	private static final ASN_Tag TitanEnumerated_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 10)};
	public static final ASN_BERdescriptor TitanEnumerated_Ber_ = new ASN_BERdescriptor(1, TitanEnumerated_tag_);

	private static final ASN_Tag TitanEmbedded_PDV_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 11)};
	public static final ASN_BERdescriptor TitanEmbedded_PDV_Ber_ = new ASN_BERdescriptor(1, TitanEmbedded_PDV_tag_);

	private static final ASN_Tag Sequence_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 16)};
	public static final ASN_BERdescriptor Sequence_Ber_ = new ASN_BERdescriptor(1, Sequence_tag_);

	private static final ASN_Tag Set_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 17)};
	public static final ASN_BERdescriptor Set_Ber_ = new ASN_BERdescriptor(1, Set_tag_);

	public static final ASN_BERdescriptor Choice_Ber_ = new ASN_BERdescriptor(0, null);

	private static final ASN_Tag TitanCharacter_String_tag_[] = new ASN_Tag[] {new ASN_Tag(ASN_TagClass.ASN_TAG_UNIV, 29)};
	public static final ASN_BERdescriptor TitanCharacter_String_Ber_ = new ASN_BERdescriptor(1, TitanCharacter_String_tag_);
}
