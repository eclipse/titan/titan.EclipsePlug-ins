/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.regressiontests.designer.statictests.Basic_tests.templates;

import java.util.ArrayList;

import org.eclipse.core.resources.IMarker;
import org.eclipse.titan.lsp.regressiontests.designer.Designer_plugin_tests;
import org.eclipse.titan.lsp.regressiontests.library.MarkerToCheck;
import org.junit.Test;

public class ImplicationMatchTemplateTest {
	//TempImplication_SE_ttcn
	@Test
	public void TempImplication_SE_ttcn() throws Exception {
		Designer_plugin_tests.checkSemanticMarkersOnFile(TempImplication_SE_ttcn_initializer(), "src/Basic_tests/templates/TempImplication_SE.ttcn");
	}

	private ArrayList<MarkerToCheck> TempImplication_SE_ttcn_initializer() {
		//TempImplication_SE.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 25;
		markersToCheck.add(new MarkerToCheck("`ifpresent' is not allowed here",  lineNum, IMarker.SEVERITY_ERROR));

		return markersToCheck;
	}
}
