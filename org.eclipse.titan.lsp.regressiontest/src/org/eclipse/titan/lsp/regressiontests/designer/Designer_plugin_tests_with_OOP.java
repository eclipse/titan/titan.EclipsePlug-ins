/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.regressiontests.designer;

import java.util.Locale;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.titan.lspclient.Activator;
import org.eclipse.titan.lspclient.preferences.PreferenceInitializer;
import org.eclipse.titan.lsp.regressiontests.MainTestSuite;
import org.eclipse.titan.lsp.regressiontests.designer.dynamictests.ChangeTests;
import org.eclipse.titan.lsp.regressiontests.designer.statictests.StaticTests;
import org.eclipse.titan.lsp.regressiontests.library.WorkspaceHandlingLibrary;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ChangeTests.class,
	StaticTests.class })
public class Designer_plugin_tests_with_OOP extends Designer_plugin_tests {

	@BeforeClass
	public static void setUp() throws Exception {
		Locale.setDefault(new Locale("en", "EN")); // the number format used is the English one

		/**
		 * The options that could be set can be found in the Designer plug-in.
		 * Those options which would be assigned their default value, should not be set, but left as they are initialized.
		 * */
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.LICENSE_FILE_PATH, MainTestSuite.LICENSE_FILE);
		//Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.REPORTUNSUPPORTEDCONSTRUCTS, "warning");
		//Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.REPORTTYPECOMPATIBILITY, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.REPORT_NAMING_CONVENTION_PROBLEMS, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.NAMING_CONVENTION_SEVERITY, "warning");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.REPORT_STRICT_CONSTANTS, "error");
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.DISPLAY_DEBUG_INFORMATION, true);
		//Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.DEBUG_CONSOLE_LOG_TO_SYSOUT, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.ENABLE_OOP_EXTENSION, true);
		Activator.getDefault().getPreferenceStore().setValue(PreferenceInitializer.REPORT_MODULE_NAME_REUSE, "warning");
		//Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false);

		WorkspaceHandlingLibrary.setAutoBuilding(false);
		boolean found = false;
		IProject[] projects = WorkspaceHandlingLibrary.getProjectsInWorkspace();
		for (IProject project : projects) {
			if (PROJECT_NAME.equals(project.getName())) {
				found = true;
				break;
			}
		}

		if (!found) {
			WorkspaceHandlingLibrary.importProjectIntoWorkspace(PROJECT_NAME, URIUtil.append(MainTestSuite.getPathToWorkspace(), "Semantic_Analizer_Tests"));
		}
	}
}
