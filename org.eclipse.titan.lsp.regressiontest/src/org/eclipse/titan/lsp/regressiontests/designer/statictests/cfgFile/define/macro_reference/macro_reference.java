package org.eclipse.titan.lsp.regressiontests.designer.statictests.cfgFile.define.macro_reference;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	AST_syntax_warning_tests.class
})
public class macro_reference {

}
