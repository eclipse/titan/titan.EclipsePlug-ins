/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.lsp.regressiontests.designer.statictests.Basic_tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.titan.lspclient.Activator;
import org.eclipse.titan.lspclient.preferences.PreferenceInitializer;
import org.eclipse.titan.lsp.regressiontests.designer.Designer_plugin_tests;
import org.eclipse.titan.lsp.regressiontests.library.MarkerToCheck;
import org.junit.BeforeClass;
import org.junit.Test;

public class OOP_Syntax_tests {
	private static final String DIR_PATH = "src/Basic_tests/";
	private static final String OOP_POSITIVE_BASIC_SYNTAX = "/OopPositiveBasicSyntax.ttcn";
	private static final String OOP_NEGATIVE_BASIC_SYNTAX = "/OopNegativeBasicSyntax.ttcn";
	public static final String OOP_DIR_PATH = "src/OOP_conf_tests/";
	public static List<String> testFilesPositive = Arrays.asList(
		"Sem_50101_top_level_001.ttcn",
		"Sem_50101_top_level_002.ttcn",
		"Sem_50101_top_level_003.ttcn",
		"Sem_50101_top_level_004.ttcn",
//		"Sem_50101_top_level_005.ttcn",
//		"Sem_50101_top_level_006.ttcn",
		"Sem_50101_top_level_007.ttcn",
		"Sem_50101_top_level_008.ttcn",
		"Sem_5010102_abstractClasses_001.ttcn",
		"Sem_5010104_finalClasses_001.ttcn",
		"Sem_5010105_Constructors_001.ttcn",
		"Sem_5010105_Constructors_002.ttcn"
	);
	
	public static List<String> testFilesNegative = Arrays.asList(
		"NegSem_50101_top_level_002.ttcn",
		"NegSem_50101_top_level_007.ttcn",
		"NegSem_50101_top_level_008.ttcn",
		"NegSem_50101_top_level_010.ttcn",
		"NegSem_50101_top_level_011.ttcn",
		"NegSem_5010102_abstractClasses_001.ttcn",
		"NegSem_5010109_Visibility_001.ttcn",
		"NegSem_5010109_Visibility_002.ttcn",
		"NegSem_5010109_Visibility_004.ttcn"
	);
	
	private static boolean parseOOP;

	@BeforeClass
	public static void setUp() throws Exception {
		parseOOP = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceInitializer.ENABLE_OOP_EXTENSION);
		if (parseOOP)
			return;

		Designer_plugin_tests.ignoreMarkersOnFile(DIR_PATH + OOP_POSITIVE_BASIC_SYNTAX);
		Designer_plugin_tests.ignoreMarkersOnFile(DIR_PATH + OOP_NEGATIVE_BASIC_SYNTAX);
		for (String testFile : testFilesPositive) {
			Designer_plugin_tests.ignoreMarkersOnFile(OOP_DIR_PATH + "positive/" + testFile);
		}
		for (String testFile : testFilesNegative) {
			Designer_plugin_tests.ignoreMarkersOnFile(OOP_DIR_PATH + "negative/" + testFile);
		}
	}

	@Test
	public void OOPPositiveSyntax_Test() throws Exception {
		if (parseOOP) {
			checkZeroMarkersOnFile(OOP_POSITIVE_BASIC_SYNTAX);
			for (String testFile : testFilesPositive) {
				Designer_plugin_tests.checkRealZeroSemanticMarkersOnFile(OOP_DIR_PATH + "positive/" + testFile);
			}
		}
	}

	@Test
	public void OOPNegativeSyntax_Test() throws Exception {
		if (parseOOP)
			Designer_plugin_tests.checkSyntaxMarkersOnFile(oopNegative_ttcn_initializer(), DIR_PATH + OOP_NEGATIVE_BASIC_SYNTAX);
	}

	private static void checkZeroMarkersOnFile(final String fileName) {
		final String filePath = DIR_PATH + fileName;

		Designer_plugin_tests.checkRealZeroSemanticMarkersOnFile(filePath);
	}

	private ArrayList<MarkerToCheck> oopNegative_ttcn_initializer() {
		//oopNegativeBasicSyntax.ttcn
		ArrayList<MarkerToCheck> markersToCheck = new ArrayList<MarkerToCheck>(1);
		int lineNum = 21;
		markersToCheck.add(new MarkerToCheck("mismatched input ';' expecting {'mtc', 'runs', 'system', '{'}",  lineNum, IMarker.SEVERITY_ERROR));
		return markersToCheck;
	}
}
