/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.regressiontests;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;

import org.eclipse.core.runtime.URIUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.lsp4e.LanguageServersRegistry;
import org.eclipse.lsp4e.LanguageServersRegistry.LanguageServerDefinition;
import org.eclipse.lsp4e.LanguageServiceAccessor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.titan.lsp.regressiontests.designer.Designer_plugin_tests;
import org.eclipse.titan.lspclient.Activator;
import org.eclipse.ui.PlatformUI;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuppressWarnings({ "restriction" })
@RunWith(Suite.class)
@SuiteClasses({
	Designer_plugin_tests.class,
	//Designer_plugin_tests_with_OOP.class
})
public final class MainTestSuite {

	public static String LICENSE_FILE = System.getenv("TTCN3_LICENSE_FILE");

	private static URI pathToWorkspace = Paths.get(System.getProperty("user.dir"), "/../").toUri();;

	public static final String PATH_TO_WORKSPACE = "file:///" + System.getProperty("user.dir") + "/../";

	private MainTestSuite() {
		throw new UnsupportedOperationException();
	}

	@BeforeClass
	public static void setUp() throws Exception {
		if (!licenseFileExists()) {
			final String errorMsg = "The license file can not be found at the location: `" + LICENSE_FILE + "'" + System.getProperty("line.separator")
					+ "The 'org.eclipse.titan.regressiontests.MainTestSuite.LICENSE_FILE' constant should contain the proper location.";
			showError("Cannot find the license file.", errorMsg);
			Assert.fail(errorMsg);
		}

		File workspaceFolder = new File(URIUtil.append(pathToWorkspace, "Semantic_Analizer_Tests"));
		if (!workspaceFolder.exists()) {
			final String errorMsg = "Can not find the workspace at the location: `" + workspaceFolder.toURI() + "'" + System.getProperty("line.separator") 
					+ "The 'org.eclipse.titan.regressiontests.MainTestSuite.PATH_TO_WORKSPACE' constant should contain the proper location.";
			showError("Cannot find the workspace", errorMsg);
			Assert.fail(errorMsg);
		}
		final LanguageServerDefinition def = LanguageServersRegistry.getInstance().getDefinition(Activator.SERVER_ID);
        try {
            LanguageServiceAccessor.startLanguageServer(def);
            // To let the local threads complete and LS properly start
            Thread.sleep(5000L);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
	}

	/**
	 * Displays an error message. If the application is running in headless mode,
	 *  the message will be printed to stderr. Otherwise, a messagedialog will appear.
	 * @param title
	 * @param errorMsg
	 */
	public static void showError(final String title, final String errorMsg) {
		if (PlatformUI.isWorkbenchRunning()) {
			MessageDialog.openError(Display.getDefault().getActiveShell(), title, errorMsg);
		} else {
			System.err.println(errorMsg);
		}
	}

	/**
	 * Tries to find the license file.
	 * @return The license file, or null if it does not exist.
	 */
	private static boolean licenseFileExists() {
		if(LICENSE_FILE != null) {
			File licenseFile = new File(LICENSE_FILE);
			if (licenseFile != null && licenseFile.exists()) {
				return true;
			}
		}		
		return false;
	}

	public static void setPathToWorkspace(final URI pathToSet) {
		pathToWorkspace = pathToSet;
	}

	public static URI getPathToWorkspace() {
		return pathToWorkspace;
	}
}
