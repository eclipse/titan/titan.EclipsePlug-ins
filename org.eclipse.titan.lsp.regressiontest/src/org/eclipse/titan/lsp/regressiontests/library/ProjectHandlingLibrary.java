/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.lsp.regressiontests.library;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.lsp4e.LSPEclipseUtils;
import org.eclipse.lsp4e.LanguageServers;
import org.eclipse.lsp4e.LanguageServers.LanguageServerProjectExecutor;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Range;
import org.eclipse.lsp4j.WorkspaceDiagnosticParams;
import org.eclipse.lsp4j.WorkspaceDiagnosticReport;
import org.eclipse.lsp4j.WorkspaceDocumentDiagnosticReport;
import org.eclipse.titan.lspclient.Activator;

/**
 * This class stores library functions to help writing test that involve projects without actual Eclipse knowledge.
 */
@SuppressWarnings("restriction")
public class ProjectHandlingLibrary {

	private static final Logger LOGGER = Logger.getLogger(ProjectHandlingLibrary.class.getName());

	/**
	 * The project associated with this library object
	 */
	private IProject project;

	public ProjectHandlingLibrary(final IProject project) {
		this.project = project;
	}

	/**
	 * Starts the analysis of its project and waits until it finishes.
	 */
	public void analyzeProject() {
		LOGGER.info("Analyzing project: " + project.getName());
	
		LanguageServerProjectExecutor executor = LanguageServers.forProject(project);
		try {
			executor.collectAll(
				(w, ls) -> { 
					try {
						return ls.getWorkspaceService().diagnostic(new WorkspaceDiagnosticParams());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				})
				.thenAccept((reports) -> processDiagnosticReports(reports)).get();
		} catch (Exception e) {
			
		}
		LOGGER.info("Project analyzed: " + project.getName());
	}

	private void processDiagnosticReports(final List<WorkspaceDiagnosticReport> reports) {
		for (WorkspaceDiagnosticReport reportEntry : reports) {
			List<WorkspaceDocumentDiagnosticReport> documentDiagnostics = reportEntry.getItems();
			documentDiagnostics.stream().forEach((diagnosticEntry) -> {
				try {
					final String uriString = diagnosticEntry
							.getWorkspaceFullDocumentDiagnosticReport().getUri();
					final URI Uri = URIUtil.fromString(uriString);
					final IFile[] files = ResourcesPlugin.getWorkspace().getRoot()
							.findFilesForLocationURI(Uri);
					if (files.length == 0) {
						return;
					}
					for (Diagnostic diagnostic : diagnosticEntry.getLeft().getItems()) {
						addMarker(files[0], diagnostic);
					}
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			});
		}
	}

	/**
	 * Collects markers of a given kind from the resources contained in this project.
	 *
	 * @param type the kind of marker to collect
	 * @return the markers found in the project with the given type
	 */
	public IMarker[] getMarkers(final String type) throws CoreException {
		return project.findMarkers(type, true, IResource.DEPTH_INFINITE);
	}

	public void clearMarkers(final String type) throws CoreException {
		project.deleteMarkers(type, true, IResource.DEPTH_INFINITE);
	}
	
	private void addMarker(IFile file, Diagnostic diagnostic) {
		final Map<String, Object> markerProperties = new HashMap<String, Object>();
		final Range range = diagnostic.getRange();
		try {
			final int line = range.getStart().getLine();
			if (line != -1) {
				markerProperties.put(IMarker.LINE_NUMBER, line + 1);
			}
			markerProperties.put(IMarker.SEVERITY, Integer.valueOf(LSPEclipseUtils.toEclipseMarkerSeverity(diagnostic.getSeverity())));
			markerProperties.put(IMarker.MESSAGE, diagnostic.getMessage());
			markerProperties.put(IMarker.TRANSIENT, Boolean.TRUE);
			file.createMarker(diagnostic.getSource().equals("syntax") ?	Activator.TITAN_LSP_MARKER_TYPE_SYNTACTIC
					: Activator.TITAN_LSP_MARKER_TYPE_SYNTACTIC, markerProperties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

