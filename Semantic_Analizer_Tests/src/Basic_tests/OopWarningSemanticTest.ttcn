/******************************************************************************
 * Copyright (c) 2000-2025 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 *
 * Contributors:
 *   Magyari, Miklos
 *
 ******************************************************************************/

module classesWarningSemantic { 

type component CT {
	port PT pt_PT;
}

type component CT2 {
	port PT pt2_PT;
}

type port PT message { inout integer; } with { extension "internal" }

type union MyUnion {
	integer a,
	charstring b
};

type class Casting {}

// raise exception
function f_with_exception(integer pl_int) return integer exception(integer) {
	if (pl_int > 100) {
		raise integer:0;		
		var float vl_never_reached := 0.0;          // never reached
	}

	return pl_int * 2;
} catch (integer e) {
	log("Catch block");
} finally {
	log("Finally block");
}

testcase tc_basicWarning() runs on CT {
	var Casting vl_casting := Casting.create;
	var object vl_obj := vl_casting => Casting;		// class cast to itself
}

} 